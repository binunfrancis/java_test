// create the controller and inject Angular's $scope
sportsApp.controller('memberController', function($scope, $rootScope, $filter, $http) {
	$rootScope.active = 'member';
	$scope.page='Member';
	$scope.genders=["Male","Female"];
	$scope.proofs=["Aadhar","Voter ID"];
	$scope.member = {};
	$scope.member.memberId = 0;
	$scope.memberEdit = {};
	$scope.today = new Date();
	$scope.allMemberBackup =[];
//	$scope.filePreviewSelected = true;
	
	loadRoles();
	loadMembers();
	loadMembershiptypes();
	loadfacilitytypes();
	$scope.$watch("member.dob",function(){
		$scope.member.age = getAge(new Date($scope.member.dob),new Date()); 
	});
	//Functions
	function loadRoles(){
		$http.get('/api/allRoles').then(function (response) {
			$scope.roles = response.data;
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});	
	}
	function loadfacilitytypes(){
		$http.get('/dashboard/allFacilityTypes').then(function (response) {
			$scope.facilityTypes = response.data;
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
	function loadMembers(){
		$http.get('/api/allMembers').then(function (response) {
			$scope.allMemberBackup = angular.copy(response.data);
			sortMemberData(response.data);
			//$scope.members = response.data;
			
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});	
	}
	function loadMembershiptypes(){
		$http.get('/dashboard/allMemberShipTypes').then(function (response) {
			$scope.memberShipTypes = response.data;
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
	function getAge	(dateOfBirth, dateToCalculate) {
	    var calculateYear = dateToCalculate.getFullYear();
	    var calculateMonth = dateToCalculate.getMonth();
	    var calculateDay = dateToCalculate.getDate();

	    var birthYear = dateOfBirth.getFullYear();
	    var birthMonth = dateOfBirth.getMonth();
	    var birthDay = dateOfBirth.getDate();

	    var age = calculateYear - birthYear;
	    var ageMonth = calculateMonth - birthMonth;
	    var ageDay = calculateDay - birthDay;

	    if (ageMonth < 0 || (ageMonth == 0 && ageDay < 0)) {
	        age = parseInt(age) - 1;
	    }
	    return age.toString();
	
}

	function compare(a,b) {
		  if (a.memberId > b.memberId)
		    return -1;
		  if (a.memberId < b.memberId)
		    return 1;
		  return 0;
	}
	function sortMemberData(data){
		if(data){
		$scope.members=data.sort(compare);
		console.log(data);
		$rootScope.initPagination($scope.members.length)
//		for (var int = 0; int < $scope.members.length; int++) {
//			$scope.members[int].memberTypeStartDate = $filter('memberTypeStartDate')($scope.members[int].date, "dd-MMM-yyyy");
//		}
	}}
	
	$scope.setmemCost = function(){
		
		$scope.memCost = userExists($scope.member.memberShipTypeId.memberShipId);
	}
	function userExists(id) {
		for(var i=0;i<$scope.memberShipTypes.length;i++){
			if($scope.memberShipTypes[i].memberShipId == id){
				return $scope.memberShipTypes[i].memberShipCost;
			}
		} 
	}
	$scope.addMember = function() {
		
		
		$("#memberid").attr('src', '');
		$("#memid").val('');
		$scope.memberidproof="";
		$scope.filepreview = "";
		$scope.filePreviewSelected = false;
		$("#memphoto").val('');
		$("#nameErrorS").hide();
		$scope.member = {};
		$scope.member.memberId = 0;
		$scope.modalTitle = 'Add';
		$scope.resetAndToggle();
	}
	
	$scope.resetAndToggle = function() {
		$('#bootstrap-wizard-1').find('.form-wizard').children('li').each(function(index, value){
			$( '#wiz-prev' ).click();
		});
		$('#bootstrap-wizard-1').find('.form-wizard').children('li').each(function(index, value){
			$( this ).find('.step').html(index + 1);
			$( this ).removeClass('complete');
			if (index == 0)
				$( this ).addClass('active');
			else
				$( this ).removeClass('active');
		});
		$('#myModal').modal('toggle');
	}

	$scope.saveMember = function() {
		$scope.loadtrue = true;
		var alreadyExist=false;
		for(var i=0;i<$scope.allMemberBackup.length;i++){
			if($scope.allMemberBackup[i].memberName.toLowerCase() == $scope.member.memberName.toLowerCase() && $scope.allMemberBackup[i].memberContactNo == $scope.member.memberContactNo){
				alreadyExist = true;
				break;
			}
		}
		
		if(!alreadyExist || $scope.modalTitle =="Edit"){
			var file = $scope.uploadedFile;		   
		       if (file) {
		    	   var extn = file.name.split('.').pop();
		    	   var allowedExtension= ["jpg","jpeg","png","JPG","JPEG","PNG"];
		    	   var fsize = Math.floor(file.size/1000000);
		    	   var flag = allowedExtension.indexOf(extn);
		    	   //2 MB is set. Change if needed
		              if(fsize<5 && flag>-1){
		    		   var url = "/api/uploadfile";
		               var data = new FormData();
		               data.append('uploadfile', file);
		               var config = {
		        	   	transformRequest: angular.identity,
		        	   	transformResponse: angular.identity,
		           		headers : {
		           			'Content-Type': undefined
		           	    }
		               }
		               $http.post(url, data, config).then(function (response) {
		            	   console.log(response.data);
		            	   $scope.member.memberPhoto = response.data;
		            	   $scope.saveMem();
		            	   
		        		}, function (response) {
		        			$scope.loadtrue = false;
		        			//$scope.saveMem();
		        			alert("Failed to upload the image");
		        		});  
		    	   }else{
		    		   if(flag == -1){
		    			   alert("Only jpeg,jpg and png images are allowed");
		    		   }else{
		    			   alert("Image size exceeded the 5 MB limit");
		    		   }
		    		   
		    		   $scope.loadtrue = false;
		    	   }
		    	   
		       } else {
		    	   $scope.saveMem();
		       }

		}else{
			alert("Member Already Exist!");
			$scope.loadtrue = false;
		}   
		
	}


	$scope.saveMem = function() {
		$scope.loadtrue = true;
		$scope.member.age = getAge(new Date($scope.member.dob),new Date());
		$http({
			url : '/api/memberSave',
			method : "POST",
			data : {
				'data' : $scope.member
			}
		}).then(function(response) {
			if (response.data) {
				$('#myModal').modal('toggle');				
				$scope.loadtrue = false;
				loadMembers();
				$scope.titlehide = false;
				$scope.printForm = true;
				$scope.memberIDNumber = response.data.memberId;
				/*$scope.member = response.data;				
				$scope.members.push($scope.member);								
				alert("Saved successfully");
				$scope.loadtrue = false;
				loadMembers();
				$rootScope.currentPage = 1;
				generateQRCode($scope.member.memberId)
				generateQRCode1($scope.member.billId)
				generateBarCode($scope.member.memberId)
				generateBarcode1($scope.member.billId)
				$scope.printForm = true;*/
			}else{
				alert("Failed to save member");
				$scope.loadtrue = false;
			}
		}, function(response) {
			alert("failed");
			$scope.loadtrue = false;
		});
	}
	$scope.printmemberReceipt = function(){
		var str=location.origin+"/api/printMemberReceipt/"+$scope.memberIDNumber;
		window.open(str);
	}
	$scope.printmemberID = function(){
		var str=location.origin+"/api/printMemberCard/"+$scope.memberIDNumber;
		window.open(str);
	}
	$scope.back = function() {
		$scope.printForm = false;
		$scope.printID = false;
	}
	$scope.uploadMemberId = function(x,index){
		if(x.memberIdProof){
			$scope.memberidproof = "/idcardimage/"+x.memberIdProof;
			$("#memberid").attr('src', "/idcardimage/"+x.memberIdProof);
		}else{
			$("#memberid").attr('src', '');
			$scope.memberidproof="";
			$("#memid").val('');
		}
		$scope.currentMemberSelected=x.memberId.toString();
		$("#memberidbox").modal("toggle");
	}
	$scope.uploadidproof = function(){
		var file= document.querySelector('#memid').files[0];
		if(file){
			var url = "/api/uploadfileIdProof";
	        var data = new FormData();
	        data.append('uploadfile', file);
	        data.append('memberId',$scope.currentMemberSelected);
	        var config = {
	 	   	transformRequest: angular.identity,
	 	   	transformResponse: angular.identity,
	    		headers : {
	    			'Content-Type': undefined
	    	    }
	        }
	        $http.post(url, data, config).then(function (response) {
	     	      if(response.data){
	     	    	  alert("ID Proof uploaded successfully");
	     	    	 loadMembers();
	     	      }else{
	     	    	  alert("Failed to upload ID proof.Please try later");
	     	      }
	     	     $("#memberidbox").modal("toggle");
	 		}, function (response) {
	 			alert("Failed to upload ID proof.Please try later");
	 			$("#memberidbox").modal("toggle");
	 		});
	        
		}else{
			alert("Please select a file");
		}
		
	}
	$scope.editMember = function(x, index) {
		
		$http.get('/api/getMember/' + x.memberId).then(function (response) {
			$scope.member = response.data;
			$scope.member.memberTypeStartDate=$filter('date')($scope.member.memberTypeStartDate, "dd-MMM-yyyy");
			$scope.member.memberTypeValidity = $filter('date')($scope.member.memberTypeValidity, "dd-MMM-yyyy");
			$scope.member.dob = $filter('date')($scope.member.dob, "dd-MMM-yyyy");
			$scope.setmemCost();
			$scope.member.index = index;
			$scope.modalTitle = 'Edit';
			$scope.resetAndToggle();
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
	$scope.printMember = function(x,index){
		/*$http.get('/api/getMember/' + x.memberId).then(function (response) {
			$scope.member = response.data;
		
			
			$scope.printID = true;
			generateQRCodeid($scope.member.memberId)
			//generateQRCode1($scope.member.memberId)
			generateBarCodeid($scope.member.memberId)

		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});*/
		$scope.titlehide = true;
		$scope.memberIDNumber = x.memberId;
		$scope.printForm = true;
		
	}
	
	$scope.deleteMember = function(x, index) {
		var flag = window.confirm("Do you want to delete member?");
		if(flag){
			$http.get('/api/deleteMember/' + x.memberId).then(function (response) {
				$scope.members.splice(index, 1);
				$rootScope.deletedEntry();
				alert("Deleted Successfully");
				loadMembers();
				$rootScope.currentPage = 1;
			}, function error(response) {
				$scope.postResultMessage = "Error Status: " +  response.statusText;
			});
		}
		
	}
	 $scope.previewID = function(){
	       var file    = document.querySelector('#memid').files[0]; //sames as here
	       
	       var reader  = new FileReader();

	       reader.onloadend = function () {
	    	   $scope.memberidproof = reader.result;
	    	   $scope.$apply();
	       }

	       if (file) {
	           reader.readAsDataURL(file); //reads the data as a URL
	       } else {
	    	   $scope.memberidproof = "";
	       }
	       $scope.$apply();
	  }

});

sportsApp.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
           var model = $parse(attrs.fileModel);
           var modelSetter = model.assign;
          
           element.bind('change', function(){
              scope.$apply(function(){
                 modelSetter(scope, element[0].files[0]);
              });
              var reader = new FileReader();
              reader.onload = function(loadEvent) {
                scope.$apply(function() {
                  scope.filepreview = loadEvent.target.result;
                  scope.filePreviewSelected = true;
                });
              }
              reader.readAsDataURL(scope.uploadedFile);
           });
        }
     };
 }]); 

// create the controller and inject Angular's $scope
sportsApp.controller('memberRestoreController', function($scope, $rootScope, $http) {
	$rootScope.active = 'member';

	$http.get('/api/allDeletedMembers').then(function (response) {
		$scope.members = response.data;
	}, function error(response) {
		$scope.postResultMessage = "Error Status: " +  response.statusText;
	});

	$scope.deleteMember = function(x, index) {
		$http.get('/api/restoreMember/' + x.memberId).then(function (response) {
			$scope.members.splice(index, 1);
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
	
}); 

sportsApp.filter('timeFormat', function() {
    return function(x) {
    	var hours = parseInt(x / 60);
    	if (hours < 10)
    		hours = '0' + hours;
    	var mins = x % 60;
    	if (mins < 10)
    		mins = '0' + mins;
        return hours + ':' + mins;
    };
});