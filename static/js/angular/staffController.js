// create the controller and inject Angular's $scope
sportsApp.controller('staffController', function($scope, $rootScope, $filter, $http) {
	$rootScope.active = 'staff';
	$scope.page='staff';
	$scope.member = {};
	$scope.member.memberId = 0;
	$scope.memberEdit = {};
	$scope.today = new Date();
	loadRoles();
	loadMembers();
	loadMembershiptypes();
	$("#dobfield").datepicker();
	$scope.$watch("member.dob",function(){
		$scope.member.age = getAge(new Date($scope.member.dob),new Date()); 
	});
	function loadMembers(){
		$http.get('/api/allStaffs').then(function (response) {
			console.log(response.data);
			sortMemberData(response.data);
//			$scope.staffs = response.data;
			$rootScope.initPagination($scope.staffs.length)
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
	function loadMembershiptypes(){
		$http.get('/dashboard/allMemberShipTypes').then(function (response) {
			$scope.memberShipTypes = response.data;
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
	function loadRoles(){
		$http.get('/api/allRoles').then(function (response) {
			$scope.roles = response.data;
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
	function compare(a,b) {
		  if (a.memberId > b.memberId)
		    return -1;
		  if (a.memberId < b.memberId)
		    return 1;
		  return 0;
	}
	function sortMemberData(data){
		$scope.staffs=data.sort(compare);
		console.log(data);
	}

	$scope.addMember = function() {
		$scope.member = {};
		$scope.member.memberId = 0;
		$scope.modalTitle = 'Add';
		$scope.resetAndToggle();
	}
	
	$scope.resetAndToggle = function() {
		$('#bootstrap-wizard-1').find('.form-wizard').children('li').each(function(index, value){
			$( '#wiz-prev' ).click();
		});
		$('#bootstrap-wizard-1').find('.form-wizard').children('li').each(function(index, value){
			$( this ).find('.step').html(index + 1);
			$( this ).removeClass('complete');
			if (index == 0)
				$( this ).addClass('active');
			else
				$( this ).removeClass('active');
		});
		$('#myModal').modal('toggle');
	}

	$scope.saveMember = function() {
		   var file = $scope.uploadedFile;
		   
	       if (file) {
	    	   var fsize = Math.floor(file.size/1000000);
	    	   //2 MB is set. Change if needed
	              if(fsize<1){
	    		   var url = "/api/uploadfile";
	               var data = new FormData();
	               data.append('uploadfile', file);
	               var config = {
	        	   	transformRequest: angular.identity,
	        	   	transformResponse: angular.identity,
	           		headers : {
	           			'Content-Type': undefined
	           	    }
	               }
	               $http.post(url, data, config).then(function (response) {
	            	   console.log(response.data);
	            	   $scope.member.memberPhoto = response.data;
	            	   $scope.saveMem();
	        		}, function (response) {
	        			alert("failed");
	        		});  
	    	   }else{
	    		   alert("Image size exceeded the 1 MB limit");
	    	   }
	    	   
	       } else {
	    	   $scope.saveMem();
	       }
		}


	$scope.saveMem = function() {
		$scope.member.age = getAge(new Date($scope.member.dob),new Date());
		$http({
			url : '/api/staffSave',
			method : "POST",
			data : {
				'data' : $scope.member
			}
		}).then(function(response) {
			$scope.member.memberId = response.data;
			if ($.isNumeric($scope.member.memberId)) {
				for (var int = 0; int < $scope.memberShipTypes.length; int++) {
					if ($scope.memberShipTypes[int].memberShipId === $scope.member.memberShipTypeId.memberShipId) {
						$scope.member.memberShipTypeId.memberShipTypeName = $scope.memberShipTypes[int].memberShipTypeName;
						break;
					}
				}
				if ($.isNumeric($scope.member.index))
					$scope.staffs[$scope.member.index] = $scope.member;
				else
					$scope.staffs.push($scope.member);
				loadMembers();
				$rootScope.addedEntry();
				$('#myModal').modal('toggle');
				alert("Saved successfully");
			}
		}, function(response) {
			alert("failed");
		});
	}
	function getAge	(dateOfBirth, dateToCalculate) {
		    var calculateYear = dateToCalculate.getFullYear();
		    var calculateMonth = dateToCalculate.getMonth();
		    var calculateDay = dateToCalculate.getDate();

		    var birthYear = dateOfBirth.getFullYear();
		    var birthMonth = dateOfBirth.getMonth();
		    var birthDay = dateOfBirth.getDate();

		    var age = calculateYear - birthYear;
		    var ageMonth = calculateMonth - birthMonth;
		    var ageDay = calculateDay - birthDay;

		    if (ageMonth < 0 || (ageMonth == 0 && ageDay < 0)) {
		        age = parseInt(age) - 1;
		    }
		    return age.toString();
		
	}
	$scope.editMember = function(x, index) {
		$http.get('/api/getStaff/' + x.memberId).then(function (response) {
			$scope.member = response.data;
			$scope.member.memberTypeStartDate=$filter('date')($scope.member.memberTypeStartDate, "dd-MMM-yyyy");
			$scope.member.memberTypeValidity = $filter('date')($scope.member.memberTypeValidity, "dd-MMM-yyyy");
			$scope.member.dob = $filter('date')($scope.member.dob, "dd-MMM-yyyy");
			$scope.member.index = index;
			$scope.modalTitle = 'Edit';
			$scope.resetAndToggle();
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
	
	$scope.deleteMember = function(x, index) {
		
		$http.get('/api/deleteStaff/' + x.memberId).then(function (response) {
			$scope.staffs.splice(index, 1);
			$rootScope.deletedEntry();
			alert("Deleted Successfully");
			loadMembers();
			$rootScope.currentPage = 1;
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}

});

sportsApp.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
           var model = $parse(attrs.fileModel);
           var modelSetter = model.assign;
           
           element.bind('change', function(){
              scope.$apply(function(){
                 modelSetter(scope, element[0].files[0]);
              });
              var reader = new FileReader();
              reader.onload = function(loadEvent) {
                scope.$apply(function() {
                  scope.filepreview = loadEvent.target.result;
                });
              }
              reader.readAsDataURL(scope.uploadedFile);
           });
        }
     };
 }]); 

// create the controller and inject Angular's $scope
sportsApp.controller('staffRestoreController', function($scope, $rootScope, $http) {
	$rootScope.active = 'member';

	$http.get('/api/allDeletedStaffs').then(function (response) {
		$scope.staffs = response.data;
	}, function error(response) {
		$scope.postResultMessage = "Error Status: " +  response.statusText;
	});

	$scope.deleteStaff = function(x, index) {
		$http.get('/api/restoreStaff/' + x.memberId).then(function (response) {
			$scope.staffs.splice(index, 1);
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
	
}); 