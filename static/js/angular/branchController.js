// create the controller and inject Angular's $scope
sportsApp.controller('branchController', function($scope, $rootScope, $http) {
	$rootScope.active = 'branch';

	$http.get('/api/allBranches').then(function (response) {
		$scope.branches = response.data;
		$rootScope.initPagination($scope.branches.length)
	}, function error(response) {
		$scope.postResultMessage = "Error Status: " +  response.statusText;
	});

	$scope.branch = {};
	$scope.branch.branchId = 0;
	$scope.branchEdit = {};

	$scope.addBranch = function() {
		$scope.branch = {};
		$scope.branch.branchId = 0;
		$scope.modalTitle = 'Add';
		$scope.resetAndToggle();
	}
	
	$scope.resetAndToggle = function() {
		$('#bootstrap-wizard-1').find('.form-wizard').children('li').each(function(index, value){
			$( '#wiz-prev' ).click();
		});
		$('#bootstrap-wizard-1').find('.form-wizard').children('li').each(function(index, value){
			$( this ).find('.step').html(index + 1);
			$( this ).removeClass('complete');
			if (index == 0)
				$( this ).addClass('active');
			else
				$( this ).removeClass('active');
		});
		$('#myModal').modal('toggle');
	}

	$scope.saveBranch = function() {
		$http({
			url : '/api/branchSave',
			method : "POST",
			data : $scope.branch
		}).then(function(response) {
			if (response.data) {
				var index = $scope.branch.index;
				$scope.branch = response.data;
				if ($.isNumeric(index))
					$scope.branches[index] = $scope.branch;
				else
					$scope.branches.push($scope.branch);
				$rootScope.addedEntry();
				$('#myModal').modal('toggle');
			}
		}, function(response) {
			alert("failed");
		});
	}

	$scope.editBranch = function(x, index) {
		$http.get('/api/getBranch/' + x).then(function (response) {
			$scope.branch = response.data;
			$scope.branch.index = index;
			$scope.modalTitle = 'Edit';
			$scope.resetAndToggle();
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
	
	$scope.deleteBranch = function(x, index) {
		$http.get('/api/deleteBranch/' + x).then(function (response) {
			$scope.branches.splice(index, 1);
			$rootScope.deletedEntry();
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}

});

// create the controller and inject Angular's $scope
sportsApp.controller('branchRestoreController', function($scope, $rootScope, $http) {
	$rootScope.active = 'branch';

	$http.get('/api/allDeletedBranches').then(function (response) {
		$scope.branches = response.data;
		$rootScope.initPagination($scope.branches.length)
	}, function error(response) {
		$scope.postResultMessage = "Error Status: " +  response.statusText;
	});

	$scope.deleteBranch = function(x, index) {
		$http.get('/api/restoreBranch/' + x).then(function (response) {
			$scope.branches.splice(index, 1);
			$rootScope.deletedEntry();
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
	
}); 