sportsApp.filter('slotformat', function() {
    return function(x) {
    	var hours = parseInt(x / 60);
    	if (hours < 10)
    		hours = '0' + hours;
    	var mins = x % 60;
    	if (mins < 10)
    		mins = '0' + mins;
        return hours + ':' + mins;
    };
});
sportsApp.controller('editBookingController', function($scope, $rootScope, $http, $filter) {
	
	
	$rootScope.active = 'editBooking';
	$scope.page='editBooking';
	$scope.selectedFlag ="not-selected";
	$scope.list=[];
	
	//get bookings of the date by given member
	$scope.getBookings = function(){
		$scope.list=[];
		$scope.slots=[];
		$scope.availables=[];
		$scope.displayflag = false;
		var d=dateFomater($scope.bookingDate);
		$http({
			url : '/api/getMembersBookingsAndDate',
			method : "POST",			
			data: {"data":{"memberId":JSON.parse($scope.memberid),"bookingDate":d}}
			
		}).then(function(response) {
			if(response && response.data){
				$scope.slots = response.data;
				$scope.memberName = (response.data.length) ? response.data[0].memberId.memberName : "";
				$scope.noBooking = response.data.length == 0 ?  true:false ; 
			}
			
		}, function(response) {
			alert("Failed to process the request!");
		
		});
	}
	
	//click on slot
	$scope.addToList = function(item){
		$(".slot").removeClass("selected")
		$scope.list=[];
		$scope.list.push(item);
		event.target.closest("div").classList.add("selected");			
		$scope.displayflag = $scope.list.length > 0 ? true : false;
		
	}
	//get available time slots of propsed day
	$scope.allAvailableTimeTables = function() {
		//check date selection here
		if(new Date($scope.proposedDate) >= new Date()){
			var d=dateFomaterWithSpace($scope.proposedDate);
			$http({
				url : '/api/allAvailableTimeTables/',
				method : "POST",
				data : {
					'facility' : $scope.list[0].facilityId,
					'selectedDate' : d
				}
			}).then(function(response) {
				if(response && response.data){
					$scope.availables = response.data;
				}else{
					$scope.availables=[];
				}
				
			}, function error(response) {
				console.log("error in getting available time slots");
				$scope.availables=[];
			});
		}else{
			alert("Please select a future date!");
			$scope.availables=[];
		}
		
			
	}
	
	//on click on proposed slot, call validity check
	$scope.checkMemberValidity = function(item){		
		var data={
				"member":$scope.memberid.toString(),
				"selectedDate":dateFomaterWithSpace($scope.proposedDate)
		};
		$http({
			url : '/api/checkMemberValidityForShifting',
			method : "POST",
			data : {
				'data' : data
			}
		}).then(function(response) {
			if(response.data && response.data.length){
				alert(response.data);
			}else{
				$scope.runValidation(item);
			}
			
		}, function(response) {
			alert("Failed to check the member validity!");
		});
	}
	
	$scope.runValidation = function(item){
		if(window.confirm("Do you want to book this slot?")){
			$http({
				url : '/api/checkMemAllReadyBookedSlot',
				method : "POST",
				data : {
					'facility' : item.facilityId,
					'subFacility' : $scope.list[0].subFacilityId,
					'selectedDate' : dateFomaterWithSpace($scope.proposedDate),
					'timeTable' : item,
					'member' : $scope.memberid.toString()
				}
			}).then(function(response) {
			   if(response.data){
				   //validation result-Negative
				   alert(response.data);
			   }else{
				   //proceed to edit the booking
				   shiftBooking(item);
				  // $scope.editableSlot = true;
			   }
			   
			}, function error(response) {
				alert("Failed to check slot availability");
			});
		}
		
		
	}
	
	function shiftBooking(time){
		var data = {};
		data.bookingId =  $scope.list[0].bookingId;
		data.facility = $scope.list[0].facilityId;
		data.memberId = JSON.parse($scope.memberid);
		data.selectedDate = dateFomaterWithSpace($scope.proposedDate);
		data.subFacility = $scope.list[0].subFacilityId;
		data.timeTables = [time]
		
		$http({
			url : '/api/shiftBooking',
			method : "POST",
			data : [data]
		}).then(function(response) {
		   alert("Booking edit successfully");
    		   $scope.list=[];
			$scope.slots=[];
			$scope.availables=[];
			$scope.displayflag = false;
		   
		}, function error(response) {
			alert("Failed to edit booking");
		});

	}
	
	function dateFomater(date){
		var month_names = ["Jan", "Feb", "Mar", 
		                   "Apr", "May", "Jun", "Jul", "Aug", "Sep", 
		                   "Oct", "Nov", "Dec"];
		                   
		        var today = new Date(date);
		                   var day = today.getDate();
		                   var month_index = today.getMonth();
		                   var year = today.getFullYear();

		                return day + "-" + month_names[month_index] + "-" + year;
	}
	function dateFomaterWithSpace(date){
		var month_names = ["Jan", "Feb", "Mar", 
		                   "Apr", "May", "Jun", "Jul", "Aug", "Sep", 
		                   "Oct", "Nov", "Dec"];
		                   
		        var today = new Date(date);
		                   var day = today.getDate();
		                   var month_index = today.getMonth();
		                   var year = today.getFullYear();

		                return day + " " + month_names[month_index] + " " + year;
	}
	
});