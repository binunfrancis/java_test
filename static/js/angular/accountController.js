
sportsApp.controller('accountController', function($scope, $rootScope, $filter, $http, Flash,DTOptionsBuilder, DTColumnBuilder,$q) {
	
	$rootScope.active = 'account';
	$scope.accounts=[];
	$scope.fullData=[];
	$scope.facilityNames = [];
	$scope.subFacilityNames = [];
	$scope.centerNames=[];
	$scope.centername = "All";
	$scope.facilityname = "All";
	$scope.subfacilityname = "All";
	$scope.startDate = getCurrentDate();
	$scope.endDate = getCurrentDate();
	//var dateConst = 19800000;
	var dateConst = 0;
	$scope.totaldebitamount = 0;
	$scope.totalcreditamount = 0;
	 var vm = this;
	 
	 function getCurrentDate(){
			var today = new Date();
			var dd = today.getDate();
			var mm = today.getMonth()+1; 
			var yyyy = today.getFullYear();
			if(dd<10){
			    dd='0'+dd;
			} 
			if(mm<10){
			    mm='0'+mm;
			} 
			var today = yyyy+'-'+mm+'-'+dd;
			return today;
		} 
		
	 var setTotalAmounts = function(data){
		 if(data && data.length){
			 var d=0,c=0;
			 for(var i=0;i<data.length;i++){
				 d = d + Number(data[i].actualAmount);
				 c = c + Number(data[i].creditAmount);
			 }
			 $scope.totaldebitamount = d;
			 $scope.totalcreditamount = c;
			 var temp = {
				"actualAmount" : d,
				"creditAmount" : c,
				"typeOfBooking" : "TOTAL",
				"accountsId":"NIL",
				"paidDate":"NIL",
				"memberId":{"memberName":"NIL"}
			 };
			 data.unshift(temp);
		 }
		 return data;
	 }
	 var extractFilterOptions = function(data){
			$scope.facilityNames=[];
			$scope.centerNames=[];
			$scope.subFacilityNames=[];
			for(var i=0;i<data.length;i++){
			       //if($scope.facilityNames.indexOf(data[i].facilityId.facilityName)<0) { $scope.facilityNames.push(data[i].facilityId.facilityName); }
			    if(data[i].centerId && data[i].centerId.centreName){
					if($scope.centerNames.indexOf(data[i].centerId.centreName)<0) { $scope.centerNames.push(data[i].centerId.centreName); }
			    }   
			       //if($scope.subFacilityNames.indexOf(data[i].subFacilityId.subFacilityName)<0) { $scope.subFacilityNames.push(data[i].subFacilityId.subFacilityName); }
			}
			$scope.facilityNames.unshift("All");
			$scope.centerNames.unshift("All");
			$scope.subFacilityNames.unshift("All");
		}
	 function newPromise(){
		 $scope.start = new Date($scope.startDate);
			$scope.end= new Date($scope.endDate);
			$scope.centername;
			$scope.facilityname;
			$scope.subfacilityname;
		    var dataByDate=[],dataByCentre=[],dataByFacility=[],dataBySubFacility=[];
			var deferred=$q.defer();
	    	dataBydate = $scope.accounts.filter(function (booking) {
	    		//the const value below is time difference by one day
			    return (booking.paidDate >= ($scope.start.getTime()) && booking.paidDate <= ($scope.end.getTime()+86400000));
			});
    		if($scope.centername != "All"){
    			 dataByCentre = dataBydate.filter(function (booking) {
				    return ($scope.centername == booking.centerId.centreName);
				});
    		}else{
    			dataByCentre = dataBydate;
    		}
    		console.log("here i am");
    		dataByCentre = setTotalAmounts(dataByCentre);
	    	deferred.resolve(dataByCentre);
	    	return deferred.promise;
	    }
	 vm.newPromise=newPromise;
	 
	 var getJsonData=function(){
	    	var deferred=$q.defer();
	    	$http.get('/dashboard/allAccounts').then(function (response) {
	    		$scope.start = new Date($scope.startDate);
	    		$scope.end= new Date($scope.endDate);
	    		$scope.accounts = response.data;
	    		extractFilterOptions(response.data);
	    		var currentDateData = $scope.accounts.filter(function (booking) {
				    return (booking.paidDate >= ($scope.start.getTime()) && booking.paidDate <= ($scope.end.getTime()+86400000));
				});
	    		currentDateData = setTotalAmounts(currentDateData);
	    		deferred.resolve(currentDateData);
	    	}, function error(response) {
	    		$scope.postResultMessage = "Error Status: " +  response.statusText;
	    	});
	    	
	    	return deferred.promise;
	    }
	 
	   vm.dtOptions = DTOptionsBuilder.fromFnPromise(getJsonData)
       .withDOM('frtip')
       .withPaginationType('full_numbers')
       .withColumnFilter({
       aoColumns: [{
           type: 'number'
       }, {
           type: 'text',
           bRegex: true,
           bSmart: true
       },
//       {
//           type: 'select',
//           bRegex: false,
//           values: ['Yoda', 'Titi', 'Kyle', 'Bar', 'Whateveryournameis']
//       }
       , {
           type: 'date',
           bRegex: true,
           bSmart: true
       }]
   })
       // Active Buttons extension
       .withButtons([
//           'columnsToggle',
//           'colvis',
//           'copy',
           'print',
//           'excel',
//           {
//               text: 'Some button',
//               key: '1',
//               action: function (e, dt, node, config) {
//                   alert('Button activated');
//               }
//           }
       ]);
   vm.dtColumns = [
       DTColumnBuilder.newColumn('accountsId').withTitle('Receipt No'),
       DTColumnBuilder.newColumn('memberId.memberName').withTitle('MemberName'),
       DTColumnBuilder.newColumn('paidDate').withTitle('Receipt Date').renderWith(function(data, type) {
          	return $filter('date')(data, 'dd/MM/yyyy'); //date filter
       }),
       DTColumnBuilder.newColumn('typeOfBooking').withTitle('Receipt Type'),      
       DTColumnBuilder.newColumn('actualAmount').withTitle('Debit Amount'),
       DTColumnBuilder.newColumn('creditAmount').withTitle('Credit Amount')
       
   ];
   
  
   vm.dtInstance = {};
	

});


