
 
 sportsApp.controller('trainerController', function($scope, $rootScope, $http) {
	$rootScope.active = 'trainer';
//	$scope.page = 'Mat Price';
	$scope.trainer = {};
	$scope.trainer.trainerFinalId = 0;
	$scope.trainer = {};
	
	loadTrainers();
	function loadTrainers(){
		$http.get('/dashboard/allTrainers').then(function (response) {
			console.log(response.data);
			sortTrainerData(response.data);
//			$scope.matPrices = response.data;
			$rootScope.initPagination($scope.trainers.length)
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
	

	
	
	
	function compare(a,b) {
		  if (a.trainerFinalId > b.trainerFinalId)
		    return -1;
		  if (a.trainerFinalId < b.trainerFinalId)
		    return 1;
		  return 0;
	}
	function sortTrainerData(data){
		$scope.trainers=data.sort(compare);
		console.log('fiunnnn'+data);
	}

	$scope.addTrainer = function() {
		$scope.trainer = {};
		$scope.trainer.trainerFinalId = 0;
		$scope.modalTitle = 'Add';
		$('#myModal').modal('toggle');		
	}

	$scope.saveTrainer = function() {
//		alert(123);
		$http({
			url : '/dashboard/saveTrainer',
			method : "POST",
			data : {
				'data' : $scope.trainer
			}
		}).then(function(response) {
			$scope.trainer.trainerFinalId = response.data;
			if ($.isNumeric($scope.trainer.index))
				$scope.trainers[$scope.trainer.index] = $scope.trainer;
			else
			$scope.trainers.push($scope.trainer);
			loadTrainers();
			$rootScope.addedEntry();
//			$scope.matPrice = {};
			$('#myModal').modal('toggle');
		}, function(response) { // optional
			alert("failed");
		});

	}

	$scope.editTrainer = function(event, index) {
		$http.get('/dashboard/getTrainer/' + event.target.id).then(function (response) {
			$scope.trainer = response.data;
			$scope.trainer.index = index;
			$scope.modalTitle = 'Edit';
			$('#myModal').modal('toggle');
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
	
	$scope.deleteTrainer= function(event, index) {
		
		$http.get('/dashboard/deleteTrainer/' + event.target.id).then(function (response) {
			$scope.trainers.splice(index, 1);
			$rootScope.deletedEntry();
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
});

// create the controller and inject Angular's $scope
sportsApp.controller('trainerRestoreController', function($scope, $rootScope, $http) {
	$rootScope.active = 'trainer';
	$http.get('/api/allDeletedTrainers').then(function (response) {
		$scope.trainers = response.data;
	}, function error(response) {
		$scope.postResultMessage = "Error Status: " +  response.statusText;
	});

	$scope.deleteTrainer = function(rs, index) {
		$http.get('/api/restoreTrainer/' + rs.trainerFinalId).then(function (response) {
			$scope.trainers.splice(index, 1);
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
	
}); 