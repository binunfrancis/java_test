
 
 sportsApp.controller('matPriceController', function($scope, $rootScope, $http) {
	$rootScope.active = 'matPrice';
	$scope.page = 'Mat Price';
	$scope.matPrice = {};
	$scope.matPrice.matPriceId = 0;
	$scope.matPriceEdit = {};
	
	loadMatPrices();
	function loadMatPrices(){
		$http.get('/dashboard/allMatPrices').then(function (response) {
			console.log(response.data);
			sortMatPriceData(response.data);
//			$scope.matPrices = response.data;
			$rootScope.initPagination($scope.matPrices.length)
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
	
	function compare(a,b) {
		  if (a.matPriceId > b.matPriceId)
		    return -1;
		  if (a.matPriceId < b.matPriceId)
		    return 1;
		  return 0;
	}
	function sortMatPriceData(data){
		$scope.matPrices=data.sort(compare);
		console.log(data);
	}

	$scope.addMatPrice = function() {
		$scope.matPrice = {};
		$scope.matPrice.matPriceId = 0;
		$scope.modalTitle = 'Add';
		$('#myModal').modal('toggle');		
	}

	$scope.saveMatPrice = function() {
		$http({
			url : '/dashboard/matPriceSave',
			method : "POST",
			data : {
				'data' : $scope.matPrice
			}
		}).then(function(response) {
			$scope.matPrice.matPriceId = response.data;
			if ($.isNumeric($scope.matPrice.index))
				$scope.matPrices[$scope.matPrice.index] = $scope.matPrice;
			else
			$scope.matPrices.push($scope.matPrice);
			loadMatPrices();
			$rootScope.addedEntry();
			$scope.matPrice = {};
			$('#myModal').modal('toggle');
		}, function(response) { // optional
			alert("failed");
		});

	}

	$scope.editMatPrice = function(event, index) {
		$http.get('/dashboard/getMatPrice/' + event.target.id).then(function (response) {
			$scope.matPrice = response.data;
			$scope.matPrice.index = index;
			$scope.modalTitle = 'Edit';
			$('#myModal').modal('toggle');
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
	
	$scope.deleteMatPrice= function(event, index) {
		$http.get('/dashboard/deleteMatPrice/' + event.target.id).then(function (response) {
			$scope.matPrices.splice(index, 1);
			$rootScope.deletedEntry();
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
});

// create the controller and inject Angular's $scope
sportsApp.controller('matPriceRestoreController', function($scope, $rootScope, $http) {
	$rootScope.active = 'matPrice';

	$http.get('/api/allDeletedMatPrices').then(function (response) {
		$scope.matPrices = response.data;
	}, function error(response) {
		$scope.postResultMessage = "Error Status: " +  response.statusText;
	});

	$scope.deleteMatPrice = function(rs, index) {
		$http.get('/api/restoreMatPrice/' + rs.matPriceId).then(function (response) {
			$scope.matPrices.splice(index, 1);
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
	
}); 