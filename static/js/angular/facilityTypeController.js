sportsApp.controller('facilityTypeController', function($scope, $rootScope, $http) {
	$rootScope.active = 'facilityType';
	$scope.page='Facility Type';
	$scope.facilityType = {};
	$scope.facilityType.facilityTypeId = 0;
	$scope.facilityTypeEdit = {};

	loadAllFacilites();
	function loadAllFacilites() {
		$http.get('/dashboard/allFacilityTypes').then(function (response) {
			sortFacilityData(response.data);
//			$scope.facilityTypes = response.data;
			
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
		
	}
	function compare(a,b) {
		  if (a.facilityTypeId > b.facilityTypeId)
		    return -1;
		  if (a.facilityTypeId < b.facilityTypeId)
		    return 1;
		  return 0;
	}
	function sortFacilityData(data){
		$scope.facilityTypes=data.sort(compare);
		console.log(data);
		$rootScope.initPagination($scope.facilityTypes.length)
	}
	

	$scope.addFacilityType = function() {
		$scope.facilityType = {};
		$scope.facilityType.facilityTypeId = 0;
		$scope.modalTitle = 'Add';
		$('#myModal').modal('toggle');		
	}

	$scope.saveFacilityType = function() {
		$http({
			url : '/dashboard/facilityTypeSave',
			method : "POST",
			data : {
				'data' : $scope.facilityType
			}
		}).then(function(response) {
			$scope.facilityType.facilityTypeId = response.data;
			if ($.isNumeric($scope.facilityType.index))
				$scope.facilityTypes[$scope.facilityType.index] = $scope.facilityType;
			else
				$scope.facilityTypes.push($scope.facilityType);
			
			$rootScope.addedEntry();
			$scope.facilityType = {};
			loadAllFacilites();
			$rootScope.currentPage = 1;
			$('#myModal').modal('toggle');
			alert("Saved successfully");
		}, function(response) { // optional
			alert("failed");
		});

	}

	$scope.editFacilityType = function(event, index) {
		$http.get('/dashboard/getFacilityType/' + event.target.id).then(function (response) {
			$scope.facilityType = response.data;
			$scope.facilityType.index = index;
			if($scope.facilityType.bookStatus){
				$scope.facilityType.bookStatus = $scope.facilityType.bookStatus.toString();	
			}
			
			$scope.modalTitle = 'Edit';
			$('#myModal').modal('toggle');
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
	
	$scope.deleteFacilityType = function(event, index) {
		$http.get('/dashboard/deleteFacilityType/' + event.target.id).then(function (response) {
			$scope.facilityTypes.splice(index, 1);
			$rootScope.deletedEntry();
			alert("Deleted Successfully");
			loadAllFacilites();
			$rootScope.currentPage = 1;
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
});

sportsApp.controller('facilityTypeRestoreController', function($scope, $rootScope, $http) {
	$rootScope.active = 'facilityType';

	$http.get('/api/allDeletedFacilityTypes').then(function (response) {
		$scope.facilityTypes = response.data;
	}, function error(response) {
		$scope.postResultMessage = "Error Status: " +  response.statusText;
	});

	$scope.deleteFacilityType = function(rs, index) {
		$http.get('/api/restoreFacilityType/' + rs.facilityTypeId).then(function (response) {
			$scope.facilityTypes.splice(index, 1);
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
	
}); 