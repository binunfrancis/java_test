sportsApp.filter('dateTimeHelper', function() {
    return function(x) {
       var d = new Date(x);
       d = d.toISOString().slice(0,10);
       return d;
    };
});
sportsApp.filter('formatterEntry', function() {
    return function(x){
    	var d = new Date(x);
    	var str = d.getDate() +"-" + (d.getMonth()+1) +"-"+d.getFullYear();
        return str;
    };
});
sportsApp.filter('slotter', function() {
    return function(x) {
       var b= x.toString();
       var f;
       
       if(b.match(/[.]5$/g)){
    	   f= b.replace(/[.]5$/g,":30")
       }
       else{
    	   f=b+":00";
       }
       return f;
    };
});
sportsApp.controller('memberEntryController', function($scope, $rootScope, $http, $filter) {
	$rootScope.active = 'memberEntry';
	$scope.page='memberEntry';
	$scope.memberid;
	$scope.eyeClicked = false;
	
	$scope.getBookings = function(){
		console.log(123);
		$http.get('/api/getAllMembersBookingsCurDate/'+$scope.memberid).then(function (response) {
			$scope.bookingList = response.data;
			
			console.log(response.data);
			$rootScope.initPagination($scope.bookingList.length);
		}, function error(response) {
			alert("Could not fetch the booking data!");
			$scope.bookingList =[];
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
	$scope.updateTimeSlot = function(e,o){
		var flag = window.confirm("Do you want to proceed?");
		if(flag){
			//update group members
			if($scope.eyeClicked){
				var selected=[];
				$('#subModal input:checked').each(function() {
				    selected.push($(this).attr('id').toString());
				});
				$http({
					url : '/dashboard/updateGroupMembersBooking',
					method : "POST",
				
				
				data : {
					'data' : {
					
						"groupMemberIds" : selected,
					}
					}
				
				}).then(function(response) {
					updateTimeSlotStep(o);
				}, function(response) {
					updateTimeSlotStep(o);
				
				});
			}else{
				updateTimeSlotStep(o);
			}
		}
		
	}
	function updateTimeSlotStep(o){
		$scope.eyeClicked = false;
		$http.get('/api/updateBookingStatus/'+o.bookingId).then(function (response) {
			alert("Updated Successfully");
			$scope.getBookings();
			
		}, function error(response) {
			alert("Failed to update");
		});
	}
	$scope.viewMembers = function(fl) {
		//bookingId
		$scope.eyeClicked = true;
		$scope.additionalMembersVisited=[];
		$http.get('/dashboard/allAddMembersList/' + fl.bookingId).then(function (response) {
			console.log(response.data);
			$scope.additionalMembers = response.data;
			$('#subModal').modal('toggle');
		}, function error(response) {

			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
	$scope.viewMembersOfVisited = function(fl){
		$scope.additionalMembers=[];
		$http.get('/dashboard/allAddMembersList/' + fl.bookingId).then(function (response) {
			console.log(response.data);
			
			$scope.additionalMembersVisited = response.data;
			$('#subModal').modal('toggle');
		}, function error(response) {

			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
	
	
	
	
	
}); 

sportsApp.filter('timeFormat', function() {
    return function(x) {
    	var hours = parseInt(x / 60);
    	if (hours < 10)
    		hours = '0' + hours;
    	var mins = x % 60;
    	if (mins < 10)
    		mins = '0' + mins;
        return hours + ':' + mins;
    };
});