sportsApp.controller('holidayController', function($scope, $rootScope, $http, $filter) {
	$rootScope.active = 'holiday';
	$scope.page='Holiday';
	$scope.holiday = {};
	$scope.holiday.holidayId = 0;
	
	loadAllHolidayTypes();
	
	function loadAllHolidayTypes(){
		$http.get('/api/allHolidays').then(function (response) {
			sortHolidayData(response.data);
//			$scope.holidays = response.data;
			
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
	
	function compare(a,b) {
		  if (a.holidayId > b.holidayId)
		    return -1;
		  if (a.holidayId < b.holidayId)
		    return 1;
		  return 0;
	}
	function sortHolidayData(data){
		$scope.holidays=data.sort(compare);
		console.log(data);
		$rootScope.initPagination($scope.holidays.length)
		for (var int = 0; int < $scope.holidays.length; int++) {
			$scope.holidays[int].date = $filter('date')($scope.holidays[int].date, "dd-MMM-yyyy");
		}
	}
	

	$scope.addHoliday = function() {
		$scope.holiday = {};
		$scope.holiday.holidayId = 0;
		$scope.modalTitle = 'Add';
		$('#myModal').modal('toggle');		
	}

	$scope.saveHoliday = function() {
		$http({
			url : '/api/saveHoliday',
			method : "POST",
			data : $scope.holiday
		}).then(function(response) {
			$scope.holiday = response.data;
			$scope.holiday.date = $filter('date')($scope.holiday.date, "dd-MMM-yyyy");
			$scope.holidays.push($scope.holiday);
			$('#myModal').modal('toggle');
			alert("Saved successfully");
			$rootScope.addedEntry();
			loadAllHolidayTypes();
			$rootScope.currentPage = 1;
			
		}, function(response) {
			alert("failed");
		});
	}
	$scope.deleteHoliday = function(id, index) {
		$http.get('/api/deleteHoliday/' + id).then(function (response) {
			$scope.holidays.splice(index, 1);
			$rootScope.deletedEntry();
			alert("Deleted Successfully");
			loadAllHolidayTypes();
			$rootScope.currentPage = 1;
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
});