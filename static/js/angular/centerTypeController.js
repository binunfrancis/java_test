sportsApp.controller('centerTypeController', function($scope, $rootScope, $http) {
	$rootScope.active = 'centerType';
	$scope.centerType = {};
	$scope.page='Center Type';
	$scope.centerType.centerTypeId = 0;
	$scope.centerTypeEdit = {};
	
	
	loadAllCenterTypes();
	function loadAllCenterTypes(){
		$http.get('/dashboard/allCenterTypes').then(function (response) {
			sortCenterTypesData(response.data);
//			$scope.centerTypes = response.data;
					}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
		
	}

	function compare(a,b) {
		  if (a.centerTypeId > b.centerTypeId)
		    return -1;
		  if (a.centerTypeId < b.centerTypeId)
		    return 1;
		  return 0;
	}
	function sortCenterTypesData(data){
		$scope.centerTypes=data.sort(compare);
		console.log(data);
		$rootScope.initPagination($scope.centerTypes.length)

	}
	
	
	

	$scope.addCenterType = function() {
		$scope.centerType = {};
		$scope.centerType.centerTypeId = 0;
		$scope.modalTitle = 'Add';
		$('#myModal').modal('toggle');		
	}

	$scope.saveCenterType = function() {
		$http({
			url : '/dashboard/centerTypeSave',
			method : "POST",
			data : {
				'data' : $scope.centerType
			}
		}).then(function(response) {
			$scope.centerType.centerTypeId = response.data;
			if ($.isNumeric($scope.centerType.index))
				$scope.centerTypes[$scope.centerType.index] = $scope.centerType;
			else
				$scope.centerTypes.push($scope.centerType);
			
			$rootScope.addedEntry();
			$scope.centerType = {};
			loadAllCenterTypes();
			$rootScope.currentPage = 1;
			$('#myModal').modal('toggle');
			alert("Saved successfully");
		}, function(response) { // optional
			alert("failed");
		});

	}

	$scope.editCenterType = function(event, index) {
		$http.get('/dashboard/getCenterType/' + event.target.id).then(function (response) {
			$scope.centerType = response.data;
			$scope.centerType.index = index;
			$scope.modalTitle = 'Edit';
			$('#myModal').modal('toggle');
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
	
	$scope.deleteCenterType = function(event, index) {
		$http.get('/dashboard/deleteCenterType/' + event.target.id).then(function (response) {
			$scope.centerTypes.splice(index, 1);
			$rootScope.deletedEntry();
			loadAllCenterTypes();
			alert("Deleted Successfully");
			$rootScope.currentPage = 1;
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
});

sportsApp.controller('centerTypeRestoreController', function($scope, $rootScope, $http) {
	$rootScope.active = 'centerType';

	$http.get('/api/allDeletedCenterTypes').then(function (response) {
		$scope.centerTypes = response.data;
	}, function error(response) {
		$scope.postResultMessage = "Error Status: " +  response.statusText;
	});

	$scope.deleteCenterType = function(rs, index) {
		$http.get('/api/restoreCenterType/' + rs.centerTypeId).then(function (response) {
			$scope.centerTypes.splice(index, 1);
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
	
}); 