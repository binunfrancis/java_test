sportsApp.controller('viewBookController', function($scope, $rootScope, $filter, $http, Flash,DTOptionsBuilder, DTColumnBuilder,$q,$compile) {
	
	$rootScope.active = 'booking';
	$scope.bookings=[];
	$scope.fullData=[];
	$scope.facilityNames = [];
	$scope.subFacilityNames = [];
	$scope.centerNames=[];
	
	$scope.startDate = getCurrentDate();
	$scope.endDate = getCurrentDate();
	var dateConst = 19800000;
	var vm = this;
	loadCenterFilterOptions();
	loadFacilityOptions();
	loadSubfacilityOptions();
	$scope.centername = "All";
	$scope.facilityname = "All";
	$scope.subfacilityname = "All";
	var centerMap={};
	var facilityMap={};
	var subfacilityMap={};
	
	
	function createCenterMap(data){
		data.map(function(i) {centerMap[i.centreName] = i.centreId;});
	}
	function createFacilityMap(data){
		data.map(function(i) {facilityMap[i.facilityName] = i.facilityId;});
	}
	function createsubFacilityMap(data){
		data.map(function(i) {subfacilityMap[i.subFacilityName] = i.subFacilityId;});
	}
	function loadCenterFilterOptions(){
		$http.get('/dashboard/allCenters').then(function (response) {
			var t1=[];
			createCenterMap(response.data);
			response.data.map(function(i){ if(t1.indexOf(i.centreName)<0){t1.push(i.centreName)}});
			$scope.centerNames = t1;		
			$scope.centerNames.unshift("All");
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
	function loadFacilityOptions(){
		$http.get('/api/allFacilities').then(function (response) {
			var t2=[];
			createFacilityMap(response.data);
			response.data.map(function(i){ if(t2.indexOf(i.facilityName)<0){t2.push(i.facilityName)}});
			//$scope.facilityNames = t2;
			$scope.facilityNames.unshift("All");
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
	function loadSubfacilityOptions(f){
		$http.get('/api/allSubFacilities').then(function (response) {
			var t3=[];
			createsubFacilityMap(response.data);
			response.data.map(function(i){ if(t3.indexOf(i.subFacilityName)<0){t3.push(i.subFacilityName)}});
			//$scope.subFacilityNames = t3;	
			$scope.subFacilityNames.unshift("All");
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
	$scope.setFacility = function(){
		var centerID = centerMap[$scope.centername];
		if(centerID == undefined) { 
			$scope.facilityNames=[];
			$scope.facilityNames.unshift("All");
			$scope.facilityname ="All";
			$scope.subfacilityname ="All";
		}else{
			$http.get('/api/allFacilities/'+centerID).then(function (response) {
				var t2=[];
				//createFacilityMap(response.data);
				response.data.map(function(i){ if(t2.indexOf(i.facilityName)<0){t2.push(i.facilityName)}});
				$scope.facilityNames = t2;
				$scope.facilityNames.unshift("All");
			}, function error(response) {
				$scope.postResultMessage = "Error Status: " +  response.statusText;
			});
		}
		
	}
	$scope.setSubFacility = function(){
		var fID = facilityMap[$scope.facilityname];
		if(fID == undefined) { 
			
			$scope.subFacilityNames = [];	
			$scope.subFacilityNames.unshift("All");
			$scope.subfacilityname ="All";
		}else{
			$http.get('/api/allSubFacilities/'+fID).then(function (response) {
				var t3=[];
				//createsubFacilityMap(response.data);
				response.data.map(function(i){ if(t3.indexOf(i.subFacilityName)<0){t3.push(i.subFacilityName)}});
				$scope.subFacilityNames = t3;	
				$scope.subFacilityNames.unshift("All");
			}, function error(response) {
				$scope.postResultMessage = "Error Status: " +  response.statusText;
			});
		}
		
	}
	/*function newPromise(){
		$scope.start = new Date($scope.startDate);
		$scope.end= new Date($scope.endDate);
		$scope.centername;
		$scope.facilityname;
		$scope.subfacilityname;
	    var dataByDate=[],dataByCentre=[],dataByFacility=[],dataBySubFacility=[];
		var deferred=$q.defer();
	    	
	    		dataBydate = $scope.bookings.filter(function (booking) {
				    return (booking.bookedDate >= ($scope.start.getTime()-dateConst) && booking.bookedDate <= ($scope.end.getTime()-dateConst));
				});
	    		if($scope.centername != "All"){
	    			 dataByCentre = dataBydate.filter(function (booking) {
					    return ($scope.centername == booking.facilityId.centerId.centreName);
					});
	    		}else{
	    			dataByCentre = dataBydate;
	    		}
	    		if($scope.facilityname != "All"){
	    			 dataByFacility = dataByCentre.filter(function (booking) {
					    return ($scope.facilityname == booking.facilityId.facilityName);
					});
	    		}else{
	    			dataByFacility = dataByCentre;
	    		}
	    		if($scope.subfacilityname != "All"){
	    			 dataBySubFacility = dataByFacility.filter(function (booking) {
					    return ($scope.subfacilityname == booking.subFacilityId.subFacilityName);
					});
	    		}else{
	    			dataBySubFacility = dataByFacility;
	    		}
	    	
	    	
	    	deferred.resolve(dataBySubFacility);
	    	return deferred.promise;
	    }

	
	vm.newPromise=newPromise;
	*/
	
	var extractFilterOptions = function(data){
		$scope.facilityNames=[];
		$scope.centerNames=[];
		$scope.subFacilityNames=[];
		for(var i=0;i<data.length;i++){
			if(data[i].facilityId && data[i].facilityId.facilityName){
			       if($scope.facilityNames.indexOf(data[i].facilityId.facilityName)<0) { $scope.facilityNames.push(data[i].facilityId.facilityName); }
			}
			if(data[i].facilityId && data[i].facilityId.centerId && data[i].facilityId.centerId.centreName){
			       if($scope.centerNames.indexOf(data[i].facilityId.centerId.centreName)<0) { $scope.centerNames.push(data[i].facilityId.centerId.centreName); }
			}
			if(data[i].subFacilityId && data[i].subFacilityId.subFacilityName){
			       if($scope.subFacilityNames.indexOf(data[i].subFacilityId.subFacilityName)<0) { $scope.subFacilityNames.push(data[i].subFacilityId.subFacilityName); }
			}
		}
		$scope.facilityNames.unshift("All");
		$scope.centerNames.unshift("All");
		$scope.subFacilityNames.unshift("All");
	}
	var getFormattedTime = function(x){
		var hours = parseInt(x / 60);
    	if (hours < 10)
    		hours = '0' + hours;
    	var mins = x % 60;
    	if (mins < 10)
    		mins = '0' + mins;
        return hours + ':' + mins;
	}
	//function to convert the time format in response
	var formatResponse = function(data){
		if(data && data.length){
			for(var i=0;i<data.length;i++){
				data[i].sessionStartTime = getFormattedTime(data[i].sessionStartTime);
				data[i].sessionEndTime = getFormattedTime(data[i].sessionEndTime);
				data[i].sessionTime = data[i].sessionStartTime +" - "+data[i].sessionEndTime;
			}	
		}
		
		return data;
	}
    var getJsonData=function(){
    	var deferred=$q.defer();
    	$scope.start = new Date($scope.startDate);
		$scope.end= new Date($scope.endDate);
    	console.log("sss");
    	var data={
    		"formdate":$scope.startDate,
    		"toenddate":$scope.endDate,
    		"center":$scope.centername,
    		"facility":$scope.facilityname,
    		"subfacility":$scope.subfacilityname
    	};
    	
    	$http.post('/dashboard/allBookings', JSON.stringify(data) )
    	.success(function(data, status, headers, config) {
    		console.log(data);
    	//	extractFilterOptions(data);
    		
    		var d = formatResponse(data);
    		$scope.bookings = d;
    		deferred.resolve(d);
    	})
    	.error(function(data, status, headers, config) {
    		alert( "failure message: " + JSON.stringify({data: data}));
    		deferred.resolve();
    	});
    	/*var deferred=$q.defer();
    	$http.get('/dashboard/allBookings').then(function (response) {
    		console.log(response.data);
    		$scope.start = new Date($scope.startDate);
    		$scope.end= new Date($scope.endDate);
    		$scope.fullData = angular.copy(response.data);    		
    		extractFilterOptions(response.data);
    		$scope.bookings = response.data;
    		var currentDateData = $scope.bookings.filter(function (booking) {
			    return (booking.bookedDate >= ($scope.start.getTime()-dateConst) && booking.bookedDate <= ($scope.end.getTime()-dateConst));
			});   		
    		
    		deferred.resolve(currentDateData);
    	}, function error(response) {
    		$scope.postResultMessage = "Error Status: " +  response.statusText;
    	});
    	*/
    	 return deferred.promise;
    }
	
	
		

	
	vm.dtOptions = DTOptionsBuilder.fromFnPromise(getJsonData)
    .withDOM('frtip')
    .withPaginationType('full_numbers')
    .withOption('createdRow', createdRow)
    
    .withColumnFilter({
    aoColumns: [{
        type: 'number'
    }, {
        type: 'text',
        bRegex: true,
        bSmart: true
    }, 
    {
    	type: 'date',
        bRegex: true,
        bSmart: true
    },
    {
        type: 'date',
        bRegex: true,
        bSmart: true
    },{
        type: 'text',
        bRegex: true,
        bSmart: true
    },{
        type: 'text',
        bRegex: true,
        bSmart: true
    }
    ]
    
})

    // Active Buttons extension
    .withButtons([
        'print',

    ]);
vm.dtColumns = [
    DTColumnBuilder.newColumn('bookingId').withTitle('BookingId'),
    DTColumnBuilder.newColumn('memberId.memberName').withTitle('MemberName'),
    DTColumnBuilder.newColumn('bookedDate').withTitle('Booked Date').renderWith(function(data, type) {
    	return $filter('date')(data, 'dd/MM/yyyy'); //date filter
    }),
    DTColumnBuilder.newColumn('bookingDate').withTitle('Booking Date').renderWith(function(data, type) {
    	return $filter('date')(data, 'dd/MM/yyyy'); //date filter
    }),
    DTColumnBuilder.newColumn('facilityId.centerId.centreName').withTitle('Center Name'),
    DTColumnBuilder.newColumn('facilityId.facilityName').withTitle('Facility Name'),
    DTColumnBuilder.newColumn('subFacilityId.subFacilityName').withTitle('subfacility Name'),
    //DTColumnBuilder.newColumn('sessionStartTime').withTitle('session start time'),
    DTColumnBuilder.newColumn('sessionTime').withTitle('session time'),
    DTColumnBuilder.newColumn('bookingId').withTitle('Print').renderWith(function(data,type,full,meta){
    	return '<div class="printrec" ng-click="printbookRec('+full.bookingId +')"><i class="fa fa-print"></i></div>';
    })
    
];

function createdRow(row, data, dataIndex) {
	$compile(angular.element(row).contents())($scope);
}
$scope.printbookRec = function(id){
	var id= id.toString();
	var str=location.origin+"/api/printBookingReceiptLater/"+id;
	window.open(str,"_blank");
}

vm.dtInstance = {};



	///
	
/*	$scope.filterData=function(){
		//alert("pls filter "+$scope.startDate);
		$scope.start = new Date($scope.startDate);
		$scope.end= new Date($scope.endDate);
	
		 vm.dtOptions = DTOptionsBuilder.fromFnPromise($scope.loadTable(new Date()))
	        .withDOM('frtip')
	        .withPaginationType('full_numbers')
		        // Active Buttons extension
		        .withButtons([
		            'columnsToggle',
		            'colvis',
		            'copy',
		            'print',
		            'excel',
		            {
		                text: 'Some button',
		                key: '1',
		                action: function (e, dt, node, config) {
		                    alert('Button activated');
		                }
		            }
		        ]);
		 
	}*/
	//Function to return the current date in yyyy-mm-dd 
	function getCurrentDate(){
		var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth()+1; 
		var yyyy = today.getFullYear();
		if(dd<10){
		    dd='0'+dd;
		} 
		if(mm<10){
		    mm='0'+mm;
		} 
		var today = yyyy+'-'+mm+'-'+dd;
		return today;
	} 

	$scope.reloadData = function() {
		vm.dtInstance.reloadData()
		}



});

