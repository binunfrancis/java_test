

sportsApp.controller('accountReportsController', function($scope, $rootScope, $filter, $http, Flash) {
	
	$rootScope.active = 'account';
	

	$http.get('/dashboard/allAccounts').then(function (response) {
		console.log(response.data);
		$scope.accountList = response.data;
		for (var int = 0; int < $scope.accountList.length; int++) {
			$scope.accountList[int].paidDate = $filter('date')($scope.accountList[int].paidDate, "dd-MMM-yyyy");
		}
		
	}, function error(response) {
		$scope.postResultMessage = "Error Status: " +  response.statusText;
	});


});

