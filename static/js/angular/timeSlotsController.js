sportsApp.controller('timeSlotsController', function($scope, $rootScope, $http) {
	$rootScope.active = 'timeTable';
	$scope.page='Time Table';

	
	$scope.savingtime = false;
	$scope.timeTable = {};
	$scope.timeTable.timeTableId = 0;
	$scope.timeTableEdit = {};
	$scope.subFacilityList = [];
	$scope.facilityNames = [];
	$scope.facilityName= "All";
	loadTimeSlots();
	$scope.options = [
	    {value:'fake', selected:false,'display':false},              
	    {value:'Sunday', selected:false},
		{value:'Monday', selected:false},
		{value:'Tuesday', selected:false},
		{value:'Wednesday', selected:false},
		{value:'Thursday', selected:false},
		{value:'Friday', selected:false},
		{value:'Saturday', selected:false}
	];
	function loadTimeSlots(){
		$http.get('/dashboard/allTimeSlots').then(function (response) {
			$scope.timeSlotsList = response.data;
			$scope.allTimeSlotBackup = angular.copy(response.data);
			extractFacilityNames(response.data);
			$rootScope.initPagination($scope.timeSlotsList.length)
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});

		$http.get('/api/allFacilities').then(function (response) {
			$scope.facilityList = response.data;
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});

	}
	$scope.filterData= function(){
		if($scope.facilityName == "All"){
			$scope.timeSlotsList = $scope.allTimeSlotBackup;
		}else{
			//filter here
			var temp=[];
			for(var j=0;j<$scope.allTimeSlotBackup.length;j++){
				if($scope.allTimeSlotBackup[j].facilityId.facilityName == $scope.facilityName){
					temp.push($scope.allTimeSlotBackup[j]);
				}
			}
			$scope.timeSlotsList = temp;
			
			
		}
		$rootScope.initPagination($scope.timeSlotsList.length);
		$rootScope.currentPage = 1;
		
	}
	function extractFacilityNames(data){
		$scope.facilityNames=[];
		for(var i=0;i<data.length;i++){
			if(data[i].facilityId && data[i].facilityId.facilityName){
			       if($scope.facilityNames.indexOf(data[i].facilityId.facilityName)<0) { $scope.facilityNames.push(data[i].facilityId.facilityName); }
			}
		}
		$scope.facilityNames.unshift("All");
		
	}
	$scope.toggleAll = function() {
      var toggleStatus = !$scope.isAllSelected;
      angular.forEach($scope.options, function(itm){ itm.selected = toggleStatus; });
	}
  
	$scope.optionToggled = function(){
      $scope.isAllSelected = $scope.options.every(function(itm){ return itm.selected; })
  	}

	$scope.loadSubFacilities = function() {
		$http.get('/api/allSubFacilities/' + $scope.timeSlots.facilityId.facilityId).then(function (response) {
			$scope.subFacilityList = response.data;
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}

	$scope.addTimeSlot = function() {
		$scope.timeSlots = {};
		$scope.timeSlots.timeSlotsId = 0;
		$scope.modalTitle = 'Add';
		$('#myModal').modal('toggle');		
	}

	$scope.saveTimeSlot = function() {
		$scope.savingtime = true;
		var dayNums = [];
		angular.forEach($scope.options, function(itm,i){ 
			if (itm.selected)
				dayNums.push(i)
		});
		$http({
			url : '/dashboard/timeSlotsSave',
			method : "POST",
			data : {
				'timeSlot': $scope.timeSlots,
				'dayNums': dayNums
			}
		
		}).then(function(response) {
			if (response.data) {
				console.log('check.........'+response.data);
				
				if(response.data.length===0)
					{console.log('it comes inside');
						alert('The Entered Slots are Already Exists For Selected Facilty!')
					}else{
						alert("Saved Successfully");
					}
				
				angular.forEach(response.data, function(itm){ $scope.timeSlotsList.push(itm);$rootScope.addedEntry(); });
				
				$scope.timeSlots = {};
				loadTimeSlots();
				$rootScope.currentPage = 1;
				$('#myModal').modal('toggle');
			}
			$scope.savingtime = false;
		}, function(response) {
			alert("failed");
			$scope.savingtime = false;
		});
	}

	$scope.checkAll = function(event) {
		$http.get('/dashboard/getTimeSlot/' + event.id).then(function (response) {
//			$scope.timeSlots = response.data;
//			$scope.loadSubFacilities();
//			$scope.timeTable.index = index;
			$('#myModal').modal('toggle');
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
    function clearOptionsScope(){
    	$scope.options = [
    	                {value:'fake', selected:false},  
    	          	    {value:'Sunday', selected:false},
    	          		{value:'Monday', selected:false},
    	          		{value:'Tuesday', selected:false},
    	          		{value:'Wednesday', selected:false},
    	          		{value:'Thursday', selected:false},
    	          		{value:'Friday', selected:false},
    	          		{value:'Saturday', selected:false}
    	          	];
    }
	$scope.editTimeSlot= function(event, index) {
		$http.get('/dashboard/getTimeSlot/' + event.target.id).then(function (response) {
			console.log(response.data);
			$scope.timeSlots = response.data;
			//
			clearOptionsScope();
			var respNo = response.data.dayNum;
			for(var i=0;i<=$scope.options.length;i++)
			{
			   if(i ==  respNo)
				{
					$scope.options[i].selected = true;
				}
			}
			console.log("test",$scope.options);
			//
//			$scope.loadSubFacilities();
//			$scope.timeTable.index = index;
			$scope.modalTitle = 'Edit';
			$('#myModal').modal('toggle');
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
	$scope.deleteTimeSlot= function(event, index) {
		$http.get('/dashboard/deleteTimeSlot/' + event.target.id).then(function (response) {
		$scope.timeSlotsList.splice(index, 1);
		
		$rootScope.deletedEntry();
		alert("Deleted Successfully");
		loadTimeSlots();
		$rootScope.currentPage = 1;
		}, function error(response) {
		$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
		}
});

sportsApp.controller('timeSlotsRestoreController', function($scope, $rootScope, $http) {
	$rootScope.active = 'timeTable';

	$http.get('/api/allDeletedTimeSlots').then(function (response) {
		$scope.timeSlotsList = response.data;
	}, function error(response) {
		$scope.postResultMessage = "Error Status: " +  response.statusText;
	});

	$scope.deleteTimeSlot = function(rs, index) {
		$http.get('/api/restoreTimeSlot/' + rs.timeSlotsId).then(function (response) {
			$scope.timeSlotsList.splice(index, 1);
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
});

sportsApp.filter('dayFilter', function() {
	var cal_days = ['fake','Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
	//var ca=[{'Sunday':1},{'Monday':2},{'Tuesday':3},{'Wednesday':4},{'Thursday':5},{'Friday':6},{'Saturday':}];
    return function(x) {
        return cal_days[x];
    };
});


sportsApp.filter('hidefake', function() {
	return function(x) {
	       var d = x.value;
	       if(d == "fake") { delete x;}
	       else{ return x;}
	    };
    
});