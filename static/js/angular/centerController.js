sportsApp.controller('centerController', function($scope, $rootScope, $http, $filter) {
	$rootScope.active = 'center';
	$scope.page='Center';
	
	loadAllCenters();
	$http.get('/dashboard/allCenterTypes').then(function (response) {
		$scope.centerTypeList = response.data;
	}, function error(response) {
		$scope.postResultMessage = "Error Status: " +  response.statusText;
	});
	$http.get('/dashboard/allLocations').then(function (response) {
		$scope.locationList = response.data;
	}, function error(response) {
		$scope.postResultMessage = "Error Status: " +  response.statusText;
	});
	$scope.center = {};
	$scope.center.centreId = 0;
	$scope.centerEdit = {};
	function loadAllCenters(){
		$http.get('/dashboard/allCenters').then(function (response) {
			$scope.centers = response.data;
			$rootScope.initPagination($scope.centers.length)
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
	$scope.addCenter = function() {
		$scope.center = {};
		$scope.center.centreId = 0;
		$scope.modalTitle = 'Add';
		$('#myModal').modal('toggle');		
	}
	
	$scope.checkUnique = function() {
		$scope.nameError = "";
		for (var int = 0; int < $scope.centers.length; int++) {
			if ($scope.centers[int].centreName === $scope.center.centreName) {
				$scope.nameError = "Center Name Already Exists";
				break;
			}
		}
	}

	$scope.saveCenter = function() {
		$http({
			url : '/dashboard/centerSave',
			method : "POST",
			data : {
				'data' : $scope.center
			}
		}).then(function(response) {
			$scope.center.centreId = response.data;
			if ($.isNumeric($scope.center.centreId)) {
				for (var int = 0; int < $scope.centerTypeList.length; int++) {
					if ($scope.centerTypeList[int].centerTypeId === $scope.center.centerTypeId.centerTypeId) {
						$scope.center.centerTypeId.centerTypeName = $scope.centerTypeList[int].centerTypeName;
						break;
					}
				}
				for (var int = 0; int < $scope.locationList.length; int++) {
					if ($scope.locationList[int].locationId === $scope.center.locationId.locationId) {
						$scope.center.locationId.locationName = $scope.locationList[int].locationName;
						break;
					}
				}
				if ($.isNumeric($scope.center.index))
					$scope.centers[$scope.center.index] = $scope.center;
				else
					$scope.centers.push($scope.center);
				$rootScope.addedEntry();
					
				//issue fix- for wrong UI table updates
				loadAllCenters();
				$rootScope.currentPage = 1;
				$('#myModal').modal('toggle');
				alert("Saved successfully");
			}
		}, function(response) {
			alert("failed");
		});
	}

	$scope.editCenter = function(event, index) {
		$http.get('/dashboard/getCenter/' + event.target.id).then(function (response) {
			$scope.center = response.data;
			$scope.center.index = index;
			$scope.center.centerStartDate = $filter('date')($scope.center.centerStartDate, "dd-MMM-yyyy");
			$scope.center.centerEndDate = $filter('date')($scope.center.centerEndDate, "dd-MMM-yyyy");
			$scope.modalTitle = 'Edit';
			$('#myModal').modal('toggle');
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
	
	$scope.deleteCenter= function(event, index) {
		$http.get('/dashboard/deleteCenter/' + event.target.id).then(function (response) {
			$scope.centers.splice(index, 1);
			$rootScope.deletedEntry();
			alert("Deleted Successfully");
			loadAllCenters();
			$rootScope.currentPage = 1;
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
});

sportsApp.controller('centerRestoreController', function($scope, $rootScope, $http) {
	$rootScope.active = 'center';

	$http.get('/api/allDeletedCenters').then(function (response) {
		$scope.centers = response.data;
	}, function error(response) {
		$scope.postResultMessage = "Error Status: " +  response.statusText;
	});

	$scope.deleteCenter = function(rs, index) {
		$http.get('/api/restoreCenter/' + rs.centreId).then(function (response) {
			$scope.centers.splice(index, 1);
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
	
}); 