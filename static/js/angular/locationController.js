sportsApp.controller('locationController', function($scope, $rootScope, $http) {
	$rootScope.active = 'location';
	$scope.location = {};
	$scope.page='Location';
	$scope.location.locationId = 0;
	$scope.locationEdit = {};
	
	
	loadAllLocations();

	function loadAllLocations(){
		$http.get('/dashboard/allLocations').then(function (response) {
			sortLocationsData(response.data);
//			$scope.locations = response.data;
			
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
	
	function compare(a,b) {
		  if (a.locationId > b.locationId)
		    return -1;
		  if (a.locationId < b.locationId)
		    return 1;
		  return 0;
	}
	function sortLocationsData(data){
		$scope.locations=data.sort(compare);
		console.log(data);
		$rootScope.initPagination($scope.locations.length)
	}
	
	$scope.addLocation = function() {
		$scope.location = {};
		$scope.location.locationId = 0;
		$scope.modalTitle = 'Add';
		$('#myModal').modal('toggle');		
	}

	$scope.saveLocation = function() {
		$http({
			url : '/dashboard/locationSave',
			method : "POST",
			data : {
				'data' : $scope.location
			}
		}).then(function(response) {
			$scope.location.locationId = response.data;
			if ($.isNumeric($scope.location.index))
				$scope.locations[$scope.location.index] = $scope.location;
			else
				$scope.locations.push($scope.location);
			
			$rootScope.addedEntry();
			
			$scope.location = {};
			loadAllLocations();
			$rootScope.currentPage = 1;
			$('#myModal').modal('toggle');
			alert("Saved successfully");
		}, function(response) { // optional
			alert("failed");
		});

	}

	$scope.editLocation = function(event, index) {
		$http.get('/dashboard/getLocation/' + event.target.id).then(function (response) {
			$scope.location = response.data;
			$scope.location.index = index;
			$scope.modalTitle = 'Edit';
			$('#myModal').modal('toggle');
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
	
	$scope.deleteLocation = function(event, index) {
		$http.get('/dashboard/deleteLocation/' + event.target.id).then(function (response) {
			$scope.locations.splice(index, 1);
			$rootScope.deletedEntry();
			alert("Deleted Successfully");
			loadAllLocations();
			$rootScope.currentPage = 1;
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
});

sportsApp.controller('locationRestoreController', function($scope, $rootScope, $http) {
	$rootScope.active = 'location';

	$http.get('/api/allDeletedLocations').then(function (response) {
		$scope.locations = response.data;
	}, function error(response) {
		$scope.postResultMessage = "Error Status: " +  response.statusText;
	});

	$scope.deleteLocation = function(rs, index) {
		$http.get('/api/restoreLocation/' + rs.locationId).then(function (response) {
			$scope.locations.splice(index, 1);
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
	
}); 