sportsApp.controller('leaveTabController', function($scope, $rootScope, $http, $filter) {
	$rootScope.active = 'leaveTab';
	$scope.page='Leaves';
	$scope.leaveTab = {};
	$scope.leaveTab.leaveTabId = 0;
	$scope.leaveTabEdit = {};
	$scope.subFacilityList = [];
	
	
	loadAllLeaveTabs();
	loadAllCenters();
	loadAllFacility();
	loadAllLeaveTypes();
	
	function loadAllLeaveTabs() {
		$http.get('/dashboard/allLeaveTabs').then(function (response) {
			sortLeaveTabData(response.data);
//			$scope.leaveTabs = response.data;
			
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
		
	}
	function loadAllCenters() {
		$http.get('/dashboard/allCenters').then(function (response) {
			$scope.centerList = response.data;
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
	function loadAllFacility() {
		$http.get('/api/allFacilities').then(function (response) {
			$scope.facilityList = response.data;
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
	function loadAllLeaveTypes(){
		$http.get('/dashboard/allLeaveTypes').then(function (response) {
			$scope.leaveTypeList = response.data;
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
		
	}
	
	

	$scope.loadSubFacilities = function() {
		$http.get('/api/allSubFacilities/' + $scope.leaveTab.facilityId.facilityId).then(function (response) {
			$scope.subFacilityList = response.data;
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
	
	function compare(a,b) {
		  if (a.leaveTabId > b.leaveTabId)
		    return -1;
		  if (a.leaveTabId < b.leaveTabId)
		    return 1;
		  return 0;
	}
	function sortLeaveTabData(data){
		$scope.leaveTabs=data.sort(compare);
		console.log(data);
		$rootScope.initPagination($scope.leaveTabs.length)
		for (var int = 0; int < $scope.leaveTabs.length; int++) {
			$scope.leaveTabs[int].leaveDate = $filter('date')($scope.leaveTabs[int].leaveDate, "dd-MMM-yyyy");
		}
	}

	$scope.addLeaveTab = function() {
		$scope.leaveTab = {};
		$scope.leaveTab.leaveTabId = 0;
		$scope.modalTitle = 'Add';
		$('#myModal').modal('toggle');		
	}

	$scope.saveLeaveTab = function() {
		$http({
			url : '/dashboard/leaveTabSave',
			method : "POST",
			data : {
				'data' : $scope.leaveTab
			}
		
		}).then(function(response) {
			$scope.leaveTab.leaveTabId = response.data;
			if ($.isNumeric($scope.leaveTab.leaveTabId)) {
				for (var int = 0; int < $scope.centerList.length; int++) {
					if ($scope.centerList[int].leaveTabId === $scope.leaveTab.centerId.centreId) {
						$scope.leaveTab.centerId.centreName = $scope.centerList[int].centreName;
						break;
					}
				}
				for (var int = 0; int < $scope.facilityList.length; int++) {
					if ($scope.facilityList[int].facilityId === $scope.leaveTab.facilityId.facilityId) {
						$scope.leaveTab.facilityId.facilityName = $scope.facilityList[int].facilityName;
						break;
					}
				}
				
				for (var int = 0; int < $scope.subFacilityList.length; int++) {
					if ($scope.subFacilityList[int].subFacilityId === $scope.leaveTab.subFacilityId.subFacilityId) {
						$scope.leaveTab.subFacilityId.subFacilityName = $scope.subFacilityList[int].subFacilityName;
						break;
					}
				}
				for (var int = 0; int < $scope.leaveTypeList.length; int++) {
					if ($scope.leaveTypeList[int].leaveTypeId === $scope.leaveTab.leaveTypeId.leaveTypeId) {
						$scope.leaveTab.leaveTypeId.leaveTypeName = $scope.leaveTypeList[int].leaveTypeName;
						break;
					}
				}
				if ($.isNumeric($scope.leaveTab.index))
					$scope.leaveTabs[$scope.leaveTab.index] = $scope.leaveTab;
				else
					$scope.leaveTabs.push($scope.leaveTab);
				
				$rootScope.addedEntry();
				
				loadAllLeaveTabs();
				$rootScope.currentPage = 1;
				$('#myModal').modal('toggle');
				alert("Saved successfully");
			}
		}, function(response) {
			alert("failed");
		});
	}

	$scope.editLeaveTab = function(event, index) {
		$http.get('/dashboard/getLeaveTab/' + event.target.id).then(function (response) {
			$scope.leaveTab = response.data;
			$scope.loadSubFacilities();
			$scope.leaveTab.index = index;
			$scope.leaveTab.leaveDate = $filter('date')($scope.leaveTab.leaveDate, "dd-MMM-yyyy");
			$scope.modalTitle = 'Edit';
			$('#myModal').modal('toggle');
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
	
	$scope.deleteLeaveTab= function(event, index) {
		$http.get('/dashboard/deleteLeaveTab/' + event.target.id).then(function (response) {
			$scope.leaveTabs.splice(index, 1);
			$rootScope.deletedEntry();
			alert("Deleted Successfully");
			loadAllLeaveTabs();
			$rootScope.currentPage = 1;
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
});

sportsApp.controller('leaveTabRestoreController', function($scope, $rootScope, $http, $filter) {
	$rootScope.active = 'leaveTab';

	$http.get('/api/allDeletedLeaveTabs').then(function (response) {
		$scope.leaveTabs = response.data;
		for (var int = 0; int < $scope.leaveTabs.length; int++) {
			$scope.leaveTabs[int].leaveDate = $filter('date')($scope.leaveTabs[int].leaveDate, "dd-MMM-yyyy");
		}
	}, function error(response) {
		$scope.postResultMessage = "Error Status: " +  response.statusText;
	});

	$scope.deleteLeaveTab = function(rs, index) {
		$http.get('/api/restoreLeaveTab/' + rs.leaveTabId).then(function (response) {
			$scope.leaveTabs.splice(index, 1);
			$rootScope.deletedEntry();
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
	
}); 