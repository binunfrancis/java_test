sportsApp.controller('bookingController', function($scope, $rootScope, $filter, $http, Flash, $window, $location) {
	$rootScope.active = 'booking';

	$scope.gotoCenter = function() {
		$location.path('bookingCenter')
	}

	$scope.setSelectedDate = function(day, month, year) {
		$rootScope.selectedDate = $filter('date')(new Date(year, month - 1, day), "dd MMM yyyy");
	}
});

sportsApp.controller('bookingCenterController', function($scope, $rootScope, $filter, $http, Flash, $window, $location) {
	$scope.init = function() {
		$rootScope.center = {};
		$http.get('/dashboard/allCenters').then(function (response) {
			$scope.centers = response.data;
			angular.forEach($scope.centers, function(x) {
				$http.get('/api/allFacilities/' + x.centreId).then(function (response) {
					x.noFacilities = response.data.length == 0
				}, function error(response) {
					$scope.postResultMessage = "Error Status: " +  response.statusText;
				});
				$http({
					url : '/api/checkAvailabilityCenter/',
					method : "POST",
					data : {
						'center' : x,
						'selectedDate' : $rootScope.selectedDate,
					}
				}).then(function(response) {
					x.navail = response.data[0]
					x.avail = response.data[1]
					console.log(response.data)
				}, function error(response) {
					$scope.postResultMessage = "Error Status: " +  response.statusText;
				});
			});
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
	$scope.init();

	$scope.bookCenters = function(x) {
		if (x.noFacilities) {
			alert('No Facilies Available For ' + x.centreName)
			return
		}
		if (!x.booked || x.centerTypeId.centerTypeName.toUpperCase() == 'GOVT') {
			$rootScope.center = x
			$location.path('bookingFacility')
		} else alert('Not Available')
	}
});

sportsApp.controller('bookingFacilityController', function($scope, $rootScope, $filter, $http, Flash, $window, $location) {
	$scope.init = function() {
		$rootScope.facility = {};
		$http.get('/api/allFacilities/' + $rootScope.center.centreId).then(function (response) {
			$scope.facilities = response.data;
			angular.forEach($scope.facilities, function(x) {
				$http({
					url : '/api/checkAvailability/',
					method : "POST",
					data : {
						'facility' : x,
						'selectedDate' : $rootScope.selectedDate,
					}
				}).then(function(response) {
					x.navail = response.data[0]
					x.avail = response.data[1]
					x.totalAvail =response.data[2]
					x.bookedAvailFac =response.data[3]
					x.bookedAvailSubFac =response.data[4]
				}, function error(response) {
					$scope.postResultMessage = "Error Status: " +  response.statusText;
				});
			});
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
	$scope.init();

	$scope.bookingCenter = function() {
		$rootScope.select = 'bookCenter';
		$location.path('book')
	}

	$scope.bookFacilities = function(x) {
		if (x.avail === 0 && x.navail === 0) alert('Not Slots Available')
		else if (x.avail !== 0 || $rootScope.center.centerTypeId.centerTypeName.toUpperCase() == 'GOVT') {
			$rootScope.facility = x
			$location.path('bookingSubFacility')
		} else alert('Not Available')
	}
});

sportsApp.controller('bookingSubFacilityController', function($scope, $rootScope, $filter, $http, Flash, $window, $location) {
	$scope.init = function() {
		$rootScope.subFacility = {};
		$http.get('/api/allSubFacilities/' + $rootScope.facility.facilityId).then(function (response) {
			$scope.subFacilities = response.data;
			angular.forEach($scope.subFacilities, function(x) {
				$http({
					url : '/api/checkAvailability/',
					method : "POST",
					data : {
						'facility' : $rootScope.facility,
						'subFacility' : x,
						'selectedDate' : $rootScope.selectedDate,
					}
				}).then(function(response) {
					x.navail = response.data[0]
					x.avail = response.data[1]
					x.totalAvail =response.data[2]
					x.bookedAvailFac =response.data[3]
					x.bookedAvailSubFac =response.data[4]
					x.booked = x.avail == 0
				}, function error(response) {
					$scope.postResultMessage = "Error Status: " +  response.statusText;
				});
			});
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
	$scope.init();

	$scope.bookingFacilities = function() {
		$rootScope.select = 'bookFacility';
		$location.path('book')
	}

	$scope.bookSubFacilities = function(x) {
		if (!x.booked || $rootScope.center.centerTypeId.centerTypeName.toUpperCase() == 'GOVT') {
			$rootScope.select = 'book';
			$rootScope.subFacility = x;
			$location.path('book')
		} else alert('Not Available')
	}
});

sportsApp.controller('bookController', function($scope, $rootScope, $filter, $http, Flash, $window, $location) {
	$scope.otherMembers = [1,2,3,4,5,6,7,8];
	$scope.monthlyBooking =false;
	$scope.dayFile=null;
	$scope.groupmembers=[];
	$scope.states = ["Alabama", "Alaska", "Arizona", "Arkansas", "California", "Colorado", "Connecticut", "Delaware", "Florida", "Georgia", "Hawaii", "Idaho", "Illinois", "Indiana", "Iowa", "Kansas", "Kentucky", "Louisiana", "Maine"];
	//$scope.membernames = ["john", "bill", "charlie", "robert", "alban", "oscar", "marie", "celine", "brad", "drew", "rebecca", "michel", "francis", "jean", "paul", "pierre", "nicolas", "alfred", "gerard", "louis", "albert", "edouard", "benoit", "guillaume", "nicolas", "joseph"];
	$scope.membernames=[];
	//$scope.membergroup={};
	$scope.addRow = function(){
		if($scope.groupmembers.length){
			console.log("item",angular.element("#test"+($scope.groupmembers.length-1)).scope().membergroup);
		}
		
		$scope.groupmembers.push({});
		
	}
	$scope.del =function(index){
		if (window.confirm("Do you want to delete?")) {
			 //Remove the item from Array using Index.
				$scope.groupmembers.splice(index, 1);
			 }
	}
	//$scope.account={};
	
	var otherMembersList=[];
	
	$scope.addMember = function() {
		
		if($scope.masterTypeId == "" || $scope.masterTypeId==undefined){
			alert("Please choose Master Type");
		}else{
			$scope.member = {};
			$scope.member.memberId = 0;
			$('#bootstrap-wizard-1').find('.form-wizard').children('li').each(function(index, value){
				$( '#wiz-prev' ).click();
			});
			$('#bootstrap-wizard-1').find('.form-wizard').children('li').each(function(index, value){
				$( this ).find('.step').html(index + 1);
				$( this ).removeClass('complete');
				if (index == 0)
					$( this ).addClass('active');
				else
					$( this ).removeClass('active');
			});
			$('#myModal').modal('toggle');
		}
		
	}
$scope.setOtherMember =function(){
	var temp=[];
	for(var i=0;i<$scope.groupmembers.length;i++){
	  var mem = angular.element("#test"+i).scope().membergroup;
	  if(mem !=undefined) { temp.push(mem);  }
	}
	if(hasDuplicates(temp)){
		alert("Same member selected more than once!");
	}else if(userExists(temp,$scope.memberId)){
		alert("Main member selected as Group member!");
	}else{
		otherMembersList = temp;
		$('#myModalExtra').modal('toggle');
	}
	
}
function userExists(arr,username) {
	  return arr.some(function(el) {
	    return el.memberId == username;
	  }); 
	}
function hasDuplicates(array) {
    var valuesSoFar = Object.create(null);
    for (var i = 0; i < array.length; ++i) {
        var value = array[i].memberId;
        if (value in valuesSoFar) {
            return true;
        }
        valuesSoFar[value] = true;
    }
    return false;
}

//show stored bookings
$scope.showmultiplebookings = function(){
	$("#myModalBookings").modal("toggle");
}
//multiple booking
$scope.bookmore = function(){
	if($rootScope.storedBooking && $rootScope.storedBooking.length>1){
		alert("Maximum limit reached.Please proceed with Book Now button");
	}else{
		var t=$.map($scope.timeTablesToBook, function(val, key) {return val});
	    var t1=[];
	    for(var i=0;i<t.length;i++){
	    	if(t[i].book == true){
	    		t1.push(t[i]);
	    	}
	    }

	    var data = {
				'center' : $rootScope.center,
				'facility' : $rootScope.facility,
				'subFacility' : $rootScope.subFacility,
				'selectedDate' : $rootScope.selectedDate,
				'memberId' : $scope.memberId,
				'timeTables' : t1,
				'otherMembersList':otherMembersList,
				'account' : $scope.account,
				'bookingType' : $scope.bookingType,
				'dayFile': $scope.dayFile
			};
	    //multiple booking-get the earlier stored data
	    if($rootScope.storedBooking == undefined || $rootScope.storedBooking == null){
	    	$rootScope.storedBooking=[];
	    }
	    $rootScope.storedBooking.push(data);
		location.hash="#/booking";
	}
	
}
$scope.addExtraMember = function(){
	
	$('#myModalExtra').modal('toggle');
}

	$scope.saveMember = function() {
	   var file = $scope.uploadedFile;
       if (file) {
    	   var url = "/api/uploadfile";
           var data = new FormData();
           data.append('uploadfile', file);
           var config = {
    	   	transformRequest: angular.identity,
    	   	transformResponse: angular.identity,
       		headers : {
       			'Content-Type': undefined
       	    }
           }
           $http.post(url, data, config).then(function (response) {
        	   $scope.member.memberPhoto = response.data;
        	   $scope.saveMem();
    		}, function (response) {
    			alert("Failed to upload the photo");
    		});
       } else {
    	   $scope.saveMem();
       }
	}

	$scope.saveMem = function() {
		$http({
			url : '/api/memberSpotSave',
			method : "POST",
			data : {
				'data' : $scope.member
			}
		}).then(function(response) {
			if (response.data) {
				$scope.members.push(response.data);
				$scope.memberId = response.data.memberId;
				$scope.account.discount = 0;
				
				$scope.account.payableAmount = $scope.account.actualAmount;
				$scope.account.paidAmount = $scope.account.payableAmount;
				$scope.account.balanceAmount = 0.0;
				$('#myModal').modal('toggle');
			}
		}, function(response) {
			alert("failed");
		});
	}

	$scope.incrementDate = function(x) {
		$scope.available = false;
		$scope.bookingType = 'Selected Session';
		$scope.account.actualAmount = 0;
		$scope.account.payableAmount = 0;
		$scope.account.paidAmount = 0;
		$scope.account.cautionDeposit=0;
		$scope.account.extraAmount=0;
		$scope.account.balanceAmount =0;
		$scope.memberId = "";
		$scope.account.discount = 0;
		$scope.trainerCost=0;
		$scope.matPriceId="";
				
		formatAmountFields();
		if ($.isNumeric(x)) {
			$scope.mydate = new Date($rootScope.selectedDate);
			do {
				$scope.mydate.setDate($scope.mydate.getDate() + x);
				$rootScope.selectedDate = $filter('date')($scope.mydate, "dd MMM yyyy");
			}
			while ($scope.holidays[$rootScope.selectedDate]);
		} else {
			$rootScope.selectedDate = x
		}
		$scope.selectedDt = $rootScope.selectedDate

		if ($rootScope.select === 'bookCenter')
			$scope.bookingTypeCenterChange();
		else {
			$scope.available = false;
			$scope.allAvailableTimeTables();
			$scope.timeTable = {};
		}
	}

	$scope.allAvailableTimeTables = function() {
		$http({
			url : '/api/allAvailableTimeTables/',
			method : "POST",
			data : {
				'facility' : $rootScope.facility,
				'selectedDate' : $rootScope.selectedDate
			}
		}).then(function(response) {
			$scope.timeTables = response.data;
			if ($scope.select === 'book') {
				$http({
					url : '/api/bookings/',
					method : "POST",
					data : {
						'subFacility' : $rootScope.subFacility,
						'selectedDate' : $rootScope.selectedDate
					}
				}).then(function(response) {
					$scope.bookings = response.data;
					$scope.totalcountforday = 0;
					for (var int = 0; int < $scope.timeTables.length; int++) {
						$scope.timeTables[int].totalBookedNum=0;
						for (var j = 0; j < $scope.bookings.length; j++) {
							if ($scope.timeTables[int].timeTableId === $scope.bookings[j].timeTableId.timeTableId) {
								if ($rootScope.center.centerTypeId.centerTypeName.toUpperCase() !== 'GOVT'){
									$scope.timeTables[int].booked = true;
									$scope.timeTables[int].totalBookedNum++;
									$scope.totalcountforday++;
								}									
								else{
									$scope.timeTables[int].govtBooked = true;
									$scope.timeTables[int].totalBookedNum++;
									$scope.totalcountforday++;
								}
								
							}
						}
					}
					$scope.slotselection = true;
	
				}, function error(response) {
					$scope.postResultMessage = "Error Status: " +  response.statusText;
				});
			} else if ($scope.select === 'bookFacility') {
				angular.forEach($scope.timeTables, function(x) {
					$http({
						url : '/api/checkAvailability/',
						method : "POST",
						data : {
							'facility' : $rootScope.facility,
							'selectedDate' : $rootScope.selectedDate,
							'timeTable' : x,
							'bookingType' : "Selected Session"
						}
					}).then(function(response) {
						if (response.data == 'Not Available') {
							if ($rootScope.center.centerTypeId.centerTypeName.toUpperCase() !== 'GOVT')
								x.booked = true
							else x.govtBooked = true
						}
					}, function error(response) {
						$scope.postResultMessage = "Error Status: " +  response.statusText;
					});
				});
			}
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
	$scope.loadMasterTypes = function() {
		if ($rootScope.select === 'book') {
			$http.get('/api/priceBySubFacility/' + $rootScope.subFacility.subFacilityId).then(function (response) {
				$scope.prices = response.data;
				$scope.setMasterTypes();
			}, function error(response) {
				$scope.postResultMessage = "Error Status: " +  response.statusText;
			});
		} else if ($rootScope.select === 'bookFacility') {
			$http.get('/api/priceByFacility/' + $rootScope.facility.facilityId).then(function (response) {
				$scope.prices = response.data;
				$scope.setMasterTypes();
			}, function error(response) {
				$scope.postResultMessage = "Error Status: " +  response.statusText;
			});
		} else if ($rootScope.select === 'bookCenter') {
			$http.get('/api/priceByCenter/' + $rootScope.center.centreId).then(function (response) {
				$scope.prices = response.data;
				$scope.setMasterTypes();
			}, function error(response) {
				$scope.postResultMessage = "Error Status: " +  response.statusText;
			});
		}
	}

	$scope.setMasterTypes = function() {
		$scope.masterTypes = []
		$scope.priceMap = {}
		angular.forEach($scope.prices, function(x){
			$scope.masterTypes.push(x.masterTypeId)
			$scope.priceMap[x.masterTypeId.masterTypeId] = x;
		})
	}

	$scope.masterTypeChange = function() {
		//When master type changes, reset all fields
		$scope.available = false;
		$scope.bookingType = 'Selected Session';
		$scope.account.actualAmount = 0;
		$scope.account.payableAmount = 0;
		$scope.account.paidAmount = 0;
		$scope.account.cautionDeposit=0;
		$scope.account.extraAmount=0;
		$scope.account.balanceAmount =0;
		$scope.memberId = "";
		$scope.account.discount = 0;
		$scope.trainerCost=0;
		$scope.matPriceId="";
		angular.forEach($scope.timeTables, function(x) {
			$scope.timeTablesToBook[x.timeTableId] = x;
			x.book = false;
		});		
		formatAmountFields();
		loadTrainers();
		//$scope.available = false;
		$scope.message = 'Please Select A Time Slot /Book Type';
		$scope.price = $scope.priceMap[$scope.masterTypeId];
		/*
		$scope.price = $scope.priceMap[$scope.masterTypeId];
		$scope.account.actualAmount = $scope.price[$scope.bookingTypesAttr[$scope.bookingType]];
		if ($scope.bookingType === 'Selected Session') {
			$scope.account.actualAmount *=  Object.keys($scope.timeTablesToBook).length;
			
		}
		$scope.setDiscount();
		*/
	}
	$scope.trainerChange = function(){
		//dashboard/getTrainingCharge/{id}
		$http.get('/dashboard/getTrainingCharge/'+$scope.trainerId).then(function (response) {
			if(response.data && response.data.trainerCharge){
				$scope.trainerCost = response.data.trainerCharge;
				
			}else{
				$scope.trainerCost =0;
			}
			$scope.setDiscount(true);
						
		}, function error(response) {
			$scope.trainerCost =0;
			$scope.setDiscount(true);
		});
		
	}
	function loadTrainers(){
		var temp=[];
		$http.get('/dashboard/allTrainingCharges').then(function (response) {
			if(response.data && response.data.length){
				//filter for the master type masterTypeId
				for(var i=0;i<response.data.length;i++){
					if($scope.masterTypeId == response.data[i].masterTypeId.masterTypeId){
						temp.push(response.data[i]);
					}
				}
				$scope.trainers = temp;
			}else{
				$scope.trainers=[];
			}
						
		}, function error(response) {
			$scope.trainers = [];
		});
	}

	$scope.bookingTypeChange = function() {
		if ($rootScope.select === 'book' || $rootScope.select === 'bookFacility')
			$scope.bookingChange();
		else if ($rootScope.select === 'bookCenter')
			$scope.bookingTypeCenterChange();
	}

	$scope.bookingTypeCenterChange = function() {
		if ($scope.price)
			$scope.account.actualAmount = $scope.price[$scope.bookingTypesAttr[$scope.bookingType]];
		$scope.available = $rootScope.center.centerTypeId.centerTypeName.toUpperCase() === 'GOVT';
		$scope.message = '';
		if (!$scope.available) {
			$http({
				url : '/api/checkAvailabilityCenter/',
				method : "POST",
				data : {
					'center' : $rootScope.center,
					'selectedDate' : $rootScope.selectedDate,
					'bookingType' : $scope.bookingType
				}
			}).then(function(response) {
				$scope.message = response.data;
				if ($scope.message != 'Not Available')
					$scope.available = true;
			}, function error(response) {
				$scope.postResultMessage = "Error Status: " +  response.statusText;
			});
		}
		if ($scope.memberId)
			$scope.setDiscount();
	}

	$scope.bookingChange = function() {
		
		if ($scope.price) {
			$scope.account.actualAmount = $scope.price[$scope.bookingTypesAttr[$scope.bookingType]]
			if ($scope.bookingType === 'Selected Session') {
				$scope.account.actualAmount *=  Object.keys($scope.timeTablesToBook).length
			}
		}
		$scope.available = true;
		$scope.message = '';
		$scope.monthlyBooking = false;
		if ($scope.bookingType === 'Selected Session') {
			angular.forEach($scope.timeTables, function(x) {
				$scope.timeTablesToBook[x.timeTableId] = x;
				x.book = false;
			});
			
			$scope.available = false;
			$scope.message = 'Please Select A Time Slot /Book Type';
			$scope.account.actualAmount =0;
		}else if($scope.bookingType === 'Session For A Month'){
			angular.forEach($scope.timeTables, function(x) {
				$scope.timeTablesToBook[x.timeTableId] = x;
				x.book = false;
			});
			$scope.monthlyBooking = true;
			$scope.available = false;
			$scope.message = 'Please Select A Time Slot /Book Type';
			$scope.account.actualAmount =0;
		}
		else if($scope.bookingType  == "Session For A Week"){
			angular.forEach($scope.timeTables, function(x) {
				$scope.timeTablesToBook[x.timeTableId] = x;
				x.book = false;
			});
			$scope.available = false;
			$scope.message = 'Please Select A Time Slot /Book Type';
			$scope.account.actualAmount =0;
		}
		else if ($scope.bookingType === 'Whole Day') {
			$scope.available = true;
			if ($rootScope.center.centerTypeId.centerTypeName.toUpperCase() != 'GOVT') {
				for (var int = 0; int < $scope.timeTables.length; int++) {
					if ($scope.timeTables[int].booked) {
						$scope.available = false;
						$scope.message = 'Not Available';
						break;
					}
				}
			}
			if ($scope.available)
				angular.forEach($scope.timeTables, function(x) {
					$scope.timeTablesToBook[x.timeTableId] = x
					x.book = true
				})
		} else {
			$scope.available = false;
			if (Object.keys($scope.timeTablesToBook).length === 0) {
				$scope.message = 'Please Select A Time Slot /Book Type';
			} else if ($rootScope.center.centerTypeId.centerTypeName.toUpperCase() != 'GOVT') {
				$http({
					url : '/api/checkAvailability/',
					method : "POST",
					data : {
						'facility' : $rootScope.facility,
						'subFacility' : $rootScope.subFacility.subFacilityId ? $rootScope.subFacility : null,
						'selectedDate' : $rootScope.selectedDate,
						'timeTable' : $scope.timeTable,
						'bookingType' : $scope.bookingType
					}
				}).then(function(response) {
					$scope.message = response.data;
					if ($scope.message != 'Not Available')
						$scope.available = true;
				}, function error(response) {
					$scope.postResultMessage = "Error Status: " +  response.statusText;
				});				
			} else
				$scope.available = true;
		}
		$scope.setDiscount();
	}
	$scope.$watch("memberselected",function(){
		$scope.memberId = $scope.hashMap[$scope.memberselected];
	});
	//Function fired when clicked on a time slot
	$scope.book = function(x) {
		if($scope.memberId){
			//call service
			$http({
				url : '/api/checkMemAllReadyBookedSlot',
				method : "POST",
				data : {
					'facility' : $rootScope.facility,
					'subFacility' : $rootScope.subFacility.subFacilityId ? $rootScope.subFacility : null,
					'selectedDate' : $rootScope.selectedDate,
					'timeTable' : x,
					'member' : $scope.memberId.toString()
				}
			}).then(function(response) {
				if(response.data == ""){
					if ($scope.bookingType !== 'Whole Day') {
						x.book = !x.book
						if (x.book)
							$scope.timeTablesToBook[x.timeTableId] = x;
						else
							delete $scope.timeTablesToBook[x.timeTableId]
						if ($scope.price){
							var d=$scope.timeTablesToBook,count=0;
							Object.keys(d).forEach(function(i) { if(d[i].book == true) { count++ }});
							$scope.account.actualAmount = $scope.price[$scope.bookingTypesAttr[$scope.bookingType]] *  count;
							$scope.setDiscount();

						}
						var flag=false;
						Object.keys($scope.timeTablesToBook).forEach(function(i) { if($scope.timeTablesToBook[i].book == true) { flag=true; }});
						$scope.available = flag;
						$scope.message = '';
						if($scope.bookingType == "Session For A Month"){
							$scope.chargeByMonthUpdation();
						}
					}
				}else{
					alert(response.data);
				}
				
			}, function error(response) {
				alert("Failed to check slot availability");
			});

		}else{
			alert("Please select Member to proceed");
		}
				
	}
	//Function fired when member change
	//This function being called from other functions too to set all amount fields.
	$scope.setDiscount = function(flag) {
		var x = {};
		if(flag == undefined){
			
			for (var j = 0; j < $scope.members.length; j++) {
				if ($scope.members[j].memberId === $scope.memberId) {
					x = $scope.members[j];
					
				}
			}
			if(x.memberShipTypeId){
				$scope.account.discount = x.memberShipTypeId.memberShipDiscount;
			}
		}
		
		//Calculate payable amount
		$scope.account.payableAmount = $scope.account.actualAmount * (100 - $scope.account.discount) / 100;
		//Add Extra amount (if any)
		if(Number($scope.account.extraAmount)){
			$scope.account.payableAmount = Number($scope.account.payableAmount) + Number($scope.account.extraAmount);
		}
		//Add caution deposit (if any)
		if(Number($scope.account.cautionDeposit)){
			$scope.account.payableAmount = Number($scope.account.payableAmount) +Number($scope.account.cautionDeposit);

		}
		//Add trainer charge
		if(Number($scope.trainerCost)){
			$scope.account.payableAmount = Number($scope.account.payableAmount) +Number($scope.trainerCost);
		}
		$scope.account.paidAmount = $scope.account.payableAmount;
		$scope.account.balanceAmount =0;
		
		//format values
		formatAmountFields();
		
		//why set these below items-no clue-print purpose
		if(x.memberName){
			$scope.data[0] = x.memberName;	
		}
		$scope.data[1] = $filter('date')(new Date(), "dd MMM yyyy");
	}
	function formatAmountFields(){
		$scope.account.payableAmount = (Number($scope.account.payableAmount)).toFixed(2);
		$scope.account.actualAmount = (Number($scope.account.actualAmount)).toFixed(2);
		$scope.account.paidAmount  = (Number($scope.account.paidAmount)).toFixed(2);
		$scope.account.cautionDeposit=(Number($scope.account.cautionDeposit)).toFixed(2);
		$scope.account.extraAmount=(Number($scope.account.extraAmount)).toFixed(2);
		$scope.account.balanceAmount = (Number($scope.account.balanceAmount)).toFixed(2);
		$scope.trainerCost = Number(($scope.trainerCost)).toFixed(2);
	}
	
	//Watch block for caution deposit
	//$scope.$watch("account.cautionDeposit",function(){
		/*
		$scope.account.payableAmount = Number($scope.account.actualAmount) * (100 - Number($scope.account.discount)) / 100;
		if(Number($scope.account.extraAmount)){
			$scope.account.payableAmount = Number($scope.account.payableAmount) + Number($scope.account.extraAmount);
		}
		$scope.account.payableAmount = Number($scope.account.payableAmount) + Number($scope.account.cautionDeposit);
		$scope.account.paidAmount = $scope.account.payableAmount;
		$scope.account.balanceAmount =0;*/
		//$scope.setDiscount(true);
	//});
	$scope.$watch("account.discount",function(){
		if(Number($scope.account.discount)>100){
			alert("Maximum available discount is 100%");
			$scope.account.discount=100;
		}
		$scope.setDiscount(true);
	});
	$scope.updateAmounts = function(){
		$scope.setDiscount(true);
	}
	//Function fired when the additional facility is opted/changed
	$scope.additionalFacilityChange = function() {
		
		var y = {};
		for (var k = 0; k < $scope.matPrices.length; k++) {
			if ($scope.matPrices[k].matPriceId == $scope.matPriceId) {
				var y = $scope.matPrices[k];				
			}
			
		}
		$scope.account.extraAmount = (y.matPriceCost) ? y.matPriceCost : 0; 
		$scope.setDiscount(true);
		
	}
	//Function fired when paid amount is entered manually
	$scope.calcBalance = function() {
		if (Number($scope.account.paidAmount) > Number($scope.account.payableAmount)) {
			$scope.account.paidAmount = 0;
			$scope.account.balanceAmount = $scope.account.payableAmount;
			alert('Paid amount cannot be greater than payable amount');
			return false;
		}
		$scope.account.balanceAmount = Number($scope.account.payableAmount) - Number($scope.account.paidAmount);
	}
	function getActualAmount(){
		var d=$scope.timeTablesToBook,count=0;
		Object.keys(d).forEach(function(i) { if(d[i].book == true) { count++ }});
		return ($scope.price[$scope.bookingTypesAttr[$scope.bookingType]] *  count);
	}
	//function when fired number of months selected
	$scope.chargeByMonthUpdation = function(){
		$scope.account.actualAmount = getActualAmount();
		if(Number($scope.dayFile)){
			$scope.account.actualAmount = $scope.account.actualAmount * $scope.dayFile;
		}
		
		$scope.setDiscount(true);
	}
	//$scope.states= ["Alabama", "Alaska", "Arizona", "Arkansas", "California", "Colorado", "Connecticut", "Delaware", "Florida", "Georgia", "Hawaii", "Idaho", "Illinois", "Indiana", "Iowa", "Kansas", "Kentucky", "Louisiana", "Maine", "Maryland", "Massachusetts", "Michigan", "Minnesota", "Mississippi", "Missouri", "Montana", "Nebraska", "Nevada", "New Hampshire", "New Jersey", "New Mexico", "New York", "North Dakota", "North Carolina", "Ohio", "Oklahoma", "Oregon", "Pennsylvania", "Rhode Island", "South Carolina", "South Dakota", "Tennessee", "Texas", "Utah", "Vermont", "Virginia", "Washington", "West Virginia", "Wisconsin", "Wyoming"];
	$scope.saveBooking = function() {
		if($scope.bookingType=="Session For A Month"){
			if($scope.dayFile == undefined || $scope.dayFile == null || $scope.dayFile == ""){
				alert("Please select number of months for booking!");
				return;
			}
		}
		//check member validity for weekly / monthly
		if($scope.bookingType =="Session For A Week" || $scope.bookingType=="Session For A Month"){
			var data={
					"member":$scope.memberId.toString(),
					"dayList":$scope.dayFile,
					"bookingType":$scope.bookingType,
					"selectedDate":$rootScope.selectedDate
			};
			$http({
				url : '/api/checkMemberValidityOver',
				method : "POST",
				data : {
					'data' : data
				}
			}).then(function(response) {
				if(response.data && response.data.length){
					alert(response.data);
				}else{
					doBooking();
				}
				
			
			}, function(response) {
				alert("Failed to check the member validity!");
			});

		}else{
			doBooking();
		}
		

	}
	//
	function doBooking(){
		var dataToServer=[];
	    $('#formSave').attr('disabled','disabled');
	    var t=$.map($scope.timeTablesToBook, function(val, key) {return val});
	    var t1=[];
	    for(var i=0;i<t.length;i++){
	    	if(t[i].book == true){
	    		t1.push(t[i]);
	    	}
	    }
//	    alert('account data is...............'+$scope.account);
//	    console.log('fun...........'+$scope.account);
	    var data = {
				'center' : $rootScope.center,
				'facility' : $rootScope.facility,
				'subFacility' : $rootScope.subFacility,
				'selectedDate' : $rootScope.selectedDate,
				'memberId' : $scope.memberId,
				'timeTables' : t1,
				'otherMembersList':otherMembersList,
				'account' : $scope.account,
				'bookingType' : $scope.bookingType,
				'dayFile': $scope.dayFile
			};
	    //multiple booking-get the earlier stored data
	    if($rootScope.storedBooking && $rootScope.storedBooking instanceof Array){
	    	for(var j=0;j<$rootScope.storedBooking.length;j++){
	    		dataToServer.push($rootScope.storedBooking[j]);
	    	}
		    	
		}
	    dataToServer.push(data);
	    
	    //Kiran, to enable this change, change the data to dataToServer in below $http block.
	    //Note: object is changed to array as we need to hold multiple objects.
	    
		$http({
			url : '/api/saveBooking/',
			method : "POST",
			data : dataToServer
//			data : data
		}).then(function(response) {
			//clear group member models
			
			$scope.slotselection = false;
			$rootScope.storedBooking=[];
//			alert(123);
			var daa=$.map($scope.timeTablesToBook, function(val, key) {return val});
//			alert(daa);
			var temp=[];
			if($scope.timeTables && $scope.timeTables.length){
				for(var i=0;i<$scope.timeTables.length;i++){
					   if($scope.timeTables[i].book == true) {
					         temp.push($scope.timeTables[i]);
					}}	
			}
			
			
			$scope.recId=response.data[0].accountsId.toString();
			$scope.bookingres = response.data;
			//$scope.bookingres.push(response.data);
			$scope.select = 'print';
			$scope.printForm = true;
			if ($scope.timeTable)
				$scope.timeTable.booked = true;
//			alert( $scope.timeTables);
			if ($scope.bookingType === 'Whole Day') {
				for (var int = 0; int < $scope.timeTables.length; int++) {
					$scope.timeTables[int].booked = true;
				}
			}
			$scope.account.referenceNo = '';
			$scope.available = false;
			$scope.message = 'Please Select A Time Slot /Book Type';
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
		//

	}
    $scope.printBookingReceipt = function(id){
    	var id= id.toString();
    	var str=location.origin+"/api/printBookingReceipt/"+id;
    	window.open(str,"_blank");
    }
	$scope.success = function () {
		var cal_days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
	    var message = '<strong>Booked!</strong> ';
	    if ($scope.unavailable.length > 0) {
	    	for (var int = 0; int < $scope.unavailable.length; int++) {
				message += cal_days[$scope.unavailable[int] - 1] + (int === $scope.unavailable.length -1 ? " " : ", ");
			}
	    	message += ' does not have selected session';
	    }
	    $scope.select = 'print';
	};
	var createMemberHashMap=function(){
		var temp=[];
		$scope.hashMap = {};
		for(var i=0;i<$scope.members.length;i++){
			var displayName = $scope.members[i].memberName+"-"+$scope.members[i].memberContactNo+"-"+$scope.members[i].street;
			//var displayName = $scope.members[i].memberName;
			$scope.hashMap[displayName] = $scope.members[i].memberId;
			temp.push(displayName);
		}
		$scope.membernames = temp;
	}
	$scope.init = function() {
		$scope.selectedDt = $rootScope.selectedDate
		$scope.data = [];
		$rootScope.today = $filter('date')(new Date(), "dd MMM yyyy");
		$scope.message = 'Please Select A Time Slot /Book Type';
		//$scope.bookingTypes = ["Selected Session", "Whole Day", "Session For A Week", "Session For A Month"];
		$scope.bookingTypes =[{"name":"Select Session","value":"Selected Session"},
		                      {"name":"Whole Day","value":"Whole Day"},
		                      {"name":"Session For A Week","value":"Session For A Week"},
		                      {"name":"Session For Month","value":"Session For A Month"}
		];
		$scope.bookingTypesAttr = {
			"Selected Session" : "ratePerHour",
			"Whole Day" : "ratePerDay",
			"Session For A Week" : "ratePerWeek",
			"Session For A Month" : "ratePerMonth",
			"For 12 Hours" : "ratePerDay",
			"For 24 Hours" : "ratePerMonth"
		};
		$scope.bookingType = ($rootScope.select === 'bookCenter') ? 'For 12 Hours' : 'Selected Session';

		$http.get('/api/allMembers').then(function (response) {
			$scope.members = response.data;
			var arr = [];
			createMemberHashMap();
			//$scope.memberNames  = arr;
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
		
		$http.get('/dashboard/allMatPrices').then(function (response) {
			$scope.matPrices = response.data;
			$rootScope.initPagination($scope.matPrices.length)
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
		$http.get('/dashboard/allMemberShipTypes').then(function (response) {
			$scope.memberShipTypes = response.data;
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
		$http.get('/api/allRoles').then(function (response) {
			$scope.roles = response.data;
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});

		$scope.account = {};
		$scope.account.referenceNo ="Nill";
		$scope.account.paymentType = "By Cash";
		$scope.loadMasterTypes();
		$scope.account.actualAmount =0 ;
		$scope.account.extraAmount = 0;
		$scope.account.paidAmount =0;
		$scope.account.payableAmount = 0;
		$scope.account.balanceAmount = 0;
		$scope.account.cautionDeposit=0;
		$scope.account.discount=0;
		$scope.trainerCost=0;
		$scope.holidays = {}
		$scope.leave = false;
		$scope.timeTablesToBook = {}

		$http.get('/api/allHolidays').then(function (response) {
			var holidays = response.data;
			for (var int = 0; int < holidays.length; int++)
				$scope.holidays[$filter('date')(holidays[int].date, "dd MMM yyyy")] = holidays[int].description;
			
			if ($rootScope.subFacility && $rootScope.subFacility.subFacilityId) {
				$http.get('/api/leavesBySubFacility/' + $rootScope.subFacility.subFacilityId).then(function (response) {
					var holidays = response.data;
					for (var int = 0; int < holidays.length; int++)
						$scope.holidays[$filter('date')(holidays[int].leaveDate, "dd MMM yyyy")] = holidays[int].leaveTypeId.leaveTypeName;
					$scope.leave = $scope.holidays[$rootScope.selectedDate]
					if (!$scope.leave)
						$scope.allAvailableTimeTables();
					
					
				}, function error(response) {
					$scope.postResultMessage = "Error Status: " +  response.statusText;
				});
			} else if ($rootScope.facility && $rootScope.facility.facilityId) $scope.allAvailableTimeTables();
			
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
		if ($rootScope.select === 'bookCenter')
			$scope.bookingTypeCenterChange();

	}
	$scope.init();
});

sportsApp.filter('timeFormat', function() {
    return function(x) {
    	var hours = parseInt(x / 60);
    	if (hours < 10)
    		hours = '0' + hours;
    	var mins = x % 60;
    	if (mins < 10)
    		mins = '0' + mins;
        return hours + ':' + mins;
    };
});

sportsApp.filter('facilityfilter', function() {
    return function(inputArray,filterCriteria) {
    	if(inputArray){
    		return inputArray.filter(function(item){
    	  	      if(item && item.facilityTypeId && item.facilityTypeId.facilityTypeId)
    	  	    	  return  angular.equals(item.facilityTypeId.facilityTypeId,filterCriteria);
    	  	    });
    	}
    	
    };
});
sportsApp.directive('clientAutoCompleteDir', function($filter) {
    return {
        restrict: 'A',       
        link: function (scope, elem, attrs) {
            elem.autocomplete({
                source: function (request, response) {

                    //term has the data typed by the user
                    var params = request.term;
                    
                    //simulates api call with odata $filter
                    var data = [{name:'Oscar',id:1000},{name:'Olgina',id:2000},{name:'Oliver',id:3000},{name:'Orlando',id:4000},{name:'Osark',id:5000}, {name:'Osos',id:5000}, {name:'Oscarlos',id:5000}];
                                                         
                    if (data) { 
                        var result = $filter('filter')(data, {name:params});
                        angular.forEach(result, function (item) {
                            item['value'] = item['name'];
                        });                       
                    }
                    response(result);

                },
                minLength: 1,                       
                select: function (event, ui) {
                   //force a digest cycle to update the views
                   scope.$apply(function(){
                   	scope.setClientData(ui.item);
                   });                       
                },
               
            });
        }

    };
});
sportsApp.directive('autoComplete', function($timeout) {
    return function(scope, iElement, iAttrs) {
    	scope.$watch('membernames',function(){
    		iElement.autocomplete({
                source: scope[iAttrs.uiItems],
                select: function() {
                    $timeout(function() {
                      iElement.trigger('input');
                    }, 0);
                }
            });
    	})
            
    };
})
/*
.filter('excludeFrom',function(){
	  return function(inputArray,filterCriteria){
	    return inputArray.filter(function(item){
	      // if the value of filterCriteria is "falsy", retain the inputArray as it is
	      // then check if the currently checked item in the inputArray is different from the filterCriteria,
	      // if so, keep it in the filtered results
	      return !filterCriteria || !angular.equals(item,filterCriteria);
	    });
	  };
	}])*/