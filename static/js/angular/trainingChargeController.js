
 
 sportsApp.controller('trainingChargeController', function($scope, $rootScope, $http) {
	$rootScope.active = 'trainingCharge';
	$scope.page = 'Mat Price';
	$scope.trainingCharge = {};
	$scope.trainingCharge.trainerChargeId = 0;
	$scope.trainingCharge = {};
	
	loadTrainingCharges();
	loadAllFacility();
	loadAllMasterTypes();
	loadAllTrainers();
	function loadTrainingCharges(){
		$http.get('/dashboard/allTrainingCharges').then(function (response) {
			console.log(response.data);
			sortTrainingChargeData(response.data);
//			$scope.matPrices = response.data;
			$rootScope.initPagination($scope.trainingCharges.length)
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
	
	function loadAllFacility() {
		$http.get('/api/allFacilities').then(function (response) {
			$scope.facilityList = response.data;
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
	function loadAllMasterTypes() {
		$http.get('/dashboard/allMasterTypes').then(function (response) {
			$scope.masterTypeList = response.data;
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
	
	function loadAllTrainers() {
		$http.get('/dashboard/allTrainers').then(function (response) {
			$scope.trainerList = response.data;
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
	
	
	
	function compare(a,b) {
		  if (a.trainerChargeId > b.trainerChargeId)
		    return -1;
		  if (a.trainerChargeId < b.trainerChargeId)
		    return 1;
		  return 0;
	}
	function sortTrainingChargeData(data){
		$scope.trainingCharges=data.sort(compare);
		console.log(data);
	}

	$scope.addTrainingCharge = function() {
		$scope.trainingCharge = {};
		$scope.trainingCharge.trainerChargeId = 0;
		$scope.modalTitle = 'Add';
		$('#myModal').modal('toggle');		
	}

	$scope.saveTrainingCharge = function() {
//		alert(123);
		$http({
			url : '/dashboard/saveTrainingCharge',
			method : "POST",
			data : {
				'data' : $scope.trainingCharge
			}
		}).then(function(response) {
			if ($.isNumeric($scope.trainingCharge.index))
				$scope.trainingCharges[$scope.trainingCharge.index] = $scope.trainingCharge;
			else
			$scope.trainingCharges.push($scope.trainingCharge);
			loadTrainingCharges();
			$rootScope.addedEntry();
			$scope.matPrice = {};
			$('#myModal').modal('toggle');
		}, function(response) { // optional
			alert("failed");
		});

	}

	$scope.editTrainingCharge = function(event, index) {
		$http.get('/dashboard/getTrainingCharge/' + event.target.id).then(function (response) {
			$scope.trainingCharge = response.data;
			$scope.trainingCharge.index = index;
			$scope.modalTitle = 'Edit';
			$('#myModal').modal('toggle');
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
	
	$scope.deleteTrainingCharge= function(event, index) {
		$http.get('/dashboard/deleteTrainingCharge/' + event.target.id).then(function (response) {
			$scope.trainingCharges.splice(index, 1);
			$rootScope.deletedEntry();
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
});

// create the controller and inject Angular's $scope
sportsApp.controller('trainerChargeRestoreController', function($scope, $rootScope, $http) {
	$rootScope.active = 'trainingCharge';

	$http.get('/api/allDeletedTrainingCharges').then(function (response) {
		$scope.trainingCharges = response.data;
	}, function error(response) {
		$scope.postResultMessage = "Error Status: " +  response.statusText;
	});

	$scope.deleteTrainingCharge = function(rs, index) {
		$http.get('/api/restoreTrainingCharge/' + rs.trainerId).then(function (response) {
			$scope.trainingCharges.splice(index, 1);
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
	
}); 