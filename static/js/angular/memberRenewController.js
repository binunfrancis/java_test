// create the controller and inject Angular's $scope
sportsApp.controller('memberRenewController', function($scope, $rootScope, $filter, $http) {
	$rootScope.active = 'memberRenewal';
	$scope.page='Member';
	$scope.member = {};
	$scope.member.memberId = 0;
	$scope.memberEdit = {};
	$scope.today = new Date();
	
	loadRoles();
	loadMembers();
	loadMembershiptypes();
	loadfacilitytypes();
	
	function getAge	(dateOfBirth, dateToCalculate) {
	    var calculateYear = dateToCalculate.getFullYear();
	    var calculateMonth = dateToCalculate.getMonth();
	    var calculateDay = dateToCalculate.getDate();

	    var birthYear = dateOfBirth.getFullYear();
	    var birthMonth = dateOfBirth.getMonth();
	    var birthDay = dateOfBirth.getDate();

	    var age = calculateYear - birthYear;
	    var ageMonth = calculateMonth - birthMonth;
	    var ageDay = calculateDay - birthDay;

	    if (ageMonth < 0 || (ageMonth == 0 && ageDay < 0)) {
	        age = parseInt(age) - 1;
	    }
	    return age.toString();
	
}
	//Functions
	function loadMembers(){
		$http.get('/api/allMembersRenewal').then(function (response) {
			sortMemberData(response.data);
//			$scope.members = response.data;
			$rootScope.initPagination($scope.members.length)
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
	function loadMembershiptypes(){
		$http.get('/dashboard/allMemberShipTypes').then(function (response) {
			$scope.memberShipTypes = response.data;
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
	function loadfacilitytypes(){
		$http.get('/dashboard/allFacilityTypes').then(function (response) {
			$scope.facilityTypes = response.data;
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
	function loadRoles(){
		$http.get('/api/allRoles').then(function (response) {
			$scope.roles = response.data;
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
	
	function compare(a,b) {
		  if (a.memberId > b.memberId)
		    return -1;
		  if (a.memberId < b.memberId)
		    return 1;
		  return 0;
	}
	function sortMemberData(data){
		$scope.members=data.sort(compare);
		console.log(data);
	}
	

	$scope.addMember = function() {
		$scope.member = {};
		$scope.member.memberId = 0;
		$scope.modalTitle = 'Add';
		$scope.resetAndToggle();
	}
	
	$scope.resetAndToggle = function() {
		$('#bootstrap-wizard-1').find('.form-wizard').children('li').each(function(index, value){
			$( '#wiz-prev' ).click();
		});
		$('#bootstrap-wizard-1').find('.form-wizard').children('li').each(function(index, value){
			$( this ).find('.step').html(index + 1);
			$( this ).removeClass('complete');
			if (index == 0)
				$( this ).addClass('active');
			else
				$( this ).removeClass('active');
		});
		$('#myModal').modal('toggle');
	}

	$scope.saveMember = function() {
	   var file = $scope.uploadedFile;
       if (file) {
    	   var url = "/api/uploadfile";
           var data = new FormData();
           data.append('uploadfile', file);
           var config = {
    	   	transformRequest: angular.identity,
    	   	transformResponse: angular.identity,
       		headers : {
       			'Content-Type': undefined
       	    }
           }
           $http.post(url, data, config).then(function (response) {
        	   console.log(response.data);
        	   $scope.member.memberPhoto = response.data;
        	   $scope.saveMem();
    		}, function (response) {
    			alert("failed");
    		});
       } else {
    	   $scope.saveMem();
       }
	}

	$scope.saveMem = function() {
		$http({
			url : '/api/memberRenewalSave',
			method : "POST",
			data : {
				'data' : $scope.member
			}
		}).then(function(response) {
			if (response.data) {
				$scope.member = response.data;
				$scope.members.push($scope.member);
				$('#myModal').modal('toggle');
				loadMembers();
//				generateQRCode()
				generateQRCode($scope.member.memberId)
				generateQRCode1($scope.member.billId)
				generateBarCode($scope.member.memberId)
				generateBarcode1($scope.member.billId)
				$scope.printForm = true;
			}
		}, function(response) {
			alert("failed");
		});
	}

	$scope.back = function() {
		$scope.printForm = false;
	}

	$scope.editMember = function(x, index) {
		$http.get('/api/getMemberRenewal/' + x.memberId).then(function (response) {
			$scope.member = response.data;
			$scope.member.memberTypeStartDate=$filter('date')($scope.member.memberTypeStartDate, "dd-MMM-yyyy");
			$scope.member.memberTypeValidity = $filter('date')($scope.member.memberTypeValidity, "dd-MMM-yyyy");
			$scope.member.dob = $filter('date')($scope.member.dob, "dd-MMM-yyyy");
			$scope.member.index = index;
			$scope.modalTitle = 'Edit';
			$scope.resetAndToggle();
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
	
//	$scope.deleteMember = function(x, index) {
//		$http.get('/api/deleteMember/' + x.memberId).then(function (response) {
//			$scope.members.splice(index, 1);
//			$rootScope.deletedEntry();
//		}, function error(response) {
//			$scope.postResultMessage = "Error Status: " +  response.statusText;
//		});
//	}

});

sportsApp.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
           var model = $parse(attrs.fileModel);
           var modelSetter = model.assign;
           
           element.bind('change', function(){
              scope.$apply(function(){
                 modelSetter(scope, element[0].files[0]);
              });
              var reader = new FileReader();
              reader.onload = function(loadEvent) {
                scope.$apply(function() {
                  scope.filepreview = loadEvent.target.result;
                });
              }
              reader.readAsDataURL(scope.uploadedFile);
           });
        }
     };
 }]); 

