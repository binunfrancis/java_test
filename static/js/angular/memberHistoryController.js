sportsApp.filter('formatterHistory', function() {
    return function(x) {
    	if(x=="" || x==undefined || x==null){
    		return "";
    	}
    	var d = new Date(x);
    	var str = d.getDate() +"-" + (d.getMonth()+1) +"-"+d.getFullYear();
        return str;
    };
});
sportsApp.filter('slotter', function() {
    return function(x) {
       var b= x.toString();
       var f;
       
       if(b.match(/[.]5$/g)){
    	   f= b.replace(/[.]5$/g,":30")
       }
       else{
    	   f=b+":00";
       }
       return f;
    };
});
sportsApp.directive('clientAutoCompleteDir',function ($filter) {
    return {
        restrict: 'A',       
        link: function (scope, elem, attrs) {
            elem.autocomplete({
                source: function (request, response) {

                    //term has the data typed by the user
                    var params = request.term;
                    
                    //simulates api call with odata $filter
                    var data = scope.dataSource;                                     
                    if (data) { 
                        var result = $filter('filter')(data, {name:params});
                        angular.forEach(result, function (item) {
                            item['value'] = item['name'];
                        });                       
                    }
                    response(result);

                },
                minLength: 1,                       
                select: function (event, ui) {
                   //force a digest cycle to update the views
                   scope.$apply(function(){
                   	scope.setClientData(ui.item);
                   });                       
                },
               
            });
        }

    };
});
sportsApp.controller('memberHistoryController', function($scope, $rootScope, $http, $filter) {
	
	
	$rootScope.active = 'memberHistory';
	$scope.page='memberHistory';
	$scope.memberid;
	loadMembers();		
	function loadMembers(){
		$http.get('/api/allMembers').then(function (response) {
			$scope.memberList = response.data;
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
	$scope.getHistory = function(){
		var id = $scope.selectedmemid || $scope.memberid;
		$http.get('/api/getMember/'+id).then(function (response) {
			if(response.data){
				getMemberPersonalInfo(response.data);	
				$http.get('/api/getAllMembersBookings/'+id).then(function (response) {
					if(response.data){
//						$scope.memAge = $filter('date')($scope.member.dob, "dd-MMM-yyyy");
						extractResponse(response.data);
						//getMemberPersonalInfo(response.data);	
					}else{
						alert("Booking details not found!");
					}
					
				}, function error(response) {
					alert("Booking details not found!");
					$scope.postResultMessage = "Error Status: " +  response.statusText;
				});
			}else{
				$scope.memName = "";
				$scope.memAge = "";
				$scope.memMail = "";
				$scope.memDistrict = "";
				$scope.memType = "";
				$scope.memContact = "";
				$scope.memId = "";
				$scope.visitList=[];
				$scope.activeList = [];
				$rootScope.initPagination($scope.visitList.length);
				$rootScope.initPagination1($scope.activeList.length);
				alert("Member details not found!");
			}
			
		}, function error(response) {
			$scope.memName = "";
			$scope.memAge = "";
			$scope.memMail = "";
			$scope.memDistrict = "";
			$scope.memType = "";
			$scope.memContact = "";
			$scope.memId = "";
			$scope.visitList=[];
			$scope.activeList = [];
			$rootScope.initPagination($scope.visitList.length);
			$rootScope.initPagination1($scope.activeList.length);
			alert("Member details not found!");
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
		
	
	}
	function extractResponse(data){
		$scope.visitList = [];
		$scope.activeList = [];
		for(var i=0;i<data.length;i++){
			if(data[i].entryStatus == 1){
				$scope.visitList.push(data[i]);
			}else{				
				$scope.activeList.push(data[i]);
			}
		}
		$rootScope.initPagination($scope.visitList.length);
		$rootScope.initPagination1($scope.activeList.length);
	}
	function getMemberPersonalInfo(data){
		if(data){
			var d= data;
			$scope.memName = d.memberName;
			$scope.memAge = d.dob;
			$scope.memMail = d.email;
			$scope.memDistrict = d.district;
			$scope.memType = d.memberShipTypeId.memberShipTypeName;
			$scope.memContact = d.memberContactNo;
			$scope.memId = d.memberId;
		}
	}
	
}); 
sportsApp.filter('timeFormat', function() {
    return function(x) {
    	var hours = parseInt(x / 60);
    	if (hours < 10)
    		hours = '0' + hours;
    	var mins = x % 60;
    	if (mins < 10)
    		mins = '0' + mins;
        return hours + ':' + mins;
    };
});