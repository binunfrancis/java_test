// create the module and name it sportsApp
var sportsApp = angular.module('sportsApp', ['ngRoute','datatables','datatables.buttons','datatables.columnfilter','datatables.bootstrap']);

//,'ngMaterial'

// configure our routes
sportsApp.config(function($routeProvider) {
	$routeProvider

		.when('/', {
			template	: '<script src="../static/js/jquery.backstretch.min.js"></script><script>$(function(){$("body").css("background","none");$(".backstretch").show();});$.ajax({url: "/getBackgroundImages",success: function(data) {$.backstretch(data, {duration: 3000, fade:1000})}})</script>',
			controller  : 'welcomeController'
		})
	
		.when('/dashboard', {
			templateUrl : 'dashboardSports',
			controller  : 'dashboardController'
		})
		.when('/background', {
			templateUrl : 'background',
			controller  : 'backgroundController'
		})

		.when('/roleSports', {
			templateUrl : 'roleSports',
			controller  : 'rolesController'
		})
		
		.when('/rolesCreate', {
			templateUrl : 'rolesCreate',
			controller  : 'rolesCreateController'
		})
		.when('/memberHistory', {
			templateUrl : 'memberHistory',
			controller : 'memberHistoryController'
		})
		.when('/memberEntry', {
			templateUrl : 'memberEntry',
			controller : 'memberEntryController'
		})
		.when('/customerEntry', {
			templateUrl : 'customerEntry',
			controller : 'memberEntryController'
		})
		
		.when('/editBooking', {
			templateUrl : 'editBooking',
			controller : 'editBookingController'
		})
		.when('/rolesRestore', {
			templateUrl : 'rolesRestore',
			controller  : 'rolesRestoreController'
		})

		.when('/userSports', {
			templateUrl : 'userSports',
			controller  : 'userController'
		})

		.when('/branchSports', {
			templateUrl : 'branchSports',
			controller  : 'branchController'
		})

		.when('/masterTypeSports', {
			templateUrl : 'masterTypeSports',
			controller  : 'masterTypeController'
		}) 

		.when('/masterTypeRestore', {
			templateUrl : 'masterTypeRestore',
			controller  : 'masterTypeRestoreController'
		})

		.when('/pricing', {
			templateUrl : 'pricing',
			controller  : 'pricingController'
		}) 

		.when('/pricingRestore', {
			templateUrl : 'pricingRestore',
			controller  : 'pricingRestoreController'
		})

		.when('/memberShipSports', {
			templateUrl : 'memberShipSports',
			controller  : 'memberShipTypeController'
		})

		.when('/memberShipTypeRestore', {
			templateUrl : 'memberShipTypeRestore',
			controller  : 'memberShipTypeRestoreController'
		})
		
		.when('/matPriceSports', {
			templateUrl : 'matPriceSports',
			controller  : 'matPriceController'
		})

		.when('/matPriceRestore', {
			templateUrl : 'matPriceRestore',
			controller  : 'matPriceRestoreController'
		})
		
		.when('/trainerChargeSports', {
			templateUrl : 'trainerChargeSports',
			controller  : 'trainingChargeController'
		})
		
		.when('/trainerChargeRestore', {
			templateUrl : 'trainerChargeRestore',
			controller  : 'trainerChargeRestoreController'
		})
		
		
		.when('/trainerSports', {
			templateUrl : 'trainerSports',
			controller  : 'trainerController'
		})
		
		.when('/trainerRestore', {
			templateUrl : 'trainerRestore',
			controller  : 'trainerRestoreController'
		})


		.when('/locationSports', {
			templateUrl : 'locationSports',
			controller  : 'locationController'
		})

		.when('/locationRestore', {
			templateUrl : 'locationRestore',
			controller  : 'locationRestoreController'
		})

		.when('/centerSports', {
			templateUrl : 'centerSports',
			controller  : 'centerController'
		})

		.when('/centerRestore', {
			templateUrl : 'centerRestore',
			controller  : 'centerRestoreController'
		})

		.when('/facilityTypeSports', {
			templateUrl : 'facilityTypeSports',
			controller  : 'facilityTypeController'
		})
		
		.when('/facilityTypeRestore', {
			templateUrl : 'facilityTypeRestore',
			controller  : 'facilityTypeRestoreController'
		})

		.when('/financialYearSports', {
			templateUrl : 'financialYearSports',
			controller  : 'financialYearController'
		})

		.when('/memberSports', {
			templateUrl : 'memberSports',
			controller  : 'memberController'
		})
		
		.when('/memberRenewalSports', {
			templateUrl : 'memberRenewalSports',
			controller  : 'memberRenewController'
		})

		.when('/memberRestore', {
			templateUrl : 'memberRestore',
			controller  : 'memberRestoreController'
		})

		.when('/facilitySports', {
			templateUrl : 'facilitySports',
			controller  : 'facilityController'
		})

		.when('/facilityRestore', {
			templateUrl : 'facilityRestore',
			controller  : 'facilityRestoreController'
		})

		.when('/centerTypeSports', {
			templateUrl : 'centerTypeSports',
			controller  : 'centerTypeController'
		})

		.when('/centerTypeRestore', {
			templateUrl : 'centerTypeRestore',
			controller  : 'centerTypeRestoreController'
		})

		.when('/facility', {
			templateUrl : 'facility',
			controller  : 'facilityController'
		})

		.when('/leaveTypeSports', {
			templateUrl : 'leaveTypeSports',
			controller  : 'leaveTypeController'
		})

		.when('/leaveTypeRestore', {
			templateUrl : 'leaveTypeRestore',
			controller  : 'leaveTypeRestoreController'
		})

		.when('/leaveTabSports', {
			templateUrl : 'leaveTabSports',
			controller  : 'leaveTabController'
		})

		.when('/leaveTabRestore', {
			templateUrl : 'leaveTabRestore',
			controller  : 'leaveTabRestoreController'
		})

		.when('/timeSlotsSports', {
			templateUrl : 'timeSlotsSports',
			controller  : 'timeSlotsController'
		})

		.when('/timeSlotsRestore', {
			templateUrl : 'timeSlotsRestore',
			controller  : 'timeSlotsRestoreController'
		})

		.when('/booking', {
			templateUrl : 'booking',
			controller  : 'bookingController'
		})
		.when('/bookingCenter', {
			templateUrl : 'bookingCenter',
			controller  : 'bookingCenterController'
		})
		.when('/bookingFacility', {
			templateUrl : 'bookingFacility',
			controller  : 'bookingFacilityController'
		})
		.when('/bookingSubFacility', {
			templateUrl : 'bookingSubFacility',
			controller  : 'bookingSubFacilityController'
		})
		.when('/book', {
			templateUrl : 'book',
			controller  : 'bookController'
		})
		
		.when('/viewBooking', {
			templateUrl : 'viewBooking'
		})
		
		.when('/staffSports', {
			templateUrl : 'staffSports',
			controller  : 'staffController'
		})
		
		
		.when('/viewAccounts', {
			templateUrl : 'viewAccounts',
			controller  : 'accountController'
		})
		
		
		.when('/viewSelectedBookings', {
			templateUrl : 'viewSelectedBookings',
			controller  : 'viewBookController'
		})
		
		.when('/staffRestore', {
			templateUrl : 'staffRestore',
			controller  : 'staffRestoreController'
		})
		.when('/holiday', {
			templateUrl : 'holiday',
			controller  : 'holidayController'
		})
		.when('/caution', {
			templateUrl : 'caution',
			controller  : 'cautionController'
		})
		
		.when('/allRoleActionsEdits', {
			templateUrl : 'allRoleActionsEdits',
			controller  : 'roleEditController'
		})


		.when('/changePassword', {
			templateUrl : 'changePassword',
			controller  : 'changePasswordController'
		})

		.otherwise({templateUrl:'pageNotFound'}); // Render 404 view
});

sportsApp.controller('welcomeController', function($rootScope) {
	$rootScope.active = 'welcome';
	if($rootScope.backgroundUpdated){
		$rootScope.backgroundUpdated=false;
		location.reload();
	}
	$('#myModalsCheck').modal('show');
});

sportsApp.controller('dashboardController', function($rootScope) {
	$rootScope.active = 'dashboard';
});

sportsApp.controller('menuController', function($rootScope, $window, $http) {
	$rootScope.goBack = function() {
		$window.history.back()
	}
	$http.get('/api/getRoleAccess/').then(function (response) {
		$rootScope.roleAccess = {};
		angular.forEach(response.data, function(x){ $rootScope.roleAccess[x.pageName] = x.access });
	}, function error(response) {
		$scope.postResultMessage = "Error Status: " +  response.statusText;
	});

	$rootScope.export = function(x){
		html2canvas(document.getElementById('print'), {
		    onrendered: function (canvas) {
		        var data = canvas.toDataURL();
		        var docDefinition = {
	        		pageSize: 'A4',
	        		pageMargins: [ 0, 0, 0, 0 ],
	        		content: [{
		                image: data,
		                width: 594,
		                height:840
		            }]
		        };
		        //pdfMake.createPdf(docDefinition).download(x + ".pdf");
		        //pdfMake.createPdf(docDefinition).open()
		        pdfMake.createPdf(docDefinition).print();
	        }
	    });
	}
	$rootScope.exportid = function(x){
		html2canvas(document.getElementById('printid'), {
		    onrendered: function (canvas) {
		        var data = canvas.toDataURL();
		        var docDefinition = {
	        		pageSize: 'A4',
	        		pageMargins: [ 0, 0, 0, 0 ],
	        		content: [{
		                image: data,
		                width: 594,
		                height:840
		            }]
		        };
		        //pdfMake.createPdf(docDefinition).download(x + ".pdf");
		        //pdfMake.createPdf(docDefinition).open()
		        pdfMake.createPdf(docDefinition).print();
	        }
	    });
	}
	$rootScope.initPagination = function(x) {
		$rootScope.totalEntries = x;
		$rootScope.showingFrom = $rootScope.totalEntries > 0 ? 1 : 0;
		$rootScope.showingTo = $rootScope.totalEntries > 10 ? 10 : $rootScope.totalEntries;
		$rootScope.pages = [];
		var x = parseInt($rootScope.totalEntries / 10);
		if ($rootScope.totalEntries % 10 != 0)
			x++
		for (var int = 1; int <= x; int++) {
			$rootScope.pages.push(int)
		}
	}

	$rootScope.currentPage = 1;

	$rootScope.nextPage = function() {
		if ($rootScope.currentPage != $rootScope.pages.length) {
			$rootScope.currentPage += 1
			$rootScope.showingFrom = $rootScope.showingTo + 1;
			$rootScope.showingTo += 10;
			if ($rootScope.showingTo > $rootScope.totalEntries)
				$rootScope.showingTo = $rootScope.totalEntries
		}
	}

	$rootScope.prevPage = function() {
		if ($rootScope.currentPage != 1) {
			$rootScope.currentPage -= 1
			$rootScope.showingTo = $rootScope.showingFrom - 1
			$rootScope.showingFrom -= 10
		}
	}

	$rootScope.goToPage = function(x) {
		if ($rootScope.currentPage != x) {
			$rootScope.currentPage = x
			$rootScope.showingTo = $rootScope.currentPage * 10
			$rootScope.showingFrom = $rootScope.showingTo - 9
			if ($rootScope.showingTo > $rootScope.totalEntries)
				$rootScope.showingTo = $rootScope.totalEntries
		}
	}

	$rootScope.firstPage = function() {
		if ($rootScope.currentPage != 1) {
			$rootScope.currentPage = 1
			$rootScope.showingFrom = 1
			$rootScope.showingTo = $rootScope.totalEntries > 10 ? 10 : $rootScope.totalEntries;
		}
	}

	$rootScope.lastPage = function() {
		if ($rootScope.currentPage != $rootScope.pages.length) {
			$rootScope.currentPage = $rootScope.pages.length
			$rootScope.showingFrom = ($rootScope.currentPage * 10) - 9
			$rootScope.showingTo = $rootScope.totalEntries
		}
	}
//
	$rootScope.initPagination1 = function(x) {
		$rootScope.totalEntries1 = x;
		$rootScope.showingFrom1 = $rootScope.totalEntries1 > 0 ? 1 : 0;
		$rootScope.showingTo1 = $rootScope.totalEntries1 > 10 ? 10 : $rootScope.totalEntries1;
		$rootScope.pages1 = [];
		var x = parseInt($rootScope.totalEntries1 / 10);
		if ($rootScope.totalEntries1 % 10 != 0)
			x++
		for (var int = 1; int <= x; int++) {
			$rootScope.pages1.push(int)
		}
	}

	$rootScope.currentPage1 = 1;

	$rootScope.nextPage1 = function() {
		if ($rootScope.currentPage1 != $rootScope.pages1.length) {
			$rootScope.currentPage1 += 1
			$rootScope.showingFrom1 = $rootScope.showingTo1 + 1;
			$rootScope.showingTo1 += 10;
			if ($rootScope.showingTo1 > $rootScope.totalEntries1)
				$rootScope.showingTo1 = $rootScope.totalEntries1
		}
	}

	$rootScope.prevPage1 = function() {
		if ($rootScope.currentPage1 != 1) {
			$rootScope.currentPage1 -= 1
			$rootScope.showingTo1 = $rootScope.showingFrom1 - 1
			$rootScope.showingFrom1 -= 10
		}
	}

	$rootScope.goToPage1 = function(x) {
		if ($rootScope.currentPage1 != x) {
			$rootScope.currentPage1 = x
			$rootScope.showingTo1 = $rootScope.currentPage1 * 10
			$rootScope.showingFrom1 = $rootScope.showingTo1 - 9
			if ($rootScope.showingTo1 > $rootScope.totalEntries1)
				$rootScope.showingTo1 = $rootScope.totalEntries1
		}
	}

	$rootScope.firstPage1 = function() {
		if ($rootScope.currentPage1 != 1) {
			$rootScope.currentPage1 = 1
			$rootScope.showingFrom1 = 1
			$rootScope.showingTo1 = $rootScope.totalEntries1 > 10 ? 10 : $rootScope.totalEntries1;
		}
	}

	$rootScope.lastPage1 = function() {
		if ($rootScope.currentPage1 != $rootScope.pages1.length) {
			$rootScope.currentPage1 = $rootScope.pages1.length
			$rootScope.showingFrom1 = ($rootScope.currentPage1 * 10) - 9
			$rootScope.showingTo1 = $rootScope.totalEntries1
		}
	}

	//
	
	
	$rootScope.deletedEntry = function() {
		if ($rootScope.showingTo === $rootScope.totalEntries)
			$rootScope.showingTo--;
		$rootScope.totalEntries--;
	}

	$rootScope.addedEntry = function() {
		$rootScope.totalEntries++;
		if ($rootScope.showingTo % 10 != 0)
			$rootScope.showingTo++;
	}
});

sportsApp.controller('backgroundController', function($rootScope, $scope, $http) {
	$rootScope.active = 'background';
	loadImages();
	function loadImages(){
		$http.get('/api/allBackgroundPics').then(function (response) {
			$scope.imageList = response.data;
			$rootScope.initPagination($scope.imageList.length);
		}, function error(response) {
			alert("Failed to list saved images");
		});
	}
	$scope.viewImage = function(e,o){
		$scope.imgpath = o.bckPathName;
		$('#myModalExtra').modal('toggle');
	}
	$scope.deleteImage = function(e,o){
		//api/deleteBackgroundPics/{id} backgroundId
		$http.get('/api/deleteBackgroundPics/'+o.backgroundId).then(function (response) {
			alert("Deleted Successfully");
			loadImages();
			$rootScope.backgroundUpdated=true;
		}, function error(response) {
			alert("Failed to delete the image!");
		});
	}
	$scope.upload = function() {
	   var file = $scope.uploadedFile;
	   if (file) {
		   var url = "/api/uploadImage";
	       var data = new FormData();
	       data.append('uploadfile', file);
	       var config = {
		   	transformRequest: angular.identity,
		   	transformResponse: angular.identity,
	   		headers : {
	   			'Content-Type': undefined
	   	    }
	       }
	       $http.post(url, data, config).then(function (response) {
	    	   $scope.uploadedFile = undefined;
	    	  alert(response.data);
	    	  $rootScope.backgroundUpdated=true;
	    	  loadImages();
			}, function (response) {
				alert("failed");
			});
	   }
	}
});

sportsApp.controller('changePasswordController', function($scope, $rootScope, $http) {
	$rootScope.active = 'changePassword';

	$scope.newPassword = '';
	$scope.confirmPassword = '';
	$scope.message = '';
	$scope.msgColor = 'red-text';

	$scope.setMessageText = function() {
		$scope.message = !$scope.newPassword && !$scope.confirmPassword  ? '' : $scope.newPassword === $scope.confirmPassword ? 'Matching' : 'Not Matching';
		$scope.msgColor = $scope.newPassword === $scope.confirmPassword ? 'green-text' : 'red-text';
	}

	$scope.resetPassword = function() {
		$http({
			url : '/api/resetPassword',
			method : "POST",
			data : $scope.newPassword
		}).then(function(response) {
			$scope.newPassword = '';
			$scope.confirmPassword = '';
			$scope.message = '';
		}, function(response) {
			alert("failed");
		});
	}
});

sportsApp.directive('pagination', function () {
    return {
        templateUrl: '../static/html/pagination.html'
    };
});
sportsApp.directive('paginationsec', function () {
    return {
        templateUrl: '../static/html/paginationsec.html'
    };
});

(function() {
    'use strict';
    var app = angular.module('flash', []);

    sportsApp.run(function($rootScope) {
        // initialize variables
        $rootScope.flash = {};
        $rootScope.flash.text = '';
        $rootScope.flash.type = '';
        $rootScope.flash.timeout = 5000;
        $rootScope.hasFlash = false;
    });

    // Directive for compiling dynamic html
    sportsApp.directive('dynamic', function($compile) {
        return {
            restrict: 'A',
            replace: true,
            link: function(scope, ele, attrs) {
                scope.$watch(attrs.dynamic, function(html) {
                    ele.html(html);
                    $compile(ele.contents())(scope);
                });
            }
        };
    });

    // Directive for closing the flash message
    sportsApp.directive('closeFlash', function($compile, Flash) {
        return {
            link: function(scope, ele) {
                ele.on('click', function() {
                    Flash.dismiss();
                });
            }
        };
    });

    // Create flashMessage directive
    sportsApp.directive('flashMessage', function($compile, $rootScope) {
        return {
            restrict: 'A',
            template: '<div role="alert" ng-show="hasFlash" class="alert {{flash.addClass}} alert-{{flash.type}} alert-dismissible ng-hide alertIn alertOut "> <span dynamic="flash.text"></span> <button type="button" class="close" close-flash><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> </div>',
            link: function(scope, ele, attrs) {
                // get timeout value from directive attribute and set to flash timeout
                $rootScope.flash.timeout = parseInt(attrs.flashMessage, 10);
            }
        };
    });

    sportsApp.factory('Flash', ['$rootScope', '$timeout',
        function($rootScope, $timeout) {

            var dataFactory = {},
                timeOut;

            // Create flash message
            dataFactory.create = function(type, text, addClass) {
                var $this = this;
                $timeout.cancel(timeOut);
                $rootScope.flash.type = type;
                $rootScope.flash.text = text;
                $rootScope.flash.addClass = addClass;
                $timeout(function() {
                    $rootScope.hasFlash = true;
                }, 100);
                timeOut = $timeout(function() {
                    $this.dismiss();
                }, $rootScope.flash.timeout);
            };

            // Cancel flashmessage timeout function
            dataFactory.pause = function() {
                $timeout.cancel(timeOut);
            };

            // Dismiss flash message
            dataFactory.dismiss = function() {
                $timeout.cancel(timeOut);
                $timeout(function() {
                    $rootScope.hasFlash = false;
                });
            };
            return dataFactory;
        }
    ]);
}())