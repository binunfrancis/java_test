sportsApp.controller('memberShipTypeController', function($scope, $rootScope, $http) {
	$rootScope.active = 'memberShipType';
	$scope.page = 'Membership Type';
	$scope.memberShipType = {};
	$scope.memberShipType.memberShipId = 0;
	$scope.memberShipTypeEdit = {};
	
	loadMembershiptypes();
	function loadMembershiptypes(){
		$http.get('/dashboard/allMemberShipTypes').then(function (response) {
			sortMemberShipData(response.data);
//			$scope.memberShipTypes = response.data;
			
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
	
	function compare(a,b) {
		  if (a.memberShipId > b.memberShipId)
		    return -1;
		  if (a.memberShipId < b.memberShipId)
		    return 1;
		  return 0;
	}
	function sortMemberShipData(data){
		$scope.memberShipTypes=data.sort(compare);
		console.log(data);
		$rootScope.initPagination($scope.memberShipTypes.length)
	}

	$scope.addMemberShipType = function() {
		$scope.memberShipType = {};
		$scope.memberShipType.memberShipId = 0;
		$scope.modalTitle = 'Add';
		$('#myModal').modal('toggle');		
	}

	$scope.saveMemberShipType = function() {
		$scope.loadtrue = true;
		$scope.memberShipType.memberIdCardAmount = ($scope.memberShipType.memberIdCardAmount.toString());
		$scope.memberShipType.memberShipCost = ($scope.memberShipType.memberShipCost.toString());
		$scope.memberShipType.memberShipDiscount = ($scope.memberShipType.memberShipDiscount.toString());
		$http({
			url : '/dashboard/memberShipTypeSave',
			method : "POST",
			data : {
				'data' : $scope.memberShipType
			}
		}).then(function(response) {
			$scope.memberShipType.memberShipId = response.data;
			if ($.isNumeric($scope.memberShipType.index))
				$scope.memberShipTypes[$scope.memberShipType.index] = $scope.memberShipType;
			else
			$scope.memberShipTypes.push($scope.memberShipType);
			
			$rootScope.addedEntry();
			$scope.memberShipType = {};
			loadMembershiptypes();
			$rootScope.currentPage = 1;
			$('#myModal').modal('toggle');
			alert("Saved successfully");
			$scope.loadtrue = false;
		}, function(response) { // optional
			alert("failed");
			$scope.loadtrue = false;
		});

	}

	$scope.editMemberShipType = function(event, index) {
		$http.get('/dashboard/getMemberShipType/' + event.target.id).then(function (response) {
			$scope.memberShipType = response.data;
			$scope.memberShipType.index = index;
			$scope.modalTitle = 'Edit';
			$('#myModal').modal('toggle');
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
	
	$scope.deleteMemberShipType = function(event, index) {
		$http.get('/dashboard/deleteMemberShipType/' + event.target.id).then(function (response) {
			$scope.memberShipTypes.splice(index, 1);
			$rootScope.deletedEntry();
			alert("Deleted Successfully");
			loadMembershiptypes();
			$rootScope.currentPage = 1;
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
});

// create the controller and inject Angular's $scope
sportsApp.controller('memberShipTypeRestoreController', function($scope, $rootScope, $http) {
	$rootScope.active = 'memberShipType';

	$http.get('/api/allDeletedMemberShipTypes').then(function (response) {
		$scope.memberShipTypes = response.data;
	}, function error(response) {
		$scope.postResultMessage = "Error Status: " +  response.statusText;
	});

	$scope.deleteMemberShipType = function(rs, index) {
		$http.get('/api/restoreMemberShipType/' + rs.memberShipId).then(function (response) {
			$scope.memberShipTypes.splice(index, 1);
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
	
}); 