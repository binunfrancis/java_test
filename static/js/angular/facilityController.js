// create the controller and inject Angular's $scope
sportsApp.controller('facilityController', function($scope, $rootScope, $http) {
	$rootScope.active = 'facility';
	$scope.page='Facility';
	loadAllFacilities();
	
	$http.get('/dashboard/allCenters').then(function (response) {
		$scope.centers = response.data;
	}, function error(response) {
		$scope.postResultMessage = "Error Status: " +  response.statusText;
	});
	$http.get('/dashboard/allFacilityTypes').then(function (response) {
		$scope.facilityTypes = response.data;
	}, function error(response) {
		$scope.postResultMessage = "Error Status: " +  response.statusText;
	});
	
	
	
	$scope.facility = {};
	$scope.facility.facilityId = 0;
	$scope.facilityEdit = {};
	$scope.subFacilities = [];
	$scope.subFacilitiesRemoved = [];
	$scope.subFacility = {};

	function loadAllFacilities(){
		$http.get('/api/allFacilities').then(function (response) {
			$scope.facilities = response.data;
			$rootScope.initPagination($scope.facilities.length)
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
	$scope.addFacility = function() {
		$scope.facility = {};
		$scope.facility.facilityId = 0;
		$scope.subFacilities = [];
		$scope.subFacilitiesRemoved = []
		$scope.modalTitle = 'Add';
		$scope.resetAndToggle();
	}
	
	$scope.resetAndToggle = function() {
		$('#bootstrap-wizard-1').find('.form-wizard').children('li').each(function(index, value){
			$( '#wiz-prev' ).click();
		});
		$('#bootstrap-wizard-1').find('.form-wizard').children('li').each(function(index, value){
			$( this ).find('.step').html(index + 1);
			$( this ).removeClass('complete');
			if (index == 0)
				$( this ).addClass('active');
			else
				$( this ).removeClass('active');
		});
		$('#myModal').modal('toggle');
	}

	$scope.saveFacility = function() {
		if ($scope.subFacilities.length > 0) {
			for (var int = 0; int < $scope.subFacilities.length; int++) {
				if ($scope.subFacilities[int].subFacilityId < 0) {
					$scope.subFacilities[int].subFacilityId = 0;
				}
			}
			$http({
				url : '/api/facilitySave',
				method : "POST",
				data : {
					'facility' : $scope.facility,
					'subFacilities' : $scope.subFacilities,
					'subFacilitiesRemoved' : $scope.subFacilitiesRemoved
				}
			}).then(function(response) {
				$scope.facility.facilityId = response.data;
				if ($.isNumeric($scope.facility.facilityId)) {
					for (var int = 0; int < $scope.centers.length; int++) {
						if ($scope.centers[int].centreId === $scope.facility.centerId.centreId) {
							$scope.facility.centerId.centreName = $scope.centers[int].centreName;
							break;
						}
					}
					for (var int = 0; int < $scope.facilityTypes.length; int++) {
						if ($scope.facilityTypes[int].facilityTypeId === $scope.facility.facilityTypeId.facilityTypeId) {
							$scope.facility.facilityTypeId.facilityTypeName = $scope.facilityTypes[int].facilityTypeName;
							break;
						}
					}
					if ($.isNumeric($scope.facility.index))
						$scope.facilities[$scope.facility.index] = $scope.facility;
					else
					$scope.facilities.push($scope.facility);
					$rootScope.addedEntry();
					$scope.facility = {};
					loadAllFacilities();
					$('#myModal').modal('toggle');
					alert("Saved Successfully");
				}
			}, function(response) {
				alert("failed");
			});
		} else {
			alert('Atleast one sub-facility should be added')
		}
	}

	$scope.editFacility = function(event, index) {
		$http.get('/api/getFacility/' + event.target.id).then(function (response) {
			$scope.facility = response.data;
			$scope.facility.index = index;
			$http.get('/api/allSubFacilities/' + $scope.facility.facilityId).then(function (response) {
				$scope.subFacilities = response.data;
				$scope.subFacilitiesRemoved = []
			}, function error(response) {
				$scope.postResultMessage = "Error Status: " +  response.statusText;
			});
			$scope.modalTitle = 'Edit';
			$scope.resetAndToggle();
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
	
	$scope.deleteFacility = function(event, index) {
		$http.get('/api/deleteFacility/' + event.target.id).then(function (response) {
			$scope.facilities.splice(index, 1);
			$rootScope.deletedEntry();
			alert("Deleted Successfully");
			loadAllFacilities();
			$rootScope.currentPage = 1;
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
	
	$scope.viewSubFacilities = function(fl) {
		$http.get('/api/allSubFacilities/' + fl.facilityId).then(function (response) {
			$scope.subFacilities = response.data;
			$scope.subFacilitiesRemoved = []
			$('#subModal').modal('toggle');
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}

	$scope.getTemplate = function (rs) {
		if (rs.subFacilityId === $scope.subFacility.subFacilityId)
			return 'edit';
		return 'display';
	};

	$scope.addSubFacility = function () {
		$scope.reset();
		$scope.subFacility.subFacilityId = $scope.subFacilities.length * -1;
        $scope.subFacilities.push($scope.subFacility);
    };

    $scope.saveSubFacility = function (idx) {
    	if ($scope.subFacility.subFacilityName && $scope.subFacility.subFacilityName.length > 0) {
    		$scope.subFacilities[idx] = angular.copy($scope.subFacility);
            $scope.reset();
    	} else {
    		alert('Sub-facility name cannot be empty')
    	}
    };

	$scope.editSubFacility = function (rs) {
        $scope.subFacility = angular.copy(rs);
    };
	
	$scope.deleteSubFacility = function(index, id) {
		if (id > 0)
			$scope.subFacilitiesRemoved.push(id);
		$scope.subFacilities.splice(index, 1);
		$scope.reset();
	}

    $scope.reset = function () {
    	if (!$scope.subFacility.subFacilityName || $scope.subFacility.subFacilityName.length === 0)
    		$scope.subFacilities.pop();
        $scope.subFacility = {};
    };
});

// create the controller and inject Angular's $scope
sportsApp.controller('facilityRestoreController', function($scope, $rootScope, $http) {
	$rootScope.active = 'facility';

	$http.get('/api/allDeletedFacilities').then(function (response) {
		$scope.facilities = response.data;
	}, function error(response) {
		$scope.postResultMessage = "Error Status: " +  response.statusText;
	});

	$scope.deleteFacility = function(fl, index) {
		$http.get('/api/restoreFacility/' + fl.facilityId).then(function (response) {
			$scope.facilities.splice(index, 1);
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
	
}); 