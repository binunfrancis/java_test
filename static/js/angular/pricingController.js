// create the controller and inject Angular's $scope
sportsApp.controller('pricingController', function($scope, $rootScope, $http) {
	$rootScope.active = 'pricing';
	$scope.page='pricing';
	loadPricing();
	
	function loadPricing(){
			$http.get('/api/allPricings').then(function (response) {
			$scope.pricings = response.data;
			$rootScope.initPagination($scope.pricings.length)
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
	
	$scope.addPricing = function() {
		$scope.price = {};
		$scope.price.priceId = 0;
		$scope.bookingType = 0;
		$scope.bType = '';
		$scope.bTypeId = '';
		$scope.bTypeName = '';
		$scope.modalTitle = 'Add';
		$('#myModal').modal('toggle');
	}

	$scope.editPricing = function(x) {
		$scope.price = x;
		$scope.bType = x.centreId ? 'Center' : x.facilityId ? 'Facility' : 'Sub Facility';
		$scope.loadBTypes();
		$scope.bookingType = x.centreId ? x.centreId.centreId : x.facilityId ? x.facilityId.facilityId : x.subFacilityId.subFacilityId;
		$scope.modalTitle = 'Edit';
		$('#myModal').modal('toggle');
	}

	$scope.loadBTypes = function() {
		$scope.bookingType = 0;
		var url = $scope.bType === 'Center' ? '/dashboard/allCenters' : $scope.bType === 'Facility' ? '/api/allFacilities' : '/api/allSubFacilities';
		$scope.bTypeName = $scope.bType === 'Center' ? 'centreName' :  $scope.bType === 'Facility' ? 'facilityName' : 'subFacilityName';
		$scope.bTypeId =  $scope.bType === 'Center' ? 'centreId' :  $scope.bType === 'Facility' ? 'facilityId' : 'subFacilityId';
		$http.get(url).then(function (response) {
			$scope.bTypes = response.data;
			if ($scope.bType != 'Center') {
				angular.forEach($scope.bTypes, function(x) {
					x[$scope.bTypeName] = x[$scope.bTypeName] + ' - ' + x.centerId.centreName
				});
			}
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
		var url2 = $scope.bType === 'Center' ? '/api/masterTypesByCenter' : $scope.bType === 'Facility' ? '/api/masterTypesByFacility' : '/api/masterTypesBySubFacility';
		$http.get(url2).then(function (response) {
			$scope.masterTypes = response.data;
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}

	$scope.savePricing = function() {
		$scope.price[$scope.bTypeId] = {};
		$scope.price[$scope.bTypeId][$scope.bTypeId] = $scope.bookingType;
		$http({
			url : '/api/pricingSave',
			method : "POST",
			data : $scope.price
		}).then(function(response) {
			$scope.price.priceId = response.data;
			for (var int = 0; int < $scope.masterTypes.length; int++) {
				if ($scope.masterTypes[int].masterTypeId === $scope.price.masterTypeId.masterTypeId) {
					$scope.price.masterTypeId.masterTypeName = $scope.masterTypes[int].masterTypeName;
					break;
				}
			}
			if ($.isNumeric($scope.price.index))
				$scope.pricings[$scope.price.index] = $scope.price;
			else
				$scope.pricings.push($scope.price);
				
				$rootScope.addedEntry();
				loadPricing();
				$rootScope.currentPage = 1;
			$('#myModal').modal('toggle');
			alert("Saved Successfully");
		}, function(response) {
			alert("failed");
		});
	}

	$scope.deletePricing = function(id, idx) {
		$http.get('/api/deletePricing/' + id).then(function (response) {
			
			$scope.$apply(function(){
				$scope.pricings.splice(idx, 1);
				$rootScope.deletedEntry();	
			});
			alert("Deleted Successfully");
			loadPricing();
			$rootScope.currentPage = 1;
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
});

// create the controller and inject Angular's $scope
sportsApp.controller('pricingRestoreController', function($scope, $rootScope, $http) {
	$rootScope.active = 'pricing';
	$http.get('/api/allDeletedPricings').then(function (response) {
		$scope.pricings = response.data;
	}, function error(response) {
		$scope.postResultMessage = "Error Status: " +  response.statusText;
	});
	$scope.deletePricing = function(id, idx) {
		$http.get('/api/restorePricing/' + id).then(function (response) {
			$scope.pricings.splice(idx, 1);
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
});