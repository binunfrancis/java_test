sportsApp.controller('leaveTypeController', function($scope, $rootScope, $http) {
	$rootScope.active = 'leaveType';
	$scope.page='Leave Type';
	$scope.leaveType = {};
	$scope.leaveType.leaveTypeId = 0;
	$scope.leaveTypeEdit = {};
	
	loadAllLeaveTypes();
	
	function loadAllLeaveTypes(){
		$http.get('/dashboard/allLeaveTypes').then(function (response) {
			sortLeaveTypeData(response.data);
//			$scope.leaveTypes = response.data;
			
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
		
	}
	
	function compare(a,b) {
		  if (a.leaveTypeId > b.leaveTypeId)
		    return -1;
		  if (a.leaveTypeId < b.leaveTypeId)
		    return 1;
		  return 0;
	}
	function sortLeaveTypeData(data){
		$scope.leaveTypes=data.sort(compare);
		$rootScope.initPagination($scope.leaveTypes.length)
		console.log(data);
	}
	
	

	$scope.addLeaveType = function() {
		$scope.leaveType = {};
		$scope.leaveType.leaveTypeId = 0;
		$scope.modalTitle = 'Add';
		$('#myModal').modal('toggle');		
	}

	$scope.saveLeaveType = function() {
		$http({
			url : '/dashboard/leaveTypeSave',
			method : "POST",
			data : {
				'data' : $scope.leaveType
			}
		}).then(function(response) {
			$scope.leaveType.leaveTypeId = response.data;
			if ($.isNumeric($scope.leaveType.index))
				$scope.leaveTypes[$scope.leaveType.index] = $scope.leaveType;
			else
				$scope.leaveTypes.push($scope.leaveType);
			
			$rootScope.addedEntry();
			$scope.leaveType = {};
			loadAllLeaveTypes();
			$rootScope.currentPage = 1;
			$('#myModal').modal('toggle');
			alert("Saved successfully");
		}, function(response) { // optional
			alert("failed");
		});

	}

	$scope.editLeaveType = function(event, index) {
		$http.get('/dashboard/getLeaveType/' + event.target.id).then(function (response) {
			$scope.leaveType = response.data;
			$scope.leaveType.index = index;
			$scope.modalTitle = 'Edit';
			$('#myModal').modal('toggle');
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
	
	$scope.deleteLeaveType = function(event, index) {
		$http.get('/dashboard/deleteLeaveType/' + event.target.id).then(function (response) {
			$scope.leaveTypes.splice(index, 1);
			$rootScope.deletedEntry();
			alert("Deleted Successfully");
			loadAllLeaveTypes();
			$rootScope.currentPage = 1;
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
});

sportsApp.controller('leaveTypeRestoreController', function($scope, $rootScope, $http) {
	$rootScope.active = 'leaveType';

	$http.get('/api/allDeletedLeaveTypes').then(function (response) {
		$scope.leaveTypes = response.data;
	}, function error(response) {
		$scope.postResultMessage = "Error Status: " +  response.statusText;
	});

	$scope.deleteLeaveType = function(rs, index) {
		$http.get('/api/restoreLeaveType/' + rs.leaveTypeId).then(function (response) {
			$scope.leaveTypes.splice(index, 1);
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
	
}); 