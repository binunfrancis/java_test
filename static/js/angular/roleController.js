sportsApp.controller('roleEditController', function($scope, $rootScope, $http,$location) {
	$rootScope.active = 'role';
	$scope.page='Role';

	
	$rootScope.active = 'role';
	$scope.role = {};
	$scope.role.roleId = $rootScope.roleId | 1;
//	alert($rootScope.roleId);

	$http.get('/api/getRoleAccesses/' + $rootScope.roleId).then(function (response) {
		$scope.roleAccesses = response.data;
//		console.log(response.data);
		for (var int = 0; int < $scope.roleAccesses.length; int++) {
//			if ($scope.role.roleId === 1)
//				$scope.roleAccesses[int].roleAccessId = 0;
			$scope.roleAccesses[int].rd = $scope.roleAccesses[int].access === 2;
			$scope.roleAccesses[int].vo = $scope.roleAccesses[int].access === 1;
			$scope.roleAccesses[int].na = $scope.roleAccesses[int].access === 0;
		}
	}, function error(response) {
		$scope.postResultMessage = "Error Status: " +  response.statusText;
	});
	
	$scope.checkUnique = function() {
		$scope.nameError = "";
		for (var int = 0; int < $scope.roles.length; int++) {
			if (('ROLE_' + $scope.role.roleName).toUpperCase() === $scope.roles[int].roleName.toUpperCase()) {
				$scope.nameError = "Role Name Already Exists";
				break;
			}
		}
	}

	$scope.checkALL = function(x) {
		$scope.rd = (x === 'rd');
		$scope.vo = (x === 'vo');
		$scope.na = (x === 'na');
		for (var int = 0; int < $scope.roleAccesses.length; int++) {
			$scope.roleAccesses[int].rd = $scope.rd;
			$scope.roleAccesses[int].vo = $scope.vo;
			$scope.roleAccesses[int].na = $scope.na;
		}
	}

	$scope.resetCheckBoxes = function(x, index) {
		$scope.roleAccesses[index].rd = (x === 'rd');
		$scope.roleAccesses[index].vo = (x === 'vo');
		$scope.roleAccesses[index].na = (x === 'na');
	}
	

	$scope.updateRoleName = function() {
//		alert($rootScope.roleId);
		for (var int = 0; int < $scope.roleAccesses.length; int++)
			$scope.roleAccesses[int].access =  $scope.roleAccesses[int].rd ? 2 : $scope.roleAccesses[int].vo ? 1 : 0;
		$http({
			url : '/dashboard/roleUpdate',
			method : "POST",
			data : {
				'roleAccesses' : $scope.roleAccesses,
				'role' : $scope.role,
				'roleId': $rootScope.roleId
			}
		}).then(function(response) {
			$scope.role.roleId = response.data[0];
			$scope.roleAccesses = response.data[1];
			for (var int = 0; int < $scope.roleAccesses.length; int++) {
				if ($scope.role.roleId === 1)
					$scope.roleAccesses[int].roleAccessId = 0;
				$scope.roleAccesses[int].rd = $scope.roleAccesses[int].access === 2;
				$scope.roleAccesses[int].vo = $scope.roleAccesses[int].access === 1;
				$scope.roleAccesses[int].na = $scope.roleAccesses[int].access === 0;
			}
			alert("success");
		}, function(response) {
			alert("failed");
		});
	}
});

sportsApp.controller('rolesController', function($scope, $rootScope, $http,$location) {
	$rootScope.active = 'role';
	reloadTable();
	function reloadTable(){
		$http.get('/dashboard/allRoles').then(function (response) {
			$scope.roles = response.data;
			
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});	
	} 
	
	
	$scope.editRole = function (x) {
//		alert('checking..'+x.roleId);
		$http.get('/dashboard/allRoleActionsEdit/'+x.roleId).then(function (response) {
//			console.log(response.data);
			$scope.roleActions = response.data;
			$rootScope.roleId=x.roleId;
			$location.path('allRoleActionsEdits')
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
	

	$scope.deleteRole = function(x, index) {
		$http.get('/dashboard/deleteRole/' + x.roleId).then(function (response) {
			$scope.roles.splice(index, 1);
			reloadTable();
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}

});


sportsApp.controller('rolesCreateController', function($scope, $rootScope, $http) {
	$rootScope.active = 'role';
	$scope.role = {};
	$scope.role.roleId = $rootScope.roleId | 1;

	$http.get('/api/getRoleAccesses/' + $scope.role.roleId).then(function (response) {
		$scope.roleAccesses = response.data;
		for (var int = 0; int < $scope.roleAccesses.length; int++) {
			if ($scope.role.roleId === 1)
				$scope.roleAccesses[int].roleAccessId = 0;
			$scope.roleAccesses[int].rd = $scope.roleAccesses[int].access === 2;
			$scope.roleAccesses[int].vo = $scope.roleAccesses[int].access === 1;
			$scope.roleAccesses[int].na = $scope.roleAccesses[int].access === 0;
		}
	}, function error(response) {
		$scope.postResultMessage = "Error Status: " +  response.statusText;
	});

	$http.get('/dashboard/allRoles').then(function (response) {
		$scope.roles = response.data;
	}, function error(response) {
		$scope.postResultMessage = "Error Status: " +  response.statusText;
	});

	$scope.rd = true;
	$scope.vo = false;
	$scope.na = false;
	$scope.nameError = "";

	$scope.checkUnique = function() {
		$scope.nameError = "";
		for (var int = 0; int < $scope.roles.length; int++) {
			if (('ROLE_' + $scope.role.roleName).toUpperCase() === $scope.roles[int].roleName.toUpperCase()) {
				$scope.nameError = "Role Name Already Exists";
				break;
			}
		}
	}

	$scope.checkALL = function(x) {
		$scope.rd = (x === 'rd');
		$scope.vo = (x === 'vo');
		$scope.na = (x === 'na');
		for (var int = 0; int < $scope.roleAccesses.length; int++) {
			$scope.roleAccesses[int].rd = $scope.rd;
			$scope.roleAccesses[int].vo = $scope.vo;
			$scope.roleAccesses[int].na = $scope.na;
		}
	}

	$scope.resetCheckBoxes = function(x, index) {
		$scope.roleAccesses[index].rd = (x === 'rd');
		$scope.roleAccesses[index].vo = (x === 'vo');
		$scope.roleAccesses[index].na = (x === 'na');
	}

	$scope.saveRoleName = function() {
		for (var int = 0; int < $scope.roleAccesses.length; int++)
			$scope.roleAccesses[int].access =  $scope.roleAccesses[int].rd ? 2 : $scope.roleAccesses[int].vo ? 1 : 0;
		$http({
			url : '/dashboard/roleSave',
			method : "POST",
			data : {
				'roleAccesses' : $scope.roleAccesses,
				'role' : $scope.role
			}
		}).then(function(response) {
			$scope.role.roleId = response.data[0];
			$scope.roleAccesses = response.data[1];
			for (var int = 0; int < $scope.roleAccesses.length; int++) {
				if ($scope.role.roleId === 1)
					$scope.roleAccesses[int].roleAccessId = 0;
				$scope.roleAccesses[int].rd = $scope.roleAccesses[int].access === 2;
				$scope.roleAccesses[int].vo = $scope.roleAccesses[int].access === 1;
				$scope.roleAccesses[int].na = $scope.roleAccesses[int].access === 0;
			}
			alert("success");
		}, function(response) {
			alert("failed");
		});
	}
});
sportsApp.controller('rolesRestoreController', function($scope, $rootScope, $http) {
	$rootScope.active = 'role';

	$http.get('/dashboard/allDeletedRoles').then(function (response) {
		$scope.roles = response.data;
	}, function error(response) {
		$scope.postResultMessage = "Error Status: " +  response.statusText;
	});

	$scope.deleteRole = function(fl, index) {
		$http.get('/dashboard/restoreRole/' + fl.roleId).then(function (response) {
			$scope.roles.splice(index, 1);
			$rootScope.deletedEntry();
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
}); 

sportsApp.filter('roleFilter', function() {
    return function(x) {
    	if (x && x.length > 5)
    		return x.substring(5, x.length);
    	return '';
    };
});