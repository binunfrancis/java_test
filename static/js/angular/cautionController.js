

sportsApp.controller('cautionController', function($scope, $rootScope, $filter, $http, Flash) {
	
	$rootScope.active = 'account';
	$scope.disablerefund = true;
	$scope.refunderror = false;
	$scope.errormessage="";
	$scope.recDate = "NA";
	$scope.memName = "NA";
	$scope.recAmount = 	"NA";
	$scope.refAmount = "NA";
	
	//loadCaution();
	
	$scope.$watch("refundingAmount",function(){
		if(Number($scope.refundingAmount)>Number($scope.refAmount)){
			$scope.refunderror = true;
			$scope.disablerefund = true;
			$scope.errormessage="Amount cannot be greater than Re fundable amount";
		}else if(Number($scope.refundingAmount) ==0){
			$scope.refunderror = true;
			$scope.disablerefund = true;
			$scope.errormessage="Amount cannot be 0";
		}else if(Number($scope.refundingAmount) < 0){
			$scope.refunderror = true;
			$scope.disablerefund = true;
			$scope.errormessage="Amount cannot be less than 0";
		}
		else if($scope.refundingAmount == undefined){		
			$scope.disablerefund = true;
		}else{
		
			$scope.refunderror = false;
			$scope.disablerefund = false;
			$scope.errormessage="";
		}
		
	});
	function loadCaution(){
		$http.get('/dashboard/allAccountsWithCaution').then(function (response) {
			console.log(response.data);
			$scope.accounts = response.data;
			$rootScope.initPagination($scope.accounts.length)
			console.log('dsfdf.................'+$scope.accountsFile);
//			for (var int = 0; int < $scope.accountList.length; int++) {
//				$scope.accountList[int].paidDate = $filter('date')($scope.accountList[int].paidDate, "dd-MMM-yyyy");
//			}
			
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
	
	//Function fired on clicking getdate button
	$scope.getCaution = function(){
		$http.get('/dashboard/getCheckCautionDepositUpadtedOrNot/'+$scope.recNo).then(function (response) {				
			if(response.data && response.data.length){
				alert(response.data);
			}else{
				$http.get('/dashboard/getCautionDepositForSelectedMember/'+$scope.recNo).then(function (response) {				
					if(response.data){				
						$scope.recDate = response.data.paidDate;
						$scope.memName = response.data.memberId.memberName;
						$scope.recAmount = 	response.data.payableAmount;
						$scope.refAmount = response.data.cautionDeposit;
						$scope.refundingAmount = response.data.cautionDeposit;
					}					
					
				}, function error(response) {
					alert("Record Not Found!");
				});
			}					
			
		}, function error(response) {
			//alert("Record Not Found!");
			$http.get('/dashboard/getCautionDepositForSelectedMember/'+$scope.recNo).then(function (response) {				
				if(response.data){				
					$scope.recDate = response.data.paidDate;
					$scope.memName = response.data.memberId.memberName;
					$scope.recAmount = 	response.data.payableAmount;
					$scope.refAmount = response.data.cautionDeposit;
					$scope.refundingAmount = response.data.cautionDeposit;
				}					
				
			}, function error(response) {
				alert("Record Not Found!");
			});
		});						
	}
	//function fired on clicking re fund button
	$scope.refund = function(){
		
		$http({
			url : '/dashboard/getOldCautionUpdates',
			method : "POST",
			data : {
				'data' : {
				
					"recNo" : $scope.recNo.toString(),
					"refAmount" : $scope.refundingAmount.toString()
				}
			}
			
			
		}).then(function(response) {
			if (response.data) {
				
					if(response.data == "Caution Renewal Success"){
						alert("Request Processed Successfully");
						$scope.recNo="";
						$scope.recDate = "";
						$scope.memName = "";
						$scope.recAmount = 	"";
						$scope.refAmount = "";
						$scope.refundingAmount="";
					}
				
			}
		}, function(response) {
			alert("Failed to process the request!");
		
		});
		
		
	}
	
	
	$scope.updateCautionStatus = function(e,o){
		//take o here
		
		$http.get('/dashboard/getCautionDepositUpd/'+o.accountsId).then(function (response) {
			alert("Updated Successfully");	
			loadCaution();
		}, function error(response) {
			loadCaution();
		});
		
	}

});
sportsApp.filter('formatterCaution', function() {
    return function(x) {
    	if(x == ""){ return "";}
    	if(x =="NA") { return "NA"; }
    	var d = new Date(x);
    	var str = d.getDate() +"-" + (d.getMonth()+1) +"-"+d.getFullYear();
        return str;
    };
});
sportsApp.filter('dateTimeHelper', function() {
    return function(x) {
    if(x==""){ return "";}
       var d = new Date(x);
       d = d.toISOString().slice(0,10);
       return d;
    };
});