sportsApp.controller('masterTypeController', function($scope, $rootScope, $http) {
	$rootScope.active = 'masterType';
	$scope.page = 'Master Type';
	$scope.masterType = {};
	$scope.masterType.masterTypeId = 0;
	$scope.memberShipTypeEdit = {};
	
	loadallMasterTypes();
	function loadallMasterTypes(){

		$http.get('/dashboard/allMasterTypes').then(function (response) {
			sortMasterTypeData(response.data);
			
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
	
	function compare(a,b) {
		  if (a.masterTypeId > b.masterTypeId)
		    return -1;
		  if (a.masterTypeId < b.masterTypeId)
		    return 1;
		  return 0;
	}
	function sortMasterTypeData(data){
		$scope.masterTypes=data.sort(compare);
		console.log(data);
		$rootScope.initPagination($scope.masterTypes.length)
	}

	$scope.addMasterType = function() {
		$scope.masterType = {};
		$scope.masterType.masterTypeId = 0;
		$scope.modalTitle = 'Add';
		$('#myModal').modal('toggle');		
	}

	$scope.saveMasterType = function() {
				
		$http({
			url : '/dashboard/masterTypeSave',
			method : "POST",
			data : {
				'data' : $scope.masterType
			}
		}).then(function(response) {
			$scope.masterType.masterTypeId = response.data;
			if ($.isNumeric($scope.masterType.index))
				$scope.masterTypes[$scope.masterType.index] = $scope.masterType;
			else
				$scope.masterTypes.push($scope.masterType);
			
			$rootScope.addedEntry();
			$scope.masterType = {};
			loadallMasterTypes();
			$rootScope.currentPage = 1;
			$('#myModal').modal('toggle');
			alert("Saved successfully");
		}, function(response) { // optional
			alert("failed");
		});

	}

	$scope.editMasterType = function(event, index) {
		$http.get('/dashboard/getMasterType/' + event.target.id).then(function (response) {
			$scope.masterType = response.data;
			$scope.masterType.index = index;
			$scope.modalTitle = 'Edit';
			$('#myModal').modal('toggle');
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
	
	$scope.deleteMasterType = function(event, index) {
		$http.get('/dashboard/deleteMasterType/' + event.target.id).then(function (response) {
			$scope.masterTypes.splice(index, 1);
			$rootScope.deletedEntry();
			alert("Deleted Successfully");
			loadallMasterTypes();
			$rootScope.currentPage = 1;
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
	$scope.fixTypeSelectoin1 = function(model){
	/*	$scope.masterType.centerCheck = 0;
		$scope.masterType.facilityCheck = 0;
		$scope.masterType.subFacilityCheck = 0;
		if(model==1){
			$scope.masterType.centerCheck = true;
		}
		if(model ==2){
			$scope.masterType.facilityCheck=true;
		}
		if(model ==3){
			$scope.masterType.subFacilityCheck=true;
		}*/
		console.log($scope.masterType.centerCheck);
		
	}
	$scope.fixTypeSelectoin2 = function(model){
		console.log($scope.masterType.facilityCheck)
	}
	$scope.fixTypeSelectoin3 = function(model){
		console.log($scope.masterType.subFacilityCheck)
	}
	
	
});

// create the controller and inject Angular's $scope
sportsApp.controller('masterTypeRestoreController', function($scope, $rootScope, $http) {
	$rootScope.active = 'masterType';

	$http.get('/dashboard/allDeletedMasterTypes').then(function (response) {
		$scope.masterTypes = response.data;
	}, function error(response) {
		$scope.postResultMessage = "Error Status: " +  response.statusText;
	});

	$scope.deleteMasterType = function(rs, index) {
		$http.get('/dashboard/restoreMasterType/' + rs.masterTypeId).then(function (response) {
			$scope.masterTypes.splice(index, 1);
		}, function error(response) {
			$scope.postResultMessage = "Error Status: " +  response.statusText;
		});
	}
	
}); 