<!DOCTYPE html>
<html lang="en-us" id="extr-page">

<!-- Mirrored from 192.241.236.31/themes/preview/smartadmin/1.8.x/ajax/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 02 Feb 2016 04:28:26 GMT -->
<head>
<meta charset="utf-8">
<title> Active Sports Run </title>


<!-- #CSS Links -->
<!-- Basic Styles -->
<link rel="stylesheet" type="text/css" media="screen"
	href="static/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" media="screen"
	href="static/css/font-awesome.min.css">

<!-- SmartAdmin Styles : Caution! DO NOT change the order -->
<link rel="stylesheet" type="text/css" media="screen"
	href="static/css/smartadmin-production-plugins.min.css">
<link rel="stylesheet" type="text/css" media="screen"
	href="static/css/smartadmin-production.min.css">
<link rel="stylesheet" type="text/css" media="screen"
	href="static/css/smartadmin-skins1.min.css">

<!-- SmartAdmin RTL Support -->
<link rel="stylesheet" type="text/css" media="screen"
	href="static/css/smartadmin-rtl.min.css">
<link rel="stylesheet" type="text/css" media="screen" href="static/css/style.css">


<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
<link rel="stylesheet" type="text/css" media="screen"
	href="static/css/demo.min.css">



<!-- #GOOGLE FONT -->
<link rel="stylesheet"
	href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">

<!-- #APP SCREEN / ICONS -->
<!-- Specifying a Webpage Icon for Web Clip 
			 Ref: https://developer.apple.com/library/ios/documentation/AppleApplications/Reference/SafariWebContent/ConfiguringWebApplications/ConfiguringWebApplications.html -->
<link rel="apple-touch-icon"
	href="static/img/splash/sptouch-icon-iphone.png">
<link rel="apple-touch-icon" sizes="76x76"
	href="static/img/splash/touch-icon-ipad.png">
<link rel="apple-touch-icon" sizes="120x120"
	href="static/img/splash/touch-icon-iphone-retina.png">
<link rel="apple-touch-icon" sizes="152x152"
	href="static/img/splash/touch-icon-ipad-retina.png">

<!-- iOS web-app metas : hides Safari UI Components and Changes Status Bar Appearance -->
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">

<!-- Startup image for web apps -->
<link rel="apple-touch-startup-image"
	href="static/img/splash/ipad-landscape.png"
	media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape)">
<link rel="apple-touch-startup-image"
	href="static/img/splash/ipad-portrait.png"
	media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait)">
<link rel="apple-touch-startup-image"
	href="static/img/splash/iphone.png"
	media="screen and (max-device-width: 320px)">
	
<!-- #FAVICONS -->
		<link rel="shortcut icon" href="img/favicon/favicon.ico" type="image/x-icon">
		<link rel="icon" href="img/favicon/favicon.ico" type="image/x-icon">
<link rel="manifest" href="static/fev/manifest.json">

<style>
#extr-page #header #logo {
	margin-top: 14px;
	margin-left: 0px;
}
</style>
</head>

<body class="animated fadeInDown">

	<header id="header">

		<div id="logo-group">
			<span id="logo"> <img src="static/img/logo.png"
				alt="SmartAdmin">
			</span>
		</div>

		<!-- <span id="extr-page-header-space"> <span class="hidden-mobile hidden-xs">Need an account?</span> <a href="register.html" class="btn btn-danger">Create account</a> </span>
 -->
	</header>

	<div id="main" role="main">

		<!-- MAIN CONTENT -->
		<div id="content" class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-5 col-lg-4 col-md-offset-4 t-20">
					<div class="well no-padding">
						<form role="form" action="login" method="post" id="login-form"
							class="smart-form client-form">

							<header> Sign In </header>
							<input type="hidden" name="${_csrf.parameterName}"
								value="${_csrf.token}" />
							<fieldset>

								<section>
									<label class="label">E-mail</label> <label class="input">
										<i class="icon-append fa fa-user"></i> <input type="text"
										name="username" id="email" required> <b
										class="tooltip tooltip-top-right"><i
											class="fa fa-user txt-color-teal"></i> Please enter email
											address/username</b>
									</label>
								</section>

								<section>
									<label class="label">Password</label> <label class="input">
										<i class="icon-append fa fa-lock"></i> <input type="password"
										name="password" id="password" required> <b
										class="tooltip tooltip-top-right"><i
											class="fa fa-lock txt-color-teal"></i> Enter your password</b>
									</label>
<!-- 									<div class="note"> -->
<!-- 										<a href="forgotpassword.html">Forgot password?</a> -->
<!-- 									</div> -->
								</section>

								<section>
									<label class="checkbox"> <input type="checkbox"
										name="remember-me" id="remember-me"> <i></i>Stay
										signed in
									</label>
								</section>
								<p style="text-align: center; color: red;">${error }</p>
							</fieldset>
							<footer>
								<button type="submit" class="btn btn-primary">Sign in</button>
							</footer>
						</form>
					</div>

					<!-- 	<h5 class="text-center"> - Or sign in using -</h5>
															
							<ul class="list-inline text-center">
								<li>
									<a href="javascript:void(0);" class="btn btn-primary btn-circle"><i class="fa fa-facebook"></i></a>
								</li>
								<li>
									<a href="javascript:void(0);" class="btn btn-info btn-circle"><i class="fa fa-twitter"></i></a>
								</li>
								<li>
									<a href="javascript:void(0);" class="btn btn-warning btn-circle"><i class="fa fa-linkedin"></i></a>
								</li>
							</ul> -->

				</div>
			</div>
		</div>

	</div>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
	<script>
		if (!window.jQuery) {
			document.write('<script src="static/js/libs/jquery-2.1.1.min.js"><\/script>');
		}
	</script>
	<script src="static/js/jquery.backstretch.min.js"></script>
	<script>
		$.backstretch(["static/img/bgs/bg_1.jpg",
		               "static/img/bgs/bg_2.jpg",
		               "static/img/bgs/bg_3.jpg",
		               "static/img/bgs/bg_4.jpg",
		               "static/img/bgs/bg_5.jpg",
		               "static/img/bgs/bg_6.jpg"], {duration: 3000, fade:1000})
	</script>
</body>

</html>