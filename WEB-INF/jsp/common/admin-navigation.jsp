<aside id="left-panel">
	<div class="login-info">
		<span>
			<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
				<img src="../static/img/avatars/male.png" alt="me" class="online" />
				<span> ${sessionScope.currentUser } </span>
				<i class="fa fa-angle-down"></i>
			</a>
		</span>
	</div>
		<nav>
		<ul ng-controller="menuController">
			<li ng-class="active === 'welcome' ? 'active' : ''">
				<a href="#" title="Home">
					<i class="fa fa-lg fa-fw fa-home"></i>
					<span class="menu-item-parent">Home</span>
				</a>
			</li>
			<li ng-class="active === 'dashboard' ? 'active' : ''">
				<a href="#dashboard" title="Dashboard">
					<i class="fa fa-lg fa-fw fa-home"></i>
					<span class="menu-item-parent">Dashboard</span>
				</a>
			</li>
			<li class="top-menu-invisible">
				<a href="#">
					<i class="fa fa-lg fa-fw fa-user"></i>
					<span class="menu-item-parent">Account</span>
				</a>
				<ul>
					<li ng-class="active === 'role' ? 'active' : ''"><a href="#roleSports">Role</a></li>
					
				</ul>
			</li>
			<li ng-class="active === 'booking' ? 'active' : ''">
				<a href="#booking" title="Bookings">
					<i class="fa fa-shopping-cart"></i> <span class="menu-item-parent">Booking</span>
				</a>
			</li>
			<li class="top-menu-invisible">
				<a href="#">
					<i class="fa fa-lg fa-fw fa-user"></i>
					<span class="menu-item-parent">Master</span>
				</a>
				<ul>
					<li ng-class="active === 'memberShipType' ? 'active' : ''"><a href="#memberShipSports">Membership Type</a></li>
					<li ng-class="active === 'masterType' ? 'active' : ''"><a href="#masterTypeSports">Pricing Master Type</a></li>
					<li ng-class="active === 'location' ? 'active' : ''"><a href="#locationSports">Location</a></li>
					<li ng-class="active === 'centerType' ? 'active' : ''"><a href="#centerTypeSports">Center Type</a></li>
					<li ng-class="active === 'facilityType' ? 'active' : ''"><a href="#facilityTypeSports">Facility Type</a></li>
					<li ng-class="active === 'leaveType' ? 'active' : ''"><a href="#leaveTypeSports">Leave Type</a></li>
					<li ng-class="active === 'leaveTab' ? 'active' : ''"><a href="#leaveTabSports">Leave</a></li>
					<li ng-class="active === 'holiday' ? 'active' : ''"><a href="#holiday">Holiday</a></li>
					<li ng-class="active === 'matPrice' ? 'active' : ''"><a href="#matPriceSports">Mat Price</a></li>
					<li ng-class="active === 'trainer' ? 'active' : ''"><a href="#trainerSports">Trainers</a></li>
					<li ng-class="active === 'trainingCharge' ? 'active' : ''"><a href="#trainerChargeSports">Training Charge</a></li>
				</ul>
			</li>
			<li class="top-menu-invisible">
				<a href="#">
					<i class="fa fa-lg fa-fw fa-trophy"></i>
					<span class="menu-item-parent">Sports</span>
				</a>
				<ul>
				<li ng-class="active === 'staff' ? 'active' : ''"><a href="#staffSports">Staff</a></li>
					<li ng-class="active === 'member' ? 'active' : ''"><a href="#memberSports">Member</a></li>
					<li ng-class="active === 'memberRenewal' ? 'active' : ''"><a href="#memberRenewalSports">Member Renewal</a></li>
					<li ng-class="active === 'center' ? 'active' : ''"><a href="#centerSports">Center</a></li>
					<li ng-class="active === 'facility' ? 'active' : ''"><a href="#facility">Facility</a></li>
					<li ng-class="active === 'pricing' ? 'active' : ''"><a href="#pricing">Pricing</a></li>
					<li ng-class="active === 'timeTable' ? 'active' : ''"><a href="#timeSlotsSports">Time Table</a></li>
					<li ng-class="active === 'booking' ? 'active' : ''"><a href="#booking">Booking</a></li>
					<li ng-class="active === 'viewBooking' ? 'active' : ''"><a href="#viewBooking">Booking Reports</a></li>
					<li ng-class="active === 'editBooking' ? 'active' : ''"><a href="#editBooking">Edit Booking</a></li>
					<li ng-class="active === 'viewAccounts' ? 'active' : ''"><a href="#viewAccounts">Accounts Reports</a></li>
					 <li ng-class="active === 'memberHistory' ? 'active' : ''"><a href="#memberHistory">Member History</a></li>
					<li ng-class="active === 'customerEntry' ? 'active' : ''"><a href="#customerEntry">Member Entry</a></li>
					<li ng-class="active === 'caution' ? 'active' : ''"><a href="#caution">Caution Deposit Refund</a></li>
				</ul>
			</li>
			<li ng-class="active === 'background' ? 'active' : ''">
				<a href="#background" title="Background Images">
					<i class="fa fa-lg fa-fw fa-dashboard"></i> <span class="menu-item-parent">Background Images</span>
				</a>
			</li>
			<li ng-class="active === 'changePassword' ? 'active' : ''">
				<a href="#changePassword" title="Change Password">
					<i class="fa fa-lg fa-fw fa-key"></i> <span class="menu-item-parent">Change Password</span>
				</a>
			</li>
		</ul>
	</nav>
	<span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i></span>
</aside>