<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<title></title>
<%@include file="../common/headercss.jsp"%>

<!-- HEADER -->
<header id="header">
<div id="logo-group">

	<!-- PLACE YOUR LOGO HERE -->
	<span id="logo"> <img src="../static/img/logo.png" alt="Active Sports Run">
	</span>
	<!-- END LOGO PLACEHOLDER -->

	<!-- AJAX-DROPDOWN : control this dropdown height, look and feel from the LESS variable file -->
	<div class="ajax-dropdown">

		<!-- the ID links are fetched via AJAX to the ajax container "ajax-notifications" -->
		<!-- <div class="btn-group btn-group-justified" data-toggle="buttons">
						<label class="btn btn-default">
							<input type="radio" name="activity" id="../../static/ajax/notify/mail.html">
							Msgs (14) </label>
						<label class="btn btn-default">
							<input type="radio" name="activity" id="../../static/ajax/notify/notifications.html">
							notify (3) </label>
						<label class="btn btn-default">
							<input type="radio" name="activity" id="../../static/ajax/notify/tasks.html">
							Tasks (4) </label>
					</div> -->

		<!-- notification content -->
		<div class="ajax-notifications custom-scroll">

			<ul class="notification-body">
				<c:forEach items="${notifications}" var="notifi">
				
				<c:if test="${notifi.read == true }">
				<li>
				<span class="padding-10"> 
				<em class="badge padding-5 no-border-radius pull-left margin-right-5">
				<i class="fa fa-warning fa-fw fa-2x"></i> 
				</em>
				<span><a href="${sessionScope.role == 'ROLE_STORE_MANAGER' ? 'showNotifications' : ' ' }" >
				<b>${notifi.title }</b>
				</a>  
				<span class="state pull-right"> 
				<a rel="tooltip" data-placement="bottom" data-original-title="Readed" > 
				<i class="fa fa-check-circle-o fa-lg txt-color-red pull-right"></i>
				</a>
				</span> 
				<br> 
				<span>${notifi.message}</span> 
				<span class="pull-right font-xs text-muted" id="readBy${notifi.notificationId}">
				${notifi.readByName}
				</span>
				</span>
      		</span>
     		</li>
			</c:if>
			
			<c:if test="${notifi.read == false }">
				
				<li>
				
				<span class="padding-10"> 
				<em class="badge padding-5 no-border-radius pull-left margin-right-5">
				<i class="fa fa-warning fa-fw fa-2x"></i> 
				</em>
				<span><b>${notifi.title }</b> 
				<span class="state pull-right"> 
				<a rel="tooltip" data-placement="bottom" data-original-title="Mark as read"
				onclick="markRead(${notifi.notificationId},'${notifi.read}')" style="cursor: pointer;"> 
				<i class="fa fa-circle-o fa-lg txt-color-red pull-right" id="circle${notifi.notificationId}"></i>
				</a>
				</span> 
				<br> 
				<span>${notifi.message}</span> 
				<span class="pull-right font-xs text-muted" id="readBy${notifi.notificationId}">
				<fmt:formatDate value="${notifi.createDate}" var="formattedDate" type="date" pattern="dd-MM-yyyy hh:mma" />
				${formattedDate}
				
				</span>
						
			</span>
      </span>
     </li>
     </c:if>
     
     
 </c:forEach>
</ul>

			<!-- <div class="alert alert-transparent">
							<h4>Click a button to show messages here</h4>
							This blank page message helps protect your privacy, or you can show the first message here automatically.
						</div> -->

			<!-- <i class="fa fa-lock fa-4x fa-border"></i> -->

		</div>
		<!-- end notification content -->

		<!-- footer: refresh area -->
		<fmt:formatDate value="<%=new Date()%>" var="formattedDate"
			type="date" pattern="dd/MM/yyyy hh:mma" />
		<span> Last updated on: ${formattedDate}
			<button type="button" onclick="refreshNotification();"
				data-loading-text="<i class='fa fa-refresh fa-spin'></i> Loading..."
				class="btn btn-xs btn-default pull-right">
				<i class="fa fa-refresh"></i>
			</button>
		</span>
		<!-- end footer -->

	</div>
	<!-- END AJAX-DROPDOWN -->
</div>

<!-- end projects dropdown --> <!-- pulled right: nav area -->
<div class="pull-right">
	<!-- #MOBILE -->
	<!-- Top menu profile link : this shows only when top menu is active -->
	<ul id="mobile-profile-img"
		class="header-dropdown-list hidden-xs padding-5">
		<li class=""><a href="#"
			class="dropdown-toggle no-margin userdropdown" data-toggle="dropdown">
				<img src="../static/img/avatars/sunny.png" alt="John Doe"
				class="online" />
		</a>
			<ul class="dropdown-menu pull-right">
				<li><a href="javascript:void(0);"
					class="padding-10 padding-top-0 padding-bottom-0"><i
						class="fa fa-cog"></i> Setting</a></li>
				<li class="divider"></li>
				<li><a href="profile.html"
					class="padding-10 padding-top-0 padding-bottom-0"> <i
						class="fa fa-user"></i> <u>P</u>rofile
				</a></li>
				<li class="divider"></li>
				<li><a href="javascript:void(0);"
					class="padding-10 padding-top-0 padding-bottom-0"
					data-action="toggleShortcut"><i class="fa fa-arrow-down"></i> <u>S</u>hortcut</a>
				</li>
				<li class="divider"></li>
				<li><a href="javascript:void(0);"
					class="padding-10 padding-top-0 padding-bottom-0"
					data-action="launchFullscreen"><i class=" fa fa-arrows-alt"></i>
						Full <u>S</u>creen</a></li>
				<li class="divider"></li>
				<li><a href="javascript:formSubmit()"
					class="padding-10 padding-top-5 padding-bottom-5"
					data-action="userLogout"><i class="icon-style-sport
					fa fa-sign-out fa-lg"></i> <strong><u>L</u>ogout</strong></a>
				</li>
			</ul></li>
	</ul>

	<c:url value="/logout" var="logoutUrl" />
	<form action="${logoutUrl}" method="post" id="logoutForm">
		<input type="hidden" name="${_csrf.parameterName}"
			value="${_csrf.token}" />
	</form>
	<script>
		function formSubmit() {
			document.getElementById("logoutForm").submit();
		}
	</script>

	<!-- logout button -->
	<div id="logout" class="btn-header transparent pull-right">
		<span> <a href="javascript:formSubmit()" title="Sign Out"
			data-action="userLogout"
			data-logout-msg="You can improve your security further after logging out by closing this opened browser"><i
				class="icon-style-sport fa fa-sign-out"></i></a>
		</span>
	</div>
	<!-- end logout button -->

	<!-- search mobile button (this is hidden till mobile view port) -->
	<div id="search-mobile" class="btn-header transparent pull-right">
		<span> <a href="javascript:void(0)" title="Search"><i
				class="fa fa-search"></i></a>
		</span>
	</div>
	<!-- end search mobile button -->

	<!-- fullscreen button -->
	<div id="fullscreen" class="btn-header transparent pull-right">
		<span> <a href="javascript:void(0);"
			data-action="launchFullscreen" title="Full Screen"><i
				class="icon-style-sport fa fa-arrows-alt"></i></a>
		</span>
	</div>
	<!-- end fullscreen button -->

	<div class="btn-header transparent pull-right hidden-sm hidden-xs"
		style="width: 30px;"></div>
	<!-- end voice command -->



</div>
<!-- end pulled right: nav area --> </header>
<!-- END HEADER -->

<script>
function markRead(id,read) {
	if(read == "true") {
		
	} else {
	$("#circle"+id).removeClass("fa fa-circle-o fa-lg txt-color-red pull-right");
	$.ajax({
		url: "/rest/markReadNotification", 
		data : { id: id },
		success: function(res){
			$("#circle"+id).addClass("fa fa-check-circle-o fa-lg txt-color-red pull-right");
			$("#readBy"+id).html("<i>Read by "+res+"</i>");
		}});
	}
}

function refreshNotification() {
	$.ajax({
		url: "/rest/refreshNotification", 
		success: function(res){
			location.reload();
		}});
}
</script>