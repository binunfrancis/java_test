<div class="bglightpattern container">
	<div class="row">
		<header class="span12">
			<div class="logo dark">
				<p>OUCH!</p>
				<h1>Sorry, the page you are looking for does not exist</h1>
				<h1><a href="#" class="btn btn-info">Go Home</a></h1>
			</div>
		</header>
	</div>
</div>