<div class="bglightpattern container">
	<div class="row">
		<header class="span12">
			<div class="logo dark">
				<p>OOPS!</p>
				<h1>You don't have permission to view this page</h1>
				<h1><a href="#" class="btn btn-info">Go Home</a></h1>
			</div>
		</header>
	</div>
</div>