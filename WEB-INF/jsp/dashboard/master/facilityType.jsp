	<div ng-controller="facilityTypeController"> 

		<!-- RIBBON -->
		<div id="ribbon">
			<!-- breadcrumb -->
			<ol class="breadcrumb">
				<li>Home</li>
				<li>Facility Type</li>
			</ol>
			<!-- end breadcrumb -->

		</div>
		<!-- END RIBBON -->

		<!-- MAIN CONTENT -->
		<div id="content">
			<div class="row">
				<div class="dash col-md-12">
					<div class="breadcrumb">
						<h1 class="page-title txt-color-blueDark">
							<i class="fa-fw fa fa-home"></i>Master
						</h1>
						<a href="#facilityTypeRestore" class="header-a" title="Restore Deleted Facility Types"><i class="fa fa-trash fa-lg"></i></a>
						<a href="javascript:void(0)"  ng-click="addFacilityType()" class="btn small-btn-blue btn-lg pull-right header-btn hidden-mobile"><i class="fa fa-circle-arrow-up fa-lg"></i>Add Facility Type</a>
					</div>
				</div>
			</div>

			<!-- Modal -->
			<div class="modal fade" id="myModal" tabindex="-1" role="dialog">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"
								aria-hidden="true">&times;</button>
							<h4 class="modal-title">{{modalTitle}} Facility Type</h4>
						</div>
						<div class="modal-body no-padding">

							<form class="smart-form"  ng-submit="saveFacilityType()">
							
								<input ng-model="facilityType.facilityTypeId" type="hidden"></input>
								<fieldset>
								
									<section>
									<div class="row">
										<label class="label col col-3">Facility Type Name</label>
										<div class="col col-9">
											<label class="input"> <input ng-model="facilityType.facilityTypeName" onkeyup="checkFacilityTypeName(this.value)"
													type="text"  class="jName" name="facilityTypeName" required="required"></input>
													<span id="nameError"></span>
											</label>
										</div>
									</div>
									<div class="row">
										<label class="label col col-3">Over Booking Allowed?</label>
										<div class="col col-9">
											<label class="input"> <select ng-model="facilityType.bookStatus" 
													  class="form-control" name="facilityTypeStatus" required="required">
													<option value=""></option>  
													<option value="true">Yes</option>
													<option value="false">No</option> 
													</select>
													
											</label>
										</div>
									</div>
									</section>
									
									

									

									
									
								
									
									
								</fieldset>

								<footer>
									<input type="submit" class="btn btn-primary" id="submit" value="Save" ></input>
									<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
									
								</footer>
							</form>


						</div>

					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->

			<!-- widget grid -->
			<section id="widget-grid" class=""> <!-- row -->
			<div class="row">

				<!-- NEW WIDGET START -->
				<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-3"
					data-widget-editbutton="false">

					<header> <span class="widget-icon"> <i
						class="fa fa-table"></i>
					</span>
					<h2>Facility Type</h2>
					

					</header>

					<!-- widget div-->
					<div>

						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->
						</div>
						<!-- end widget edit box -->

						<!-- widget content -->
						<div class="widget-body no-padding">
							<div class="dt-toolbar">
								<div class="col-xs-12 col-sm-6">
									<div class="dataTables_filter">
										<label>
											<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
											<input ng-model="search" class="form-control">
										</label>
									</div>
								</div>
							</div>
							<table class="table table-striped table-bordered table-hover">
								<thead>
									<tr>
										<th data-hide="phone">Facility Type </th>	
										<th data-hide="phone,tablet">Edit</th>
										<th>Delete</th>
									</tr>
								</thead>
								<tbody>
								<tr ng-repeat="rs in facilityTypes | filter : search " ng-show="$index >= showingFrom - 1 && $index <= showingTo - 1">
											<td>{{rs.facilityTypeName}}</td>
											
											<td  ><a href="javascript:void(0)" ng-click="editFacilityType($event, $index)"><i class="fa fa-edit" id="{{rs.facilityTypeId}}"></i></a></td>
											<td ><a href="javascript:void(0)" ng-click="deleteFacilityType($event, $index)"><i class="fa fa-trash-o" id="{{rs.facilityTypeId}}"></i></a></td>
										</tr>
									
								</tbody>
							</table>
							<div pagination></div>
						</div>
						<!-- end widget content -->
					</div>
					<!-- end widget div -->
				</div>
				<!-- end widget --> </article>

			</div>
			</section>

		</div>
		<!-- END MAIN CONTENT -->

	</div>
	<script src="../static/js/sports.min.js"></script>
	<!-- END MAIN PANEL -->
	 <script>
		function checkFacilityTypeName(value) {
			
			$.ajax({
				url : "/rest/checkFacilityTypeName",
				data : {
					name : value
				},
				success : function(res) {
					
					if (res == 'new') {
						$("#nameError").hide();
						$("#button").attr("disabled", false);
					} else {
						$("#nameError").show();
						$("#nameError").html(res).css("color", "orange");
						$("#button").attr("disabled", true);
					}

				}
			});
		}
	</script>

	
	