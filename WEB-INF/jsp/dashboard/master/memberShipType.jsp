	<div ng-controller="memberShipTypeController"> 
<div class="loadingscreen" ng-show="loadtrue">
		<div class="loader"></div>
	</div>
		<!-- RIBBON -->
		<div id="ribbon">
			<!-- breadcrumb -->
			<ol class="breadcrumb">
				<li>Home</li>
				<li>Membership Type</li>
			</ol>
			<!-- end breadcrumb -->

		</div>
		<!-- END RIBBON -->

		<!-- MAIN CONTENT -->
		<div id="content">
			<div class="row">
				<div class="dash col-md-12">
					<div class="breadcrumb">
						<h1 class="page-title txt-color-blueDark">
							<i class="fa-fw fa fa-home"></i>Master
						</h1>
						<a href="#memberShipTypeRestore" class="header-a" title="Restore Deleted Membership Types"><i class="fa fa-trash fa-lg"></i></a>
						<a href="javascript:void(0)" ng-click="addMemberShipType()" class="btn small-btn-blue btn-lg pull-right header-btn hidden-mobile"><i class="fa fa-circle-arrow-up fa-lg"></i>Add Membership Type</a>
					</div>
				</div>
			</div>

			<!-- Modal -->
			<div class="modal fade" id="myModal" tabindex="-1" role="dialog">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"
								aria-hidden="true">&times;</button>
							<h4 class="modal-title">{{modalTitle}} Membership Type</h4>
						</div>
						<div class="modal-body no-padding">

							<form class="smart-form" ng-submit="saveMemberShipType()">

								<fieldset>
								<input ng-model="memberShipType.memberShipId" type="hidden"></input>
									<section>
									<div class="row">
										<label class="label col col-4">Membership Type Name</label>
										<div class="col col-8">
											<label class="input"> <input ng-model="memberShipType.memberShipTypeName" class="jName"
													 type="text" onkeyup="checkShutteringName(this.value)" id="FieldName" required="required"></input>
											<span id="nameError"></span>
											</label>
										</div>
									</div>
									</section>
									
									<section>
									<div class="row">
										<label class="label col col-4">Membership Type Cost</label>
										<div class="col col-8">
											<label class="input"> <input ng-model="memberShipType.memberShipCost"
													type="text" class="jFloat" name="memberShipCost" required="required"></input>
											</label>
										</div>
									</div>
									</section>

									<section>
									<div class="row">
										<label class="label col col-4">Membership Type Discount</label>
										<div class="col col-8">
											<label class="input"> <input ng-model="memberShipType.memberShipDiscount"
													type="text" class="jFloat" name="memberShipDiscount" required="required"></input>
													
											</label>
										</div>
									</div>
									</section>
									
									<section>
									<div class="row">
										<label class="label col col-4">Membership IdCard Amount</label>
										<div class="col col-8">
											<label class="input"> <input ng-model="memberShipType.memberIdCardAmount"
													type="text" class="jFloat" name="memberIdCardAmount" required="required"></input>
													
											</label>
										</div>
									</div>
									</section>
								</fieldset>

								<footer>
									<input type="submit" id="button" class="btn btn-primary" value="Save"></input>
									<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
								</footer>
							</form>


						</div>

					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->

			<!-- widget grid -->
			<section id="widget-grid" class=""> <!-- row -->
			<div class="row">

				<!-- NEW WIDGET START -->
				<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-3"
					data-widget-editbutton="false">

					<header> <span class="widget-icon"> <i
						class="fa fa-table"></i>
					</span>
					<h2>Membership Type</h2>

					</header>

					<!-- widget div-->
					<div>

						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->
						</div>
						<!-- end widget edit box -->

						<!-- widget content -->
						<div class="widget-body no-padding">
							<div class="dt-toolbar">
								<div class="col-xs-12 col-sm-6">
									<div class="dataTables_filter">
										<label>
											<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
											<input ng-model="search" class="form-control">
										</label>
									</div>
								</div>
							</div>
							<table id="datatable_tabletools"
								class="table table-striped table-bordered table-hover"
								width="100%">
								<thead>
									<tr>
										<th data-hide="phone">Membership Type Name</th>
										<th data-hide="phone">Membership Type Cost</th>
										<th data-hide="phone">Membership Type Discount</th>
										<th data-hide="phone">Membership ID Card Amount</th>
										<th data-hide="phone,tablet" >Edit</th>
										<th >Delete</th>
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="rs in memberShipTypes | filter : search "   ng-show="$index >= showingFrom - 1 && $index <= showingTo - 1">
										<td>{{rs.memberShipTypeName}}</td>
										<td>{{rs.memberShipCost}}</td>
										<td>{{rs.memberShipDiscount}}</td>
										<td>{{rs.memberIdCardAmount}}</td>
										<td ><a href="javascript:void(0)" ng-click="editMemberShipType($event, $index)"><i class="fa fa-edit" id="{{rs.memberShipId}}"></i></a></td>
										<td ><a href="javascript:void(0)" ng-click="deleteMemberShipType($event, $index)"><i class="fa fa-trash-o" id="{{rs.memberShipId}}"></i></a></td>
									</tr>
								</tbody>
							</table>
							<div pagination></div>
						</div>
						<!-- end widget content -->
					</div>
					<!-- end widget div -->
				</div>
				<!-- end widget --> </article>

			</div>
			</section>

		</div>
		<!-- END MAIN CONTENT -->

	</div>
	<script src="../static/js/sports.min.js"></script>
	<!-- END MAIN PANEL -->
	<script>
		function checkShutteringName(value) {
			
			$.ajax({
				url : "/rest/checkMemberShipTypeName",
				data : {
					name : value
				},
				success : function(res) {
					
					if (res == 'new') {
						$("#nameError").hide();
						$("#button").attr("disabled", false);
					} else {
						$("#nameError").show();
						$("#nameError").html(res).css("color", "orange");
						$("#button").attr("disabled", true);
					}

				}
			});
		}
	</script>

<style>
.loadingscreen{
	width: 113%;
    height: 100%;
    position: absolute;
    opacity: 0.7;
    background-color: grey;
    z-index: 999999999999;
    margin-left: -149px;
    margin-top: -73px;
    }
    .loader {
      border: 3px solid #f3f3f3;
    border-radius: 50%;
    border-top: 3px solid #3498db;
    width: 80px;
    height: 80px;
    -webkit-animation: spin 2s linear infinite;
    margin: 0 auto;
    margin-top: 20%;
    animation: spin 2s linear infinite;
}

/* Safari */
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}</style>
	
  