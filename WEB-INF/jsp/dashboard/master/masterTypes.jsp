	<div ng-controller="masterTypeController"> 

		<!-- RIBBON -->
		
		<!-- END RIBBON -->

		<!-- MAIN CONTENT -->
		<div id="content">
			<div class="row">
				<div class="dash col-md-12">
					<div class="breadcrumb">
						<h1 class="page-title txt-color-blueDark">
							<i class="fa-fw fa fa-home"></i>Master
						</h1>
						<a href="#masterTypeRestore" class="header-a" title="Restore Deleted Master Types"><i class="fa fa-trash fa-lg"></i></a>
						<a href="javascript:void(0)"  ng-click="addMasterType()" class="btn small-btn-blue btn-lg pull-right header-btn hidden-mobile"><i class="fa fa-circle-arrow-up fa-lg"></i>Add Pricing Master Type</a>
					</div>
				</div>
			</div>

			<!-- Modal -->
			<div class="modal fade" id="myModal" tabindex="-1" role="dialog">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"
								aria-hidden="true">&times;</button>
							<h4 class="modal-title">{{modalTitle}} Pricing Master Type</h4>
						</div>
						<div class="modal-body no-padding">

							<form class="smart-form" ng-submit="saveMasterType()">

								<fieldset>
								<input ng-model="masterType.masterTypeId" type="hidden"></input>
									<section>
									<div class="row">
										<label class="label col col-4">Pricing Master Type Name</label>
										<div class="col col-8">
											<label class="input"> <input ng-model="masterType.masterTypeName" class="jName"
													 type="text" onkeyup="checkTypeName(this.value)" id="FieldName" required="required"></input>
											<span id="nameError"></span>
											</label>
										</div>
									</div>
									</section>
									
									<section>
									<div class="row">
									<label class="label col col-4">Select Reserved Items</label>&nbsp;&nbsp;&nbsp;<input type="checkbox" class="single-checkbox"  ng-model="masterType.centerCheck" name="centerCheck" ng-disabled="masterType.facilityCheck==true || masterType.subFacilityCheck==true"  />
									<label>Center</label>&nbsp;&nbsp;&nbsp;<input type="checkbox" class="single-checkbox"  ng-model="masterType.facilityCheck"  name="centerCheck"  ng-disabled="masterType.centerCheck==true || masterType.subFacilityCheck==true" />
									<label>Facility</label>&nbsp;&nbsp;&nbsp;<input type="checkbox" class="single-checkbox"  ng-model="masterType.subFacilityCheck"  name="centerCheck"  ng-disabled="masterType.centerCheck==true || masterType.facilityCheck==true" /><label>SubFacility</label>
										</div>
										
									
									</section>

								</fieldset>

								<footer>
									<input type="submit" id="button" class="btn btn-primary" value="Save"></input>
									<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
								</footer>
							</form>


						</div>

					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->

			<!-- widget grid -->
			<section id="widget-grid" class=""> <!-- row -->
			<div class="row">

				<!-- NEW WIDGET START -->
				<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-3"
					data-widget-editbutton="false">

					<header> <span class="widget-icon"> <i
						class="fa fa-table"></i>
					</span>
					<h2>Pricing Master Type</h2>

					</header>

					<!-- widget div-->
					<div>

						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->
						</div>
						<!-- end widget edit box -->

						<!-- widget content -->
						<div class="widget-body no-padding">
							<div class="dt-toolbar">
								<div class="col-xs-12 col-sm-6">
									<div class="dataTables_filter">
										<label>
											<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
											<input ng-model="search" class="form-control">
										</label>
									</div>
								</div>
							</div>
							<table id="datatable_tabletools"
								class="table table-striped table-bordered table-hover"
								width="100%">
								<thead>
									<tr>
										<th data-hide="phone">Master Type Name</th>
										<th data-hide="phone">Center</th>
										<th data-hide="phone">Facility</th>
										<th data-hide="phone">SubFacility</th>
										
										<th data-hide="phone,tablet">Edit</th>
										<th>Delete</th>
									</tr>
								</thead>
								<tbody>
								
										<tr ng-repeat="rs in masterTypes | filter : search"  ng-show="$index >= showingFrom - 1 && $index <= showingTo - 1">
											<td>{{rs.masterTypeName}}</td>
											
											
										   <td><input type="checkbox" ng-disabled="true"  ng-model="rs.centerCheck"/></td>
										
											<td><input type="checkbox" ng-disabled="true"  ng-model="rs.facilityCheck"/></td>
											
											<td><input type="checkbox" ng-disabled="true" ng-model="rs.subFacilityCheck"/></td>
											<td ><a href="javascript:void(0)" ng-click="editMasterType($event, $index)"><i class="fa fa-edit" id="{{rs.masterTypeId}}"></i></a></td>
											<td ><a href="javascript:void(0)" ng-click="deleteMasterType($event, $index)"><i class="fa fa-trash-o" id="{{rs.masterTypeId}}"></i></a></td>
										</tr>
									
								</tbody>
							</table>
						<div pagination></div>
						</div>
						<!-- end widget content -->
					</div>
					<!-- end widget div -->
				</div>
				<!-- end widget --> </article>

			</div>
			</section>

		</div>
		<!-- END MAIN CONTENT -->

	</div>
	<script src="../static/js/sports.min.js"></script>
	<!-- END MAIN PANEL -->
	<script>
		function checkTypeName(value) {
			
			$.ajax({
				url : "/rest/checkTypeName",
				data : {
					name : value
				},
				success : function(res) {
					
					if (res == 'new') {
						$("#nameError").hide();
						$("#button").attr("disabled", false);
					} else {
						$("#nameError").show();
						$("#nameError").html(res).css("color", "orange");
						$("#button").attr("disabled", true);
					}

				}
			});
		}
	</script>
	<script>
	/*
	var limit = 1;
	$('input.single-checkbox').on('change', function(evt) {
	   if($(this).siblings(':checked').length >= limit) {
	       this.checked = false;
	   }
	});*/
	</script>


	
  