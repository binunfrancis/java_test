<div ng-controller="trainerChargeRestoreController">
	<div id="ribbon">
		<ol class="breadcrumb">
			<li>Home</li>
			<li>Training Charge</li>
		</ol>
	</div>
	<div id="content">
		<div class="row">
			<div class="dash col-md-12">
				<div class="breadcrumb">
					<h1 class="page-title txt-color-blueDark">
							<i class="fa-fw fa fa-home"></i> Master <span>
						</h1>
						<a href="#trainerChargeSports" class="header-a" title="Go Back"><i class="fa fa-chevron-left fa-lg"></i></a>
					</div>
				</div>
			</div>

			<section id="widget-grid" class=""><div class="row"><article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="jarviswidget jarviswidget-color-blueDark">
				<header>
					<span class="widget-icon"> <i class="fa fa-table"></i> </span>
					<h2>Training Charge</h2>
				</header>
				<div>
					<div class="widget-body no-padding">
						<div class="dt-toolbar">
							<div class="col-xs-12 col-sm-6">
								<div class="dataTables_filter">
									<label>
										<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
										<input ng-model="search" class="form-control">
									</label>
								</div>
							</div>
						</div>
						<table class="table table-striped table-bordered table-hover">
								<thead>
									<tr>
										<th data-hide="phone">Trainer Name</th>
										<th data-hide="phone">Trainer Charge</th>
<!-- 										<th data-hide="phone">Membership Type Discount</th> -->
										<th data-hide="phone,tablet">Restore</th>
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="rs in trainingCharges | filter : search">
										<td>{{rs.trainerName}}</td>
										<td>{{rs.trainerCharge}}</td>
										<td><a href="javascript:void(0)" ng-click="deleteTrainingCharge(rs, $index)"><i class="fa fa-undo"></i></a></td>
									</tr>
							</tbody>
						</table>

					</div>
				</div>
			</div>
		</article></div></section>
	</div>
</div>