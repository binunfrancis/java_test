	<div ng-controller="trainerController"> 

		<!-- RIBBON -->
		<div id="ribbon">
			<!-- breadcrumb -->
			<ol class="breadcrumb">
				<li>Home</li>
				<li>Trainers</li>
			</ol>
			<!-- end breadcrumb -->

		</div>
		<!-- END RIBBON -->

		<!-- MAIN CONTENT -->
		<div id="content">
			<div class="row">
				<div class="dash col-md-12">
					<div class="breadcrumb">
						<h1 class="page-title txt-color-blueDark">
							<i class="fa-fw fa fa-home"></i>Master
						</h1>
						<a href="#trainerChargeRestore" class="header-a" title="Restore Deleted Trainer Charge"><i class="fa fa-trash fa-lg"></i></a>
						<a href="javascript:void(0)"   ng-click="addTrainer()" class="btn small-btn-blue btn-lg pull-right header-btn hidden-mobile"><i class="fa fa-circle-arrow-up fa-lg"></i>Add Training Charge</a>
					</div>
				</div>
			</div>

			<!-- Modal -->
			<div class="modal fade" id="myModal" tabindex="-1" role="dialog">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"
								aria-hidden="true">&times;</button>
							<h4 class="modal-title">{{modalTitle}} Trainer</h4>
						</div>
						<div class="modal-body no-padding">

							<form class="smart-form" ng-submit="saveTrainer()">

								<fieldset>
								<input ng-model="trainingCharge.trainerId" type="hidden"></input>
									<section>
									<div class="row">
										<label class="label col col-4">Trainer Name</label>
										<div class="col col-8">
											<label class="input"> <input ng-model="trainer.trainerName" class="jName"
													 type="text" onkeyup="checkTrainerName(this.value)" id="FieldName" required="required"></input>
											<span id="nameError"></span>
											</label>
										</div>
									</div>
									</section>
									
									
									
									
									<section>
									<div class="row">
										<label class="label col col-4">Trainer Email</label>
										<div class="col col-8">
											<label class="input"> <input ng-model="trainer.emailId"
													type="email" class="" name="emailId" required="required"></input>
											</label>
										</div>
									</div>
									</section>
									
									<section>
									<div class="row">
										<label class="label col col-4">Trainer PhoneNo</label>
										<div class="col col-8">
											<label class="input"> <input ng-model="trainer.phoneNo"
													type="text" class="jFloat" name="phoneNo" required="required"></input>
											</label>
										</div>
									</div>
									</section>

									
								</fieldset>

								<footer>
									<input type="submit" id="button" class="btn btn-primary" value="Save"></input>
									<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
								</footer>
							</form>


						</div>

					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->

			<!-- widget grid -->
			<section id="widget-grid" class=""> <!-- row -->
			<div class="row">

				<!-- NEW WIDGET START -->
				<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-3"
					data-widget-editbutton="false">

					<header> <span class="widget-icon"> <i
						class="fa fa-table"></i>
					</span>
					<h2>Trainers</h2>

					</header>

					<!-- widget div-->
					<div>

						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->
						</div>
						<!-- end widget edit box -->

						<!-- widget content -->
						<div class="widget-body no-padding">
							<div class="dt-toolbar">
								<div class="col-xs-12 col-sm-6">
									<div class="dataTables_filter">
										<label>
											<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
											<input ng-model="search" class="form-control">
										</label>
									</div>
								</div>
							</div>
							<table id="datatable_tabletools"
								class="table table-striped table-bordered table-hover"
								width="100%">
								<thead>
									<tr>
										<th data-hide="phone">Trainer Name</th>
										<th data-hide="phone">Trainer PhoneNo</th>
										<th data-hide="phone">Trainer Email</th>
										<th data-hide="phone,tablet" >Edit</th>
										<th >Delete</th>
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="rs in trainers | filter : search "   ng-show="$index >= showingFrom - 1 && $index <= showingTo - 1">
										<td>{{rs.trainerName}}</td>
										<td>{{rs.phoneNo}}</td>
										<td>{{rs.emailId}}</td>
										<td ><a href="javascript:void(0)" ng-click="editTrainer($event, $index)"><i class="fa fa-edit" id="{{rs.trainerFinalId}}"></i></a></td>
										<td ><a href="javascript:void(0)" ng-click="deleteTrainer($event, $index)"><i class="fa fa-trash-o" id="{{rs.trainerFinalId}}"></i></a></td>
									</tr>
								</tbody>
							</table>
							<div pagination></div>
						</div>
						<!-- end widget content -->
					</div>
					<!-- end widget div -->
				</div>
				<!-- end widget --> </article>

			</div>
			</section>

		</div>
		<!-- END MAIN CONTENT -->

	</div>
	<script src="../static/js/sports.min.js"></script>
	<!-- END MAIN PANEL -->
	<script>
		function checkTrainerName(value) {
			
			$.ajax({
				url : "/rest/checkTrainerName",
				data : {
					name : value
				},
				success : function(res) {
					
					if (res == 'new') {
						$("#nameError").hide();
						$("#button").attr("disabled", false);
					} else {
						$("#nameError").show();
						$("#nameError").html(res).css("color", "orange");
						$("#button").attr("disabled", true);
					}

				}
			});
		}
	</script>


	
  