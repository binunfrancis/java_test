<div id="ribbon">
	<ol class="breadcrumb">
		<li>Home</li>
		<li>Holiday</li>
	</ol>
</div>
<div id="content">
	<div class="row">
		<div class="dash col-md-12">
			<div class="breadcrumb">
				<h1 class="page-title txt-color-blueDark">
					<i class="fa-fw fa fa-home"></i>Sports
				</h1>
					<a href="javascript:void(0)" ng-click="addHoliday()" class="btn small-btn-blue btn-lg pull-right header-btn hidden-mobile"><i class="fa fa-circle-arrow-up fa-lg"></i>Add Holiday</a>
			</div>
		</div>
	</div>
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title">Add Holiday</h4>
				</div>
				<div class="modal-body no-padding">
					<form class="smart-form" ng-submit="saveHoliday()">
					<fieldset>
					<input ng-model="centerType.centerTypeId" type="hidden"></input>
						
							<section><div class="row">
								<label class="label col col-3">Date</label>
								<div class="col col-9">
									<label class="input">
										<label class="input"><input readonly="readonly" class="jDate" ng-model="holiday.date" type="text" required="required"></input></label>
									</label>
								</div>
							</div></section>
							
							<input ng-model="holiday.holidayId" type="hidden"></input>
									<section>
									<div class="row">
										<label class="label col col-3">Description</label>
										<div class="col col-9">
											<label class="input">
											 <input ng-model="holiday.description" class="jName" type="text" required="required"></input>
											</label>
										</div>
									</div>
									</section>
						</fieldset>
						<footer>
							<input type="submit" id="button" class="btn btn-primary" value="Save"></input>
									<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
						</footer>
					</form>
				</div>
			</div>
		</div>
	</div>
	<section id="widget-grid" class=""><div class="row"><article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="jarviswidget jarviswidget-color-blueDark">
			<header>
				<span class="widget-icon"> <i class="fa fa-table"></i> </span>
				<h2>Holiday</h2>
			</header>
			<div>
				<div class="widget-body no-padding">
					<div class="dt-toolbar">
						<div class="col-xs-12 col-sm-6">
							<div class="dataTables_filter">
								<label>
									<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
									<input ng-model="search" class="form-control">
								</label>
							</div>
						</div>
					</div>
					<table class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
							<tr>
							<th data-hide="phone">Holiday</th>
									<th data-hide="phone"> Description</th>
									
								
								<th>Delete</th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="rs in holidays | filter : search"  ng-show="$index >= showingFrom - 1 && $index <= showingTo - 1">
								<td>{{ rs.date }}</td>
								<td>{{ rs.description }}</td>
								<td  ><a href="javascript:void(0)" ng-click="deleteHoliday(rs.holidayId)"><i class="fa fa-trash-o"></i></a></td>
							</tr>
						</tbody>
					</table>
					<div pagination></div>
				</div>
			</div>
		</div>
	</article></div></section>
</div>
<script src="../static/js/sports.min.js"></script>