<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html ng-app="sportsApp">
<head>
	<meta charset="utf-8"></meta>
	<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"></meta>-->
	<title>Active Sports Run</title>
	<meta name="description" content=""></meta>
	<meta name="author" content=""></meta>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"></meta>
	<script src="../static/js/libs/jquery-2.1.1.min.js"></script>
	<script src="../static/js/libs/jquery-ui-1.10.3.min.js"></script>
	<script src="../static/js/pdfmake.min.js"></script>
  	<script src="../static/js/html2canvas.min.js"></script>
	<script src="../static/js/jquery.timepicker.min.js"></script>
	<script src="../static/js/angular/angular.min.js"></script>
	<script src="../static/js/angular/angular-route.js"></script>
	<script src="../static/js/angular/script.js"></script>
	<script src="../static/js/angular/memberController.js"></script>
	<script src="../static/js/angular/memberRenewController.js"></script>
	<script src="../static/js/angular/memberHistoryController.js"></script>
	<script src="../static/js/angular/memberEntryController.js"></script>
	<script src="../static/js/angular/editBookingController.js"></script>
	<script src="../static/js/angular/staffController.js"></script>
	<script src="../static/js/angular/centerController.js"></script>
	<script src="../static/js/angular/memberShipTypeController.js"></script>
	<script src="../static/js/angular/facilityController.js"></script>
	<script src="../static/js/angular/leaveTabController.js"></script>
	<script src="../static/js/angular/centerTypeController.js"></script>
	<script src="../static/js/angular/facilityTypeController.js"></script>
	<script src="../static/js/angular/leaveTypeController.js"></script>
	<script src="../static/js/angular/locationController.js"></script>
	<script src="../static/js/angular/bookingController.js"></script>
	<script src="../static/js/angular/roleController.js"></script>
	<script src="../static/js/angular/viewBookController.js"></script>
	<script src="../static/js/angular/timeSlotsController.js"></script>
	<script src="../static/js/angular/masterTypeController.js"></script>
	<script src="../static/js/angular/accountController.js"></script>
	<script src="../static/js/angular/matPriceController.js"></script>
	<script src="../static/js/angular/pricingController.js"></script>
	<script src="../static/js/angular/holidayController.js"></script>
	<script src="../static/js/angular/trainingChargeController.js"></script>
	<script src="../static/js/angular/trainerController.js"></script>
	<script src="../static/js/angular/cautionController.js"></script>
	
	 <link rel="stylesheet" href="../static/css/table/angular-datatables.min.css"> 
	 <link rel="stylesheet" href="../static/css/table/buttons.dataTables.min.css">
	  <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/angular_material/1.1.0/angular-material.min.css">
	
</head>
<body class="smart-style-6">
	<%@ include file="../common/header.jsp"%>
	<%@ include file="../common/navigation.jsp"%>

	<div id="main" role="main">
		<div ng-view></div>
	</div>
	
	<div id="myModalsCheck" class="modal fade" role="dialog">
  <div class="modal-dialog height20">
    <!-- Modal content-->
    <div class="modal-content">

      <div class="modal-body signIn-padding"  style="display:inline-block;">
      <div class="center-block">
      <a href="#booking" class="signin-width btn btn-default btn-lg pull-right header-btn hidden-mobile" onclick="$('#myModalsCheck').modal('hide')"> Booking</a>
      
        <button type="button" class="signin-width btn btn-default btn-lg pull-right header-btn hidden-mobile" data-dismiss="modal">Surfing</button>
        </div>
<!--        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
      </div>
<!--       <div class="modal-footer"> -->
<!--         <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
<!--       </div> -->
    </div>

  </div>
</div>

	<%@ include file="../common/footer-html.jsp"%>
	<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
	<script data-pace-options='{ "restartOnRequestAfter": true }'
		src="../static/js/plugin/pace/pace.min.js"></script>

	<!-- IMPORTANT: APP CONFIG -->
	<script src="../static/js/app.config.js"></script>

	<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
	<script
		src="../static/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script>


	<!-- BOOTSTRAP JS -->
	<script src="../static/js/bootstrap/bootstrap.min.js"></script>

	<!-- CUSTOM NOTIFICATION -->
	<script src="../static/js/notification/SmartNotification.min.js"></script>

	<!-- JARVIS WIDGETS -->
	<script src="../static/js/smartwidgets/jarvis.widget.min.js"></script>

	<!-- EASY PIE CHARTS -->
	<script
		src="../static/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>

	<!-- SPARKLINES -->
	<script src="../static/js/plugin/sparkline/jquery.sparkline.min.js"></script>

	<!-- JQUERY VALIDATE -->
	<script
		src="../static/js/plugin/jquery-validate/jquery.validate.min.js"></script>

	<!-- JQUERY MASKED INPUT -->
	<script
		src="../static/js/plugin/masked-input/jquery.maskedinput.min.js"></script>

	<!-- JQUERY SELECT2 INPUT -->
	<script src="../static/js/plugin/select2/select2.min.js"></script>

	<!-- JQUERY UI + Bootstrap Slider -->
	<script
		src="../static/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>

	<!-- browser msie issue fix -->
	<script src="../static/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>

	<!-- FastClick: For mobile devices -->
	<script src="../static/js/plugin/fastclick/fastclick.min.js"></script>

	<!--[if IE 8]>

		<h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>

		<![endif]-->

	<!-- Demo purpose only -->
	<script src="../static/js/demo.min.js"></script>

	<!-- MAIN APP JS FILE -->
	<script src="../static/js/app.min.js"></script>

	<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
	<!-- Voice command : plugin -->
	<script src="../static/js/speech/voicecommand.min.js"></script>

	<!-- SmartChat UI : plugin -->
	<script src="../static/js/smart-chat-ui/smart.chat.ui.min.js"></script>
	<script src="../static/js/smart-chat-ui/smart.chat.manager.min.js"></script>

	<!-- PAGE RELATED PLUGIN(S) -->

	<!-- Flot Chart Plugin: Flot Engine, Flot Resizer, Flot Tooltip -->
	<script src="../static/js/plugin/flot/jquery.flot.cust.min.js"></script>
	<script src="../static/js/plugin/flot/jquery.flot.resize.min.js"></script>
	<script src="../static/js/plugin/flot/jquery.flot.time.min.js"></script>
	<script src="../static/js/plugin/flot/jquery.flot.tooltip.min.js"></script>

	<!-- Vector Maps Plugin: Vectormap engine, Vectormap language -->
	<script
		src="../static/js/plugin/vectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script src="http://jvectormap.com/js/jquery-jvectormap-in-mill.js"></script>
	<!-- <script src="../static/js/plugin/vectormap/jquery-jvectormap-world-mill-en.js"></script> -->

	<!-- Full Calendar -->
	<script src="../static/js/plugin/moment/moment.min.js"></script>
	<script
		src="../static/js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>

	<!-- Morris Chart Dependencies -->
	<script src="../static/js/plugin/morris/raphael.min.js"></script>
	<script src="../static/js/plugin/morris/morris.min.js"></script>

	<!-- PAGE RELATED PLUGIN(S) -->
	<script src="../static/js/plugin/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
	<script src="../static/js/plugin/fuelux/wizard/wizard.min.js"></script>

	<script src="../static/js/calendar.js"></script>
 <script src="../static/js/table/jquery.dataTables.min.js"></script>
  <script src="../static/js/table/angular-datatables.min.js"></script>
  
  <script src="../static/js/table/angular-datatables.buttons.min.js"></script>
 <script src="../static/js/table/dataTables.buttons.min.js"></script>
<script src="../static/js/table/buttons.colVis.min.js"></script>
<script src="../static/js/table/buttons.print.min.js"></script>
 <script src="../static/js/table/angular-animate.min.js"></script> 
<!--   <script src="../static/js/table/angular-aria.min.js"></script> -->
<!--   <script src="../static/js/table/angular-messages.min.js"></script> -->
<script src="../static/js/table/angular-material.min.js"></script>
<script src="../static/js/table/dataTables.columnFilter.js"></script>
<script src="../static/js/table/angular-datatables.columnfilter.min.js"></script>
<script src="../static/js/table/angular-datatables.bootstrap.min.js"></script>

	<script> 
	$(window).load(function(){        
// 		var firstTime=localStorage.getItem("firstTime");
// 		if(!firstTime){
// 			localStorage.setItem("firstTime",true);
// 				}
		// $('#myModalsCheck').modal('show');
			
    }); </script>
    <style>
    .signin-width{
   		 width: 43%;
		margin: 0px 20px;
		background-color:rgba(12,63,207, 0.8);
		border: 1px solid #fff;
		color: #fff;
		text-transform: uppercase;
    }
    .signin-main-width{
    width: 100%;
    }
    .signIn-padding
    {
    	padding: 25px 1%;
		width: 100%;
		background-color: rgba(12,63,207, 0.8);
    
    }
    .height20{
		margin: 20% auto;    
    }
    #myModalsCheck{ 
     pointer-events:none !important; 
    } 
    #myModalsCheck .modal-content{ 
    pointer-events:auto !important; 
    } 
    
    
    </style>
    

</body>
</html>