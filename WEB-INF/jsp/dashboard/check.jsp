		<div id="ribbon">
			<ol class="breadcrumb">
				<li>Home</li>
				<li>Dashboard</li>
			</ol>
		</div>
		<div id="content">
			<div class="row">
				<div class="dash col-md-12">
					<div class="breadcrumb">
						<h1 class="page-title txt-color-blueDark">
							<i class="fa-fw fa fa-home"></i> Dashboard <span>
						</h1>
					</div>
				</div>
			</div>
			<section id="widget-grid" class="">
				<div class="container-2">
					<div id="page-wrapper">
						<div class="row">
							<div class="row">
								<div class="col-lg-2 col-sm-2">
									<a href="userSports">
										<div class="circle-tile">
											<div class="circle-tile-content dark-blue">
												<i class="fa fa-users fa-fw fa-5x text-faded"></i>
												<div class="circle-tile-footer">User <i class="fa fa-chevron-circle-right"></i></div>
											</div>
										</div>
									</a>
								</div>
								<div class="col-lg-2 col-sm-2">
									<a href="institutionSports">
										<div class="circle-tile">
											<div class="circle-tile-content green">
												<i class="fa fa-university fa-fw fa-5x text-faded"></i>
												<div class="circle-tile-footer">Institution <i class="fa fa-chevron-circle-right"></i></div>
											</div>
										</div>
									</a>
								</div>
								<div class="col-lg-2 col-sm-2">
									<a href="financialYearSports">
										<div class="circle-tile">
											<div class="circle-tile-content orange">
												<i class="fa fa-line-chart fa-fw fa-5x text-faded"></i>
												<div class="circle-tile-footer">Financial Year <i class="fa fa-chevron-circle-right"></i></div>
											</div>
										</div>
									</a>
								</div>
								<div class="col-lg-2 col-sm-2">
									<a href="branchSports">
										<div class="circle-tile">
											<div class="circle-tile-content blue">
												<i class="fa fa-building fa-fw fa-5x text-faded"></i>
												<div class="circle-tile-footer">Branch <i class="fa fa-chevron-circle-right"></i></div>
											</div>
										</div>
									</a>
								</div>
								<div class="col-lg-2 col-sm-2">
									<a href="memberSports">
										<div class="circle-tile">
											<div class="circle-tile-content red">
												<i class="fa fa-user fa-fw fa-5x text-faded"></i>
												<div class="circle-tile-footer">Member <i class="fa fa-chevron-circle-right"></i></div>
											</div>
										</div>
									</a>
								</div>
								<div class="col-lg-2 col-sm-2">
									<a href="roleSports">
										<div class="circle-tile">
											<div class="circle-tile-content purple">
												<i class="fa fa-universal-access fa-fw fa-5x text-faded"></i>
												<div class="circle-tile-footer">Role <i class="fa fa-chevron-circle-right"></i></div>
											</div>
										</div>
									</a>
								</div>
								<div class="col-lg-2 col-sm-2">
									<a href="centerSports">
										<div class="circle-tile">
											<div class="circle-tile-content blue">
												<i class="fa fa-map-marker fa-fw fa-5x text-faded"></i>
												<div class="circle-tile-footer">Center <i class="fa fa-chevron-circle-right"></i></div>
											</div>
										</div>
									</a>
								</div>
								<div class="col-lg-2 col-sm-2">
									<a href="centerFacilitySports">
										<div class="circle-tile">
											<div class="circle-tile-content red">
											<i class="fa fa-futbol-o fa-fw fa-5x text-faded"></i>
											<div class="circle-tile-footer">Center Facility <i class="fa fa-chevron-circle-right"></i></div>
										</div>
										</div>
									</a>
								</div>
								<div class="col-lg-2 col-sm-2">
									<a href="facilityTariffSports">
										<div class="circle-tile">
											<div class="circle-tile-content purple">
												<i class="fa fa-inr fa-fw fa-5x text-faded"></i>
												<div class="circle-tile-footer">Facility Tariff <i class="fa fa-chevron-circle-right"></i></div>
											</div>
										</div>
									</a>
								</div>
								<div class="col-lg-2 col-sm-2">
									<a href="bookingSports">
										<div class="circle-tile">
											<div class="circle-tile-content green">
												<i class="fa fa-shopping-cart fa-fw fa-5x text-faded"></i>
												<div class="circle-tile-footer">Booking <i class="fa fa-chevron-circle-right"></i></div>
											</div>
										</div>
									</a>
								</div>
								<div class="col-lg-2 col-sm-2">
									<a href="changePassword">
										<div class="circle-tile">
											<div class="circle-tile-content dark-blue">
												<i class="fa fa-key fa-fw fa-5x text-faded"></i>
												<div class="circle-tile-footer">Change Password <i class="fa fa-chevron-circle-right"></i></div>
											</div>
										</div>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>