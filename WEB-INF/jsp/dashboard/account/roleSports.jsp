<%-- <%@ page language="java" contentType="text/html; charset=ISO-8859-1" --%>
<%-- 	pageEncoding="ISO-8859-1"%> --%>
<!-- <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> -->
<!-- <html ng-app="sportsApp"> -->
<!-- <head> -->
<!-- <meta charset="utf-8"> -->
<!-- <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">--> -->

<!-- <title> Active Sports Run </title> -->
<!-- <meta name="description" content=""> -->
<!-- <meta name="author" content=""> -->

<!-- <meta name="viewport" -->
<!-- 	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"> -->



<!-- </head> -->

<!-- <body class="smart-style-6" ng-controller="dashboardController" > -->

<%-- 	<%@ include file="../../common/header.jsp"%> --%>
<%-- 	<%@ include file="../../common/navigation.jsp"%> --%>


<!-- 	<!-- MAIN PANEL -->
<!-- 	<div id="main" role="main"> -->

		<!-- RIBBON -->
		<div id="ribbon">
			<!-- breadcrumb -->
			<ol class="breadcrumb">
				<li>Home</li>
				<li>Role</li>
			</ol>
			<!-- end breadcrumb -->

		</div>
		<!-- END RIBBON -->

		<!-- MAIN CONTENT -->
		<div id="content">
			<div class="row">
				<div class="dash col-md-12">
					<div class="breadcrumb">
						<h1 class="page-title txt-color-blueDark">
							<i class="fa-fw fa fa-home"></i> Account <span>
						</h1>
					</div>
				</div>
			</div>
			<!-- <div class="row" style="margin-bottom: 20px;">
				<div class="col-md-3 col-md-offset-9">
					Button trigger modal
					<a data-toggle="modal" href="#myModal"
						class="btn btn-success btn-lg pull-right header-btn hidden-mobile">
						<i class="fa fa-circle-arrow-up fa-lg"></i>Add Role
					</a>
				</div>
			</div> -->

			<!-- Modal -->
			<div class="modal fade" id="myModal" tabindex="-1" role="dialog">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"
								aria-hidden="true">&times;</button>
							<h4 class="modal-title">Add</h4>
						</div>
						<div class="modal-body no-padding">

						


						</div>

					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->

			<!-- widget grid -->
			<section id="widget-grid" class=""> <!-- row -->
			<div class="row">

				<!-- NEW WIDGET START -->
				<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-3"
					data-widget-editbutton="false">

					<header> <span class="widget-icon"> <i
						class="fa fa-table"></i>
					</span>
					<h2>Role</h2>

					</header>

					<!-- widget div-->
					<div>

						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->

						</div>
						<!-- end widget edit box -->

						<!-- widget content -->
						<div class="widget-body no-padding">

							<table id="datatable_tabletools"
								class="table table-striped table-bordered table-hover"
								width="100%">
								<thead>
									<tr>
										<th data-hide="phone">ID</th>
										<th data-hide="phone">Name</th>
									
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${userRoles}" var="rs">
										<tr>
											<td>${rs.userRoleId}</td>
											<td>${rs.role}</td>
											
										</tr>
									</c:forEach>
								</tbody>
							</table>

						</div>
						<!-- end widget content -->
					</div>
					<!-- end widget div -->
				</div>
				<!-- end widget --> </article>

			</div>
			</section>

		</div>
		<!-- END MAIN CONTENT -->

	</div>
	<!-- END MAIN PANEL -->

<%-- 	<%@ include file="../../common/footer-html.jsp"%> --%>

	<!--================================================== -->

<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
	<script data-pace-options='{ "restartOnRequestAfter": true }'
		src="../static/js/plugin/pace/pace.min.js"></script>

	<!-- Link to Google CDN's jQuery + jQueryUI; fall back to local -->
	<script
		src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
	<script>
		if (!window.jQuery) {
			document
					.write('<script src="../static/js/libs/jquery-2.1.1.min.js"><\/script>');
		}
	</script>

	<script
		src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
	<script>
		if (!window.jQuery.ui) {
			document
					.write('<script src="../static/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
		}
	</script>

	<!-- IMPORTANT: APP CONFIG -->
	<script src="../static/js/app.config.js"></script>

	<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
	<script
		src="../static/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script>

	<!-- BOOTSTRAP JS -->
	<script src="../static/js/bootstrap/bootstrap.min.js"></script>

	<!-- CUSTOM NOTIFICATION -->
	<script src="../static/js/notification/SmartNotification.min.js"></script>

	<!-- JARVIS WIDGETS -->
	<script src="../static/js/smartwidgets/jarvis.widget.min.js"></script>

	<!-- EASY PIE CHARTS -->
	<script
		src="../static/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>

	<!-- SPARKLINES -->
	<script src="../static/js/plugin/sparkline/jquery.sparkline.min.js"></script>

	<!-- JQUERY VALIDATE -->
	<script
		src="../static/js/plugin/jquery-validate/jquery.validate.min.js"></script>

	<!-- JQUERY MASKED INPUT -->
	<script
		src="../static/js/plugin/masked-input/jquery.maskedinput.min.js"></script>

	<!-- JQUERY SELECT2 INPUT -->
	<script src="../static/js/plugin/select2/select2.min.js"></script>

	<!-- JQUERY UI + Bootstrap Slider -->
	<script
		src="../static/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>

	<!-- browser msie issue fix -->
	<script src="../static/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>

	<!-- FastClick: For mobile devices -->
	<script src="../static/js/plugin/fastclick/fastclick.min.js"></script>

	<!--[if IE 8]>

		<h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>

		<![endif]-->

	<!-- Demo purpose only -->
	<script src="../static/js/demo.min.js"></script>

	<!-- MAIN APP JS FILE -->
	<script src="../static/js/app.min.js"></script>

	<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
	<!-- Voice command : plugin -->
	<script src="../static/js/speech/voicecommand.min.js"></script>

	<!-- SmartChat UI : plugin -->
	<script src="../static/js/smart-chat-ui/smart.chat.ui.min.js"></script>
	<script src="../static/js/smart-chat-ui/smart.chat.manager.min.js"></script>

	<!-- PAGE RELATED PLUGIN(S) -->

	<!-- Flot Chart Plugin: Flot Engine, Flot Resizer, Flot Tooltip -->
	<script src="../static/js/plugin/flot/jquery.flot.cust.min.js"></script>
	<script src="../static/js/plugin/flot/jquery.flot.resize.min.js"></script>
	<script src="../static/js/plugin/flot/jquery.flot.time.min.js"></script>
	<script src="../static/js/plugin/flot/jquery.flot.tooltip.min.js"></script>

	<!-- Vector Maps Plugin: Vectormap engine, Vectormap language -->
	<script
		src="../static/js/plugin/vectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script src="http://jvectormap.com/js/jquery-jvectormap-in-mill.js"></script>
	<!-- <script src="../static/js/plugin/vectormap/jquery-jvectormap-world-mill-en.js"></script> -->

	<!-- Full Calendar -->
	<script src="../static/js/plugin/moment/moment.min.js"></script>
	<script
		src="../static/js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>

	<!-- Morris Chart Dependencies -->
	<script src="../static/js/plugin/morris/raphael.min.js"></script>
	<script src="../static/js/plugin/morris/morris.min.js"></script>

	<!-- PAGE RELATED PLUGIN(S) -->
	<script src="../static/js/plugin/datatables/jquery.dataTables.min.js"></script>
	<script src="../static/js/plugin/datatables/dataTables.colVis.min.js"></script>
	<script
		src="../static/js/plugin/datatables/dataTables.tableTools.min.js"></script>
	<script
		src="../static/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
	<script
		src="../static/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>

	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"
		type="text/javascript"></script>


	<script>
		$(document)
				.ready(
						function() {

							// DO NOT REMOVE : GLOBAL FUNCTIONS!
							pageSetUp();

							/* BASIC ;*/
							var responsiveHelper_dt_basic = undefined;
							var responsiveHelper_datatable_fixed_column = undefined;
							var responsiveHelper_datatable_col_reorder = undefined;
							var responsiveHelper_datatable_tabletools = undefined;

							var breakpointDefinition = {
								tablet : 1024,
								phone : 480
							};

							$('#datatable_tabletools')
									.dataTable(
											{

												// Tabletools options: 
												//   https://datatables.net/extensions/tabletools/button_options
												"sDom" : "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'T>r>"
														+ "t"
														+ "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
												"oTableTools" : {
													"aButtons" : [
															"copy",
															"csv",
															"xls",
															{
																"sExtends" : "pdf",
																"sTitle" : "LiviC_PDF",
																"sPdfMessage" : "LiviC PDF Export",
																"sPdfSize" : "letter"
															},
															{
																"sExtends" : "print",
																"sMessage" : "Generated by LiviC <i>(press Esc to close)</i>"
															} ],
													"sSwfPath" : "../static/js/plugin/datatables/swf/copy_csv_xls_pdf.swf"
												},
												"autoWidth" : true,
												"preDrawCallback" : function() {
													// Initialize the responsive datatables helper once.
													if (!responsiveHelper_datatable_tabletools) {
														responsiveHelper_datatable_tabletools = new ResponsiveDatatablesHelper(
																$('#datatable_tabletools'),
																breakpointDefinition);
													}
												},
												"rowCallback" : function(nRow) {
													responsiveHelper_datatable_tabletools
															.createExpandIcon(nRow);
												},
												"drawCallback" : function(
														oSettings) {
													responsiveHelper_datatable_tabletools
															.respond();
												}
											});

							/* END TABLETOOLS */
							$( "#dob-add" ).datepicker({dateFormat:'yy-mm-dd'});
						  	$( "#dob-edit" ).datepicker({dateFormat:'yy-mm-dd'});
						});
	</script>



