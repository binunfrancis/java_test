	<div ng-controller="roleEditController"> 

		<!-- RIBBON -->
		<div id="ribbon">
			<!-- breadcrumb -->
			<ol class="breadcrumb">
				<li>Home</li>
				<li>Edit Role</li>
			</ol>
			<!-- end breadcrumb -->

		</div>
		<!-- END RIBBON -->

		<!-- MAIN CONTENT -->
		<div id="content">
			<div class="row">
				<div class="dash col-md-12">
					<div class="breadcrumb">
						<h1 class="page-title txt-color-blueDark">
							<i class="fa-fw fa fa-home"></i>Account
						</h1>
						
					</div>
				</div>
			</div>


			<!-- widget grid -->
			<section id="widget-grid" class=""> <!-- row -->
			<div class="row">

				<!-- NEW WIDGET START -->
				<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-3"
					data-widget-editbutton="false">

					<header> <span class="widget-icon"> <i
						class="fa fa-table"></i>
					</span>
					<h2>Edit Roles</h2>
					

					</header>

					<!-- widget div-->
					<div>

						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->
						</div>
						<!-- end widget edit box -->

						<!-- widget content -->
						<div class="widget-body no-padding">
						<form class="smart-form" id="wizard-1" ng-submit="updateRoleName()">
							<div class="dt-toolbar">
								<div class="col-xs-12 col-sm-6">
<!-- 									<div class="dataTables_filter"> -->
<!-- 										<label> -->
<!-- 											<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span> -->
<!-- 											<input ng-model="search" class="form-control"> -->
<!-- 										</label> -->
<!-- 									</div> -->
									<div class="col col-2">
												<input type="submit" class="btn btn-b1" value="Update" ></input>
											</div>
								</div>
							</div>
							<table class="table table-striped table-bordered table-hover">
							<table class="table table-striped table-bordered table-hover">
								<thead>
									<tr>
										<th>Page</th>
										<th>Read/Write</th>
<!-- 										<th>View Only</th> -->
										<th>No Access</th>
									</tr>
									<tr>
										<th>Select All</th>
										<th><center><input type="checkbox" ng-change="checkALL('rd')" ng-model="rd" /></center></th>
										<th><center><input type="checkbox" ng-change="checkALL('vo')" ng-model="vo" /></center></th>
										<th><center><input type="checkbox" ng-change="checkALL('na')" ng-model="na" /></center></th>
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="x in roleAccesses | filter : search">
										<td>{{ x.pageName }}</td>
										<td><center><input type="checkbox" ng-change="resetCheckBoxes('rd', $index)" ng-model="x.rd" /></center></td>
										<td><center><input type="checkbox" ng-change="resetCheckBoxes('vo', $index)" ng-model="x.vo" /></center></td>
										<td><center><input type="checkbox" ng-change="resetCheckBoxes('na', $index)" ng-model="x.na" /></center></td>
									</tr>
								</tbody>
							</table>
<!-- 								<thead> -->
<!-- 									<tr> -->
<!-- 									<th data-hide="phone">Select</th> -->
<!-- 										<th data-hide="phone">Role Actions</th> -->
										
<!-- <!-- 										<th>Delete</th> -->
<!-- 									</tr> -->
<!-- 								</thead> -->
<!-- 								<tbody> -->
<!-- 									<tr><td> -->
<!-- 									<label class="label col col-3">Role Name</label> -->
<!-- 										<div class="col col-9"> -->
<!-- 											<label class="input"> <input ng-model="role.roleName"  onkeyup="checkRoleName(this.value)" -->
<!-- 													type="text" name="roleName" required="required"></input> -->
<!-- 													<span id="nameError"></span> -->
<!-- 											</label> -->
<!-- 									</td> -->
<!-- 									<td><div class="col-md-4 col-md-offset-4" > -->
<!-- 									<button type="submit" class="btn btn-lg txt-color-darken" style="width: 100%;" >Save</button>  -->
<!-- 								</div></td></tr> -->
									
<!-- 									<tr ng-repeat="rs in roleAccesses | filter : search"> -->
<!-- 									<td><input type="checkbox"  ng-model="rs.isActive" /></td> -->
<!-- 										<td>{{rs.listingName}}</td> -->
										
<!-- 									</tr> -->
<!-- 								</tbody> -->
							</table>
						</form>
						</div>
						<!-- end widget content -->
					</div>
					<!-- end widget div -->
				</div>
				<!-- end widget --> </article>

			</div>
			</section>

		</div>
		<!-- END MAIN CONTENT -->

	</div>
	<!-- END MAIN PANEL -->
	 <script>
		function checkRoleName(value) {
			
			$.ajax({
				url : "/rest/checkRoleName",
				data : {
					name : value
				},
				success : function(res) {
					
					if (res == 'new') {
						$("#nameError").hide();
						$("#button").attr("disabled", false);
					} else {
						$("#nameError").show();
						$("#nameError").html(res).css("color", "orange");
						$("#button").attr("disabled", true);
					}

				}
			});
		}
	</script>

	

	