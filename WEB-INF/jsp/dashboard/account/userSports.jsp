<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->

<title> Active Sports Run </title>
<meta name="description" content="">
<meta name="author" content="">

<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">



</head>

<body class="smart-style-6">

	<%@ include file="../../common/header.jsp"%>
	<%@ include file="../../common/navigation.jsp"%>


	<!-- MAIN PANEL -->
	<div id="main" role="main">

		<!-- RIBBON -->
		<div id="ribbon">
			<!-- breadcrumb -->
			<ol class="breadcrumb">
				<li>Home</li>
				<li>User</li>
			</ol>
			<!-- end breadcrumb -->

		</div>
		<!-- END RIBBON -->

		<!-- MAIN CONTENT -->
		<div id="content">
			<div class="row">
				<div class="dash col-md-12">
					<div class="breadcrumb">
						<h1 class="page-title txt-color-blueDark">
							<i class="fa-fw fa fa-home"></i> Account <span>
						</h1>
						<a data-toggle="modal" href="#myModal" class="btn small-btn-blue btn-lg pull-right header-btn hidden-mobile">
							<i class="fa fa-circle-arrow-up fa-lg"></i>Add User
						</a>
					</div>
				</div>
			</div>
			<!-- Modal -->
			<div class="modal fade" id="myModal" tabindex="-1" role="dialog">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"
								aria-hidden="true">&times;</button>
							<h4 class="modal-title">Add User</h4>
						</div>
						<div class="modal-body no-padding">

							<form:form action="userSportsSave" id="login-form"
								class="smart-form" method="post" commandName="userSports"
								onsubmit="document.getElementById('save').disabled=true;">

								<fieldset>
									<section>
									<div class="row">
										<label class="label col col-3">User Type</label>
										<div class="col col-9">
										<label class="input"> <form:select class="form-control" path="userType">
						                <form:option value="Primary User">Primary User</form:option>
						                <form:option value="Secondary User">Secondary User</form:option>
						            
						            </form:select>
											</label>
										</div>
									</div>
									</section>
									
									<section>
									<div class="row">
										<label class="label col col-3">Company</label>
										<div class="col col-9">
											<label class="input"> <form:select id="institution-Id"
												class="form-control" path="institutionId" autocomplete="off"
												required="required">
												<c:forEach items="${institutionList}" var="item">
													<option value="${item.institutionId }">${item.institutionName}</option>
												</c:forEach>
											</form:select> 
											</label>
										</div>
									</div>
									</section>
									
									<section>
									<div class="row">
										<label class="label col col-3">Branch</label>
										<div class="col col-9">
											<label class="input"> <form:select
												class="form-control" path="branchId" autocomplete="off"
												required="required">
												<c:forEach items="${branchList}" var="item">
													<option value="${item.branchId }">${item.branchName}</option>
												</c:forEach>
											</form:select> 
											</label>
										</div>
									</div>
									</section>

									<section>
									<div class="row">
										<label class="label col col-3">First Name</label>
										<div class="col col-9">
											<label class="input"> 
													<form:input path="firstName"
													type="text" name="firstName" required="required"></form:input>
											</label>
										</div>
									</div>
									</section>

									<section>
									<div class="row">
										<label class="label col col-3">Last Name</label>
										<div class="col col-9">
											<label class="input"> <form:input
													path="lastName" type="text" name="lastName"
													required="required"></form:input>
											</label>
										</div>
									</div>
									</section>
									

									<section>
									<div class="row">
										<label class="label col col-3">Date of Birth</label>
										<div class="col col-9">
											<label class="input"> <form:input id="dob-edit"
													path="DOB" type="date" name="DOB"
													required="required"></form:input>
											</label>
										</div>
									</div>
									</section>

									<section>
									<div class="row">
										<label class="label col col-3">Email </label>
										<div class="col col-9">
											<label class="input"> <form:input
													path="email" type="email" name="email"
													required="required"></form:input>
											</label>
										</div>
									</div>
									</section>
									<section>
									<div class="row">
										<label class="label col col-3">Role</label>
										<div class="col col-9">
											<label class="input"> <form:select
												class="form-control" path="userRoleId" autocomplete="off"
												required="required">
												<c:forEach items="${roleList}" var="item">
													<option value="${item.userRoleId }">${item.role}</option>
												</c:forEach>
											</form:select> 
											</label>
										</div>
									</div>
									</section>
									
									<section>
									<div class="row">
										<label class="label col col-3">User ID</label>
										<div class="col col-9">
											<label class="input"> <form:input
													path="userSportsLogId" type="text" name="userSportsLogId"
													required="required"></form:input>
											</label>
										</div>
									</div>
									</section>
									<section>
									<div class="row">
										<label class="label col col-3">Password</label>
										<div class="col col-9">
											<label class="input"> <form:input
													path="password" type="password" name="password"
													required="required"></form:input>
											</label>
										</div>
									</div>
									</section>
									<p style="text-align: center; color: red;">${error }</p>
								</fieldset>

								<footer> <form:button type="submit" id="save"
									class="btn btn-primary">
									Save
								</form:button>
								<button type="button" class="btn btn-default"
									data-dismiss="modal">Cancel</button>

								</footer>
							</form:form>


						</div>

					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->

			<!-- widget grid -->
			<section id="widget-grid" class=""> <!-- row -->
			<div class="row">

				<!-- NEW WIDGET START -->
				<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-3"
					data-widget-editbutton="false">

					<header> <span class="widget-icon"> <i
						class="fa fa-table"></i>
					</span>
					<h2>User</h2>
					<p style="text-align: center; color: red;">${error }</p>

					</header>

					<!-- widget div-->
					<div>

						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->
						</div>
						<!-- end widget edit box -->

						<!-- widget content -->
						<div class="widget-body no-padding">

							<table id="datatable_tabletools"
								class="table table-striped table-bordered table-hover"
								width="100%">
								<thead>
									<tr>
<!-- 										<th data-hide="phone">ID</th> -->
										<th data-hide="phone">Name</th>
										<th data-hide="phone,tablet">User Id</th>
										
										<th data-hide="phone,tablet">Edit</th>
										<th>Delete</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${userSportsLists}" var="rs">
										<tr>
											<td>${rs.firstName}</td>
											<td>${rs.userSportsLogId}</td>
											
											
											<td><a data-toggle="modal" href="#expand${rs.userSportsId}"><i
													class="fa fa-edit "></i></a>

												<div class="modal fade" id="expand${rs.userSportsId}"
													tabindex="-1" role="dialog">
													<div class="modal-dialog">
														<div class="modal-content">
															<div class="modal-header">
																<button type="button" class="close" data-dismiss="modal"
																	aria-hidden="true">&times;</button>
																<h4 class="modal-title">Edit User</h4>
															</div>
															<div class="modal-body no-padding">

																<form:form action="userSportsEdit" id="login-form"
																	class="smart-form" method="post"
																	commandName="userSports"
																	onsubmit="document.getElementById('editButton').disabled=true;">

																	<fieldset>

																		<form:input path="userSportsId" value="${rs.userSportsId}"
																			type="hidden" name="userSportsId" required="required"></form:input>
																		<form:input path="institutionId" value="${rs.institutionId.institutionId}"
																			type="hidden" name="institutionId" required="required"></form:input>
																		<form:input path="branchId" value="${rs.branchId.branchId}"
																			type="hidden" name="branchId" required="required"></form:input>
																		<form:input path="userRoleId" value="${rs.userRoleId.userRoleId}"
																			type="hidden" name="userRoleId" required="required"></form:input>
														 			    <form:input path="password" value="${rs.password}"
																			type="hidden" name="password" required="required"></form:input>
																		 <form:input path="userSportsLogId" value="${rs.userSportsLogId}"
																			type="hidden" name="userSportsLogId" required="required"></form:input>
																		 <form:input path="branchId" value="${rs.branchId}"
																			type="hidden" name="branchId" required="required"></form:input>
																		
																		
																		<section class="">
																		<div class="row">
																			 <label
																				class="label col col-3">First Name </label> <label
																				class="input col col-9"> <form:input
																					value="${rs.firstName}" path="firstName"
																					autocomplete="off" type="text" name="firstName"
																					required="required"></form:input>
																			</label> 
																			
																		</div></section>
																		<section>
																		<div class="row">
																			 <label
																				class="label col col-3">Last Name</label> <label
																				class="input col col-9"><form:input
																					value="${rs.lastName}" path="lastName"
																					autocomplete="off" type="text" name="lastName"
																					required="required"></form:input>
																				
																			</label> 

																		</div></section>
																		<section>
																		<div class="row">
																		
																			 <label
																				class="label col col-3">Date Of Birth</label> <label class="input col col-9">
																				<form:input id="dob-add"
																					value="${rs.DOB}" path="DOB"
																					autocomplete="off" type="date" name="DOB"
																					required="required"></form:input>
																				
																			</label> 
																		</div>
																		</section>

																		<section><div class="row"> <label class="label col col-3">Email</label> <label class="input col col-9"> <form:input
																				autocomplete="off" value="${rs.email}"
																				path="email" type="email"
																				name="email"></form:input>
																		</label> </div></section>
																		
																	

																		

																	</fieldset>

																	<footer> <form:button type="submit"
																		id="editButton" class="btn btn-primary">
																			Save</form:button>
																	<button type="button" class="btn btn-default"
																		data-dismiss="modal">Cancel</button>

																	</footer>
																</form:form>

															</div>

														</div>
														<!-- /.modal-content -->
													</div>
													<!-- /.modal-dialog -->
												</div> <!-- /.modal --></td>
												<td><a href="userSportsDelete?userSportsId=${rs.userSportsId}"><i class="fa fa-trash-o"></i></a></td>
										</tr>
									</c:forEach>
								</tbody>
							</table>

						</div>
						<!-- end widget content -->
					</div>
					<!-- end widget div -->
				</div>
				<!-- end widget --> </article>

			</div>
			</section>

		</div>
		<!-- END MAIN CONTENT -->

	</div>
	<!-- END MAIN PANEL -->

	<%@ include file="../../common/footer-html.jsp"%>

	<!--================================================== -->

	<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
	<script data-pace-options='{ "restartOnRequestAfter": true }'
		src="../static/js/plugin/pace/pace.min.js"></script>

	<!-- Link to Google CDN's jQuery + jQueryUI; fall back to local -->
	<script
		src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
	<script>
		if (!window.jQuery) {
			document
					.write('<script src="../static/js/libs/jquery-2.1.1.min.js"><\/script>');
		}
	</script>

	<script
		src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
	<script>
		if (!window.jQuery.ui) {
			document
					.write('<script src="../static/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
		}
	</script>

	<!-- IMPORTANT: APP CONFIG -->
	<script src="../static/js/app.config.js"></script>

	<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
	<script
		src="../static/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script>

	<!-- BOOTSTRAP JS -->
	<script src="../static/js/bootstrap/bootstrap.min.js"></script>

	<!-- CUSTOM NOTIFICATION -->
	<script src="../static/js/notification/SmartNotification.min.js"></script>

	<!-- JARVIS WIDGETS -->
	<script src="../static/js/smartwidgets/jarvis.widget.min.js"></script>

	<!-- EASY PIE CHARTS -->
	<script
		src="../static/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>

	<!-- SPARKLINES -->
	<script src="../static/js/plugin/sparkline/jquery.sparkline.min.js"></script>

	<!-- JQUERY VALIDATE -->
	<script
		src="../static/js/plugin/jquery-validate/jquery.validate.min.js"></script>

	<!-- JQUERY MASKED INPUT -->
	<script
		src="../static/js/plugin/masked-input/jquery.maskedinput.min.js"></script>

	<!-- JQUERY SELECT2 INPUT -->
	<script src="../static/js/plugin/select2/select2.min.js"></script>

	<!-- JQUERY UI + Bootstrap Slider -->
	<script
		src="../static/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>

	<!-- browser msie issue fix -->
	<script src="../static/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>

	<!-- FastClick: For mobile devices -->
	<script src="../static/js/plugin/fastclick/fastclick.min.js"></script>

	<!--[if IE 8]>

		<h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>

		<![endif]-->

	<!-- Demo purpose only -->
	<script src="../static/js/demo.min.js"></script>

	<!-- MAIN APP JS FILE -->
	<script src="../static/js/app.min.js"></script>

	<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
	<!-- Voice command : plugin -->
	<script src="../static/js/speech/voicecommand.min.js"></script>

	<!-- SmartChat UI : plugin -->
	<script src="../static/js/smart-chat-ui/smart.chat.ui.min.js"></script>
	<script src="../static/js/smart-chat-ui/smart.chat.manager.min.js"></script>

	<!-- PAGE RELATED PLUGIN(S) -->

	<!-- Flot Chart Plugin: Flot Engine, Flot Resizer, Flot Tooltip -->
	<script src="../static/js/plugin/flot/jquery.flot.cust.min.js"></script>
	<script src="../static/js/plugin/flot/jquery.flot.resize.min.js"></script>
	<script src="../static/js/plugin/flot/jquery.flot.time.min.js"></script>
	<script src="../static/js/plugin/flot/jquery.flot.tooltip.min.js"></script>

	<!-- Vector Maps Plugin: Vectormap engine, Vectormap language -->
	<script
		src="../static/js/plugin/vectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script src="http://jvectormap.com/js/jquery-jvectormap-in-mill.js"></script>
	<!-- <script src="../static/js/plugin/vectormap/jquery-jvectormap-world-mill-en.js"></script> -->

	<!-- Full Calendar -->
	<script src="../static/js/plugin/moment/moment.min.js"></script>
	<script
		src="../static/js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>

	<!-- Morris Chart Dependencies -->
	<script src="../static/js/plugin/morris/raphael.min.js"></script>
	<script src="../static/js/plugin/morris/morris.min.js"></script>

	<!-- PAGE RELATED PLUGIN(S) -->
	<script src="../static/js/plugin/datatables/jquery.dataTables.min.js"></script>
	<script src="../static/js/plugin/datatables/dataTables.colVis.min.js"></script>
	<script
		src="../static/js/plugin/datatables/dataTables.tableTools.min.js"></script>
	<script
		src="../static/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
	<script
		src="../static/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>

	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"
		type="text/javascript"></script>


	<script>
		$(document)
				.ready(
						function() {

							// DO NOT REMOVE : GLOBAL FUNCTIONS!
							pageSetUp();

							/* BASIC ;*/
							var responsiveHelper_dt_basic = undefined;
							var responsiveHelper_datatable_fixed_column = undefined;
							var responsiveHelper_datatable_col_reorder = undefined;
							var responsiveHelper_datatable_tabletools = undefined;

							var breakpointDefinition = {
								tablet : 1024,
								phone : 480
							};

							$('#datatable_tabletools')
									.dataTable(
											{

												// Tabletools options: 
												//   https://datatables.net/extensions/tabletools/button_options
												"sDom" : "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'T>r>"
														+ "t"
														+ "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
												"oTableTools" : {
													"aButtons" : [
															"copy",
															"csv",
															"xls",
															{
																"sExtends" : "pdf",
																"sTitle" : "LiviC_PDF",
																"sPdfMessage" : "LiviC PDF Export",
																"sPdfSize" : "letter"
															},
															{
																"sExtends" : "print",
																"sMessage" : "Generated by LiviC <i>(press Esc to close)</i>"
															} ],
													"sSwfPath" : "../static/js/plugin/datatables/swf/copy_csv_xls_pdf.swf"
												},
												"autoWidth" : true,
												"preDrawCallback" : function() {
													// Initialize the responsive datatables helper once.
													if (!responsiveHelper_datatable_tabletools) {
														responsiveHelper_datatable_tabletools = new ResponsiveDatatablesHelper(
																$('#datatable_tabletools'),
																breakpointDefinition);
													}
												},
												"rowCallback" : function(nRow) {
													responsiveHelper_datatable_tabletools
															.createExpandIcon(nRow);
												},
												"drawCallback" : function(
														oSettings) {
													responsiveHelper_datatable_tabletools
															.respond();
												}
											});

							/* END TABLETOOLS */
							$( "#dob-add" ).datepicker({dateFormat:'yy-mm-dd'});
						  	$( "#dob-edit" ).datepicker({dateFormat:'yy-mm-dd'});
						});
	</script>


</body>

</html>

</body>
</html>