	<div ng-controller="roleController"> 

		<!-- RIBBON -->
		<div id="ribbon">
			<!-- breadcrumb -->
			<ol class="breadcrumb">
				<li>Home</li>
				<li>Roles</li>
			</ol>
			<!-- end breadcrumb -->

		</div>
		<!-- END RIBBON -->

		<!-- MAIN CONTENT -->
		<div id="content">
			<div class="row">
				<div class="dash col-md-12">
					<div class="breadcrumb">
						<h1 class="page-title txt-color-blueDark">
							<i class="fa-fw fa fa-home"></i>Account
						</h1>
						<a href="#roleRestore" class="header-a" title="Restore Deleted Center Types"><i class="fa fa-trash fa-lg"></i></a>
						
						<a href="#roleCreation" class="header-a" title="Restore Deleted Center Types">Create Roles</a>
						
					</div>
				</div>
			</div>


			<!-- widget grid -->
			<section id="widget-grid" class=""> <!-- row -->
			<div class="row">

				<!-- NEW WIDGET START -->
				<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-3"
					data-widget-editbutton="false">

					<header> <span class="widget-icon"> <i
						class="fa fa-table"></i>
					</span>
					<h2>Center Type</h2>
					

					</header>

					<!-- widget div-->
					<div>

						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->
						</div>
						<!-- end widget edit box -->

						<!-- widget content -->
						<div class="widget-body no-padding">
							<div class="dt-toolbar">
								<div class="col-xs-12 col-sm-6">
									<div class="dataTables_filter">
										<label>
											<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
											<input ng-model="search" class="form-control">
										</label>
									</div>
								</div>
							</div>
							<table class="table table-striped table-bordered table-hover">
								<thead>
									<tr>
										<th data-hide="phone">Center Type</th>
										<th data-hide="phone,tablet">Edit</th>
										<th>Delete</th>
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="rs in roles | filter : search">
										<td>{{rs.roleName}}</td>
										<td><a href="javascript:void(0)" ng-click="deleteCenterType($event, $index)"><i class="fa fa-trash-o" id="{{rs.centerTypeId}}"></i></a></td>
									</tr>
								</tbody>
							</table>

						</div>
						<!-- end widget content -->
					</div>
					<!-- end widget div -->
				</div>
				<!-- end widget --> </article>

			</div>
			</section>

		</div>
		<!-- END MAIN CONTENT -->

	</div>
	<!-- END MAIN PANEL -->
	 <script>
		function checkCenterTypeName(value) {
			
			$.ajax({
				url : "/rest/checkCenterTypeName",
				data : {
					name : value
				},
				success : function(res) {
					
					if (res == 'new') {
						$("#nameError").hide();
						$("#button").attr("disabled", false);
					} else {
						$("#nameError").show();
						$("#nameError").html(res).css("color", "orange");
						$("#button").attr("disabled", true);
					}

				}
			});
		}
	</script>

	

	