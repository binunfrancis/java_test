	<div ng-controller="rolesController"> 

		<!-- RIBBON -->
		<div id="ribbon">
			<!-- breadcrumb -->
			<ol class="breadcrumb">
				<li>Home</li>
				<li>Roles</li>
			</ol>
			<!-- end breadcrumb -->

		</div>
		<!-- END RIBBON -->

		<!-- MAIN CONTENT -->
		<div id="content">
			<div class="row">
				<div class="dash col-md-12">
					<div class="breadcrumb">
						<h1 class="page-title txt-color-blueDark">
							<i class="fa-fw fa fa-home"></i>Account
						</h1>
						<a href="#rolesRestore" class="header-a" title="Restore Deleted Roles"><i class="fa fa-trash fa-lg"></i></a>
						<a href="#rolesCreate" class="btn small-btn-blue btn-lg pull-right header-btn hidden-mobile">Create Roles</a>
						
					</div>
				</div>
			</div>


			<!-- widget grid -->
			<section id="widget-grid" class=""> <!-- row -->
			<div class="row">

				<!-- NEW WIDGET START -->
				<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-3"
					data-widget-editbutton="false">

					<header> <span class="widget-icon"> <i
						class="fa fa-table"></i>
					</span>
					<h2>Roles</h2>
					

					</header>

					<!-- widget div-->
					<div>

						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->
						</div>
						<!-- end widget edit box -->

						<!-- widget content -->
						<div class="widget-body no-padding">
							<div class="dt-toolbar">
								<div class="col-xs-12 col-sm-6">
									<div class="dataTables_filter">
										<label>
											<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
											<input ng-model="search" class="form-control">
										</label>
									</div>
								</div>
							</div>
							<table class="table table-striped table-bordered table-hover">
								<thead>
									<tr>
										<th data-hide="phone">Role Name</th>
										<th>Edit</th>
										<th>Delete</th>
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="rs in roles | filter : search">
										<td >{{rs.roleName | roleFilter}}</td>
										<td><a href="javascript:void(0)" ng-disabled="rs.roleName === 'ROLE_ADMIN'" ng-click="editRole(rs)"><i class="fa fa-edit" ng-hide="rs.roleName === 'ROLE_ADMIN'" ></i></a></td>
										<td><a href="javascript:void(0)"  ng-click="deleteRole(rs)"><i class="fa fa-trash-o"></i></a></td>
									</tr>
								</tbody>
							</table>
							
						</div>
					</div>
				</div></article>
			</div>
			</section>

		</div>
		<!-- END MAIN CONTENT -->

	</div>
	<!-- END MAIN PANEL -->
	 <script>
		function checkRoleName(value) {
			
			$.ajax({
				url : "/rest/checkRoleName",
				data : {
					name : value
				},
				success : function(res) {
					
					if (res == 'new') {
						$("#nameError").hide();
						$("#button").attr("disabled", false);
					} else {
						$("#nameError").show();
						$("#nameError").html(res).css("color", "orange");
						$("#button").attr("disabled", true);
					}

				}
			});
		}
	</script>
<script src="../static/js/sports.min.js"></script>