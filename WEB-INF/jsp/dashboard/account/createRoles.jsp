	<div ng-controller="rolesCreateController"> 

		<!-- RIBBON -->
		<div id="ribbon">
			<!-- breadcrumb -->
			<ol class="breadcrumb">
				<li>Home</li>
				<li>Create Role</li>
			</ol>
			<!-- end breadcrumb -->

		</div>
		<!-- END RIBBON -->

		<!-- MAIN CONTENT -->
		<div id="content">
			<div class="row">
				<div class="dash col-md-12">
					<div class="breadcrumb">
						<h1 class="page-title txt-color-blueDark">
							<i class="fa-fw fa fa-home"></i>Account
						</h1>
						<a href="#roleSports" class="header-a" title="Go Back"><i class="fa fa-chevron-left fa-lg"></i></a>
					</div>
				</div>
			</div>


			<!-- widget grid -->
			<section id="widget-grid" class=""> <!-- row -->
			<div class="row">

				<!-- NEW WIDGET START -->
				<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-3"
					data-widget-editbutton="false">

					<header> <span class="widget-icon"> <i
						class="fa fa-table"></i>
					</span>
					<h2>Create Roles</h2>
					

					</header>

					<!-- widget div-->
					<div>

						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->
						</div>
						<!-- end widget edit box -->

						<!-- widget content -->
						<div class="widget-body no-padding">
						<form class="smart-form" id="wizard-1" ng-submit="saveRoleName()">
							<div class="dt-toolbar box-sizing-bb">
								<div class="col-xs-12 col-sm-6">
									<div class="dataTables_filter">
										<label>
											<span class="input-group-addon no-padding"><i class="glyphicon glyphicon-search p-10"></i></span>
											<input type="text" ng-model="search" class="form-control">
										</label>
									</div>
								</div>
							</div>
							<fieldset>
								<section>
									<div class="row">
										<div class="col-md-6">
											<label class="label col col-3">Role Name</label>
											<div class="col col-7">
												<label class="input">
													<input ng-model="role.roleName" ng-keyup="checkUnique()" type="text" required="required"></input>
													<span>{{ nameError }}</span>
												</label>
											</div>
											<div class="col col-2">
												<input type="submit" class="btn btn-b1" value="Save" ng-disabled="nameError != ''"></input>
											</div>
										</div>
									</div>
								</section>
							</fieldset>
							<table class="table table-striped table-bordered table-hover">
								<thead>
									<tr>
										<th>Page</th>
										<th>Read/Write</th>
										<th>View Only</th>
										<th>No Access</th>
									</tr>
									<tr>
										<th>Select All</th>
										<th><center><input type="checkbox" ng-change="checkALL('rd')" ng-model="rd" /></center></th>
										<th><center><input type="checkbox" ng-change="checkALL('vo')" ng-model="vo" /></center></th>
										<th><center><input type="checkbox" ng-change="checkALL('na')" ng-model="na" /></center></th>
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="x in roleAccesses | filter : search">
										<td>{{ x.pageName }}</td>
										<td><center><input type="checkbox" ng-change="resetCheckBoxes('rd', $index)" ng-model="x.rd" /></center></td>
										<td><center><input type="checkbox" ng-change="resetCheckBoxes('vo', $index)" ng-model="x.vo" /></center></td>
										<td><center><input type="checkbox" ng-change="resetCheckBoxes('na', $index)" ng-model="x.na" /></center></td>
									</tr>
								</tbody>
							</table>
						</form>
						</div>
					</div>
				</div></article>
			</div>
			</section>
		</div>
	</div>
<script src="../static/js/sports.min.js"></script>