<div ng-controller="changePasswordController">
	<div id="ribbon">
		<ol class="breadcrumb">
			<li>Home</li>
			<li>Change Password</li>
		</ol>
	</div>
	<div id="content">
		<div class="row">
			<div class="dash col-md-12">
				<div class="breadcrumb">
					<h1 class="page-title txt-color-blueDark">
						<i class="fa-fw fa fa-home"></i>Sports
					</h1>
				</div>
			</div>
		</div>
		<section id="widget-grid" class=""><div class="row"><article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="jarviswidget jarviswidget-color-blueDark">
				<header>
					<span class="widget-icon"> <i class="fa fa-table"></i> </span>
					<h2>Change Password</h2>
				</header>
				<div>
					<div class="widget-body no-padding">
						<form class="smart-form" ng-submit="resetPassword()">
							<fieldset>
								<section>
									<label class="input">
										<i class="icon-append fa fa-lock"></i>
										<input type="password" ng-model="newPassword" placeholder="New Password" ng-keyup="setMessageText()" required> <b class="tooltip tooltip-bottom-right">Don't forget your password</b>
									</label>
								</section>
								<section>
									<label class="input">
										<i class="icon-append fa fa-lock"></i>
										<input type="password" placeholder="Confirm Password" ng-model="confirmPassword" ng-keyup="setMessageText()" required> <b class="tooltip tooltip-bottom-right">Don't forget your password</b>
									</label>
									<span ng-class="msgColor">{{ message }}</span>
								</section>
							</fieldset>
							<footer>
								<button type="submit" class="btn btn-primary" id="save" ng-disabled="message != 'Matching'">Save</button>
							</footer>
						</form>
					</div>
				</div>
			</div>
		</article></div></section>
	</div>
</div>
<style>
.red-text{color: red}
.green-text{color: green}
</style>
<script src="../static/js/sports.min.js"></script>