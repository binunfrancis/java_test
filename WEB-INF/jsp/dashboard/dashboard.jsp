		<div id="ribbon">
			<ol class="breadcrumb">
				<li>Home</li>
				<li>Dashboard</li>
			</ol>
		</div>
		<div id="content">
			<div class="row">
				<div class="dash col-md-12">
					<div class="breadcrumb">
						<h1 class="page-title txt-color-blueDark">
							<i class="fa-fw fa fa-home"></i>Dashboard
						</h1>
					</div>
				</div>
			</div>
			<section id="widget-grid" class="">
				<div class="container-2">
					<div id="page-wrapper">
						<div class="row">
							<div class="row">
								
								<div class="col-lg-2 col-sm-2">
									<a href="#memberSports">
										<div class="rect-tile">
											<div class="circle-tile-content border-radius-0 red">
												<i class="fa fa-user fa-fw fa-5x text-faded"></i>
												<div class="circle-tile-footer">Member <i class="fa fa-chevron-circle-right"></i></div>
											</div>
										</div>
									</a>
								</div>
								<div class="col-lg-2 col-sm-2">
									<a href="#roleSports">
										<div class="rect-tile">
											<div class="circle-tile-content border-radius-0 purple">
												<i class="fa fa-universal-access fa-fw fa-5x text-faded"></i>
												<div class="circle-tile-footer">Role <i class="fa fa-chevron-circle-right"></i></div>
											</div>
										</div>
									</a>
								</div>
								<div class="col-lg-2 col-sm-2">
									<a href="#centerSports">
										<div class="rect-tile">
											<div class="circle-tile-content border-radius-0 blue">
												<i class="fa fa-map-marker fa-fw fa-5x text-faded"></i>
												<div class="circle-tile-footer">Center <i class="fa fa-chevron-circle-right"></i></div>
											</div>
										</div>
									</a>
								</div>
								
								<div class="col-lg-2 col-sm-2">
									<a href="#facility">
										<div class="rect-tile">
											<div class="circle-tile-content border-radius-0 purple">
												<i class="fa fa-inr fa-fw fa-5x text-faded"></i>
												<div class="circle-tile-footer">Facility  <i class="fa fa-chevron-circle-right"></i></div>
											</div>
										</div>
									</a>
								</div>
								<div class="col-lg-2 col-sm-2">
									<a href="#booking">
										<div class="rect-tile">
											<div class="circle-tile-content border-radius-0 green">
												<i class="fa fa-shopping-cart fa-fw fa-5x text-faded"></i>
												<div class="circle-tile-footer">Booking <i class="fa fa-chevron-circle-right"></i></div>
											</div>
										</div>
									</a>
								</div>
								<div class="col-lg-2 col-sm-2">
									<a href="#changePassword">
										<div class="rect-tile">
											<div class="circle-tile-content border-radius-0 dark-blue">
												<i class="fa fa-key fa-fw fa-5x text-faded"></i>
												<div class="circle-tile-footer">Change Password <i class="fa fa-chevron-circle-right"></i></div>
											</div>
										</div>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
<script src="../static/js/sports.min.js"></script>