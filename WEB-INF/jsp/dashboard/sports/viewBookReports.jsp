
<%@page import="java.util.Date"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<link rel="stylesheet" type="text/css" media="screen"
	href="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/a549aa8780dbda16f6cff545aeabc3d71073911e/build/css/bootstrap-datetimepicker.css">
<div>
	<!-- MAIN CONTENT -->
	<div id="content">
		<div class="row">
			<div class="dash col-md-12">
				<div class="breadcrumb">
					<h1 class="page-title txt-color-blueDark">
						<i class="fa-fw fa fa-home"></i>Sports
					</h1>

				</div>
			</div>
		</div>
	</div>
	<!-- END MAIN CONTENT -->

</div>
<div class="col col-md-10"></div>

<!-- END MAIN PANEL -->

<section id="widget-grid" class="">
	<!-- row -->
	<div class="bookReportBody">

		<!-- NEW WIDGET START -->
		<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

			<div ng-controller="viewBookController as showCase">
			<div class="col-xs-12">
			<!-- Filter Section -->
			<div class="row filter-box-container col-md-12">
				<div class="row uppersect">
					<div class="col-md-3">
					<label>Start Date</label>
					<input class="jDate1 row form-control" ng-model="startDate" type="text" name="centerE"	required="required" ></input>
					</div>
					<div class=" col-md-3">
					<label>End Date</label>
				 	<input class="jDate1 form-control" ng-model="endDate" type="text" name="centerEndDate" required="required"></input> 
					</div>
					<div class=" col-md-3">
					<label>Choose centre</label>
				 	<select class=" form-control" ng-model="centername" ng-options="x for x in centerNames" placeholder="center name" ng-change="setFacility()">
				 	</select> 
					</div>
				</div>
				<div class="row lowersect">
					<div class=" col-md-3">
					<label>Choose facility</label>
				 	<select class="form-control facilityFilter" ng-model="facilityname" ng-options="x for x in facilityNames" placeholder="facility" ng-change="setSubFacility()">
				 	</select> 
					</div>
					<div class=" col-md-3">
					<label>Choose sub facility</label>
				 	<select class=" form-control" ng-model="subfacilityname" ng-options="x for x in subFacilityNames" placeholder="sub facility">
				 	</select> 
					</div>
					
					<div class=" col-md-3">
				 	<input type="button" class="btn btn-default col-xs-2 bookingFilterBtn" ng-click="reloadData()" value="Filter"></input>
					</div>
				</div>
				
				
			</div>
			
			
			
			
				

			    
			
				<table datatable="" dt-options="showCase.dtOptions" dt-instance="showCase.dtInstance"
					dt-columns="showCase.dtColumns" class="row-border hover table table-striped table-bordered">
					<tfoot>
						<tr>
							<th>ID</th>
							<th> Name</th>
							<th>Booked Date</th>
							<th>Booking Date</th>
							<th>Center Name</th>
							<th> Facility Name</th>
                            <th> Sub Facility Name</th>
                            <th> Session Time</th>
                            <th>Print</th>
						</tr>
 					</tfoot>
				</table>
				
			</div>



	</div>

		</article>

	</div>
</section>


<!-- END MAIN CONTENT -->


<!-- END MAIN PANEL -->





<script src="../static/js/sports.min.js"></script>
<style>
.bookReportBody {
	width: 99%;
	margin-left: 10px;
}

.row {
margin-bottom: 0px !important;
}
tfoot {
    /*display: table-header-group !important;*/
    display:none !important;
}
.dataTables_filter label{
display:none !important;
}
.dataTables_filter input{
display:none !important;
}
.bookingFilterBtn{
width:66%;
    margin-top: 21px;
    background-color: slategrey;
    color: white;
    font-weight: bold;
}
.facilityFilter , .jDate1{
margin-left:0px !important;
}
.row.lowersect{
margin-top:7px;
}
.facilityFilter{
margin-left:-13px;
}
.filter-box-container input,.filter-box-container select{
border-radius:6px !important;
}
.filter-box-container label {
font-weight:bold !important;
}
</style>








