	<div> 

		<!-- RIBBON -->
		<div id="ribbon">
			<!-- breadcrumb -->
			<ol class="breadcrumb">
				<li>Home</li>
				<li>Center</li>
			</ol>
			<!-- end breadcrumb -->

		</div>
		<!-- END RIBBON -->

		<!-- MAIN CONTENT -->
		<div id="content">
			<div class="row">
				<div class="dash col-md-12">
					<div class="breadcrumb">
						<h1 class="page-title txt-color-blueDark">
							<i class="fa-fw fa fa-home"></i>Sports
						</h1>
						<a href="#centerRestore" class="header-a" title="Restore Deleted Centers"><i class="fa fa-trash fa-lg"></i></a>
						<a href="javascript:void(0)" ng-show="roleAccess[page] === 2"  ng-click="addCenter()" class="btn small-btn-blue btn-lg pull-right header-btn hidden-mobile"><i class="fa fa-circle-arrow-up fa-lg"></i>Add Center</a>
					</div>
				</div>
			</div>

			<!-- Modal -->
			<div class="modal fade" id="myModal" tabindex="-1" role="dialog">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"
								aria-hidden="true">&times;</button>
							<h4 class="modal-title">{{modalTitle}} Center</h4>
						</div>
						<div class="modal-body no-padding">

							<form class="smart-form" ng-submit="saveCenter()">

								<fieldset>
									<section>
									<div class="row">
										<label class="label col col-3">Center Name</label>
										<div class="col col-9">
											<label class="input">
												<input ng-model="center.centreName" class="jName" type="text" ng-keyup="checkUnique()" required="required"></input>
												<span>{{ nameError }}</span>
											</label>
										</div>
									</div>
									</section>
									
									
									<section>
									<div class="row">
										<label class="label col col-3">Start Date</label>
										<div class="col col-9">
											<label class="input"><input class="jDate"
													ng-model="center.centerStartDate" readonly="readonly" type="text" name="centerStartDate"
													required="required"></input>
											
											</label>
										</div>
									</div>
									</section>
									<section>
									<div class="row">
										<label class="label col col-3">End Date</label>
										<div class="col col-9">
											<label class="input"><input class="jDate"
													ng-model="center.centerEndDate" readonly="readonly" type="text" name="centerEndDate"
													required="required"></input>
											
											</label>
										</div>
									</div>
									</section>
									
									<section>
									<div class="row">
										<label class="label col col-3">Select Center Type</label>
										<div class="col col-9">
											<label class="input">
												<select ng-options="item.centerTypeId as item.centerTypeName for item in centerTypeList"
													class="form-control" ng-model="center.centerTypeId.centerTypeId" autocomplete="off" required="required">
													<option value=""></option>
												</select>
											</label>
										</div>
									</div>
									</section>
									
									<section>
									<div class="row">
										<label class="label col col-3">Select Location</label>
										<div class="col col-9">
											<label class="input">
												<select ng-options="item.locationId as item.locationName for item in locationList"
													class="form-control" ng-model="center.locationId.locationId" autocomplete="off" required="required">
													<option value=""></option>
												</select>
											</label>
										</div>
									</div>
									</section>
									
								</fieldset>

								<footer>
									<input type="submit" class="btn btn-primary" id="button" value="Save"></input>
									<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
								</footer>
							</form>


						</div>

					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->

			<!-- widget grid -->
			<section id="widget-grid" class=""> <!-- row -->
			<div class="row">

				<!-- NEW WIDGET START -->
				<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-3"
					data-widget-editbutton="false">

					<header> <span class="widget-icon"> <i
						class="fa fa-table"></i>
					</span>
					<h2>Center</h2>
					

					</header>

					<!-- widget div-->
					<div>

						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->
						</div>
						<!-- end widget edit box -->

						<!-- widget content -->
						<div class="widget-body no-padding">
							<div class="dt-toolbar">
								<div class="col-xs-12 col-sm-6">
									<div class="dataTables_filter">
										<label>
											<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
											<input ng-model="search" class="form-control">
										</label>
									</div>
								</div>
							</div>
							<table class="table table-striped table-bordered table-hover">
								<thead>
									<tr>
										<th data-hide="phone">Center Name</th>
										<th data-hide="phone">Center Type</th>
										<th data-hide="phone">Location</th>
										<th data-hide="phone,tablet">Edit</th>
										<th>Delete</th>
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="rs in centers | filter : search | orderBy:'-centreId'  "  ng-show="$index >= showingFrom - 1 && $index <= showingTo - 1">
										<td>{{rs.centreName}}</td>
										<td>{{rs.centerTypeId.centerTypeName}}</td>
										<td>{{rs.locationId.locationName}}</td>
										<td ng-show="roleAccess[page] === 2" ><a href="javascript:void(0)" ng-click="editCenter($event, $index)"><i class="fa fa-edit" id="{{rs.centreId}}"></i></a></td>
										<td ng-show="roleAccess[page] === 2" ><a href="javascript:void(0)" ng-click="deleteCenter($event, $index)"><i class="fa fa-trash-o" id="{{rs.centreId}}"></i></a></td>
									</tr>
								</tbody>
							</table>
						<div pagination></div>
						</div>
						<!-- end widget content -->
					</div>
					<!-- end widget div -->
				</div>
				<!-- end widget --> </article>
			</div>
			</section>
		</div>
		<!-- END MAIN CONTENT -->
	</div>
	<script src="../static/js/sports.min.js"></script>