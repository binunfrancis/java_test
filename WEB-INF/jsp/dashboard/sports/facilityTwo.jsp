<div>
	<div id="ribbon">
		<ol class="breadcrumb">
			<li>Home</li>
			<li>Facility</li>
		</ol>
	</div>
	<div id="content">
		<div class="row">
			<div class="dash col-md-12">
				<div class="breadcrumb">
					<h1 class="page-title txt-color-blueDark">
						<i class="fa-fw fa fa-home"></i>Sports
					</h1>
						<a href="#facilityRestore" class="header-a" title="Restore Deleted Facilities"><i class="fa fa-trash fa-lg"></i></a>
						<a href="javascript:void(0)" ng-show="roleAccess[page] === 2"  ng-click="addFacility()" class="btn small-btn-blue btn-lg pull-right header-btn hidden-mobile"><i class="fa fa-circle-arrow-up fa-lg"></i>Add Facility</a>
				</div>
			</div>
		</div>

		<div class="modal fade" id="subModal" tabindex="-1" role="dialog"><div class="modal-dialog modal-width"><div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Sub Facilities</h4>
			</div>
			<div class="modal-body no-padding"><div class="widget-body"><div class="">
				<table class="table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th data-hide="phone">Sub Facility Name</th>
							
						</tr>
					</thead>
					<tbody>
						<tr ng-repeat="rs in subFacilities">
							<td>{{ rs.subFacilityName }}</td>
							
						</tr>
					</tbody>
				</table>
			</div></div></div>
		</div></div></div>
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog"><div class="modal-dialog modal-width"><div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">{{modalTitle}} Facility</h4>
			</div>
			<div class="modal-body no-padding"><div class="widget-body"><div class="row">
				<form class="smart-form" id="wizard-1" ng-submit="saveFacility()">
					<div id="bootstrap-wizard-1" class="col-sm-12">
						<div class="form-bootstrapWizard mt-25 ml-60">
							<ul class="bootstrapWizard form-wizard">
								<li class="active" data-target="#step1" style="pointer-events: none;">
									<a data-target="#tab1" data-toggle="tab"><span class="step">1</span><span class="title">Basic Information</span></a>
								</li>
								<li data-target="#step2" style="pointer-events: none;">
									<a data-target="#tab2" data-toggle="tab"><span class="step">2</span><span class="title">Sub Facilities</span></a>
								</li>
								<li data-target="#step3" style="pointer-events: none;">
									<a data-target="#tab3" data-toggle="tab"><span class="step">3</span><span class="title">Save Form</span></a>
								</li>
							</ul>
							<div class="clearfix"></div>
						</div>
						<div class="tab-content">
							<div class="tab-pane active" id="tab1"><div class="p-60-40 t-20 t-20-blocker">
								<br>
								<h3><strong>Step 1</strong> - Basic Information</h3>

								<div class="row">
									<div class="col-sm-6"><div class="form-group t-20 t-20-blocker"><div class="input-group">
										<span class="input-group-addon"><i class="fa fa-pencil-square-o fa-lg fa-fw" style="line-height: .75em;"></i></span>
										<input ng-model="facility.facilityName" placeholder="Facility Name"  onkeyup="checkFacilityName(this.value)" class="form-control input-lg jName" type="text" name="name"></input>
										<span>{{ nameError }}</span>
									</div></div></div>
									<div class="col-sm-6"><div class="form-group t-20 t-20-blocker"><div class="input-group">
										<span class="input-group-addon"><i class="fa fa-institution fa-lg fa-fw" style="line-height: .75em;"></i></span>
										<select ng-options="item.centreId as item.centreName for item in centers"
											class="form-control input-lg p-10" ng-model="facility.centerId.centreId" autocomplete="off" name="centre">
											<option value="" ></option>
										</select>
									</div></div></div>
								</div>

<!-- 								<div class="row"> -->
<!-- 									<div class="col-sm-6"><div class="form-group"><div class="input-group"> -->
<!-- 										<span class="input-group-addon"><i class="fa fa-hourglass-start fa-lg fa-fw" style="line-height: .75em;"></i></span> -->
<!-- 										<input ng-model="facility.rateHour" placeholder="Rate Hour" class="form-control input-lg jFloat" type="text" name="hour"></input> -->
<!-- 									</div></div></div> -->
<!-- 									<div class="col-sm-6"><div class="form-group"><div class="input-group"> -->
<!-- 										<span class="input-group-addon"><i class="fa fa-tasks fa-lg fa-fw" style="line-height: .75em;"></i></span> -->
<!-- 										<input ng-model="facility.rateDaily" placeholder="Rate Daily" class="form-control input-lg jFloat" type="text" name="daily"></input> -->
<!-- 									</div></div></div> -->
<!-- 								</div> -->

<!-- 								<div class="row"> -->
<!-- 									<div class="col-sm-6"><div class="form-group"><div class="input-group"> -->
<!-- 										<span class="input-group-addon"><i class="fa fa-calendar-o fa-lg fa-fw" style="line-height: .75em;"></i></span> -->
<!-- 										<input ng-model="facility.rateWeekly" placeholder="Rate Weekly" type="text" class="form-control input-lg jFloat" name="week"></input> -->
<!-- 									</div></div></div> -->
<!-- 									<div class="col-sm-6"><div class="form-group"><div class="input-group"> -->
<!-- 										<span class="input-group-addon"><i class="fa fa-calendar-check-o fa-lg fa-fw" style="line-height: .75em;"></i></span> -->
<!-- 										<input ng-model="facility.rateMonthly" placeholder="Rate Monthly" type="text" class="form-control input-lg jFloat" name="month"></input> -->
<!-- 									</div></div></div> -->
<!-- 								</div> -->

								<div class="row">
									<div class="col-sm-6"><div class="form-group t-20 t-20-blocker"><div class="input-group">
										<span class="input-group-addon"><i class="fa fa-shield fa-lg fa-fw" style="line-height: .75em;"></i></span>
										<select ng-options="item.facilityTypeId as item.facilityTypeName for item in facilityTypes"
											class="form-control input-lg p-10" ng-model="facility.facilityTypeId.facilityTypeId" autocomplete="off" name="facility">
											<option value="" ></option>
										</select>
									</div></div></div>
								</div>
							</div></div>
							<div class="tab-pane" id="tab2"><div class="p-60-40 t-20 t-20-blocker">
								<br>
								<h3><strong>Step 2</strong> - Sub Facilities</h3>

								<div class="row"><div class="col-sm-12 t-20 t-20-blocker">
									<a href="javascript:void(0)" ng-click="addSubFacility()" class="btn small-btn-blue btn-lg pull-right header-btn hidden-mobile"><i class="fa fa-circle-arrow-up fa-lg"></i>Add Sub Facility</a>
									<table class="table table-striped table-bordered table-hover">
										<thead>
											<tr>
												<th data-hide="phone">Sub Facility Name</th>
												
												<th data-hide="phone,tablet">{{modalTitle}}</th>
												<th>Delete</th>
											</tr>
										</thead>
										<tbody>
											<tr ng-repeat="rs in subFacilities" ng-include="getTemplate(rs)">
											</tr>
										</tbody>
									</table>
									<script type="text/ng-template" id="display">
										<td>{{ rs.subFacilityName }}</td>
										
										<td><a href="javascript:void(0)" ng-click="editSubFacility(rs)"><i class="fa fa-edit"></i></a></td>
										<td><a href="javascript:void(0)" ng-click="deleteSubFacility($index, rs.subFacilityId)"><i class="fa fa-trash-o"></i></a></td>
    								</script>
    								<script type="text/ng-template" id="edit">
        								<td><input type="text" class="jName" ng-model="subFacility.subFacilityName" /></td>
        								
										<td><a href="javascript:void(0)" ng-click="saveSubFacility($index)"><i class="fa fa-floppy-o"></i></a>&nbsp;&nbsp;<a href="javascript:void(0)" ng-click="reset()"><i class="fa fa-times "></i></a></td>
										<td><a href="javascript:void(0)" ng-click="deleteSubFacility($index, subFacility.subFacilityId)"><i class="fa fa-trash-o"></i></a></td>
    								</script>
								</div></div>

							</div></div>
							<div class="tab-pane" id="tab3"><div class="p-60-40 t-20 t-20-blocker">
								<br><h3><strong>Step 3</strong> - Save Form </h3><br>
								<h1 class="text-center text-success t-20 t-20-blocker"><strong><i class="fa fa-check fa-lg"></i> Complete</strong></h1>
								<div class="col-md-12" >
									<button type="submit" class="btn btn-lg center-block txt-color-darken" style="width: 20%;" >Save</button> 
								</div>
								<br><br>
							</div></div>
							<div class="form-actions mr">
								<div class="row">
									<div class="col-sm-12">
										<ul class="pager wizard no-margin">
											<li class="previous disabled"><a id="wiz-prev" href="javascript:void(0);" class="btn btn-lg btn-default"> Previous </a></li>
											<li class="next"><a href="javascript:void(0);" class="btn btn-lg txt-color-darken"> Next </a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div></div></div>
		</div></div></div>

		<section id="widget-grid" class=""><div class="row"><article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="jarviswidget jarviswidget-color-blueDark">
				<header>
					<span class="widget-icon"> <i class="fa fa-table"></i> </span>
					<h2>Facility</h2>
				</header>
				<div>
					<div class="widget-body no-padding">
						<div class="dt-toolbar">
							<div class="col-xs-12 col-sm-6">
								<div class="dataTables_filter">
									<label>
										<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
										<input ng-model="search" class="form-control">
									</label>
								</div>
							</div>
						</div>
						<table class="table table-striped table-bordered table-hover">
							<thead>
								<tr>
									<th data-hide="phone">Facility Name</th>
									<th data-hide="phone">Facility Type</th>
									<th data-hide="phone">Center Name</th>
									<th data-hide="phone">Sub Facilities</th>
									<th data-hide="phone,tablet">Edit</th>
									<th>Delete</th>
								</tr>
							</thead>
							<tbody>
								<tr ng-repeat="rs in facilities | filter : search | orderBy:'-facilityId' "  ng-show="$index >= showingFrom - 1 && $index <= showingTo - 1">
									<td>{{ rs.facilityName }}</td>
									<td>{{ rs.facilityTypeId.facilityTypeName }}</td>
									<td>{{ rs.centerId.centreName }}</td>
									<td><a href="javascript:void(0)" ng-click="viewSubFacilities(rs)"><i class="fa fa-eye"></i></a></td>
									<td ng-show="roleAccess[page] === 2" ><a href="javascript:void(0)" ng-click="editFacility($event, $index)"><i class="fa fa-edit" id="{{rs.facilityId}}"></i></a></td>
									<td ng-show="roleAccess[page] === 2" ><a href="javascript:void(0)" ng-click="deleteFacility($event, $index)"><i class="fa fa-trash-o" id="{{rs.facilityId}}"></i></a></td>
								</tr>
							</tbody>
						</table>
						<div pagination></div>
					</div>
				</div>
			</div>
		</article></div></section>
	</div>
</div>
	<script src="../static/js/sports.min.js"></script>
<script>
  var $validator = $("#wizard-1").validate({
		  rules: {
		      name: {
		        required: true,
		        
		      },
		     centre: {
		        required: true
		      },
		      hour: {
		        required: true
		      },
		     daily: {
			    required: true
			  },
			  week: {
				    required: true
				  },
			  month: {
				    required: true
				  },
		      facility: {
		        required: true
		      },
		  },
    highlight: function (element) {
      $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
    },
    unhighlight: function (element) {
      $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
    },
    errorElement: 'span',
    errorClass: 'help-block',
    errorPlacement: function (error, element) {
      if (element.parent('.input-group').length) {
        error.insertAfter(element.parent());
      } else {
        error.insertAfter(element);
      }
    }
  });
  $('#bootstrap-wizard-1').bootstrapWizard({
    'tabClass': 'form-wizard',
    'onNext': function (tab, navigation, index) {
      var $valid = $("#wizard-1").valid();
      if (!$valid) {
        $validator.focusInvalid();
        return false;
      } else {
        $('#bootstrap-wizard-1').find('.form-wizard').children('li').eq(index - 1).addClass('complete');
        $('#bootstrap-wizard-1').find('.form-wizard').children('li').eq(index - 1).find('.step').html('<i class="fa fa-check"></i>');
      }
    }
  });
</script>
<script>
		function checkFacilityName(value) {
			
			$.ajax({
				url : "/rest/checkFacilityName",
				data : {
					name : value
				},
				success : function(res) {
					
					if (res == 'new') {
						$("#nameError").hide();
						$("#button").attr("disabled", false);
					} else {
						$("#nameError").show();
						$("#nameError").html(res).css("color", "orange");
						$("#button").attr("disabled", true);
					}

				}
			});
		}
	</script>