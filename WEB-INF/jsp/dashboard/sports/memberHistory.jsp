<div>
	<div class="loadingscreen" ng-show="loadtrue">
		<div class="loader"></div>
	</div>
	<div id="ribbon">
		<ol class="breadcrumb">
			<li>Home</li>
			<li>Member</li>
		</ol>
	</div>
	<div id="content">
		<div class="row">
			<div class="dash col-md-12">
				<div class="breadcrumb">
					<h1 class="page-title txt-color-blueDark">
						<i class="fa-fw fa fa-home"></i>Sports
					</h1>
				</div>
			</div>
		</div>
		<!--  <div class="row">
			<div class="col-md-3 form-group">
				<label class="control-label">Member ID</label>
				<input type="text" class="form-control"/>
			</div>
			<div class="col-md-3">
			<input type="button" value="View History" class="form-control" />
			</div>
		</div>-->
		<div class="row">
		<form class="form-horizontal">
  <div class="form-group">
    <label for="inputEmail3" class="col-sm-1 control-label">Member ID</label>
    <div class="col-sm-3">
      <input type="text" class="form-control" id="inputEmail3" placeholder="Member ID" ng-model="memberid">
    </div>
    <div class="col-sm-3">
    	<select name="states" id="states" type="text" placeholder="enter member name" ng-model="selectedmemid" ng-options="item.memberId as item.memberName for item in memberList  " class="form-control">
    	<option value="">Select</option>
    	</select>
    </div>
    <div class="col-sm-offset-0 col-sm-4">
      <button type="submit" class="btn filtrBtn" ng-click="getHistory()">Get History</button>
    </div>
  </div>
</form>
		</div>
		
		<div class="row memberGenInfo">
			<div class="col-md-3 form-group">
				<label class="genLabel">Member Name : </label>
				<span ng-bind="memName"></span>
			</div>
			<div class="col-md-3 form-group">
				<label class="genLabel">Member ID : </label>
				<span ng-bind="memId"></span>
			</div>
			<div class="col-md-3 form-group">
				<label class="genLabel">Member DOB : </label>
				<span ng-bind="memAge |formatterHistory"></span>
			</div>
			<div class="col-md-3 form-group">
				<label class="genLabel">Member Mail : </label>
				<span ng-bind="memMail"></span>
			</div>
			
			<div class="col-md-3 form-group">
				<label class="genLabel">Member District : </label>
				<span ng-bind="memDistrict"></span>
			</div>
			
			<div class="col-md-3 form-group">
				<label class="genLabel">Membership Type : </label>
				<span ng-bind="memType"></span>
			</div>
			<div class="col-md-3 form-group">
				<label class="genLabel">Contact Number : </label>
				<span ng-bind="memContact"></span>
			</div>
		</div>

	<div class="row bookingInfo">
		<section id="widget-grid" class=""> <!-- row -->
			<div class="row">

				<!-- NEW WIDGET START -->
				<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-3"
					data-widget-editbutton="false">

					<header> <span class="widget-icon"> <i
						class="fa fa-table"></i>
					</span>
					<h2>Active Booking History</h2>

					</header>

					<!-- widget div-->
					<div>

						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->
						</div>
						<!-- end widget edit box -->

						<!-- widget content -->
						<div class="widget-body no-padding">
							<div class="dt-toolbar">
								<div class="col-xs-12 col-sm-6">
									<div class="dataTables_filter">
										<label>
											<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
											<input ng-model="search1" class="form-control">
										</label>
									</div>
								</div>
							</div>
							<table class="table table-striped table-bordered table-hover">
								<thead>
									<tr>
										<th data-hide="phone">Booked Date</th>
										<th data-hide="phone">Facility</th>
										<th data-hide="phone">Slot</th>
										<!-- <th data-hide="phone">Time</th>  -->
										<!-- <th data-hide="phone">Sub Facility Name</th> -->
										
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="rs in activeList | filter : search1" ng-show="$index >= showingFrom1 - 1 && $index <= showingTo1 - 1" >
										<td>{{rs.bookedDate | formatterHistory}}</td>
										<td>{{rs.facilityId.facilityName}}</td>
										<td>{{rs.sessionStartTime |timeFormat}} - {{rs.sessionEndTime |timeFormat}}</td>										
									</tr>
								</tbody>
							</table>
							<div paginationsec></div>
						</div>
						<!-- end widget content -->
					</div>
					<!-- end widget div -->
				</div>
				<!-- end widget --> </article>

			</div>
			</section>
		
	</div>
	
	<div class="row bookingInfo">
		<section id="widget-grid-2" class=""> <!-- row -->
			<div class="row">

				<!-- NEW WIDGET START -->
				<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-4"
					data-widget-editbutton="false">

					<header> <span class="widget-icon"> <i
						class="fa fa-table"></i>
					</span>
					<h2>Visiting History</h2>

					</header>

					<!-- widget div-->
					<div>

						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->
						</div>
						<!-- end widget edit box -->

						<!-- widget content -->
						<div class="widget-body no-padding">
							<div class="dt-toolbar">
								<div class="col-xs-12 col-sm-6">
									<div class="dataTables_filter">
										<label>
											<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
											<input ng-model="search2" class="form-control">
										</label>
									</div>
								</div>
							</div>
							<table class="table table-striped table-bordered table-hover">
								<thead>
									<tr>
										<th data-hide="phone">Booked Date</th>
										<th data-hide="phone">Visited Date</th>
										<th data-hide="phone">Facility</th>
										<th data-hide="phone">Slot</th>
										<!-- <th data-hide="phone">Sub Facility Name</th> -->
										
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="rs in visitList | filter : search2" ng-show="$index >= showingFrom - 1 && $index <= showingTo - 1" >
										<td>{{rs.bookedDate| formatterHistory}}</td>
										<td>{{rs.bookedDate| formatterHistory}}</td>
										<td>{{rs.facilityId.facilityName}}</td>
										<td>{{rs.sessionStartTime/60}} - {{rs.sessionEndTime/60}}</td>										
									</tr>
								</tbody>
							</table>
							<div pagination></div>
						</div>
						<!-- end widget content -->
					</div>
					<!-- end widget div -->
				</div>
				<!-- end widget --> </article>

			</div>
			</section>
		
	</div>
	
		
	</div>
</div>
<script src="../static/js/sports.min.js"></script>
<script>
  var $validator = $("#wizard-1").validate({
	  rules: {
	      email: {
	        required: true,
	        email: "Your email address must be in the format of name@domain.com"
	      },
	      mValid: {
	        required: true
	      },
	      msId: {
	        required: true
	      },
	      aadhar: {
		    required: true
		  },
		  role: {
			    required: true
			  },
		  bar: {
			    required: true
			  },
	      country: {
	        required: true
	      },
	      city: {
	        required: true
	      },
	      postal: {
	        required: true,
	        minlength: 6
	      },
	      district: {
	        required: true,
	      },
	      state: {
	        required: true,
	      },
	      mobileNo: {
	        required: true,
	        minlength: 10,
	      },
	     street: {
		    required: true,
		      },
		      check: {
				    required: true,
				      },    
		  userName: {
			required: true,
			     },
		      password: {
				    required: true
				  },
	    },
	    
	    messages: {
	      email: {
	        email: "Your email address must be in the format of name@domain.com"
	      },
	    },
	    
    highlight: function (element) {
      $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
    },
    unhighlight: function (element) {
      $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
    },
    errorElement: 'span',
    errorClass: 'help-block',
    errorPlacement: function (error, element) {
      if (element.parent('.input-group').length) {
        error.insertAfter(element.parent());
      } else {
        error.insertAfter(element);
      }
    }
  });
  $('#bootstrap-wizard-1').bootstrapWizard({
    'tabClass': 'form-wizard',
    'onNext': function (tab, navigation, index) {
      var $valid = $("#wizard-1").valid();
      if (!$valid) {
        $validator.focusInvalid();
        return false;
      } else {
        $('#bootstrap-wizard-1').find('.form-wizard').children('li').eq(index - 1).addClass('complete');
        $('#bootstrap-wizard-1').find('.form-wizard').children('li').eq(index - 1).find('.step').html('<i class="fa fa-check"></i>');
      }
    }
  });
</script>
<script>
		function checkTypeName(value) {
			
			$.ajax({
				url : "/rest/checkMemberName",
				data : {
					name : value
				},
				success : function(res) {
					console.log(res);
					
					if (res == 'new') {
						$("#nameError").hide();
						$("#button").attr("disabled", false);
					} else {
						$("#nameError").show();
						$("#nameError").html(res).css("color", "orange");
						$("#button").attr("disabled", true);
					}

				}
			});
		}
	</script>
	<style>
	.loadingscreen{
	width: 113%;
    height: 100%;
    position: absolute;
    opacity: 0.7;
    background-color: grey;
    z-index: 999999999999;
    margin-left: -149px;
    margin-top: -73px;
    }
    .loader {
      border: 3px solid #f3f3f3;
    border-radius: 50%;
    border-top: 3px solid #3498db;
    width: 80px;
    height: 80px;
    -webkit-animation: spin 2s linear infinite;
    margin: 0 auto;
    margin-top: 20%;
    animation: spin 2s linear infinite;
}

/* Safari */
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}

.filtrBtn{
width: 56%;
    height: 30px;
    background-color: #3c56a2;
    color: white;
}


	</style>