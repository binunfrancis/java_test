<link rel="stylesheet" href="../static/css/jquery-ui.css">

	<style>
	#slotselection{
    overflow: auto;
    width: 100%;    
    padding: 3px;
    margin-left: 20px;
    font-size: 15px;
    font-weight: bold;
	}
	
	</style>
	<div id="ribbon">
		<ol class="breadcrumb">
			<li>Home</li>
			<li>Booking</li>
		</ol>
	</div>
	<div id="content">
		<div class="row">
			<div class="dash col-md-12">
				<div class="breadcrumb">
					<h1 class="page-title txt-color-blueDark">
						<i class="fa-fw fa fa-home"></i>Booking
					</h1>
					
					<!-- <a href="javascript:void(0)" ng-show="printForm" ng-click="export('member')" class="header-a" title="Print"><i class="fa fa-print fa-lg"></i></a>-->
					<a href="javascript:void(0)" ng-click="goBack()" class="header-a" title="Go Back"><i class="fa fa-chevron-left fa-lg"></i></a>
<!-- 					<a href="javascript:void(0)" ng-show="select === 'print'" ng-click="export('Booking')" class="header-a" title="Print"><i class="fa fa-print fa-lg"></i></a> -->
				</div>
			</div>
			
			<div ng-show="slotselection" id="slotselection">
			  <span>Facility : {{facility.facilityName}}</span>
			  <span style="margin-left: 48%;">Total count of booking for the day : {{totalcountforday}}</span>
			</div>

		</div>
			<div class="modal fade" id="myModal" tabindex="-1" role="dialog">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"
								aria-hidden="true">&times;</button>
							<h4 class="modal-title">Add Spot Members</h4>
						</div>
						<div class="modal-body no-padding">

							<form class="smart-form" id="wizard-2"  ng-submit="saveMember()">

								<fieldset>
<!-- 								<input ng-model="memberShipType.memberShipId" type="hidden"></input> -->
									<section>
									<div class="row">
										<label class="label col col-4">Member Name</label>
										<div class="col col-8">
											<label class="input"> <input ng-model="member.memberName" class="jName" 
													 type="text" id="FieldName" required="required"></input>
											<span id="nameError"></span>
											</label>
										</div>
									</div>
									</section>
									
									<section>
									<div class="row">
										<label class="label col col-4">Member PhoneNo</label>
										<div class="col col-8">
											<label class="input"> <input ng-model="member.memberContactNo"
													type="text" class="input-lg jInt" placeholder="Contact No" name="mobileNo" required="required"></input>
											<l>
										</div>
									</div>
									</section>
								</fieldset>

								<footer>
									<input type="submit" id="button" class="btn btn-primary" value="Save"></input>
									<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
								</footer>
							</form>


						</div>

					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			
			<div class="modal fade" id="myModalBookings" tabindex="-1" role="dialog">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"
								aria-hidden="true">&times;</button>
							<h4 class="modal-title">Multiple Bookings(This session)</h4>
						</div>
						<div class="modal-body no-padding">
						<div class="widget-body"><div class="">
				<table class="table table-striped table-bordered table-hover">
					
					<tbody>
				<tr ng-repeat="rs in storedBooking">
					<td>{{ rs.facility.facilityName}}</td>
					<td>{{ rs.subFacility.subFacilityName}}</td>
					<td>{{ rs.selectedDate}}</td>		
				</tr>						
					</tbody>
				</table>
			</div></div>							
						</div>
					</div>
				</div>
			</div>
			<div class="modal fade" id="myModalExtra" data-backdrop="static" tabindex="-1" role="dialog">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"
								aria-hidden="true">&times;</button>
							<h4 class="modal-title">Add Other Members</h4>
						</div>
						<div class="modal-body no-padding">
							<form class="smart-form" ng-submit="setOtherMember()">
								<fieldset>
									<section>
									    <input type="button" value="ADD" ng-click="addRow()" class="btn btn-primary" style="width: 100px;height: 30px;">
	    								<table>
	    								<tr ng-repeat="mem in groupmembers">
										<td><input type="text" ng-model="searchterm" placeholder="Search..." class="form-control" style="border: solid 1px #ecd9d9;"></td>
														
	    									<td style="width:60%;"><select  id="test{{$index}}" ng-options="item as item.memberName + ' - ' + item.memberContactNo +' - ' + item.street for item in members | filter:searchterm" ng-model="membergroup" class="form-control"></select></td>
	    									<td><input type="button" value="Delete" ng-click='del($index)' class="btn btn-danger" style="height: 30px;width: 66px;"></td>
	    								</tr>
	    								</table>									
									
									</section>
									</fieldset>
									<footer>
									<input type="submit" id="button" class="btn btn-primary" value="Done"></input>
									<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
								</footer>
									</form>
									</div>
									</div>
									</div>
								
			</div>			
			
			<div class="modal fade" id="myModalExtra1" tabindex="-1" role="dialog">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"
								aria-hidden="true">&times;</button>
							<h4 class="modal-title">Add Other Members</h4>
						</div>
						<div class="modal-body no-padding">

							<form class="smart-form" ng-submit="setOtherMember()">

								<fieldset>
<!-- 								<input ng-model="memberShipType.memberShipId" type="hidden"></input> -->
									
									
									<section>
									<div class="row">
										<label class="label col col-4">Member 1</label>
										<div class="col col-8">
											<label class="input"> 
											<select ng-options="item as item.memberName + ' - ' + item.memberContactNo +' - ' + item.street for item in members" class="form-control"
													ng-model="memberId1" autocomplete="off">
													<option value=""></option>
												</select>
											</label>
										</div>
									</div>
									</section>
									<section>
									<div class="row">
										<label class="label col col-4">Member 2</label>
										<div class="col col-8">
											<label class="input"> <select ng-options="item as item.memberName + ' - ' + item.memberContactNo +' - ' + item.street for item in members" class="form-control"
													ng-model="memberId2" autocomplete="off">
													<option value=""></option>
												</select>
											</label>
										</div>
									</div>
									</section>
									<section>
									<div class="row">
										<label class="label col col-4">Member 3</label>
										<div class="col col-8">
											<label class="input"><select ng-options="item as item.memberName + ' - ' + item.memberContactNo +' - ' + item.street for item in members" class="form-control"
													ng-model="memberId3" autocomplete="off">
													<option value=""></option>
												</select>
											</label>
										</div>
									</div>
									</section>
									<section>
									<div class="row">
										<label class="label col col-4">Member 4</label>
										<div class="col col-8">
											<label class="input"><select ng-options="item as item.memberName + ' - ' + item.memberContactNo +' - ' + item.street for item in members" class="form-control"
													ng-model="memberId4" autocomplete="off">
													<option value=""></option>
												</select>
											</label>
										</div>
									</div>
									</section>
									<section>
									<div class="row">
										<label class="label col col-4">Member 5</label>
										<div class="col col-8">
											<label class="input"><select ng-options="item as item.memberName + ' - ' + item.memberContactNo +' - ' + item.street for item in members" class="form-control"
													ng-model="memberId5" autocomplete="off">
													<option value=""></option>
												</select>
											</label>
										</div>
									</div>
									</section>
									<section>
									<div class="row">
										<label class="label col col-4">Member 6</label>
										<div class="col col-8">
											<label class="input"><select ng-options="item as item.memberName + ' - ' + item.memberContactNo +' - ' + item.street for item in members" class="form-control"
													ng-model="memberId6" autocomplete="off">
													<option value=""></option>
												</select>
											</label>
										</div>
									</div>
									</section>
									<section>
									<div class="row">
										<label class="label col col-4">Member 7</label>
										<div class="col col-8">
											<label class="input"> <select ng-options="item as item.memberName + ' - ' + item.memberContactNo +' - ' + item.street for item in members" class="form-control"
													ng-model="memberId7" autocomplete="off">
													<option value=""></option>
												</select>
											</label>
										</div>
									</div>
									</section>
									<section>
									<div class="row">
										<label class="label col col-4">Member 8</label>
										<div class="col col-8">
											<label class="input"><select ng-options="item as item.memberName + ' - ' + item.memberContactNo +' - ' + item.street for item in members" class="form-control"
													ng-model="memberId8" autocomplete="off">
													<option value=""></option>
												</select>
											</label>
										</div>
									</div>
									</section>
									
									
								</fieldset>

								<footer>
									<input type="submit" id="button" class="btn btn-primary" value="Done"></input>
									<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
								</footer>
							</form>


						</div>

					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
		<div class="modal fade" id="myModals" tabindex="-1" role="dialog"><div class="modal-dialog modal-width"><div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Add Member</h4>
			</div>
			<div class="modal-body no-padding"><div class="widget-body"><div class="row">
				<form class="smart-form" id="wizard-1" ng-submit="saveMember()">
					<div id="bootstrap-wizard-1" class="col-sm-12">
						<div class="form-bootstrapWizard mt-25 ml-60">
							<ul class="bootstrapWizard form-wizard">
								<li class="active" data-target="#step1" style="pointer-events: none;">
									<a data-target="#tab1" data-toggle="tab"><span class="step">1</span><span class="title">Basic Information</span></a>
								</li>
<!-- 								<li data-target="#step2" style="pointer-events: none;"> -->
<!-- 									<a data-target="#tab2" data-toggle="tab"><span class="step">2</span><span class="title">Address Information</span></a> -->
<!-- 								</li> -->
<!-- 								<li data-target="#step3" style="pointer-events: none;"> -->
<!-- 									<a data-target="#tab3" data-toggle="tab"><span class="step">3</span><span class="title">Authentication Information</span></a> -->
<!-- 								</li> -->
<!-- 								<li data-target="#step4" style="pointer-events: none;"> -->
<!-- 									<a data-target="#tab4" data-toggle="tab"><span class="step">4</span><span class="title">Upload Photo</span></a> -->
<!-- 								</li> -->
<!-- 								<li data-target="#step5" style="pointer-events: none;"> -->
<!-- 									<a data-target="#tab5" data-toggle="tab"><span class="step">5</span><span class="title">Save Form</span></a> -->
<!-- 								</li> -->
							</ul>
							<div class="clearfix"></div>
						</div>
						<div class="tab-content">
							<div class="tab-pane active" id="tab1"><div class="p-60-40 t-20">
								<br>
								<h3><strong>Step 1</strong> - Basic Information</h3>

								<div class="row">
									<div class="col-sm-6"><div class="form-group t-20  t-20-blocker"><div class="input-group">
										<span class="input-group-addon"><i class="fa fa-pencil-square-o fa-lg fa-fw" style="line-height: .75em;"></i></span>
										<input ng-model="member.memberName" placeholder="Member Name" class="form-control input-lg jName" type="text" required="required"></input>
									</div></div></div>
									<div class="col-sm-6"><div class="form-group t-20 t-20-blocker"><div class="input-group">
										<span class="input-group-addon"><i class="fa fa-user fa-lg fa-fw" style="line-height: .75em;"></i></span>
										
										<select ng-options="item.memberShipId as item.memberShipTypeName for item in memberShipTypes"
											class="form-control input-lg p-10" ng-model="member.memberShipTypeId.memberShipId" autocomplete="off" name="msId">
											<option value=""></option>
										</select>
										
									</div></div></div>
								</div>
								<div class="row">
									<div class="col-sm-6"><div class="form-group"><div class="input-group">
										<span class="input-group-addon"><i class="fa fa-check fa-lg fa-fw" style="line-height: .75em;"></i></span>
										<input ng-model="member.fatherName" placeholder="Father Name" name="mValid" class="form-control input-lg" type="text" required="required"></input>
									</div></div></div>
									<div class="col-sm-6"><div class="form-group"><div class="input-group">
										<span class="input-group-addon"><i class="fa fa-id-card fa-lg fa-fw" style="line-height: .75em;"></i></span>
										<input ng-model="member.age" placeholder="Member Age" type="text" class="form-control input-lg jInt" ></input>
									</div></div></div>
								</div>

								<div class="row">
									<div class="col-sm-6"><div class="form-group"><div class="input-group">
										<span class="input-group-addon"><i class="fa fa-check fa-lg fa-fw" style="line-height: .75em;"></i></span>
										<input ng-model="member.memberTypeValidity" placeholder="Member Type Validity" name="mValid" class="form-control input-lg" type="text" required="required"></input>
									</div></div></div>
									<div class="col-sm-6"><div class="form-group"><div class="input-group">
										<span class="input-group-addon"><i class="fa fa-id-card fa-lg fa-fw" style="line-height: .75em;"></i></span>
										<input ng-model="member.memberAadharNo" placeholder="Member Aadhar No" type="text" class="form-control input-lg" name="aadhar"></input>
									</div></div></div>
								</div>

								<div class="row">
									<div class="col-sm-6"><div class="form-group"><div class="input-group">
										<span class="input-group-addon"><i class="fa fa-barcode fa-lg fa-fw" style="line-height: .75em;"></i></span>
										<input ng-model="member.memberBarCode" placeholder="Member Bar Code" type="text" class="form-control input-lg" name="bar"></input>
									</div></div></div>
									<div class="col-sm-6"><div class="form-group"><div class="input-group">
										<span class="input-group-addon"><i class="fa fa-user-secret fa-lg fa-fw" style="line-height: .75em;"></i></span>
										<select ng-options="item.roleId as item.roleName for item in roles"
											class="form-control input-lg p-10" ng-model="member.roleId.roleId" autocomplete="off" name="role">
											<option value=""></option>
										</select>
									</div></div></div>
								</div>
							</div></div>
							<div class="tab-pane" id="tab2"><div class="p-60-40 t-20">
								<br>
								<h3><strong>Step 2</strong> - Address Information</h3>

								<div class="row">
									<div class="col-sm-6"><div class="form-group t-20"><div class="input-group">
										<span class="input-group-addon"><i class="fa fa-street-view fa-lg fa-fw" style="line-height: .75em;"></i></span>
										<input ng-model="member.street" class="form-control input-lg" placeholder="Street" type="text" name="street"></input>
									</div></div></div>
									<div class="col-sm-6"><div class="form-group t-20"><div class="input-group">
										<span class="input-group-addon"><i class="fa fa-building-o fa-lg fa-fw" style="line-height: .75em;"></i></span>
										<input ng-model="member.city" type="text" class="form-control input-lg" placeholder="City" name="city"></input>
									</div></div></div>
								</div>

								<div class="row">
									<div class="col-sm-6"><div class="form-group"><div class="input-group">
										<span class="input-group-addon"><i class="fa fa-map-marker fa-lg fa-fw" style="line-height: .75em;"></i></span>
										<input ng-model="member.district" type="text" class="form-control input-lg" placeholder="District" name="district"></input>
									</div></div></div>
									<div class="col-sm-6"><div class="form-group"><div class="input-group">
										<span class="input-group-addon"><i class="fa fa-location-arrow fa-lg fa-fw" style="line-height: .75em;"></i></span>
										<input ng-model="member.state" type="text" class="form-control input-lg" placeholder="State"  name="state"></input>
									</div></div></div>
								</div>

								<div class="row">
									<div class="col-sm-6"><div class="form-group"><div class="input-group">
										<span class="input-group-addon"><i class="fa fa-globe fa-lg fa-fw" style="line-height: .75em;"></i></span>
										<input ng-model="member.country" type="text" class="form-control input-lg" placeholder="Country" name="country"></input>
									</div></div></div>
									<div class="col-sm-6"><div class="form-group"><div class="input-group">
										<span class="input-group-addon"><i class="fa fa-envelope fa-lg fa-fw" style="line-height: .75em;"></i></span>
										<input ng-model="member.pincode" type="text" class="form-control input-lg jInt" placeholder="Pincode" name="postal"></input>
									</div></div></div>
								</div>

								<div class="row">
									<div class="col-sm-6"><div class="form-group"><div class="input-group">
										<span class="input-group-addon"><i class="fa fa-address-card-o fa-lg fa-fw" style="line-height: .75em;"></i></span>
										<input ng-model="member.memberContactNo" type="text" class="form-control input-lg jInt" placeholder="Contact No" name=""></input>
									</div></div></div>
									<div class="col-sm-6"><div class="form-group"><div class="input-group">
										<span class="input-group-addon"><i class="fa fa-envelope-o fa-lg fa-fw" style="line-height: .75em;"></i></span>
										<input ng-model="member.email" type="email" class="form-control input-lg" placeholder="Email" name="email"></input>
									</div></div></div>
								</div>

							</div></div>
							<div class="tab-pane" id="tab3"><div class="p-60-40 t-20">
								<br>
								<h3><strong>Step 3</strong> - Authentication Information</h3>
								<div class="row">
									<div class="col-sm-6"><div class="form-group t-20"><div class="input-group">
										<span class="input-group-addon"><i class="fa fa-user-circle fa-lg fa-fw" style="line-height: .75em;"></i></span>
										<input ng-model="member.userName" type="text" class="form-control input-lg" placeholder="Username" ng-disabled="modalTitle === 'Edit'"></input>
									</div></div></div>
									<div class="col-sm-6"><div class="form-group t-20"><div class="input-group">
										<span class="input-group-addon"><i class="fa fa-key fa-lg fa-fw" style="line-height: .75em;"></i></span>
										<input ng-model="member.password" type="password" class="form-control input-lg" placeholder="Password" ng-disabled="modalTitle === 'Edit'"></input>
									</div></div></div>
								</div>

							</div></div>
							<div class="tab-pane" id="tab4"><div class="p-60-40 t-20">
								<br>
								<h3><strong>Step 4</strong> - Upload Photo</h3>
								<div class="row">
									<div class="col-sm-4"><div class="form-group t-20"><div class="input-group">
										<input class="form-control" type="file" file-model="uploadedFile" placeholder="Upload File" filepreview="filepreview"></input>
									</div></div></div>
									<div class="col-sm-4"><div class="form-group t-20"><div class="input-group">
										<h3>Preview</h3>
										<div class="photo"><img ng-src="{{ filepreview }}" class="img-responsive" ng-show="filepreview"/></div>
									</div></div></div>
									<div class="col-sm-4"><div class="form-group t-20"><div class="input-group">
										<h3>Original</h3>
										<div class="photo"><img ng-src="../static/temp/{{ member.memberPhoto }}" class="img-responsive" /></div>
									</div></div></div>
								</div>
							</div></div>
							<div class="tab-pane" id="tab5"><div class="p-60-40 t-20">
								<br><h3><strong>Step 5</strong> - Save Form </h3><br>
								<h1 class="text-center text-success t-20"><strong><i class="fa fa-check fa-lg"></i> Complete</strong></h1>
								<div class="col-md-4 col-md-offset-4" >
									<button type="submit" class="btn btn-lg txt-color-darken" style="width: 100%;" >Save</button> 
								</div>
								<br><br>
							</div></div>
							<div class="form-actions mr">
								<div class="row">
									<div class="col-sm-12">
										<ul class="pager wizard no-margin">
											<li class="previous disabled"><a id="wiz-prev" href="javascript:void(0);" class="btn btn-lg btn-default"> Previous </a></li>
											<li class="next"><a href="javascript:void(0);" class="btn btn-lg txt-color-darken"> Next </a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div></div></div>
		</div></div></div>

		<section id="widget-grid">
			<div class="container-2">
				<div id="page-wrapper">
					<div class="row">
						<div class="row">
							<div ng-show="select === 'print'">
									<div class="jarviswidget" ng-show="printForm"  style="background: white;padding: 20px 0;  ">
									<center><div id="print" ng-include="'../../../../static/html/report.html'" style="width:padding: 0;background: white;"></div></center>
									</div>
									
							</div>
							<div class="dash" ng-hide="select === 'print'">
								<div class="col-md-12" ng-show="leave">
									{{selectedDate | date}} is not available for <span ng-show="select === 'bookCenter'">{{center.centreName}}</span>
									<span ng-show="select === 'bookFacility'">{{facility.facilityName}}</span>
									<span ng-show="select === 'book'">{{subFacility.subFacilityName}}</span>
								</div>
								<div class="col-md-12" ng-hide="select === 'bookCenter' || leave">
									<div ng-repeat="x in timeTables |orderBy:'sessionStartTime'">
										<button class="time-slots" ng-class="x.book ? 'time-slots-selected' : x.govtBooked ? 'time-slots-booked' : ''" ng-click="book(x)" ng-disabled="x.booked">
											{{x.sessionStartTime | timeFormat}} - {{x.sessionEndTime | timeFormat}} ( {{x.totalBookedNum}} )
										</button> 
									</div>
								</div>
								<form ng-submit="saveBooking()" class="breadcrumb col-md-12 smart-form" ng-class="select === 'bookCenter' ? 'col-md-offset-4' : ''" ng-hide="leave">
									<div class="form-control-2">
									<div class="col-sm-6">
									<section class="">
									<div class="">
										<div class="col col-10">
											<div class="input-group">
												<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
												<input id="jNoPast" ng-model="selectedDt" ng-change="incrementDate(selectedDt)" placeholder="Select Date" class="form-control" type="text" required="required"></input>
											</div>
										</div>
										<i class="fa fa-caret-up arrow-up" ng-click="incrementDate(1)"></i>
										<i class="fa fa-caret-down arrow-down" ng-hide="selectedDate == today" ng-click="incrementDate(-1)"></i>
									</div>
									<div class="">
										<label class="label col col-4">Master Type</label>
										<div class="col col-8">
											<label class="input">
												<select ng-options="item.masterTypeId as item.masterTypeName for item in masterTypes" class="form-control"
													ng-model="masterTypeId" autocomplete="off" required="required" ng-change="masterTypeChange()">
													<option value=""></option>
												</select>
											</label>
										</div>
									</div>
									<div class="">
										<label class="label col col-4">Member</label>
										<div class="col col-8">
											<label class="input">
											<input auto-complete ui-items="membernames" ng-model="memberselected">
												<!--    <div class="col-sm-8"><select id="myelem" ng-options="item.memberId as item.memberName + ' - ' + item.memberContactNo +' - ' + item.street for item in members |filter:memberSearchTerm" class="form-control"
													ng-model="memberId" autocomplete="off" required="required" ng-change="setDiscount()">
													<option value=""></option>
												</select></div>
												<div class="col-sm-4"><input type="text" ng-model="memberSearchTerm" id="mye" placeholder="Search..." style="border: solid 0.5px grey;"/></div>
												  -->
												  
						
												
												<a href="javascript:void(0)" ng-click="addMember()" title="Add Member"><i class="fa fa-user"></i>Add Spot Member</a>
 											<a href="javascript:void(0)" ng-click="addExtraMember()" title="Add Extra Member"><i class="fa fa-user"></i>Add Group Members</a>
											</label>
											
										</div>
									</div>
									<div class="">
										<label class="label col col-4">Booking Type</label>
										<div class="col col-8">
											<label class="input">
												<select class="form-control" ng-model="bookingType" ng-hide="select === 'bookCenter'" ng-change="bookingTypeChange()" ng-options="x.value as x.name for x in bookingTypes"></select>
												<select class="form-control" ng-model="bookingType" ng-show="select === 'bookCenter'" ng-change="bookingTypeChange()">
													<option value="For 12 Hours">For First 12 Hours</option>
													<option value="For 24 Hours">For Second 12 Hours</option>
												</select>
											</label>
										</div>
									</div>
									<div class="" ng-show="monthlyBooking">
									<label class="label col col-4">Number of Months</label>
										<div class="col col-8">
											<label class="input">
												<select class="form-control" ng-model="dayFile" ng-change="chargeByMonthUpdation()">
												    <option value="null">Select</option>
													<option value="1">1</option>
													<option value="2">2</option>
													<option value="3">3</option>
													<option value="4">4</option>
													<option value="5">5</option>
													<option value="6">6</option>
													<option value="7">7</option>
													<option value="8">8</option>
													<option value="9">9</option>
													<option value="10">10</option>
													<option value="11">11</option>
													<option value="12">12</option>													
												</select>
											</label>
										</div>
									</div>
									<div class="">
										<label class="label col col-4">Additional Facility</label>
										<div class="col col-8">
											<label class="input">
												<select ng-options="item.matPriceId as item.matPriceName for item in matPrices" class="form-control"
													ng-model="matPriceId" autocomplete="off"  ng-change="additionalFacilityChange()">
													<option value=""></option>
												</select>
												
											</label>
										</div>
									</div>
									<div class="">
										<label class="label col col-4">Trainer Name</label>
										<div class="col col-8">
											<label class="input">
												<select ng-options="item.trainerChargeId as item.trainerFinalId.trainerName for item in trainers" class="form-control"
													ng-model="trainerId" autocomplete="off"  ng-change="trainerChange()">
													<option value=""></option>
												</select>
												
											</label>
										</div>
									</div>
									<div class="">
										<label class="label col col-4">Payment Type</label>
										<div class="col col-8">
											<label class="input">
												<select class="form-control" ng-model="account.paymentType" required="required">
													<option value=""></option>
													<option value="By Cash">By Cash</option>
													<option value="By Card">By Card</option>
												</select>
											</label>
										</div>
									</div>
									<div class="">
										<label class="label col col-4">Remarks</label>
										<div class="col col-8">
											<label class="input">
												<input ng-model="account.referenceNo" type="text"  ></input>
											</label>
										</div>
									</div>
									
									</section>
									</div> <!-- ull -->
									<div class="col-sm-6">
									<div>
									<section>
									<div class="">
										<label class="label col col-4">Actual Amount</label>
										<div class="col col-8">
											<label class="input"><input ng-model="account.actualAmount" type="text" style="font-size: medium;font-weight: bold;text-align:right;" disabled="disabled"></input></label>
										</div>
									</div>
									<div class="">
										<label class="label col col-4">Discount %</label>
										<div class="col col-8">
											<label class="input">
												<input ng-model="account.discount" type="text" style="font-size: medium;font-weight: bold;text-align:right;"></input>
											</label>
										</div>
									</div>
									
									<div class="">
										<label class="label col col-4">Additional Charge</label>
										<div class="col col-8">
											<label class="input">
												<input ng-model="account.extraAmount"  type="text" style="font-size: medium;font-weight: bold;text-align:right;"  disabled="disabled"></input>
											</label>
										</div>
									</div>
									<div class="">
										<label class="label col col-4">Caution Deposit</label>
										<div class="col col-8">
											<label class="input">
												<input ng-model="account.cautionDeposit" type="text"  ng-blur="updateAmounts()" style="font-size: medium;font-weight: bold;text-align:right;"></input>
											</label>
										</div>
									</div>
									<div class="">
										<label class="label col col-4">Trainer Cost</label>
										<div class="col col-8">
											<label class="input">
												<input ng-model="trainerCost" type="text"  style="font-size: medium;font-weight: bold;text-align:right;" disabled="disabled"></input>
											</label>
										</div>
									</div>
									<div class="">
										<label class="label col col-4">Payable Amount</label>
										<div class="col col-8">
											<label class="input">
												<input ng-model="account.payableAmount" type="text" style="font-size: medium;font-weight: bold;text-align:right;" disabled="disabled"></input>
											</label>
										</div>
									</div>
									<div class="">
										<label class="label col col-4">Paid Amount</label>
										<div class="col col-8">
											<label class="input">
												<input ng-model="account.paidAmount" class="jFloat" type="text" ng-keyup="calcBalance()"  disabled="disabled" style="font-size: medium;font-weight: bold;text-align:right;" required="required"></input>
											</label>
										</div>
									</div>
									<div class="">
										<label class="label col col-4">Balance Amount</label>
										<div class="col col-8">
											<label class="input">
												<input ng-model="account.balanceAmount" type="text" style="font-size: medium;font-weight: bold;text-align:right;" disabled="disabled"></input>
											</label>
										</div>
									</div>
<!-- 									<section> -->
<!-- 									<section></section> -->
									</section>
									</div> <!-- ull-2-->
									
									
									<footer>
										{{message}}
										<input type="submit" class="btn btn-primary" ng-show="available" id="formSave" value="Book Now"></input>
										<input type="button" class="btn btn-primary" ng-show="available" id="formSave1" ng-click="bookmore()" value="Book More"></input>
										<input type="button" class="btn btn-primary" ng-show="available" id="formSave1" ng-click="showmultiplebookings()" value="Show Bookings"></input>
										<div flash-message="3000"></div>
									</footer>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
<script src="../static/js/sports.min.js"></script>
<script>
  var $validator = $("#wizard-2").validate({
	  rules: {
	      email: {
	        required: true,
	        email: "Your email address must be in the format of name@domain.com"
	      },
	      mValid: {
	        required: true
	      },
	      msId: {
	        required: true
	      },
	      aadhar: {
		    required: true
		  },
		  role: {
			    required: true
			  },
		  bar: {
			    required: true
			  },
	    
	      mobileNo: {
	    	  
	        required: true,
	        minlength: 10,
	        maxlength:13,
	      },
	    
	    },
	    
	    messages: {
	      email: {
	        email: "Your email address must be in the format of name@domain.com"
	      },
	    },
	    
    highlight: function (element) {
      $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
    },
    unhighlight: function (element) {
      $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
    },
    errorElement: 'span',
    errorClass: 'help-block',
    errorPlacement: function (error, element) {
      if (element.parent('.input-group').length) {
        error.insertAfter(element.parent());
      } else {
        error.insertAfter(element);
      }
    }
  });
  $('#bootstrap-wizard-1').bootstrapWizard({
    'tabClass': 'form-wizard',
    'onNext': function (tab, navigation, index) {
      var $valid = $("#wizard-1").valid();
      if (!$valid) {
        $validator.focusInvalid();
        return false;
      } else {
        $('#bootstrap-wizard-1').find('.form-wizard').children('li').eq(index - 1).addClass('complete');
        $('#bootstrap-wizard-1').find('.form-wizard').children('li').eq(index - 1).find('.step').html('<i class="fa fa-check"></i>');
      }
    }
  });
  $( "#jNoPast" ).datepicker({ dateFormat:'dd M yy', minDate: 0});
</script>
