<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->

<title> Active Sports Run </title>
<meta name="description" content="">
<meta name="author" content="">

<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">



</head>

<body class="smart-style-6">

	<%@ include file="../../common/header.jsp"%>
	<%@ include file="../../common/navigation.jsp"%>


	<!-- MAIN PANEL -->
	<div id="main" role="main">

		<!-- RIBBON -->
		<div id="ribbon">

			<span class="ribbon-button-alignment"> <span id="refresh"
				class="btn btn-ribbon" data-action="resetWidgets"
				data-title="refresh" rel="tooltip" data-placement="bottom"
				data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings."
				data-html="true"> <i class="fa fa-refresh"></i>
			</span>
			</span>

			<!-- breadcrumb -->
			<ol class="breadcrumb">
				<li>Home</li>
				<li>Center Facility</li>
			</ol>
			<!-- end breadcrumb -->

		</div>
		<!-- END RIBBON -->

		<!-- MAIN CONTENT -->
		<div id="content">
			<div class="row">
				<div class="dash col-md-12">
					<div class="breadcrumb">
						<h1 class="page-title txt-color-blueDark">
							<i class="fa-fw fa fa-home"></i> Sports <span>
						</h1>
						<a data-toggle="modal" href="#myModal" class="btn small-btn-blue btn-lg pull-right header-btn hidden-mobile">
							<i class="fa fa-circle-arrow-up fa-lg"></i>Add Center Facility
						</a>
					</div>
				</div>
			</div>

			<!-- Modal -->
			<div class="modal fade" id="myModal" tabindex="-1" role="dialog">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"
								aria-hidden="true">&times;</button>
							<h4 class="modal-title">Add Center Facility</h4>
						</div>
						<div class="modal-body no-padding">

							<form:form action="centerFacilitySave" id="login-form"
								class="smart-form" method="post" commandName="centreFacility"
								onsubmit="document.getElementById('save').disabled=true;">

								<fieldset>
									<section>
									<div class="row">
										<label class="label col col-4">Center Facility Code</label>
										<div class="col col-8">
											<label class="input"> <form:input path="centreFacilityCode"
													type="text" name="centreFacilityCode" required="required"></form:input>
											</label>
										</div>
									</div>
									</section>

									<section>
									<div class="row">
										<label class="label col col-4">Center Facility Name</label>
										<div class="col col-8">
											<label class="input"> <form:input path="centreFacilityName"
													type="text" name="centreFacilityName" required="required"></form:input>
											</label>
										</div>
									</div>
									</section>
									<section>
									<div class="row">
										<label class="label col col-4">Parent</label>
										<div class="col col-8">
											<label class="input"><form:select
												class="form-control" path="parent" autocomplete="off"
												>
												<option value="null">Select Parent</option>
												<c:forEach items="${centerFacilityRoles}" var="item">
													<option value="${item.centreFacilityName }">${item.centreFacilityName}</option>
												</c:forEach>
											</form:select>
											</label>
										</div>
									</div>
									</section>
									
									
									

									<section>
									<div class="row">
										<label class="label col col-4">Facility</label>
										<div class="col col-8">
											<label class="input"> <form:select
			     								class="form-control" path="facility" autocomplete="off"
												required="required">
												<option value="Indoor Stadium">Indoor Stadium</option>
												<option value="Outdoor Stadium">Outdoor Stadium</option>
												<option value="Badminton">Badminton</option>
												<option value="Volley Ball">Volley Ball</option>
												<option value="Basket Ball">Basket Ball</option>
												<option value="Table Tennis">Table Tennis</option>
												<option value="Gym">Gym</option>
												<option value="Gymnastics">Gymnastics</option>
												<option value="Swimming Pool">Swimming Pool</option>
											</form:select> 
											</label>
										</div>
									</div>
									</section>
									
									<section>
									<div class="row">
										<label class="label col col-4">Center</label>
										<div class="col col-8">
											<label class="input"><form:select
												class="form-control" path="centreId" autocomplete="off"
												required="required">
												<c:forEach items="${centerLis}" var="item">
													<option value="${item.centreId}">${item.centreName}</option>
												</c:forEach>
											</form:select>
											</label>
										</div>
									</div>
									</section>
									
									
									 

									<section>
									<div class="row">
										<label class="label col col-4">In Charge</label>
										<div class="col col-8">
											<label class="input"> <form:input
													path="inCharge" type="text" name="inCharge"
													required="required"></form:input>
											</label>
										</div>
									</div>
									</section>
									<section>
									<div class="row">
										<label class="label col col-4">Max Allowed Persons</label>
										<div class="col col-8">
											<label class="input"> <form:input
													path="maxAllowedPersons" type="number" name="maxAllowedPersons"
													required="required"></form:input>
											</label>
										</div>
									</div>
									</section>
									<section>
									<div class="row">
										<label class="label col col-4">Booking Type</label>
										<div class="col col-8">
											<label class="input"> <form:select
			     								class="form-control" path="bokingType" autocomplete="off"
												required="required">
												
												<option value="Group">Group</option>
												<option value="Individual">Individual</option>
												
											</form:select> 
											</label>
										</div>
									</div>
									
									</section>
									
									<section>
									<div class="row">
										<label class="label col col-4">No Of Facilities</label>
										<div class="col col-8">
											<label class="input"> <form:input
													path="noOfFacility" type="number" name="noOfFacility"
													required="required"></form:input>
											</label>
										</div>
									</div>
									
									
									</section>
									<p style="text-align: center; color: red;">${error }</p>
								</fieldset>

								<footer> <form:button type="submit" id="save"
									class="btn btn-primary">
									Save
								</form:button>
								<button type="button" class="btn btn-default"
									data-dismiss="modal">Cancel</button>

								</footer>
							</form:form>


						</div>

					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->

			<!-- widget grid -->
			<section id="widget-grid" class=""> <!-- row -->
			<div class="row">

				<!-- NEW WIDGET START -->
				<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-3"
					data-widget-editbutton="false">

					<header> <span class="widget-icon"> <i
						class="fa fa-table"></i>
					</span>
					<h2>Center Facility</h2>
					<p style="text-align: center; color: red;">${error }</p>

					</header>

					<!-- widget div-->
					<div>

						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->
						</div>
						<!-- end widget edit box -->

						<!-- widget content -->
						<div class="widget-body no-padding">

							<table id="datatable_tabletools"
								class="table table-striped table-bordered table-hover"
								width="100%">
								<thead>
									<tr>
										<th data-hide="phone">Center Facility Code</th>
										<th data-hide="phone">Center Facility Name</th>
										<th data-hide="phone">Facility</th>
										<th data-hide="phone">Center Name</th>
										<th data-hide="phone">No Of Facility</th>										
										<th data-hide="phone,tablet">Edit</th>
										<th>Delete</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${centerFacilityRoles}" var="rs">
										<tr>
											<td>${rs.centreFacilityCode}</td>
											<td>${rs.centreFacilityName}</td>
											<td>${rs.facility}</td>
											<td>${rs.centreId.centreName}</td>
											<td>${rs.noOfFacility}</td>
											
											<td><a data-toggle="modal" href="#expand${rs.centreFacilityId}"><i
													class="fa fa-edit "></i></a>

												<div class="modal fade" id="expand${rs.centreFacilityId}"
													tabindex="-1" role="dialog">
													<div class="modal-dialog">
														<div class="modal-content">
															<div class="modal-header">
																<button type="button" class="close" data-dismiss="modal"
																	aria-hidden="true">&times;</button>
																<h4 class="modal-title">Edit Center Facility</h4>
															</div>
															<div class="modal-body no-padding">

																<form:form action="centerFacilityEdit" id="login-form"
																	class="smart-form" method="post"
																	commandName="centreFacility"
																	onsubmit="document.getElementById('editButton').disabled=true;">

																	<fieldset>

																		<form:input path="centreFacilityId" value="${rs.centreFacilityId}"
																			type="hidden" name="centreFacilityId" required="required"></form:input>
																	
																		<section class="">
																		<div class="row">
																			 <label
																				class="label col col-4">Center Facility Code</label> <label
																				class="input col col-8"> <form:input
																					value="${rs.centreFacilityCode}" path="centreFacilityCode"
																					autocomplete="off" type="text" name="centreCode"
																					required="required"></form:input>
																			</label> 
																			
																		</div></section>
																		<section>
																		<div class="row">
																			 <label
																				class="label col col-4">center Facility Name</label> <label
																				class="input col col-8"><form:input
																					value="${rs.centreFacilityName}" path="centreFacilityName"
																					autocomplete="off" type="text" name="centreFacilityName"
																					required="required"></form:input>
																				
																			</label> 

																		</div></section>
																		<section>
																		<div class="row">
																			 <label
																				class="label col col-4">Parent</label> <label
																				class="input col col-8"><form:select
											     								class="form-control full-width" path="parent" autocomplete="off"
																				>
																				<option value="null">select parent</option>
																				<c:forEach items="${centerFacilityList}" var="item">
																				<option value="${item.centreFacilityName }">${item.centreFacilityName}</option>
																			</c:forEach>
																			</form:select> 
																				
																			</label> 

																		</div></section>
																		
																			<section>
																			<div class="row">
																				<label class="label col col-4">Facility</label>
																				<div class="col col-8">
																					<label class="input"> <form:select
													     								class="form-control full-width" path="facility" autocomplete="off"
																						required="required">
																						<option value="Indoor Stadium">Indoor Stadium</option>
																						<option value="Outdoor Stadium">Outdoor Stadium</option>
																						<option value="Badminton">Badminton</option>
																						<option value="Volley Ball">Volley Ball</option>
																						<option value="Basket Ball">Basket Ball</option>
																						<option value="Table Tennis">Table Tennis</option>
																						<option value="Gym">Gym</option>
																						<option value="Gymnastics">Gymnastics</option>
																						<option value="Swimming Pool">Swimming Pool</option>
																					</form:select> 
																					</label>
																				</div>
																			</div>
																			</section>
									
																		<section>
																		<div class="row">
																			<label class="label col col-4">Center</label>
																			<div class="col col-8">
																				<label class="input"><form:select
																					class="form-control full-width" path="centreId" autocomplete="off"
																					required="required">
																					<c:forEach items="${centerLis}" var="item">
																						<option value="${item.centreId }">${item.centreName}</option>
																					</c:forEach>
																				</form:select>
																				</label>
																			</div>
																		</div>
																		</section>
																		<section>
																			<div class="row">
																				<label class="label col col-4">In Charge</label>
																				<div class="col col-8">
																					<label class="input"> <form:input
																							path="inCharge" value="${rs.inCharge}" type="text" name="inCharge"
																							required="required"></form:input>
																					</label>
																				</div>
																			</div>
																		</section>
																		<section>
																		<div class="row">
																			<label class="label col col-4">Max Allowed Persons</label>
																			<div class="col col-8">
																				<label class="input"> <form:input value="${rs.maxAllowedPersons}"
																						path="maxAllowedPersons" type="number" name="maxAllowedPersons"
																						required="required"></form:input>
																				</label>
																			</div>
																		</div>
																		</section>
																		<section>
																		<div class="row">
																			<label class="label col col-4">Booking Type</label>
																			<div class="col col-8">
																				<label class="input"> <form:select value="${rs.bokingType}"
												     								class="form-control full-width" path="bokingType" autocomplete="off"
																					required="required">
																					
																					<option value="Group">Group</option>
																					<option value="Individual">Individual</option>
																					
																				</form:select> 
																				</label>
																			</div>
																		</div>
																		</section>
																		<section>
																		<div class="row">
																			<label class="label col col-4">No Of Facilities</label>
																			<div class="col col-8">
																				<label class="input"> <form:input 
																						path="noOfFacility" value="${rs.noOfFacility}" type="number" name="noOfFacility"
																						required="required"></form:input>
																				</label>
																			</div>
																		</div>
																		
																		
																		</section>
																		

																		
																		
																		

																		

																	</fieldset>

																	<footer> <form:button type="submit"
																		id="editButton" class="btn btn-primary">
																			Save</form:button>
																	<button type="button" class="btn btn-default"
																		data-dismiss="modal">Cancel</button>

																	</footer>
																</form:form>

															</div>

														</div>
														<!-- /.modal-content -->
													</div>
													<!-- /.modal-dialog -->
												</div> <!-- /.modal --></td>
												<td><a href="centerFacilityDelete?centreFacilityId=${rs.centreFacilityId}"><i class="fa fa-trash-o"></i></a></td>
										</tr>
									</c:forEach>
								</tbody>
							</table>

						</div>
						<!-- end widget content -->
					</div>
					<!-- end widget div -->
				</div>
				<!-- end widget --> </article>

			</div>
			</section>

		</div>
		<!-- END MAIN CONTENT -->

	</div>
	<!-- END MAIN PANEL -->

	<%@ include file="../../common/footer-html.jsp"%>

	<!--================================================== -->

	<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
	<script data-pace-options='{ "restartOnRequestAfter": true }'
		src="../static/js/plugin/pace/pace.min.js"></script>

	<!-- Link to Google CDN's jQuery + jQueryUI; fall back to local -->
	<script
		src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
	<script>
		if (!window.jQuery) {
			document
					.write('<script src="../static/js/libs/jquery-2.1.1.min.js"><\/script>');
		}
	</script>

	<script
		src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
	<script>
		if (!window.jQuery.ui) {
			document
					.write('<script src="../static/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
		}
	</script>

	<!-- IMPORTANT: APP CONFIG -->
	<script src="../static/js/app.config.js"></script>

	<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
	<script
		src="../static/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script>

	<!-- BOOTSTRAP JS -->
	<script src="../static/js/bootstrap/bootstrap.min.js"></script>

	<!-- CUSTOM NOTIFICATION -->
	<script src="../static/js/notification/SmartNotification.min.js"></script>

	<!-- JARVIS WIDGETS -->
	<script src="../static/js/smartwidgets/jarvis.widget.min.js"></script>

	<!-- EASY PIE CHARTS -->
	<script
		src="../static/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>

	<!-- SPARKLINES -->
	<script src="../static/js/plugin/sparkline/jquery.sparkline.min.js"></script>

	<!-- JQUERY VALIDATE -->
	<script
		src="../static/js/plugin/jquery-validate/jquery.validate.min.js"></script>

	<!-- JQUERY MASKED INPUT -->
	<script
		src="../static/js/plugin/masked-input/jquery.maskedinput.min.js"></script>

	<!-- JQUERY SELECT2 INPUT -->
	<script src="../static/js/plugin/select2/select2.min.js"></script>

	<!-- JQUERY UI + Bootstrap Slider -->
	<script
		src="../static/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>

	<!-- browser msie issue fix -->
	<script src="../static/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>

	<!-- FastClick: For mobile devices -->
	<script src="../static/js/plugin/fastclick/fastclick.min.js"></script>

	<!--[if IE 8]>

		<h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>

		<![endif]-->

	<!-- Demo purpose only -->
	<script src="../static/js/demo.min.js"></script>

	<!-- MAIN APP JS FILE -->
	<script src="../static/js/app.min.js"></script>

	<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
	<!-- Voice command : plugin -->
	<script src="../static/js/speech/voicecommand.min.js"></script>

	<!-- SmartChat UI : plugin -->
	<script src="../static/js/smart-chat-ui/smart.chat.ui.min.js"></script>
	<script src="../static/js/smart-chat-ui/smart.chat.manager.min.js"></script>

	<!-- PAGE RELATED PLUGIN(S) -->

	<!-- Flot Chart Plugin: Flot Engine, Flot Resizer, Flot Tooltip -->
	<script src="../static/js/plugin/flot/jquery.flot.cust.min.js"></script>
	<script src="../static/js/plugin/flot/jquery.flot.resize.min.js"></script>
	<script src="../static/js/plugin/flot/jquery.flot.time.min.js"></script>
	<script src="../static/js/plugin/flot/jquery.flot.tooltip.min.js"></script>

	<!-- Vector Maps Plugin: Vectormap engine, Vectormap language -->
	<script
		src="../static/js/plugin/vectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script src="http://jvectormap.com/js/jquery-jvectormap-in-mill.js"></script>
	<!-- <script src="../static/js/plugin/vectormap/jquery-jvectormap-world-mill-en.js"></script> -->

	<!-- Full Calendar -->
	<script src="../static/js/plugin/moment/moment.min.js"></script>
	<script
		src="../static/js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>

	<!-- Morris Chart Dependencies -->
	<script src="../static/js/plugin/morris/raphael.min.js"></script>
	<script src="../static/js/plugin/morris/morris.min.js"></script>

	<!-- PAGE RELATED PLUGIN(S) -->
	<script src="../static/js/plugin/datatables/jquery.dataTables.min.js"></script>
	<script src="../static/js/plugin/datatables/dataTables.colVis.min.js"></script>
	<script
		src="../static/js/plugin/datatables/dataTables.tableTools.min.js"></script>
	<script
		src="../static/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
	<script
		src="../static/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>

	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"
		type="text/javascript"></script>


	<script>
		$(document)
				.ready(
						function() {

							// DO NOT REMOVE : GLOBAL FUNCTIONS!
							pageSetUp();

							/* BASIC ;*/
							var responsiveHelper_dt_basic = undefined;
							var responsiveHelper_datatable_fixed_column = undefined;
							var responsiveHelper_datatable_col_reorder = undefined;
							var responsiveHelper_datatable_tabletools = undefined;

							var breakpointDefinition = {
								tablet : 1024,
								phone : 480
							};

							$('#datatable_tabletools')
									.dataTable(
											{

												// Tabletools options: 
												//   https://datatables.net/extensions/tabletools/button_options
												"sDom" : "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'T>r>"
														+ "t"
														+ "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
												"oTableTools" : {
													"aButtons" : [
															"copy",
															"csv",
															"xls",
															{
																"sExtends" : "pdf",
																"sTitle" : "LiviC_PDF",
																"sPdfMessage" : "LiviC PDF Export",
																"sPdfSize" : "letter"
															},
															{
																"sExtends" : "print",
																"sMessage" : "Generated by LiviC <i>(press Esc to close)</i>"
															} ],
													"sSwfPath" : "../static/js/plugin/datatables/swf/copy_csv_xls_pdf.swf"
												},
												"autoWidth" : true,
												"preDrawCallback" : function() {
													// Initialize the responsive datatables helper once.
													if (!responsiveHelper_datatable_tabletools) {
														responsiveHelper_datatable_tabletools = new ResponsiveDatatablesHelper(
																$('#datatable_tabletools'),
																breakpointDefinition);
													}
												},
												"rowCallback" : function(nRow) {
													responsiveHelper_datatable_tabletools
															.createExpandIcon(nRow);
												},
												"drawCallback" : function(
														oSettings) {
													responsiveHelper_datatable_tabletools
															.respond();
												}
											});

							/* END TABLETOOLS */

						});
	</script>


</body>

</html>

</body>
</html>