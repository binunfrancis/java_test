<div>
<div class="loadingscreen" ng-show="loadtrue">
		<div class="loader"></div>
	</div>
	<div id="ribbon">
		<ol class="breadcrumb">
			<li>Home</li>
			<li>Member</li>
		</ol>
	</div>
	<div id="content">
		<div class="row">
			<div class="dash col-md-12">
				<div class="breadcrumb">
					<h1 class="page-title txt-color-blueDark">
						<i class="fa-fw fa fa-home"></i>Sports
					</h1>
				</div>
			</div>
		</div>
		<div class="modal fade" id="subModal" tabindex="-1" role="dialog"><div class="modal-dialog modal-width"><div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Members</h4>
			</div>
			<div class="modal-body no-padding"><div class="widget-body"><div class="">
				<table class="table table-striped table-bordered table-hover">
					
					<tbody>
				<tr ng-repeat="rs in additionalMembers"><td>{{ rs.memberPrimaryId.memberName}}</td><td><input type="checkbox" id="{{rs.bookAddMemId}}" ng-checked="{{rs.statusUpdate == true}}"></td>
							
						</tr>
						<tr ng-repeat="rs in additionalMembersVisited"><td>{{ rs.memberPrimaryId.memberName}}</td><td><input type="checkbox" disabled="disabled" id="{{rs.bookAddMemId}}" ng-checked="{{rs.statusUpdate == true}}"></td>
							
						</tr>
					</tbody>
				</table>
			</div></div></div>
		</div></div></div>
		<div class="row">
		<form class="form-horizontal">
  <div class="form-group">
    <label for="inputEmail3" class="col-sm-1 control-label">Member ID</label>
    <div class="col-sm-3">
      <input type="text" class="form-control" id="inputEmail3" placeholder="Member ID" ng-model="memberid">
    </div>
    <div class="col-sm-offset-0 col-sm-4">
      <button type="submit" class="btn filtrBtn" ng-click="getBookings()">Get Bookings</button>
    </div>
  </div>
</form>
		</div>
		<div class="row bookingInfo">
		<section id="widget-grid" class=""> <!-- row -->
			<div class="row">

				<!-- NEW WIDGET START -->
				<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-3"
					data-widget-editbutton="false">

					<header> <span class="widget-icon"> <i
						class="fa fa-table"></i>
					</span>
					<h2>Today's Bookings</h2>

					</header>

					<!-- widget div-->
					<div>

						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->
						</div>
						<!-- end widget edit box -->

						<!-- widget content -->
						<div class="widget-body no-padding">
							<div class="dt-toolbar">
								<div class="col-xs-12 col-sm-6">
									<div class="dataTables_filter">
										<label>
											<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
											<input ng-model="search" class="form-control">
										</label>
									</div>
								</div>
							</div>
							<table class="table table-striped table-bordered table-hover">
								<thead>
									<tr>
										<th data-hide="phone">Receipt No</th>
										<th data-hide="phone">Booking Date</th>
										<th data-hide="phone">Slot</th>
										<th data-hide="phone">Facility</th>
										<th data-hide="phone">Sub Facility</th>
										<th data-hide="phone">Center</th>
										<th data-hide="phone">Members</th>
										<th data-hide="phone"></th>
										<!-- <th data-hide="phone">Sub Facility Name</th> -->
										
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="rs in bookingList | filter : search" ng-show="$index >= showingFrom - 1 && $index <= showingTo - 1" >
										<td>{{rs.bookingId}}</td>
										<td>{{rs.bookingDate | formatterEntry}}</td>
										<td>{{rs.sessionStartTime |timeFormat}} - {{rs.sessionEndTime |timeFormat}}</td>
										<td>{{rs.facilityId.facilityName}}</td>
										<td>{{rs.subFacilityId.subFacilityName}}</td>
										<td>{{rs.facilityId.centerId.centreName}}</td>
										<td><a href="javascript:void(0)" ng-hide="{{rs.entryStatus == true}}" ng-click="viewMembers(rs)"><i class="fa fa-eye"></i></a>
										
										<a href="javascript:void(0)" ng-show="{{rs.entryStatus == true}}" ng-click="viewMembersOfVisited(rs)"><i class="fa fa-eye"></i></a>
										</td>
										
										<td ><button class="btn filtrBtn" ng-show="{{rs.entryStatus == false}}" ng-click="updateTimeSlot($event, rs)">UPDATE</button><button class="btn filtrBtn" ng-show="{{rs.entryStatus == true}}" >Visited</button></td>
									
									</tr>
								</tbody>
							</table>
							<div pagination></div>
						</div>
						<!-- end widget content -->
					</div>
					<!-- end widget div -->
				</div>
				<!-- end widget --> </article>

			</div>
			</section>
		
	</div>
		
		</div>
</div>
<style>
.filtrBtn{
width: 56%;
    height: 30px;
    background-color: #3c56a2;
    color: white;
}

</style>