	<div> 

		<!-- RIBBON -->
		<div id="ribbon">
			<!-- breadcrumb -->
			<ol class="breadcrumb">
				<li>Home</li>
				<li>Sports</li>
			</ol>
			<!-- end breadcrumb -->

		</div>
		<!-- END RIBBON -->

		<!-- MAIN CONTENT -->
		<div id="content">
			<div class="row">
				<div class="dash col-md-12">
					<div class="breadcrumb">
						<h1 class="page-title txt-color-blueDark">
							<i class="fa-fw fa fa-home"></i>Sports
						</h1>
					</div>
				</div>
			</div>
			
			<div class="row">
		<form class="form-horizontal">
  <div class="">
  
    
    <div class="col-sm-3">
    <label for="inputEmail3" class=" control-label">Member ID</label>
      <input type="text" class="form-control" id="inputEmail3" placeholder="Member ID" ng-model="memberid">
    </div>
    <div class="col-md-3">
		<label>Booking Date</label>
		<input class="jDate1 row form-control" ng-model="bookingDate" type="text" required="required" ></input>
	</div>
    <div class="col-sm-offset-0 col-sm-4">
      <button type="submit" class="btn filtrBtnn" ng-click="getBookings()">Get Bookings</button>
    </div>
  </div>
</form>
		</div>
			<div class="row slot-container">
				  <div ng-show="noBooking"><label><h5>No booking found on given date by the given member.</h5></label></div>
				  <div ng-show="slots.length>0"><label><h6>Member : {{memberName}}</h6></label></div>
			      <div class="slot" ng-click="addToList(item)" ng-class="selectedFlag" ng-repeat="item in slots"><span>{{item.sessionStartTime|slotformat}}-{{item.sessionEndTime|slotformat}}</span><br><span class="facText">{{item.facilityId.facilityName}}</span></div>
			</div>
			<hr>
			<div class="row todateform" ng-show="displayflag">
			 <label><h6>You have selected {{list.length}} slot(s) for edit.</h6></label>
			  <h5><b>Shift the Booking To:</b></h5>
				<div class="col-md-3">
				<label>Proposed Date</label>
				<input class="jDate1 row form-control" ng-model="proposedDate" type="text" required="required" ></input>
				</div>
				<div class="col-md-3">
				<button type="submit" class="btn filtrBtnn" ng-click="allAvailableTimeTables()">View Slots</button>
				</div>
			</div>
			<div class="row" ng-show="availables.length>0" >
			<label><h6>Please click on the slot you wish to shift the booking.</h6></label>
			</div>
			<div class="row availables" ng-show="displayflag">			  
			  <div class="slotAvailable" ng-click="checkMemberValidity(item)"  ng-repeat="item in availables"><span>{{item.sessionStartTime|slotformat}}-{{item.sessionEndTime|slotformat}}</span><br><span class="facText">{{item.facilityId.facilityName}}</span></div>				
			</div>
			<hr>
			<div class="row editbtn">
				<button type="submit" class="btn filtrBtnn" ng-show="editableSlot" ng-click="checkMemberValidity()">Edit Slot</button>
			
			</div>
	</div>
		<!-- END MAIN CONTENT -->
	</div>
	<script src="../static/js/sports.min.js"></script>
	<style>
	.filtrBtnn{
width: 56%;
    height: 30px;
    background-color: #3c56a2;
    color: white;
    margin-top:22px;
}
.errormsg{
color:red;
}
.form-cont{
border: solid 1px #b0b0ec;
    border-radius: 13px;
    padding: 7px;
    margin: 13px;
    background: white;
}
.form-cont label, .form-cont span{
	font-weight : bold;
}
.form-cont .col-sm-3{
margin-top:8px !important;
}
.form-group label{
	font-weight : bold;
}
.slot{
    float: left;
    width: 125px;
    height: 45px;
    background: #5ed25e;
    margin-left: 5px;
    margin-top: 5px;
    color: white;
    line-height: 2;
    text-align: center;
    text-overflow: ellipsis;
    overflow: hidden;
}
.slotAvailable{
float: left;
    width: 125px;
    height: 45px;
    background: #5ed25e;
    margin-left: 5px;
    margin-top: 5px;
    color: white;
    line-height: 2;
    text-align: center;
    text-overflow: ellipsis;
    overflow: hidden;
    background: #5ed25e;
}
.slotAvailable:hover {
    background: #50904f;
}
.selected{
background: green !important;
}
.todateform{
padding-left: 11px;
}
.facText{
font-size:11px;
}
	</style>