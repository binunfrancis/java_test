<div ng-controller="bookingController">
	<div id="ribbon">
		<ol class="breadcrumb">
			<li>Home</li>
			<li>Booking</li>
		</ol>
	</div>
	<div id="content">
		<div class="row">
			<div class="dash col-md-12">
				<div class="breadcrumb">
					<h1 class="page-title txt-color-blueDark">
						<i class="fa-fw fa fa-home"></i>Booking
					</h1>
				</div>
			</div>
		</div>
		<section id="widget-grid">
			<div class="container-2">
				<div id="page-wrapper">
					<div class="row">
						<div class="row">
							<div>
								<div class="col-md-12">
									<div id="calendar-container"></div>
								</div>
								<script type="text/javascript">
									$(function () {
										$.get( "../api/allHolidays", function( holidays ) {
											var calHolidays = {};
											for (var int = 0; int < holidays.length; int++) {
												var date = new Date(holidays[int].date);
												if (!calHolidays[date.getFullYear()])
													calHolidays[date.getFullYear()] = {};
												if (!calHolidays[date.getFullYear()][date.getMonth() + 1])
													calHolidays[date.getFullYear()][date.getMonth() + 1] = [];
												calHolidays[date.getFullYear()][date.getMonth() + 1].push(date.getDate());			
											}
											$('#calendar-container').calendar({
												disabled: calHolidays,
												onDateClick: function(day, month, year) {
													$scope = angular.element("#page-wrapper").scope();
													$scope.setSelectedDate(day, month, year);
													$scope.gotoCenter();
													$scope.$apply();
												}
											});
										});
									});
								</script>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
</div>
<script src="../static/js/sports.min.js"></script>