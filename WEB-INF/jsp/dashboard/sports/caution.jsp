	<div> 

		<!-- RIBBON -->
		<div id="ribbon">
			<!-- breadcrumb -->
			<ol class="breadcrumb">
				<li>Home</li>
				<li>Accounts</li>
			</ol>
			<!-- end breadcrumb -->

		</div>
		<!-- END RIBBON -->

		<!-- MAIN CONTENT -->
		<div id="content">
			<div class="row">
				<div class="dash col-md-12">
					<div class="breadcrumb">
						<h1 class="page-title txt-color-blueDark">
							<i class="fa-fw fa fa-home"></i>Sports
						</h1>
<!-- 						<a href="#centerRestore" class="header-a" title="Restore Deleted Centers"><i class="fa fa-trash fa-lg"></i></a> -->
<!-- 						<a href="javascript:void(0)" ng-show="roleAccess[page] === 2"  ng-click="addCenter()" class="btn small-btn-blue btn-lg pull-right header-btn hidden-mobile"><i class="fa fa-circle-arrow-up fa-lg"></i>Add Center</a> -->
					</div>
				</div>
			</div>
			
			<div class="row">
		<form class="form-horizontal">
  <div class="form-group">
    <label for="inputEmail3" class="col-sm-1 control-label">Receipt No</label>
    <div class="col-sm-3">
      <input type="text" class="form-control" id="inputEmail3" placeholder="Receipt No" ng-model="recNo">
    </div>
    <div class="col-sm-offset-0 col-sm-4">
      <button type="submit" class="btn filtrBtn" ng-click="getCaution()">Get Data</button>
    </div>
  </div>
</form>
		</div>
		<div class="row">
		<form class="form-horizontal">
		<div class="form-cont">
		  <div class="form-group">
		    <label for="inputEmail3" class="col-sm-4 control-label">Receipt Date :</label>
		    <div class="col-sm-3">
		      <span class="" id="inputEmail3" placeholder="Receipt No" ng-bind="recDate|formatterCaution"></span>
		    </div>
		  </div>
		  <div class="form-group">
		    <label for="inputEmail3" class="col-sm-4 control-label">Member Name :</label>
		    <div class="col-sm-3">
		      <span type="text" class="" id="inputEmail3"  ng-bind="memName"></span>
		    </div>
		  </div>
		  <div class="form-group">
		    <label for="inputEmail3" class="col-sm-4 control-label">Receipt Amount :</label>
		    <div class="col-sm-3">
		      <span type="text" class="" id="inputEmail3"  ng-bind="recAmount"></span>
		    </div>
		  </div>
		  <div class="form-group">
		    <label for="inputEmail3" class="col-sm-4 control-label">Refundable Amount :</label>
		    <div class="col-sm-3">
		      <span type="text" class="" id="inputEmail3"  ng-bind="refAmount"></span>
		    </div>
		  </div>
		  </div>
		  <div class="form-group">
		    <label for="inputEmail3" class="col-sm-4 control-label">Refund Amount </label>
		    <div class="col-sm-3">
		      <input type="number" class="form-control" id="inputEmail3"  ng-model="refundingAmount">
		    
		    </div>
		    <div class="col-sm-4">
		      <span class="errormsg" ng-show="refunderror" ng-bind="errormessage"></span>
		    </div>
		  </div>
		  <div class="form-group btnsect">
		   <div class="col-sm-offset-5 col-sm-2">
      				<button type="submit" class="btn filtrBtn" ng-disabled="disablerefund" ng-click="refund()">Re fund</button>
    		</div>
		  </div>
		</form>
		
		</div>
			
	</div>
		<!-- END MAIN CONTENT -->
	</div>
	<script src="../static/js/sports.min.js"></script>
	<style>
	.filtrBtn{
width: 56%;
    height: 30px;
    background-color: #3c56a2;
    color: white;
}
.errormsg{
color:red;
}
.form-cont{
border: solid 1px #b0b0ec;
    border-radius: 13px;
    padding: 7px;
    margin: 13px;
    background: white;
}
.form-cont label, .form-cont span{
	font-weight : bold;
}
.form-cont .col-sm-3{
margin-top:8px !important;
}
.form-group label{
	font-weight : bold;
}
	</style>