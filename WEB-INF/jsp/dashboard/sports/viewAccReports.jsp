
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>


<!-- 	accountController -->

<!-- MAIN CONTENT -->
<div id="content">
	<div class="row">
		<div class="dash col-md-12">
			<div class="breadcrumb">
				<h1 class="page-title txt-color-blueDark">
					<i class="fa-fw fa fa-home"></i>Sports
				</h1>

			</div>
		</div>
	</div>





</div>
<!-- END MAIN CONTENT -->

</div>
<!-- END MAIN PANEL -->

<section id="widget-grid" class="">
	<!-- row -->
	<div class="bookReportBody">

		<!-- NEW WIDGET START -->
		<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div ng-controller="accountController as showCase">
		
		<div class="col-xs-12">
		<div class="row filter-box-container col-md-12">
				<div class="row uppersect">
					<div class="col-md-3">
					<label>Start Date</label>
					<input class="jDate1 row form-control" ng-model="startDate" type="text" name="centerE"	required="required" ></input>
					</div>
					<div class=" col-md-3">
					<label>End Date</label>
				 	<input class="jDate1 form-control" ng-model="endDate" type="text" name="centerEndDate" required="required"></input> 
					</div>
					<div class=" col-md-3">
					<label>Choose centre</label>
				 	<select class=" form-control" ng-model="centername" ng-options="x for x in centerNames" placeholder="center name">
				 	</select> 
					</div>
					<div class=" col-md-3">
				 	<input type="button" class="btn btn-default col-xs-2 bookingFilterBtn" ng-click="showCase.dtInstance.changeData(showCase.newPromise)" value="Filter"></input>
					</div>
				</div>
				<div class="row uppersect lower">
					<div class="col-md-6">
					 	<span class="">Total Debit Amount : </span>
					 	<span ng-bind="totaldebitamount">400</span>
					 	&nbsp &nbsp &nbsp
					 	<span class="">Total Credit Amount : </span>
					 	<span ng-bind="totalcreditamount">700</span>
					</div>
					
				</div>
		</div>
	
				<table id="acounttable" datatable="" dt-options="showCase.dtOptions" dt-instance="showCase.dtInstance"
					dt-columns="showCase.dtColumns" class="row-border hover table table-striped table-bordered">
					<tfoot>
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td> Total Debit Amount : 600</td>
							<td> Total Debit Amount : 900</td>
						</tr>
					</tfoot>
				</table>
			</div>
		
		</article>

	</div>
</section>
<script src="../static/js/sports.min.js"></script>
<style>
#acounttable tfoot{
display:none !important;
}
.filter-box-container label {
font-weight:bold !important;
}
.bookingFilterBtn{
width:66%;
    margin-top: 21px;
    background-color: slategrey;
    color: white;
    font-weight: bold;
}
.jDate1{
margin-left:0px !important;
}
.filter-box-container input,.filter-box-container select{
border-radius:6px !important;
}
.row.lower{
font-weight:bold;
margin-top:7px;
}
#acounttable_wrapper .dt-buttons{
margin-top: -30px;
}
</style>