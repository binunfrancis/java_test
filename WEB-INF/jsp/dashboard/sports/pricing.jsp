	<div> 

		<!-- RIBBON -->
		
		<!-- END RIBBON -->

		<!-- MAIN CONTENT -->
		<div id="content">
			<div class="row">
				<div class="dash col-md-12">
					<div class="breadcrumb">
						<h1 class="page-title txt-color-blueDark">
							<i class="fa-fw fa fa-home"></i>Sports
						</h1>
						<a href="#pricingRestore" class="header-a" title="Restore Deleted leaves"><i class="fa fa-trash fa-lg"></i></a>
						<a href="javascript:void(0)" ng-show="roleAccess[page] === 2"  ng-click="addPricing()" class="btn small-btn-blue btn-lg pull-right header-btn hidden-mobile"><i class="fa fa-circle-arrow-up fa-lg"></i>Add Pricing</a>
					</div>
				</div>
			</div>

			<!-- Modal -->
			<div class="modal fade" id="myModal" tabindex="-1" role="dialog">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"
								aria-hidden="true">&times;</button>
							<h4 class="modal-title">{{modalTitle}} Pricing</h4>
						</div>
						<div class="modal-body no-padding">

							<form class="smart-form" ng-submit="savePricing()">

								<fieldset>
									<section>
									<div class="row">
										<label class="label col col-3">Booking Type</label>
										<div class="col col-9">
											<label class="input">
												<select class="form-control" ng-model="bType" required="required" ng-change="loadBTypes()" ng-disabled="modalTitle === 'Edit'">
												    <option value=""></option>
													<option value="Center">Center</option>
													<option value="Facility">Facility</option>
													<option value="Sub Facility">Sub Facility</option>
												</select>
											</label>
										</div>
									</div>
									</section>
									
									<section>
									<div class="row">
										<label class="label col col-3">Select {{bType}}</label>
										<div class="col col-9">
											<label class="input">
												<select ng-options="item[bTypeId] as item[bTypeName] for item in bTypes"
													class="form-control" ng-model="bookingType" autocomplete="off" required="required">
													<option value=""></option>
												</select>
											</label>
										</div>
									</div>
									</section>
									
									<section>
									<div class="row">
										<label class="label col col-3">Select Master Type</label>
										<div class="col col-9">
											<label class="input">
												<select ng-options="item.masterTypeId as item.masterTypeName for item in masterTypes"
													class="form-control" ng-model="price.masterTypeId.masterTypeId" autocomplete="off" required="required">
													<option value=""></option>
												</select>
											</label>
										</div>
									</div>
									</section>
									<section  ng-hide="bType === 'Center'">
									<div class="row">
										<label class="label col col-3">Rate in Hour</label>
										<div class="col col-9">
											<label class="input"> <input ng-model="price.ratePerHour"
													type="text" class= "jFloat" ng-disabled="bType === 'Center'" required="required"></input>
											
											</label>
										</div>
									</div>
									</section>
									<section ng-show="bType === 'Center'">
									<div class="row">
										<label class="label col col-3">Rate For Ist 12 Hours</label>
										<div class="col col-9"> 
											<label class="input"> <input ng-model="price.ratePerDay"
													type="text"  class= "jFloat" required="required"></input>
											
											</label>
										</div>
									</div>
									</section>
									
									
									
									<section ng-hide="bType === 'Center'">
									<div class="row">
										<label class="label col col-3">Rate in Daily</label>
										<div class="col col-9">
											<label class="input"> <input ng-model="price.ratePerDay"
													type="text"  class= "jFloat" required="required"></input>
											
											</label>
										</div>
									</div>
									</section>
									
									<section  ng-hide="bType === 'Center'">
									<div class="row">
										<label class="label col col-3">Rate in Weekly</label>
										<div class="col col-9">
											<label class="input"> <input ng-model="price.ratePerWeek" ng-disabled="bType === 'Center'"
													type="text" class= "jFloat" required="required"></input>
											
											</label>
										</div>
									</div>
									</section>
									
									<section ng-hide="bType === 'Center'">
									<div class="row">
										<label class="label col col-3">Rate in Monthly</label>
										<div class="col col-9">
											<label class="input"> <input ng-model="price.ratePerMonth" ng-disabled="bType === 'Center'"
													type="text" class= "jFloat" required="required"></input>
											
											</label>
										</div>
									</div>
									</section>
									
									<section ng-show="bType === 'Center'" >
									<div class="row">
										<label class="label col col-3">Rate For 2nd 12 Hours</label>
										<div class="col col-9">
											<label class="input"> <input ng-model="price.ratePerMonth" 
													type="text" class= "jFloat" required="required"></input>
											
											</label>
										</div>
									</div>
									</section>
									
									
								</fieldset>

								<footer>
									<input type="submit" class="btn btn-primary" id="button" value="Save"></input>
									<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
								</footer>
							</form>


						</div>

					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->

			<!-- widget grid -->
			<section id="widget-grid" class=""> <!-- row -->
			<div class="row">

				<!-- NEW WIDGET START -->
				<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-3"
					data-widget-editbutton="false">

					<header> <span class="widget-icon"> <i
						class="fa fa-table"></i>
					</span>
					<h2>Pricing</h2>
					

					</header>

					<!-- widget div-->
					<div>

						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->
						</div>
						<!-- end widget edit box -->

						<!-- widget content -->
						<div class="widget-body no-padding">
							<div class="dt-toolbar">
								<div class="col-xs-12 col-sm-6">
									<div class="dataTables_filter">
										<label>
											<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
											<input ng-model="search" class="form-control">
										</label>
									</div>
								</div>
							</div>
							<table class="table table-striped table-bordered table-hover">
								<thead>
									<tr>
										<th data-hide="phone">Booking Type</th>
										<th data-hide="phone">Center</th>
										<th data-hide="phone">Facility</th>
										<th data-hide="phone">Sub Facility</th>
										<th data-hide="phone">Master Type</th>
										<th data-hide="phone">Rate Per Hour</th>
										<th data-hide="phone">Rate Per Daily</th>
										<th data-hide="phone">Rate Per Weekly</th>
										<th data-hide="phone">Rate Per Monthly</th>
										<th data-hide="phone,tablet">Edit</th>
										<th>Delete</th>
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="x in pricings | filter : search | orderBy:'-priceId'"  ng-show="$index >= showingFrom - 1 && $index <= showingTo - 1">
										<td>{{x.centreId ? 'Center' : x.facilityId ? 'Facility' : 'Sub Facility'}}</td>
										<td>{{x.centreId.centreName}}</td>
										<td> {{x.facilityId.facilityName}}</td>
										<td>{{x.subFacilityId.subFacilityName}}</td>
										<td>{{x.masterTypeId.masterTypeName}}</td>
										<td>{{x.ratePerHour}}</td>
										<td>{{x.ratePerDay}}</td>
										<td>{{x.ratePerWeek}}</td>
										<td>{{x.ratePerMonth}}</td>
										<td ng-show="roleAccess[page] === 2" ><a href="javascript:void(0)" ng-click="editPricing(x)"><i class="fa fa-edit"></i></a></td>
										<td ng-show="roleAccess[page] === 2" ><a href="javascript:void(0)" ng-click="deletePricing(x.priceId, $index)"><i class="fa fa-trash-o"></i></a></td>
									</tr>
								</tbody>
							</table>
							<div pagination></div>
						</div>
						<!-- end widget content -->
					</div>
					<!-- end widget div -->
				</div>
				</article>

			</div>
			</section>

		</div>
		<!-- END MAIN CONTENT -->

	</div>
	<!-- END MAIN PANEL -->
	
  <script src="../static/js/sports.min.js"></script>