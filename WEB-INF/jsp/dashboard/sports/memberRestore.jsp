<div ng-controller="memberRestoreController">
	<div id="ribbon">
		<ol class="breadcrumb">
			<li>Home</li>
			<li>Member</li>
		</ol>
	</div>
	<div id="content">
		<div class="row">
			<div class="dash col-md-12">
				<div class="breadcrumb">
					<h1 class="page-title txt-color-blueDark">
						<i class="fa-fw fa fa-home"></i>Sports
					</h1>
					<a href="#memberSports" class="header-a" title="Go Back"><i class="fa fa-chevron-left fa-lg"></i></a>
				</div>
			</div>
		</div>

		<section id="widget-grid" class=""><div class="row"><article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="jarviswidget jarviswidget-color-blueDark">
				<header>
					<span class="widget-icon"> <i class="fa fa-table"></i> </span>
					<h2>Member</h2>
				</header>
				<div>
					<div class="widget-body no-padding">
						<div class="dt-toolbar">
							<div class="col-xs-12 col-sm-6">
								<div class="dataTables_filter">
									<label>
										<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
										<input ng-model="search" class="form-control">
									</label>
								</div>
							</div>
						</div>
						<table class="table table-striped table-bordered table-hover">
							<thead>
								<tr>
									<th data-hide="phone">Member Name</th>
									<th data-hide="phone">Membership Type</th>
									<th data-hide="phone">Member BarCode</th>
									<th data-hide="phone,tablet">Restore</th>
								</tr>
							</thead>
							<tbody>
								<tr ng-repeat="rs in members | filter : search">
									<td>{{ rs.memberName }}</td>
									<td>{{ rs.memberShipTypeId.memberShipTypeName }}</td>
									<td>{{ rs.memberBarCode }}</td>
									<td><a href="javascript:void(0)" ng-click="deleteMember(rs, $index)"><i class="fa fa-undo"></i></a></td>
								</tr>
							</tbody>
						</table>

					</div>
				</div>
			</div>
		</article></div></section>
	</div>
</div>