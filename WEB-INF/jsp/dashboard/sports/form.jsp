<div id="ribbon">
	<ol class="breadcrumb">
		<li>Home</li>
		<li>Member</li>
	</ol>
</div>
<div id="content">
	<div class="row">
		<div class="dash col-md-12">
			<div class="breadcrumb">
				<h1 class="page-title txt-color-blueDark">
					<i class="fa-fw fa fa-home"></i>Sports
				</h1>
			</div>
		</div>
	</div>

	<section id="widget-grid" class=""><div class="row"><article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="jarviswidget jarviswidget-color-blueDark">
			<object data="/static/temp/${path}" type="application/pdf" width="100%" height="1200">
		        <embed src="/static/temp/${path}" type="application/pdf" />
		    </object>
		</div>
	</article></div></section>
</div>
<script src="../static/js/sports.min.js"></script>