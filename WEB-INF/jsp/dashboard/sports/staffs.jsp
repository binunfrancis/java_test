
<div ng-controller="staffController">
	
	<div id="content">
		<div class="row">
			<div class="dash col-md-12">
				<div class="breadcrumb">
					<h1 class="page-title txt-color-blueDark">
						<i class="fa-fw fa fa-home"></i>Sports
					</h1>
						<a href="#staffRestore" class="header-a" title="Restore Deleted Staffs"><i class="fa fa-trash fa-lg"></i></a>
						<a href="javascript:void(0)" ng-click="addMember()" class="btn small-btn-blue btn-lg pull-right header-btn hidden-mobile"><i class="fa fa-circle-arrow-up fa-lg"></i>Add Staff</a>
				</div>
			</div>
		</div>

		<div class="modal fade" id="myModal" tabindex="-1" role="dialog"><div class="modal-dialog modal-width"><div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">{{modalTitle}} Staff</h4>
			</div>
			<div class="modal-body no-padding"><div class="widget-body"><div class="row">
				<form class="smart-form" id="wizard-1" ng-submit="saveMember()">
					<div id="bootstrap-wizard-1" class="col-sm-12">
						<div class="form-bootstrapWizard mt-25 ml-60">
							<ul class="bootstrapWizard form-wizard">
								<li class="active" data-target="#step1" style="pointer-events: none;">
									<a data-target="#tab1" data-toggle="tab"><span class="step">1</span><span class="title">Basic Information</span></a>
								</li>
								<li data-target="#step2" style="pointer-events: none;">
									<a data-target="#tab2" data-toggle="tab"><span class="step">2</span><span class="title">Address Information</span></a>
								</li>
								<li data-target="#step3" style="pointer-events: none;">
									<a data-target="#tab3" data-toggle="tab"><span class="step">3</span><span class="title">Authentication Information</span></a>
								</li>
								<li data-target="#step4" style="pointer-events: none;">
									<a data-target="#tab4" data-toggle="tab"><span class="step">4</span><span class="title">Upload Photo</span></a>
								</li>
								<li data-target="#step5" style="pointer-events: none;">
									<a data-target="#tab5" data-toggle="tab"><span class="step">5</span><span class="title">Save Form</span></a>
								</li>
							</ul>
							<div class="clearfix"></div>
						</div>
						<div class="tab-content">
<!-- 						t-20 -->
							<div class="tab-pane active" id="tab1"><div class="p-60-40">
								<br>
								<h3><strong>Step 1</strong> - Basic Information</h3>

								
								<div class="row">
									<div class="col-sm-6"><div class="form-group t-20 t-20-blocker"><div class="input-group">
										<span class="input-group-addon"><i class="fa fa-pencil-square-o fa-lg fa-fw" style="line-height: .75em;"></i></span>
										<input ng-model="member.memberName" placeholder="Staff Name" class="form-control input-lg jName" type="text" required="required"></input>
									</div></div></div>
									<div class="col-sm-6"><div class="form-group"><div class="input-group">
										<span class="input-group-addon"><i class="fa fa-user fa-lg fa-fw" style="line-height: .75em;"></i></span>
										<select ng-options="item.memberShipId as item.memberShipTypeName for item in memberShipTypes"
											class="form-control input-lg p-10" ng-model="member.memberShipTypeId.memberShipId" autocomplete="off" name="msId">
											<option value=""></option>
										</select>
									</div></div></div>
								</div>
								<div class="row">
									<div class="col-sm-6"><div class="form-group"><div class="input-group">
										<span class="input-group-addon"><i class="fa fa-check fa-lg fa-fw" style="line-height: .75em;"></i></span>
										<input ng-model="member.fatherName" placeholder="Father Name" name="mValid" class="form-control input-lg" type="text" required="required"></input>
									</div></div></div>
									<!--  <div class="col-sm-6"><div class="form-group"><div class="input-group">
										<span class="input-group-addon"><i class="fa fa-id-card fa-lg fa-fw" style="line-height: .75em;"></i></span>
										<input ng-model="member.age" placeholder="Staff Age" type="text" class="form-control input-lg jInt" ></input>
									</div></div></div>-->
									<div class="col-sm-3"><div class="form-group"><div class="input-group">
										<span class="input-group-addon"><i class="fa fa-id-card fa-lg fa-fw" style="line-height: .75em;"></i></span>
										<input ng-model="member.dob" placeholder="DOB" name="mValid" class="jDate input-lg" type="text" required="required"></input>
									</div></div></div>
									<div class="col-sm-3"><div class="form-group"><div class="input-group">
										<span class="input-group-addon"><i class="fa fa-id-card fa-lg fa-fw" style="line-height: .75em;"></i></span>
										<input ng-model="member.age" placeholder="Staff Age" type="text" class="form-control input-lg jInt" readonly></input>
									</div></div></div>
								</div>
							<div class="row">
									<div class="col-sm-6"><div class="form-group"><div class="input-group">
										<span class="input-group-addon"><i class="fa fa-check fa-lg fa-fw" style="line-height: .75em;"></i></span>
										<input ng-model="member.memberTypeStartDate" placeholder="Member Start Type Validity" readonly="readonly" style="border-top: none; border-left: none; border-right: none;border-bottom: 1px solid #ccc;height: 46px;" name="mValid" class="jDate input-lg stDate1" type="text" required="required"  ng-disabled="modalTitle === 'Edit'"></input>
									</div></div></div>
								</div>
								<div class="row">
									<div class="col-sm-6"><div class="form-group"><div class="input-group">
										<span class="input-group-addon"><i class="fa fa-check fa-lg fa-fw" style="line-height: .75em;"></i></span>
										<input ng-model="member.memberTypeValidity"  placeholder="Member End Type Validity" readonly="readonly" style="border-top: none; border-left: none; border-right: none;border-bottom: 1px solid #ccc;height: 46px;" name="mValid" class="jDate input-lg stDate2" type="text" required="required"  ng-disabled="modalTitle === 'Edit'"></input>
									</div></div></div>
									<div class="col-sm-6"><div class="form-group"><div class="input-group">
										<span class="input-group-addon"><i class="fa fa-id-card fa-lg fa-fw" style="line-height: .75em;"></i></span>
										<input ng-model="member.memberAadharNo" placeholder="Staff Aadhar No" type="text" class="form-control input-lg" name="aadhar"></input>
									</div></div></div>
								</div>

								<div class="row">
<!-- 									<div class="col-sm-6"><div class="form-group"><div class="input-group"> -->
<!-- 										<span class="input-group-addon"><i class="fa fa-barcode fa-lg fa-fw" style="line-height: .75em;"></i></span> -->
<!-- 										<input ng-model="member.memberBarCode" placeholder="Staff Bar Code" type="text" class="form-control input-lg" name="bar"></input> -->
<!-- 									</div></div></div> -->
									<div class="col-sm-6"><div class="form-group"><div class="input-group">
										<span class="input-group-addon"><i class="fa fa-user-secret fa-lg fa-fw" style="line-height: .75em;"></i></span>
										<select ng-options="item.roleId as item.roleName for item in roles"
											class="form-control input-lg p-10" ng-model="member.roleId.roleId" autocomplete="off" name="role">
											<option value=""></option>
										</select>
									</div></div></div>
								</div>
							</div></div>
							<div class="tab-pane" id="tab2"><div class="p-60-40 t-20 t-20-blocker">
								<br>
								<h3><strong>Step 2</strong> - Address Information</h3>

								<div class="row">
									<div class="col-sm-6"><div class="form-group t-20 t-20-blocker"><div class="input-group">
										<span class="input-group-addon"><i class="fa fa-street-view fa-lg fa-fw" style="line-height: .75em;"></i></span>
										<input ng-model="member.street" class="form-control input-lg" placeholder="Street" type="text" name="street"></input>
									</div></div></div>
									<div class="col-sm-6"><div class="form-group t-20 t-20-blocker"><div class="input-group">
										<span class="input-group-addon"><i class="fa fa-building-o fa-lg fa-fw" style="line-height: .75em;"></i></span>
										<input ng-model="member.city" type="text" class="form-control input-lg" placeholder="City" name="city"></input>
									</div></div></div>
								</div>

								<div class="row">
									<div class="col-sm-6"><div class="form-group"><div class="input-group">
										<span class="input-group-addon"><i class="fa fa-map-marker fa-lg fa-fw" style="line-height: .75em;"></i></span>
										<input ng-model="member.district" type="text" class="form-control input-lg" placeholder="District" name="district"></input>
									</div></div></div>
									<div class="col-sm-6"><div class="form-group"><div class="input-group">
										<span class="input-group-addon"><i class="fa fa-location-arrow fa-lg fa-fw" style="line-height: .75em;"></i></span>
										<input ng-model="member.state" type="text" class="form-control input-lg" placeholder="State"  name="state"></input>
									</div></div></div>
								</div>

								<div class="row">
									<div class="col-sm-6"><div class="form-group"><div class="input-group">
										<span class="input-group-addon"><i class="fa fa-globe fa-lg fa-fw" style="line-height: .75em;"></i></span>
										<input ng-model="member.country" type="text" class="form-control input-lg" placeholder="Country" name="country"></input>
									</div></div></div>
									<div class="col-sm-6"><div class="form-group"><div class="input-group">
										<span class="input-group-addon"><i class="fa fa-envelope fa-lg fa-fw" style="line-height: .75em;"></i></span>
										<input ng-model="member.pincode" type="text" class="form-control input-lg jInt" placeholder="Pincode" name="postal"></input>
									</div></div></div>
								</div>

								<div class="row">
									<div class="col-sm-6"><div class="form-group"><div class="input-group">
										<span class="input-group-addon"><i class="fa fa-address-card-o fa-lg fa-fw" style="line-height: .75em;"></i></span>
										<input ng-model="member.memberContactNo" type="text" class="form-control input-lg jInt" placeholder="Contact No" name="mobileNo"></input>
									</div></div></div>
									<div class="col-sm-6"><div class="form-group"><div class="input-group">
										<span class="input-group-addon"><i class="fa fa-envelope-o fa-lg fa-fw" style="line-height: .75em;"></i></span>
										<input ng-model="member.email" type="email" class="form-control input-lg" placeholder="Email" name="email"></input>
									</div></div></div>
								</div>

							</div></div>							
							<div class="tab-pane" id="tab3"><div class="p-60-40 t-20 t-20-blocker">
								<br>
								<h3><strong>Step 3</strong> - Authentication Information</h3>
								<div class="row">
									<div class="col-sm-6"><div class="form-group t-20 t-20-blocker"><div class="input-group">
										<span class="input-group-addon"><i class="fa fa-user-circle fa-lg fa-fw" style="line-height: .75em;"></i></span>
										<input ng-model="member.userName" type="text" class="form-control input-lg" placeholder="Username" ng-disabled="modalTitle === 'Edit'"></input>
									</div></div></div>
									<div class="col-sm-6"><div class="form-group t-20 t-20-blocker"><div class="input-group">
										<span class="input-group-addon"><i class="fa fa-key fa-lg fa-fw" style="line-height: .75em;"></i></span>
										<input ng-model="member.password" type="password" class="form-control input-lg" placeholder="Password" ng-disabled="modalTitle === 'Edit'"></input>
									</div></div></div>
								</div>

							</div></div>							
							<div class="tab-pane" id="tab4"><div class="p-60-40 t-20 t-20-blocker">
								<br>
								<h3><strong>Step 4</strong> - Upload Photo</h3>
								<div class="row">
									<div class="col-sm-4"><div class="form-group t-20 t-20-blocker"><div class="input-group">
										<input class="form-control" type="file" file-model="uploadedFile" placeholder="Upload File" filepreview="filepreview"></input>
									</div></div></div>
									<div class="col-sm-4"><div class="form-group t-20 t-20-blocker"><div class="input-group">
										<h3>Preview</h3>
										<div class="photo"><img ng-src="{{ filepreview }}" class="img-responsive" ng-show="filepreview"/></div>
									</div></div></div>
									<div class="col-sm-4"><div class="form-group t-20 t-20-blocker"><div class="input-group">
										<h3>Original</h3>
										<div class="photo"><img ng-src="../static/temp/{{ member.memberPhoto }}" class="img-responsive" /></div>
									</div></div></div>
								</div>
							</div></div>
							<div class="tab-pane" id="tab5"><div class="p-60-40 t-20 t-20-blocker">
								<br><h3><strong>Step 5</strong> - Save Form </h3><br>
								<h1 class="text-center text-success t-20 t-20-blocker"><strong><i class="fa fa-check fa-lg"></i> Complete</strong></h1>
								<div class="col-md-12" >
									<button type="submit" class="btn btn-lg center-block txt-color-darken" style="width: 20%;" >Save</button> 
								</div>
								<br><br>
							</div></div>
							
							<div class="form-actions mr">
								<div class="row">
									<div class="col-sm-12">
										<ul class="pager wizard no-margin">
											<li class="previous disabled"><a id="wiz-prev" href="javascript:void(0);" class="btn btn-lg btn-default"> Previous </a></li>
											<li class="next"><a href="javascript:void(0);" class="btn btn-lg txt-color-darken"> Next </a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>

					</div>
				</form>
			</div></div></div>
		</div></div></div>

		<section id="widget-grid" class=""><div class="row"><article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="jarviswidget jarviswidget-color-blueDark">
				<header>
					<span class="widget-icon"> <i class="fa fa-table"></i> </span>
					<h2>Staffs</h2>
				</header>
				<div>
					<div class="widget-body no-padding">
						<div class="dt-toolbar">
							<div class="col-xs-12 col-sm-6">
								<div class="dataTables_filter">
									<label>
										<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
										<input ng-model="search" class="form-control">
									</label>
								</div>
							</div>
						</div>
						<table class="table table-striped table-bordered table-hover">
							<thead>
								<tr>
									<th data-hide="phone">Staff Name</th>
									<th data-hide="phone">Staff Type</th>
									
									<th data-hide="phone,tablet">Edit</th>
									<th>Delete</th>
								</tr>
							</thead>
							<tbody>
								<tr ng-repeat="rs in staffs | filter : search | orderBy:'-memberId'"  ng-show="$index >= showingFrom - 1 && $index <= showingTo - 1">
									<td>{{ rs.memberName }}</td>
									<td>{{ rs.memberShipTypeId.memberShipTypeName }}</td>
									
									<td><a href="javascript:void(0)" ng-click="editMember(rs, $index)"><i class="fa fa-edit"></i></a></td>
									<td><a href="javascript:void(0)" ng-click="deleteMember(rs, $index)"><i class="fa fa-trash-o"></i></a></td>
								</tr>
							</tbody>
						</table>
						<div pagination></div>
					</div>
				</div>
			</div>
		</article></div></section>
	</div>
</div>
	<script src="../static/js/sports.min.js"></script>
<script>
  var $validator = $("#wizard-1").validate({
	  rules: {
		    email: {
		        required: true,
		        email: "Your email address must be in the format of name@domain.com"
		      },
		      mValid: {
		        required: true
		      },
		      msId: {
		        required: true
		      },
		      aadhar: {
			    required: true
			  },
			  role: {
				    required: true
				  },
			  bar: {
				    required: true
				  },
		      country: {
		        required: true
		      },
		      city: {
		        required: true
		      },
		      postal: {
		        required: true,
		        minlength: 6
		      },
		      district: {
		        required: true,
		      },
		      state: {
		        required: true,
		      },
		      mobileNo: {
		        required: true,
		        minlength: 10,
		        maxlength :13,
		      },
		     street: {
			    required: true,
			      },
			      check: {
					    required: true,
					      },    
			  userName: {
				required: true,
				     },
			  password: {
					    required: true
					  },
	    },
	    
	    messages: {
	      email: {
	        email: "Your email address must be in the format of name@domain.com"
	      },
	    },
	    
    highlight: function (element) {
      $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
    },
    unhighlight: function (element) {
      $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
    },
    errorElement: 'span',
    errorClass: 'help-block',
    errorPlacement: function (error, element) {
      if (element.parent('.input-group').length) {
        error.insertAfter(element.parent());
      } else {
        error.insertAfter(element);
      }
    }
  });
  $('#bootstrap-wizard-1').bootstrapWizard({
    'tabClass': 'form-wizard',
    'onNext': function (tab, navigation, index) {
    	if(document.querySelector(".stDate1").value=="" || document.querySelector(".stDate2").value==""){
      	  alert("Please enter validaity dates");
      	  return false;
        }
      var $valid = $("#wizard-1").valid();
      if (!$valid) {
        $validator.focusInvalid();
        return false;
      } else {
        $('#bootstrap-wizard-1').find('.form-wizard').children('li').eq(index - 1).addClass('complete');
        $('#bootstrap-wizard-1').find('.form-wizard').children('li').eq(index - 1).find('.step').html('<i class="fa fa-check"></i>');
      }
    }
  });
</script>
<style>
#dobfield{
margin-left:0px;
}
.ui-datepicker{
z-index:9999 !important;
}
</style>