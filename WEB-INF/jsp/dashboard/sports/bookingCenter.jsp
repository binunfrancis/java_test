	<div id="ribbon">
		<ol class="breadcrumb">
			<li>Home</li>
			<li>Booking</li>
		</ol>
	</div>
	<div id="content">
		<div class="row">
			<div class="dash col-md-12">
				<div class="breadcrumb">
					<h1 class="page-title txt-color-blueDark">
						<i class="fa-fw fa fa-home"></i>Booking
					</h1>
					<a href="javascript:void(0)" ng-click="goBack()" class="header-a" title="Go Back"><i class="fa fa-chevron-left fa-lg"></i></a>
				</div>
			</div>
		</div>
		<section id="widget-grid">
			<div class="container-2">
				<div id="page-wrapper">
					<div class="row">
						<div class="">
							<div ng-class="c.booked ? 'card cardCheck' : 'card'" ng-repeat="c in centers">
								<div class="card-hover" ng-hide="!c.booked || c.centerTypeId.centerTypeName.toUpperCase() != 'GOVT'">
									<p style="padding: 15px;text-align:center;font-weight:bold">{{c.centreName}}</p>
									<p style="padding: 5px;">The Center is already Booked</p>
									<input type="button" class="form-control btn btn-default" value="Continue" ng-click="bookCenters(c)">
								</div>
								<a href="javascript:void(0)" ng-click="bookCenters(c)">
									<div class="image">{{ c.centreName }}</div>
									<div class="card-container">
										<h3>{{ c.centerTypeId.centerTypeName }}</h3>
										<h4>{{ c.locationId.locationName }}</h4>
									</div>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
<script src="../static/js/sports.min.js"></script>