	<div ng-controller="timeSlotsController"> 

		<!-- RIBBON -->
		<div id="ribbon">
			<!-- breadcrumb -->
			<ol class="breadcrumb">
				<li>Home</li>
				<li>Time Table Session</li>
			</ol>
			<!-- end breadcrumb -->

		</div>
		<!-- END RIBBON -->

		<!-- MAIN CONTENT -->
		<div id="content">
			<div class="row">
				<div class="dash col-md-12">
					<div class="breadcrumb">
						<h1 class="page-title txt-color-blueDark">
							<i class="fa-fw fa fa-home"></i>Sports
						</h1>
						<a href="#timeSlotsRestore" class="header-a" title="Restore Deleted Time Tables"><i class="fa fa-trash fa-lg"></i></a>
						<a href="javascript:void(0)" ng-click="addTimeSlot()" class="btn small-btn-blue btn-lg pull-right header-btn hidden-mobile"><i class="fa fa-circle-arrow-up fa-lg"></i>Add Time Table</a>
					</div>
				</div>
			</div>
			<!-- Filter section -->
			<div class="row">
				<div class=" col-md-3">
					<label>Choose facility</label>
				 	<select class=" form-control" ng-model="facilityName" ng-options="x for x in facilityNames" >
				 	</select> 
				</div>
				<div class=" col-md-3">
				 	<input type="button" class="btn btn-default col-xs-2 filterBtn" ng-click="filterData()" value="Filter"></input>
				</div>
			</div>

			<!-- Modal -->
			<div class="modal fade" id="myModal" tabindex="-1" role="dialog">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"
								aria-hidden="true">&times;</button>
							<h4 class="modal-title">{{modalTitle}} Time Table</h4>
						</div>
						<div class="modal-body no-padding">
							<form class="smart-form" ng-submit="saveTimeSlot()">
								<fieldset>
								<input ng-model="timeSlots.timeSlotsId" type="hidden"></input>
									<section>
									<div class="row">
										<label class="label col col-3">Day</label>
										<div class="col col-9 date-checkbox daylist">
											<input type="checkbox" ng-click="toggleAll()" ng-model="isAllSelected" class="selAll">Select all
										    <br>
										    <div ng-repeat = "option in options" ng-if="option.value!='fake'">
										       <input type="checkbox" ng-model="option.selected" ng-change="optionToggled()">{{option.value }}
										    </div>
										</div>
									</div>
									</section>
									<section>
									<div class="row">
										<label class="label col col-3">Start Time</label>
										<div class="col col-9">
											<label class="input">
												<input ng-model="timeSlots.sessionStartTime"  id="timeformatExample1" type="text" class="time" required="required" ng-disabled="all"/>
											</label>
										</div>
									</div>
									</section>
									<section>
									<div class="row">
										<label class="label col col-3">End Time</label>
										<div class="col col-9">
											<label class="input">
												<input ng-model="timeSlots.sessionCloseTime"  id="timeformatExample2" type="text" class="time" required="required" />
											</label>
										</div>
									</div>
									</section>
									<section>
									<div class="row">
										<label class="label col col-3">Facility</label>
										<div class="col col-9">
											<label class="input">
												<select ng-options="item.facilityId as item.facilityName + ' - ' + item.centerId.centreName for item in facilityList"
													class="form-control" ng-model="timeSlots.facilityId.facilityId" autocomplete="off" required="required">
													<option value=""></option>
												</select>
											</label>
										</div>
									</div>
									</section>
									
									<!-- <section>
									<div class="row">
										<label class="label col col-3">Sub Facility</label>
										<div class="col col-9">
											<label class="input">
												<select ng-options="item.subFacilityId as item.subFacilityName for item in subFacilityList"
													class="form-control" ng-model="timeSlots.subFacilityId.subFacilityId" autocomplete="off" required="required">
													<option value=""></option>
												</select>
											</label>
										</div>
									</div>
									</section> -->
								</fieldset>
								<footer>
									<input type="submit" class="btn btn-primary" id="button" value="Save" ng-disabled="savingtime"></input>
									<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
								</footer>
							</form>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->

			<!-- widget grid -->
			<section id="widget-grid" class=""> <!-- row -->
			<div class="row">

				<!-- NEW WIDGET START -->
				<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-3"
					data-widget-editbutton="false">

					<header> <span class="widget-icon"> <i
						class="fa fa-table"></i>
					</span>
					<h2>Time Table</h2>

					</header>

					<!-- widget div-->
					<div>

						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->
						</div>
						<!-- end widget edit box -->

						<!-- widget content -->
						<div class="widget-body no-padding">
							<div class="dt-toolbar">
								<div class="col-xs-12 col-sm-6">
									<div class="dataTables_filter">
										<label>
											<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
											<input ng-model="search" class="form-control">
										</label>
									</div>
								</div>
							</div>
							<table class="table table-striped table-bordered table-hover">
								<thead>
									<tr>
										<th data-hide="phone">Day</th>
										<th data-hide="phone">Start Time</th>
										<th data-hide="phone">End Time</th>
										<th data-hide="phone">Facility Name</th>
										<!-- <th data-hide="phone">Sub Facility Name</th> -->
										<th data-hide="phone,tablet">Edit</th>
										<th>Delete</th>
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="rs in timeSlotsList | filter : search | orderBy:['facilityId','dayNum']" ng-show="$index >= showingFrom - 1 && $index <= showingTo - 1">
										<td>{{rs.dayNum | dayFilter}}</td>
										<td>{{rs.sessionStartTime}}</td>
										<td>{{rs.sessionCloseTime}}</td>
										<td>{{rs.facilityId.facilityName}} - {{rs.facilityId.centerId.centreName}}</td>
										<!-- <td>{{rs.subFacilityId.subFacilityName}}</td> -->
										<!--<td><input type="checkbox"  ng-model="rs.isActive" id="{{rs.timeSlotsId}}"/></td>-->
										<td ><a href="javascript:void(0)" ng-click="editTimeSlot($event, $index)"><i class="fa fa-edit" id="{{rs.timeSlotsId}}"></i></a></td>
										<td><a href="javascript:void(0)" ng-click="deleteTimeSlot($event, $index)"><i class="fa fa-trash-o" id="{{rs.timeSlotsId}}"></i></a></td>
									</tr>
								</tbody>
							</table>
							<div pagination></div>
						</div>
						<!-- end widget content -->
					</div>
					<!-- end widget div -->
				</div>
				<!-- end widget --> </article>

			</div>
			</section>

		</div>
		<!-- END MAIN CONTENT -->

	</div>
	<!-- END MAIN PANEL -->
	
  <script src="../../static/js/sports.min.js"></script>
  <script>
			$(function() {
				$('#timeformatExample1').timepicker({ 'timeFormat': 'H:i' });
				$('#timeformatExample2').timepicker({ 'timeFormat': 'H:i' });
			});
			
// 			document.querySelector("#timeformatExample2").addEventListener("keypress", function(event) {		         
// 		        event.preventDefault();
// 			}, false);
// 			document.querySelector("#timeformatExample1").addEventListener("keypress", function(event) {		         
// 		        event.preventDefault();
// 			}, false);
	</script>
	
	<style>
	.filterBtn{
	    width: 56%;
    	margin-top: 22px;
    	background-color:#5675d3;;
    	color:white;
	}
	.daylist:first-child{
	display:none !important;
	}
	
	</style>


	
 



	