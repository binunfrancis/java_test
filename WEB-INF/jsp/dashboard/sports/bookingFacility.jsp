	<div id="ribbon">
		<ol class="breadcrumb">
			<li>Home</li>
			<li>Booking</li>
		</ol>
	</div>
	<div id="content">
		<div class="row">
			<div class="dash col-md-12">
				<div class="breadcrumb">
					<h1 class="page-title txt-color-blueDark">
						<i class="fa-fw fa fa-home"></i>Booking
					</h1>
					<a href="javascript:void(0)" ng-click="goBack()" class="header-a" title="Go Back"><i class="fa fa-chevron-left fa-lg"></i></a>
					<a href="javascript:void(0)" ng-click="bookingCenter()" class="btn small-btn-blue btn-lg pull-right header-btn hidden-mobile"><i class="fa fa-circle-arrow-up fa-lg"></i>BOOK {{center.centreName}}</a>
				</div>
			</div>
		</div>
		<section id="widget-grid">
			<div class="container-2">
				<div id="page-wrapper">
					<div class="row">
						<div class="">
							<div ng-class="rs.avail === 0 ? 'card cardCheck' : 'card'" ng-repeat="rs in facilities">
								<div class="card-hover" ng-hide="rs.avail !== 0 || rs.navail === 0 || center.centerTypeId.centerTypeName.toUpperCase() != 'GOVT'">
									<!-- <p style="padding: 15px;text-align:center;font-weight:bold">{{rs.facilityName}}</p> -->
<!-- 									<p style="padding: 10px;">Booked Slots : {{rs.navail}}</p> -->
<!-- 									<p style="padding: 10px;">Available Slots : {{rs.avail}}</p> -->
									<p style="padding: 10px;">Booked Slots : {{rs.bookedAvailFac}}</p>
									<p style="padding: 10px;">Available Slots : {{rs.totalAvail}}</p>
									<!--  <p style="padding: 5px;">The Facility is Already Booked</p>-->
									<input type="button" class="form-control btn btn-default" value="Continue" ng-click="bookFacilities(rs)">
								</div>
								<div class="card-hover" ng-hide="rs.avail === 0">
									<p style="padding: 10px;">Booked Slots : {{rs.navail}}</p>
									<p style="padding: 10px;">Available Slots : {{rs.avail}}</p>
									<input type="button" class="form-control btn btn-default" value="Continue" ng-click="bookFacilities(rs)">
								</div>
								<a href="javascript:void(0)" ng-click="bookFacilities(rs)">
									<div class="image">{{ rs.facilityName}}</div>
									<div class="card-container">
										<h3>{{ rs.facilityTypeId.facilityTypeName }}</h3>
										<h4>{{ rs.centerId.centreName }}</h4>
									</div>
								</a> 
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
<script src="../static/js/sports.min.js"></script>