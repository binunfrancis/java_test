<div ng-controller="bookingController">
	<div id="ribbon">
		<ol class="breadcrumb">
			<li>Home</li>
			<li>Booking</li>
		</ol>
	</div>
	<div id="content">
		<div class="row">
			<div class="dash col-md-12">
				<div class="breadcrumb">
					<h1 class="page-title txt-color-blueDark">
						<i class="fa-fw fa fa-home"></i>Booking
					</h1>
					<a href="javascript:void(0)" ng-show="select === 'subFacility'" ng-click="bookingFacilities()" class="btn small-btn-blue btn-lg pull-right header-btn hidden-mobile"><i class="fa fa-circle-arrow-up fa-lg"></i>BOOK {{facility.facilityName}}</a>
					<a href="javascript:void(0)" ng-show="select === 'facility'" ng-click="bookingCenter()" class="btn small-btn-blue btn-lg pull-right header-btn hidden-mobile"><i class="fa fa-circle-arrow-up fa-lg"></i>BOOK {{center.centreName}}</a>
				</div>
			</div>
		</div>
		<section id="widget-grid">
			<div class="container-2">
				<div id="page-wrapper">
					<div class="row">
						<div class="row">
							<div ng-show="select === 'calendar'">
								<div class="col-md-offset-3 col-md-5">
									<div id="calendar-container"></div>
								</div>
								<script type="text/javascript">
									$(function () {
										$('#calendar-container').calendar({
											disabled: {
												2017:{
													12:[25]
												},
												2018:{
													1:[2,26],
													2:[13],
													3:[29,30],
													4:[14,15],
													5:[1],
													6:[15],
													8:[11,15,22,24,25,26,27,28],
													9:[2,20,21],
													10:[2,18,19],
													11:[6,20],
													12:[25]
												},
												2019:{
													1:[26],
													3:[4],
													4:[14,15,18,19],
													5:[1],
													6:[5],
													7:[31],
													8:[12,15,24],
													9:[10,11,12,13,20],
													10:[2,6,8,27],
													11:[10,12],
													12:[25]
												}
											},
											onDateClick: function(day, month, year) {
												$scope = angular.element("#page-wrapper").scope();
												$scope.setSelectedDate(day, month, year);
												$scope.select = 'center';
												$scope.$apply();
											}
										});
									});
								</script>
							</div>
							<div class="card" ng-repeat="c in centers" ng-show="select === 'center'">
								<a href="javascript:void(0)" ng-click="bookCenters(c)">
									<div class="image">{{ c.centreName }}</div>
									<div class="card-container">
										<h3>{{ c.centerTypeId.centerTypeName }}</h3>
										<h4>{{ c.locationId.locationName }}</h4>
									</div>
								</a> 
							</div>
							<div class="card" ng-repeat="rs in facilities" ng-show="select === 'facility'">
								<a href="javascript:void(0)" ng-click="bookFacilities(rs)">
									<div class="image">{{ rs.facilityName}}</div>
									<div class="card-container">
										<h3>{{ rs.facilityTypeId.facilityTypeName }}</h3>
										<h4>{{ rs.centerId.centreName }}</h4>
									</div>
								</a> 
							</div>
							<div class="card" ng-repeat="rs in subFacilities" ng-show="select === 'subFacility'">
								<a href="javascript:void(0)" ng-click="bookSubFacilities(rs)">
									<div class="image">{{ rs.subFacilityName}}</div>
									<div class="card-container">
										<h3>{{facility.facilityName}}</h3>
										<h4>{{center.centreName}}</h4>
									</div>
								</a>
							</div>
							<div ng-show="select === 'book' || select === 'bookFacility' || select === 'bookCenter'" class="dash">
								<div class="col-md-7" ng-hide="select === 'bookCenter'">`
									<div ng-repeat="x in timeTables">
										<button class="time-slots" ng-class="timeTable == x ? 'time-slots-selected' : ''" ng-click="book(x)" ng-disabled="x.booked">
											{{x.sessionStartTime | timeFormat}} - {{x.sessionEndTime | timeFormat}}
										</button> 
									</div>
								</div>
								<form ng-submit="saveBooking()" class="breadcrumb col-md-5 smart-form" ng-class="select === 'bookCenter' ? 'col-md-offset-4' : ''">
									<section><div class="row">
										<div class="col col-10">
											<div class="input-group">
												<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
												<input ng-model="selectedDate" ng-change="incrementDate(0)" placeholder="Select Date" class="jDate form-control" type="text" required="required"></input>
											</div>
										</div>
										<i class="fa fa-caret-up arrow-up" ng-click="incrementDate(1)"></i>
										<i class="fa fa-caret-down arrow-down" ng-click="incrementDate(-1)"></i>
									</div></section>
									<section><div class="row">
										<label class="label col col-4">Master Type</label>
										<div class="col col-8">
											<label class="input">
												<select ng-options="item.masterTypeId as item.masterTypeName for item in masterTypes" class="form-control"
													ng-model="masterTypeId" autocomplete="off" required="required" ng-change="masterTypeChange()">
													<option value=""></option>
												</select>
											</label>
										</div>
									</div></section>
									<section><div class="row">
										<label class="label col col-4">Member</label>
										<div class="col col-8">
											<label class="input">
												<select ng-options="item.memberId as item.memberName for item in members" class="form-control"
													ng-model="memberId" autocomplete="off" required="required" ng-change="setDiscount()">
													<option value=""></option>
												</select>
											</label>
										</div>
									</div></section>
									
									<section><div class="row">
										<label class="label col col-4">Actual Amount</label>
										<div class="col col-8">
											<label class="input"><input value="{{account.actualAmount}}" type="text" disabled="disabled"></input></label>
										</div>
									</div></section>
									<section><div class="row">
										<label class="label col col-4">Discount</label>
										<div class="col col-8">
											<label class="input">
												<input value="{{account.discount}}" type="text" disabled="disabled"></input>
											</label>
										</div>
									</div></section>
									<section><div class="row">
										<label class="label col col-4">Payable Amount</label>
										<div class="col col-8">
											<label class="input">
												<input value="{{account.payableAmount}}" type="text" disabled="disabled"></input>
											</label>
										</div>
									</div></section>
									<section><div class="row">
										<label class="label col col-4">Paid Amount</label>
										<div class="col col-8">
											<label class="input">
												<input ng-model="account.paidAmount" class="jFloat" type="text" ng-keyup="calcBalance()" required="required"></input>
											</label>
										</div>
									</div></section>
									<section><div class="row">
										<label class="label col col-4">Balance Amount</label>
										<div class="col col-8">
											<label class="input">
												<input ng-model="account.balanceAmount" type="text" disabled="disabled"></input>
											</label>
										</div>
									</div></section>
									<section><div class="row">
										<label class="label col col-4">Payment Type</label>
										<div class="col col-8">
											<label class="input">
												<select class="form-control" ng-model="account.paymentType" required="required">
													<option value=""></option>
													<option value="By Cash">By Cash</option>
													<option value="By Card">By Card</option>
												</select>
											</label>
										</div>
									</div></section>
									<section><div class="row">
										<label class="label col col-4">Reference No</label>
										<div class="col col-8">
											<label class="input">
												<input ng-model="account.referenceNo" type="text" required="required"></input>
											</label>
										</div>
									</div></section>
									<section><div class="row">
										<label class="label col col-4">Book</label>
										<div class="col col-8">
											<label class="input">
												<select class="form-control" ng-model="bookingType" ng-hide="select === 'bookCenter'" ng-change="bookingTypeChange()" ng-options="x for x in bookingTypes"></select>
												<select class="form-control" ng-model="bookingType" ng-show="select === 'bookCenter'" ng-change="bookingTypeChange()">
													<option value="For 12 Hours">For 12 Hours</option>
												</select>
											</label>
										</div>
									</div></section>
									<footer>
										{{message}}
										<input type="submit" class="btn btn-primary" ng-show="available" value="Book Now"></input>
										<div flash-message="3000"></div>
									</footer>
								</form>
							</div>
							<div id="print" ng-show="select === 'print'">
								<h2 style="font-weight: bold;"><center>Gimmy George Branch</center></h2>
								<table class="table table-bordered">
									<tr>
										<td>Member Name</td><td>{{data[0]}}</td>
									</tr>
									<tr>
										<td>Facility</td><td>{{facility.facilityName}}</td>
									</tr>
									<tr>
										<td>Booked Date</td><td>{{data[1]}}</td>
									</tr>
									<tr>
										<td>Payable Amount</td><td>{{account.payableAmount}}</td>
									</tr>
									<tr>
										<td>Paid Amount</td><td>{{account.paidAmount}}</td>
									</tr>
									<tr>
										<td>Balance Amount</td><td>{{account.balanceAmount}}</td>
									</tr>
								</table>
								<style>
									@media print {
										body * {
											visibility: hidden;
										}
										#print, #print * {
											visibility: visible;
										}
										#print {
											position: absolute;
											left: 0;
											top: 0;
										}
									}
								</style>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
</div>
<script src="../static/js/sports.min.js"></script>