	<div ng-controller="leaveTabController"> 

		<!-- RIBBON -->
		<div id="ribbon">
			<!-- breadcrumb -->
			<ol class="breadcrumb">
				<li>Home</li>
				<li>Leave</li>
			</ol>
			<!-- end breadcrumb -->

		</div>
		<!-- END RIBBON -->

		<!-- MAIN CONTENT -->
		<div id="content">
			<div class="row">
				<div class="dash col-md-12">
					<div class="breadcrumb">
						<h1 class="page-title txt-color-blueDark">
							<i class="fa-fw fa fa-home"></i>Sports
						</h1>
						<a href="#leaveTabRestore" class="header-a" title="Restore Deleted leaves"><i class="fa fa-trash fa-lg"></i></a>
						<a href="javascript:void(0)" ng-show="roleAccess[page] === 2"  ng-click="addLeaveTab()" class="btn small-btn-blue btn-lg pull-right header-btn hidden-mobile"><i class="fa fa-circle-arrow-up fa-lg"></i>Add Leaves</a>
					</div>
				</div>
			</div>

			<!-- Modal -->
			<div class="modal fade" id="myModal" tabindex="-1" role="dialog">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"
								aria-hidden="true">&times;</button>
							<h4 class="modal-title">{{modalTitle}} Leave</h4>
						</div>
						<div class="modal-body no-padding">

							<form class="smart-form" ng-submit="saveLeaveTab()">

								<fieldset>
								<input ng-model="leaveTab.leaveTabId" type="hidden"></input>
									
									<section>
									<div class="row">
										<label class="label col col-3">Start Date</label>
										<div class="col col-9">
											<label class="input"><input readonly="readonly" class="jDate" ng-model="leaveTab.leaveDate" type="text" required="required"></input></label>
										</div>
									</div>
									</section>
									
									
									<section>
									<div class="row">
										<label class="label col col-3">Select Center </label>
										<div class="col col-9">
											<label class="input">
												<select ng-options="item.centreId as item.centreName for item in centerList"
													class="form-control" ng-model="leaveTab.centerId.centreId" autocomplete="off" required="required">
													<option value=""></option>
												</select>
											</label>
										</div>
									</div>
									</section>
									
									<section>
									<div class="row">
										<label class="label col col-3">Select Facility</label>
										<div class="col col-9">
											<label class="input">
												<select ng-options="item.facilityId as item.facilityName for item in facilityList" ng-change="loadSubFacilities()"
													class="form-control" ng-model="leaveTab.facilityId.facilityId" autocomplete="off" required="required">
													<option value=""></option>
												</select>
											</label>
										</div>
									</div>
									</section>
									
									<section>
									<div class="row">
										<label class="label col col-3">Select Sub Facility</label>
										<div class="col col-9">
											<label class="input">
												<select ng-options="item.subFacilityId as item.subFacilityName for item in subFacilityList"
													class="form-control" ng-model="leaveTab.subFacilityId.subFacilityId" autocomplete="off" required="required">
													<option value=""></option>
												</select>
											</label>
										</div>
									</div>
									</section>
									<section>
									<div class="row">
										<label class="label col col-3">Select Leave Type</label>
										<div class="col col-9">
											<label class="input">
												<select ng-options="item.leaveTypeId as item.leaveTypeName for item in leaveTypeList"
													class="form-control" ng-model="leaveTab.leaveTypeId.leaveTypeId" autocomplete="off" required="required">
													<option value=""></option>
												</select>
											</label>
										</div>
									</div>
									</section>
									
								</fieldset>

								<footer>
									<input type="submit" class="btn btn-primary" id="button" value="Save"></input>
									<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
								</footer>
							</form>


						</div>

					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->

			<!-- widget grid -->
			<section id="widget-grid" class=""> <!-- row -->
			<div class="row">

				<!-- NEW WIDGET START -->
				<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-3"
					data-widget-editbutton="false">

					<header> <span class="widget-icon"> <i
						class="fa fa-table"></i>
					</span>
					<h2>Leaves</h2>
					

					</header>

					<!-- widget div-->
					<div>

						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->
						</div>
						<!-- end widget edit box -->

						<!-- widget content -->
						<div class="widget-body no-padding">
							<div class="dt-toolbar">
								<div class="col-xs-12 col-sm-6">
									<div class="dataTables_filter">
										<label>
											<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
											<input ng-model="search" class="form-control">
										</label>
									</div>
								</div>
							</div>
							<table class="table table-striped table-bordered table-hover">
								<thead>
									<tr>
										<th data-hide="phone">Leave Date</th>
										<th data-hide="phone">Leave Type</th>
										<th data-hide="phone,tablet">Edit</th>
										<th>Delete</th>
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="rs in leaveTabs | filter : search "  ng-show="$index >= showingFrom - 1 && $index <= showingTo - 1">
										<td>{{rs.leaveDate}}</td>
										<td>{{rs.leaveTypeId.leaveTypeName}}</td>
										<td ng-show="roleAccess[page] === 2" ><a href="javascript:void(0)" ng-click="editLeaveTab($event, $index)"><i class="fa fa-edit" id="{{rs.leaveTabId}}"></i></a></td>
										<td ng-show="roleAccess[page] === 2" ><a href="javascript:void(0)" ng-click="deleteLeaveTab($event, $index)"><i class="fa fa-trash-o" id="{{rs.leaveTabId}}"></i></a></td>
									</tr>
								</tbody>
							</table>
							<div pagination></div>
						</div>
						<!-- end widget content -->
					</div>
					<!-- end widget div -->
				</div>
				<!-- end widget --> </article>

			</div>
			</section>

		</div>
		<!-- END MAIN CONTENT -->

	</div>
	<!-- END MAIN PANEL -->
	
  <script src="../../static/js/sports.min.js"></script>