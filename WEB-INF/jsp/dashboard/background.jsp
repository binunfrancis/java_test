	<div id="ribbon">
		<ol class="breadcrumb">
			<li>Home</li>
			<li>Background Images</li>
		</ol>
	</div>
	<div id="content">
		<div class="row">
			<div class="dash col-md-12">
				<div class="breadcrumb">
					<h1 class="page-title txt-color-blueDark">
						<i class="fa-fw fa fa-home"></i>Sports
					</h1>
				</div>
			</div>
		</div>
		<!-- POPUP -->
		<div class="modal fade" id="myModalExtra" tabindex="-1" role="dialog">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							
						</div>
						<div class="modal-body no-padding">
						  <div>
							<img class="imgbox" ng-src=/backgroundImage/{{imgpath}}></img>
							</div>
							
						</div>

					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
		
		<!-- POPUP END -->
		<section id="widget-grid" class=""><div class="row"><article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="jarviswidget jarviswidget-color-blueDark">
				<header>
					<span class="widget-icon"> <i class="fa fa-table"></i> </span>
					<h2>Background Images</h2>
				</header>
				<div>
					<div class="widget-body no-padding">
						<form class="smart-form" ng-submit="upload()">
							<fieldset>
								<div class="col-md-3">
									<input class="form-control" type="file" file-model="uploadedFile" placeholder="Upload File"></input>
								</div>
								<div class="col-md-3">
								<button type="submit" class="btn btn-primary uplbtn" ng-disabled="!uploadedFile">Upload</button>
								</div>
								
							</fieldset>
							<footer>
								
							</footer>
						</form>
						<section id="widget-grid" class=""> <!-- row -->
			<div class="row">

				<!-- NEW WIDGET START -->
				<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-3"
					data-widget-editbutton="false">

					<header> <span class="widget-icon"> <i
						class="fa fa-table"></i>
					</span>
					<h2>Saved Images</h2>

					</header>

					<!-- widget div-->
					<div>

						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->
						</div>
						<!-- end widget edit box -->

						<!-- widget content -->
						<div class="widget-body no-padding">
							<div class="dt-toolbar">
								<div class="col-xs-12 col-sm-6">
									<div class="dataTables_filter">
										<label>
											<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
											<input ng-model="search" class="form-control">
										</label>
									</div>
								</div>
							</div>
							<table class="table table-striped table-bordered table-hover">
								<thead>
									<tr>
										<th data-hide="phone">No.</th>
										<th data-hide="phone">Name</th>
										<th data-hide="phone,tablet">View</th>
										<th>Delete</th>
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="rs in imageList | filter : search" ng-show="$index >= showingFrom - 1 && $index <= showingTo - 1">
										<td>{{$index}}</td>
										<td>{{rs.bckPathName}}</td>
										<td><a href="javascript:void(0)" ng-click="viewImage($event, rs)"><i class="fa fa-edit" id="{{rs.bckPathName}}"></i></a></td>
										<td><a href="javascript:void(0)" ng-click="deleteImage($event, rs)"><i class="fa fa-trash-o" id="{{rs.bckPathName}}"></i></a></td>
									</tr>
								</tbody>
							</table>
							<div pagination></div>
						</div>
						<!-- end widget content -->
					</div>
					<!-- end widget div -->
				</div>
				<!-- end widget --> </article>

			</div>
			</section>
						
					</div>
				</div>
			</div>
		</article></div></section>
	</div>
	<style>
	.uplbtn{
	height: 31px;
    width: 125px;
	}
	.imgbox{
	    width: 200px;
    height: 170px;
    /* margin: 0 auto; */
    margin-left: 30%;
	}
	</style>
	<script src="../static/js/sports.min.js"></script>