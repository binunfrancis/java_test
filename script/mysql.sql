INSERT INTO auth_user(username,email,password,enabled)
VALUES ('admin@gmail.com','shafeek.msv@gmail.com','$2a$10$L64flfe2q1yM6ChnfJNdhOVI6syZikpvgrqRFOAIvp8Q/iZKVY896', true);

INSERT INTO auth_user_roles (username, role)
VALUES ('admin@gmail.com', 'ROLE_ADMIN');

INSERT INTO auth_user(username,email,password,enabled)
VALUES ('satheesh@gmail.com','satheesh@gmail.com','$2a$10$L64flfe2q1yM6ChnfJNdhOVI6syZikpvgrqRFOAIvp8Q/iZKVY896', true);

INSERT INTO auth_user_roles (username, role)
VALUES ('satheesh@gmail.com', 'ROLE_STORE_MANAGER');

INSERT INTO auth_user(username,email,password,enabled)
VALUES ('manager@gmail.com','manager@gmail.com','$2a$10$L64flfe2q1yM6ChnfJNdhOVI6syZikpvgrqRFOAIvp8Q/iZKVY896', true);

INSERT INTO auth_user_roles (username, role)
VALUES ('manager@gmail.com', 'ROLE_INVENTORY_MANAGER');

INSERT INTO `location` (location_title) VALUES ('Inventory');
INSERT INTO `location` (location_title) VALUES ('Janatha');
INSERT INTO `location` (location_title) VALUES ('IPL Cofee Shop');

INSERT INTO company_information (company_information_id,name, address, email, telephone, mobile_no, logo, tin_no, reg_no)
VALUES (1,'MSV Group of Companies','mk Rd, Devankulangara,Near Changampuzha Park, Edappally, Ernakulam, Kerala 682024','msv@gmail.com','984667255','(+91) 75599 0466','/static/img/hsglogo.png','0000','000');


